#!/bin/bash
if ps -ef | grep mplayer | grep advertisement; then
    pkill mplayer
else
    printf "mplayer not running"
fi