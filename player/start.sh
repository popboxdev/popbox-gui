#!/bin/bash
LIST="advertisement/video32/*"
fileku=$(ls -l $LIST | awk '{print $9}' | awk 'ORS=" "')
if ps -ef | grep mplayer | grep advertisement; then
    printf "mplayer running"
else
    # mplayer -fs -loop 0 $fileku
    mplayer -endpos 60 -fs  $fileku
fi