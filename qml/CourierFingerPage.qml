import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: courier_qr_page
    property var press: '0'
    property var asset_global: 'asset/global/'
    property var asset_path: 'asset/logistic/'
    property var path_button: 'asset/home_page/button/'
    show_img: asset_global + "background/finger_bg.png"
    property int character:26
    property var ch:1 
    property var count:0
    property var identity:""
    property var level_user
    property var level_name
    property var txt_time: ""
    property bool flagInsulator: false
    

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 90
            timer_note.restart()
            press = '0';
            console.log(identity)
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    Component.onCompleted: {
        root.user_login_result.connect(handle_result)
        root.user_info_result.connect(detail_user)
        root.start_internet_status_result.connect(internet_status_result);
    }

    Component.onDestruction: {
        root.user_login_result.disconnect(handle_result)
        root.user_info_result.disconnect(detail_user)
        root.start_internet_status_result.disconnect(internet_status_result);
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("Login Dengan Sidik Jari")
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }
    
    Text {
        id: title_txt
        x:50
        y:300
        text: qsTr("Silakan Tempelkan Jarimu di <br>Sidik Jari Reader di Loker")
        font.pixelSize:32
        color:"#25213b"
        font.family: fontStyle.extrabold
    }

    Rectangle{
        id: login_info
        x: 50
        y: 548
        visible : true
        Image{
            source: path_button + "login_info.png"
            height: 100
            width: 612
            Text{
                id: text_login_info
                color:"#ffffff"
                font.pixelSize: 22
                font.family:fontStyle.extrabold
                anchors.left:parent.left
                anchors.leftMargin: 143
                anchors.top: parent.top
                anchors.topMargin:22
                text: qsTr("Tidak Bisa Login ke Akun Kamu ? <br><i>Tekan disini</i>")
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {

                }
            }
        }
    }

    Rectangle{
        id: warning_info
        x: 50
        y: 410
        visible : true
        Image{
            source: path_button + "warning_info.png"
            height: 54
            width: 606
            Text{
                id: text_warning_info
                color:"#25213b"
                font.pixelSize: 18
                font.letterSpacing:0.5
                font.family:fontStyle.regular
                anchors.left:parent.left
                anchors.leftMargin: 84
                anchors.top: parent.top
                anchors.topMargin:15
                text: qsTr("Pastikan jari menempel sempurna pada reader")
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {

                }
            }
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        // source: 'asset/global/animation/loading.gif'
    }

    function handle_result(result){
        // main_page.enabled = true
        // waiting.close()      
        console.log( "RESULT : " + result)      
        if(result == "Success"){
            console.log(identity)
            waiting.visible = false
            dimm_display.visible = false
            if(identity == "LOGISTICS_COMPANY_USER"){
                slot_handler.get_user_info()
                if(level_user==6 || level_user==7 || level_user==8 || level_user==9){
                    my_stack_view.push(courier_home_page, {c_access:"limited",pref_login_user:input_username.show_text.substring(0,3), courier_userlogin:input_username.show_text, flagInsulator: flagInsulator, show_img: show_img})
                }else if(level_user==1 || level_user==2 || level_user==3 || level_user==4 || level_user==5){
                    console.log("LEVEL USER : " + level_user)
                    my_stack_view.push(courier_service_view, {c_access:"limited",pref_login_user:input_username.show_text.substring(0,3), name:input_username.show_text, flagInsulator: flagInsulator, show_img: show_img})
                }else{
                    dimm_display.visible = true;
                    // show_fail.start();
                }
            }
            if(identity == "OPERATOR_USER"){
                // my_stack_view.push(manager_service_view)
            }
        }
        if(result == "Failure"){
            // wrong_user.visible=true
            // hide_key.start();
            dimm_display.visible = true;
            show_fail.start();
            // my_stack_view.push(courier_psw_error_view)
        }
        if(result == "NoPermission"){
            // my_stack_view.push(courier_psw_error_view)
        }
        if(result == "NetworkError"){
            // main_page.enabled = false
            // network_error.open()
        }
    }

    function detail_user(text){
        console.log("DETAIL USER : "+ JSON.stringify(text))
        var result = JSON.parse(text)
        level_user = result.role_level
        level_name = result.role_name
        var insulator_company = result.insulator_company
        if(insulator_company=="true") flagInsulator = true
        else flagInsulator = false
        console.log("LEVEL USER : "+ level_user + " LEVEL NAME : " + level_name)
    }

    function hide_animation(param){
        if(param=="keyboard") hide_key.start();
        dimm_display.visible=false
        press=0;
    }

    function internet_status_result(param) {
        console.log("INTERNET STATUS : ", param)
        if(param=="SUCCESS"){
            slot_handler.start_video_capture(input_username.show_text)
            slot_handler.background_login(input_username.show_text,password_input.show_text,identity)
        }else{
            // show_nointernet.start();
            dimm_display.visible = true
            waiting.visible = false
        }
    }

}