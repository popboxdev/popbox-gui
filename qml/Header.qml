import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id: header
    property var title_text: ""
    property bool img_vis: true
    height: 174
    width: parent.width
    property int timer_value: 999
    property int timer_counter: 10
    property bool timer_view: true
    property var status
    property var text_timer: ""
    property var funct: "default"
    

     FontType{
	    id:fontStyle		        
        //Type Font ### bold , extrabold , light , book, medium, black ###
     }

    // Rectangle{
    //     id: timer_set
    //     x:50
    //     y:50
    //     QtObject{
    //         id:abc
    //         property int counter
    //         Component.onCompleted:{
    //             abc.counter = timer_value
    //         }
    //     }

    //     Timer{
    //         id:my_timer
    //         interval:1000
    //         repeat:true
    //         running:true
    //         triggeredOnStart:true
    //         onTriggered:{
    //             if (timer_start==false) {
    //                 // my_timer.restart(); //awal
    //                 my_timer.stop(); // change
    //                 abc.counter = timer_value
    //                 timer_start=true
    //             }else{
    //                 // my_timer.stop(); // change
    //                 my_timer.start();
    //                 abc.counter -= 1
    //                 text_anim.text = abc.counter
    //                 timer_counter -=1
    //                 if(timer_counter<=0){
    //                     timer_counter = 0
    //                 }
    //                 if(abc.counter == 0){
    //                     my_timer.stop()
    //                     if (status =="send_parcel"){
    //                         send_parcel.show_timeout("timeout")
    //                         timer_note.start()
    //                     }
    //                     my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
    //                 }
    //             }
    //         }
    //     }
    // }

    // Rectangle{
    //     id: timer
    //     x:50
    //     y:50
    //     QtObject{
    //         id:secon
    //         property int count
    //         Component.onCompleted:{
    //             secon.count = 5
    //         }
    //     }

    //     Timer{
    //         id:timer_note
    //         interval:1000
    //         repeat:true
    //         running:true
    //         triggeredOnStart:true
    //         onTriggered:{
    //             secon.count -= 1
    //             if(secon.count < 0){
    //                 timer_note.stop()
    //                 my_stack_view.pop(null)
    //             }
    //         }
    //     }
    // } 


    Image {
        width:parent.width
        height:parent.height
        source:"asset/global/background/header.png"
    }

    Text {
        id: title
        x:(img_vis==true) ? 108 : 50
        y:33
        text: qsTr(title_text)
        font.pixelSize:32
        font.letterSpacing : 0.7
        color:"#3a3a3a"
        font.family: fontStyle.medium
    }

    Rectangle{
        x:25
        y:20
        width: 80
        height: 70
        color:"transparent"
        visible:img_vis
        Image {
            x:25
            y:15
            width:40
            height:40
            source:"asset/global/button/back.png"
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(funct=="home"){
                    my_stack_view.pop(null);
                }else{
                    my_stack_view.pop();
                }
            }
        }
    }

    AnimatedImage{
        id: loading_image
        visible:timer_view
        width: 63
        height: 63
        x:918
        y:25
        source: 'asset/global/animation/lgspinner.gif'
        // fillMode: Image.PreserveAspectFit
        Text{
            id:text_anim
            color:"#8c8c8c"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: text_timer
            font.pixelSize:18
            font.family: fontStyle.medium
        }
    }
    function restart_timer(){
        abc.counter = timer_value
        my_timer.start();
    }
    // function stop_timer(){
    //     timer_note.stop()
    // }
}