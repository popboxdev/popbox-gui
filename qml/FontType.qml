import QtQuick 2.4
import QtQuick.Controls 1.2

Item {
    id: fontStyle
    property var black : black_f.name
    property var bold : bold_f.name
    property var extrabold : extrabold_f.name
    property var regular : regular_f.name
    property var light : light_f.name
    property var medium : medium_f.name
    property var asset_font: 'asset/font/'

    FontLoader {
        id: regular_f
        source: asset_font + "Montserrat-Regular.ttf"
    }
    FontLoader {
        id: black_f
        source: asset_font + "Montserrat-Black.ttf"
    }
    FontLoader {
        id: bold_f
        source: asset_font + "Montserrat-Bold.ttf"
    }
    FontLoader {
        id: extrabold_f
        source: asset_font + "Montserrat-ExtraBold.ttf"
    }
    FontLoader {
        id: light_f
        source: asset_font + "Montserrat-Light.ttf"
    }
    FontLoader {
        id: medium_f
        source: asset_font + "Montserrat-Medium.ttf"
    }

}