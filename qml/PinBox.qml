import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    property var show_text:""
    property var borderColor: "#8c8c8c"
    width:79
    height:79
    color:"#ffffff"
    border.color: borderColor
    border.width: 3
    radius: 2
    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Text{
        text:show_text
        color:"#414042"
        font.family:fontStyle.regular
        font.pixelSize:32
        anchors.centerIn: parent;
    }

}
