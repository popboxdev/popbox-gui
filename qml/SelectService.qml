import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    property var press: '0'
    property var path: '/advertisement/apservice/'
    property int num_pic: 0
    property variant qml_pic: []
    property variant pic_source: []
    property var img_path: '..' + path;	
    property var language: 'ID'
    property var asset_path: 'asset/home_page/'
    property var path_button: 'asset/home_page/button/'
    property var path_background: 'asset/home_page/background/'
    property var clock_color: "#595353"
    img_vis : false
    // show_img: 'asset/base/background/bg.png'
    show_img:''
    property var contactless_status: "disable"
    property var param_lang: "bahasa"	
    property var tanggal: ""
    property var count_coming: 0

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.start_get_file_banner(path);
            slot_handler.start_get_locker_name();
            timer_clock.start();
            timer_startup.start();
            slot_handler.start_idle_mode();
            slot_handler.set_logout_user();
            press = '0';
        }
        if(Stack.status==Stack.Deactivating){
            timer_clock.stop();
            timer_startup.stop();
        }
    }

    Component.onCompleted: {
        root.start_get_gui_version_result.connect(get_gui_version);
        root.start_get_locker_name_result.connect(show_locker_name);
        root.maintenance_status_result.connect(maintenance_status);
    }

    Component.onDestruction: {
        root.start_get_gui_version_result.disconnect(get_gui_version);
        root.start_get_locker_name_result.disconnect(show_locker_name);
        root.maintenance_status_result.disconnect(maintenance_status);
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Image{
        id: bg_logo
        // y:250
        width:parent.width
        height:105
        source:asset_path + "button/rec_logo.png"
    }

    Image{
        id: popbox_logo
        y:25
        x:50
        width:130
        height:40
        source:asset_path + "background/popbox.png"
    }

    Image{
        id: service_logo
        y:10
        x:842
        width:132
        height:65
        source:asset_path + "background/service_logo.png"
    }
    Text{
        id: text_title_service
        x:50
        y:175
        color:"#25213b"
        font.pixelSize: 32
        font.family:fontStyle.extrabold
        text: qsTr("LOGIN KURIR")
    }

    Rectangle{  
        y: 183
        width:455
        height: 30
        anchors.right: parent.right
        anchors.rightMargin:50
        Text {
            id: locker_name_text
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin:0
            text: "POPBOX LOCKER" //Maks Nama Locker 35 Karakter
            font.family: fontStyle.medium
            font.letterSpacing:0.5
            font.pixelSize:22
            color:"#25213b"
        }
    }

    Text {
        id: gui_version
        x: 50
        y: 738
        text: "System loker version V.10"
        font.family: fontStyle.regular
        font.pixelSize:12
        color:"#2b2929"
    }

    Image{
        id: clock_base_img
        y: 20
        x: 400
        width: parent.width
        height: 42
        Text {
            id: timeText
            text: ""
            anchors.verticalCenter: parent.verticalCenter
            font.family: fontStyle.medium
            font.pixelSize: 18
            color: clock_color
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                count_coming +=1;
                if (count_coming==7){
                    my_stack_view.push(under_maintenance,{language: language, status_page: "coming_soon"})
                    count_coming = 0;
                }
            }
        }
    }  

    Rectangle{
        id: login_id
        x: 50
        y: 270
        visible : true
        Image{
            source: path_button + "login_id.png"
            height: 188
            width: 307
            Text{
                id: text_login_id
                color:"#ffffff"
                font.pixelSize: 24
                font.letterSpacing:0
                font.bold: true
                font.family:fontStyle.extrabold
                anchors.left:parent.left
                anchors.leftMargin: 91
                anchors.top: parent.top
                anchors.topMargin:60
                text: qsTr("Login <br>Dengan ID")
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(courier_id_page,{})
                    // my_stack_view.push(courier_home_page,{})
                }
            }
        }
    }

    Rectangle{
        id: login_fingerprint
        x: 360
        y: 270
        visible : true
        Image{
            source: path_button + "login_fingerprint.png"
            height: 188
            width: 307
            Text{
                id: text_login_fingerprint
                color:"#ffffff"
                font.pixelSize: 24
                font.family:fontStyle.extrabold
                anchors.left:parent.left
                anchors.leftMargin: 100
                anchors.top: parent.top
                anchors.topMargin:60
                text: qsTr("Login dengan <br>Sidik Jari")
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(courier_finger_page,{})
                }
            }
        }
    }

    Rectangle{
        id: login_qrcode
        x: 669
        y: 270
        visible : true
        Image{
            source: path_button + "login_qrcode.png"
            height: 188
            width: 307
            Text{
                id: text_login_qrcode
                color:"#ffffff"
                font.pixelSize: 24
                font.family:fontStyle.extrabold
                anchors.left:parent.left
                anchors.leftMargin: 103
                anchors.top: parent.top
                anchors.topMargin:60
                text: qsTr("Login <br>Dengan QR")
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(courier_qr_page,{})
                }
            }
        }
    }

    Rectangle{
        id: login_info
        x: 360
        y: 469
        visible : true
        Image{
            source: path_button + "login_info.png"
            height: 100
            width: 612
            Text{
                id: text_login_info
                color:"#ffffff"
                font.pixelSize: 22
                font.family:fontStyle.extrabold
                anchors.left:parent.left
                anchors.leftMargin: 143
                anchors.top: parent.top
                anchors.topMargin:22
                text: qsTr("Tidak Bisa Login ke Akun Kamu ? <br><i>Tekan disini</i>")
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {

                }
            }
        }
    }

    Rectangle{
        id: warning_info
        x: 50
        y: 669
        visible : true
        Image{
            source: path_button + "warning_infolong.png"
            height: 54
            width: 924
            Text{
                id: text_warning_info
                color:"#25213b"
                font.pixelSize: 18
                font.letterSpacing:0.5
                font.family:fontStyle.regular
                anchors.left:parent.left
                anchors.leftMargin: 84
                anchors.top: parent.top
                anchors.topMargin:15
                text: qsTr("Loker PopBox ini hanya diperuntukan untuk kurir")
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {

                }
            }
        }
    }

    Timer {
        id: timer_startup
        interval: 1000
        repeat: false
        running: true
        onTriggered:{   
            // None
        }
    }

    Timer {
        id: timer_clock
        interval: 1000
        repeat: true
        running: true
        onTriggered:{   
            // var tanggal
            if(language=="MY"  || language == "PH"){
                tanggal = new Date().toLocaleDateString(Qt.locale("en_US"), Locale.LongFormat) + "  " + new Date().toLocaleTimeString(Qt.locale("en_US"), "hh:mm:ss") //MY Locale
            }else{
                if (param_lang == "english"){	
                    tanggal = new Date().toLocaleDateString(Qt.locale("en_US"), Locale.LongFormat) + "  " + new Date().toLocaleTimeString(Qt.locale("en_US"), "hh:mm:ss")	
                }else{	
                    tanggal = new Date().toLocaleDateString(Qt.locale("id_ID"), Locale.LongFormat) + "  " + new Date().toLocaleTimeString(Qt.locale("id_ID"), "hh:mm:ss")	
                }
            }
            // timeText.text = tanggal.toUpperCase()
            timeText.text = tanggal
            press = '0';
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:notif_available
        // source_img: asset_path + "background/full.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text:qsTr("Yahh ,, Lokernya Sudah Penuh")
        body_text:qsTr("Silakan mencoba lagi beberapa saat lagi.")

        NotifButton{
            id:ok_yes
            x:50
            y:351
            r_radius:2
            buttonText:qsTr("YA")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_available.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    ok_yes.modeReverse = true
                }
                onExited:{
                    ok_yes.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_available.start();
                    dimm_display.visible=false;
                }
            }
        }
    }


    NumberAnimation {
        id:hide_available
        targets: notif_available
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_available
        targets: notif_available
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    function maintenance_status(rest_){
        if (rest_ == "True"){
            my_stack_view.push(under_maintenance,{language: language});
        }
    }

    function get_gui_version(vers){
        gui_version.text = "Version : " + vers
    }

    function show_locker_name(text){
        console.log("locker_name" + text)
        if(text == ""){
            return
        }
        locker_name_text.text = text.toUpperCase()
    }

}
