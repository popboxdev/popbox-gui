import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:under
    property var count : 0
    property var language: 'ID'
    property var theme: "default"
    property var status_page: 'default'
    Image{
        width: 1024
        height: 768
        x:0
        y:0
        source: check_source()
        // visible: true

    }
    Rectangle{
        id: rec_back
        x: 600
        y: 230
        width:50
        height:50
        color: "transparent"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                count +=1;
                if (count==4){
                    my_stack_view.pop();
                    count = 0;
                }
                
            }
        }
    }
    Component.onCompleted: {
        root.maintenance_status_result.connect(maintenance_status);
    }

    Component.onDestruction: {
        root.maintenance_status_result.disconnect(maintenance_status);
    }
    
    function maintenance_status(rest_){
        if (rest_ == "False"){
            my_stack_view.pop(null)
        }
    }

    function check_source() {
        if(status_page=="coming_soon"){
            set_button_back();
            return "asset/desktop/comingsoon15.jpg" 
        }else{
            if (language=='MY') return "asset/desktop/maintenance15_my.jpg"
            else if (language == 'PH') return "asset/desktop/maintenance15_ph.jpg" 
            else return "asset/desktop/maintenance15.jpg" 
        }
    }

    function set_button_back(){
        rec_back.x = 459
        rec_back.y = 100
        rec_back.width = 107
        rec_back.height = 35
    }
}

