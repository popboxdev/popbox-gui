import QtQuick 2.4
import QtQuick.Controls 1.2

Item {
    id:notif_swipe
    x:0
    y:768
    // property var font_type: "SourceSansPro-Regular"
    // property var font_type_bold: "SourceSansPro-SemiBold"
    property var asset_font: 'asset/font/'
    property var title_color:"#4a4a4a"
    property var body_color:"#4a4a4a"
    property var title_text: ""
    property var body_text: ""
    property var source_img: ""
    property int title_size: 40
    property int body_size: 24
    property int titlebody_x: 50
    property int title_y: 60
    property int body_y: 180
    property int img_y: 150
    property int r_height: 520
    property int img_height: 470
    property bool title_bold : false
    property int rec_x:42
    property int rec_y:52
    property bool rec_vis: false
    property int rec_height:115
    property int rec_width:592
    property var rec_color: "#fdb62f"
    property var title_font: fontStyle.medium
    property var body_font: fontStyle.regular


    width: parent.width
    height: r_height
    clip: true

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

  

    Rectangle {
        anchors.fill: parent
        anchors.bottomMargin: -radius
        radius: 30
        color: "white"
    }

    Image{
        id:background
        y:img_y
        height: img_height
        width: parent.width
        source:source_img
    }

    Rectangle{
        x:rec_x
        y:rec_y
        visible:rec_vis
        color: rec_color
        width: rec_width
        height: rec_height
        
    }

    Text {
        id: title
        x:titlebody_x
        y:title_y
        // font.bold: true
        font.bold: true
        font.pixelSize:title_size
        color:title_color
        font.family: title_font
        text: qsTr(title_text)
    }
    Text {
        id: body
        x:titlebody_x
        y:body_y
        font.pixelSize:body_size
        color:body_color
        font.family: body_font
        text: qsTr(body_text)
    }
}

