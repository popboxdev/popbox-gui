import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: courier_home_page
    show_img: ""
    property var press: '0'
    property var asset_global: 'asset/global/'
    property var asset_path: 'asset/logistic/'
    property var path_button: 'asset/home_page/button/'
    property var courier_name: 'Sudjiwo Tedjo'
    property var courier_type: 'PopExpress'
    property var select_courier: 'PopExpress'
    property var c_access: 'full'
    property var pref_login_user: 'undefined'
    property var courier_userlogin:'undefined'
    property var role_level
    property bool availableUse: false
    property bool flagInsulator: false

    

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){   
            slot_handler.get_user_info();
            // if (flagInsulator==true){
            //     slot_handler.start_get_free_mouth_mun(1);
            // }else{
            //     slot_handler.start_get_free_mouth_mun(0);
            // }
            press = '0';
            timer.secon = 90
            timer_note.restart()
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    Component.onCompleted: {
        root.user_info_result.connect(detail_user);
        // root.free_mouth_result.connect(show_free_mouth_num);
        // root.overdue_express_count_result.connect(overdue_count)
    }

    Component.onDestruction: {
        root.user_info_result.disconnect(detail_user);
        // root.free_mouth_result.disconnect(show_free_mouth_num);
        // root.overdue_express_count_result.disconnect(overdue_count)
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("Menu Midmile")
        img_vis:false
        timer_view:false
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    
    Text {
        id: show_detail_user
        x:50
        y:145
        text: qsTr("Hi, "+courier_name+" | "+courier_type)
        font.pixelSize:24
        color:"#414042"
        font.family: fontStyle.medium
    }

    Rectangle{
        id: btn_drop
        x: 205
        y: 297
        visible : true
        Image{
            source: path_button + "paket_drop.png"
            height: 188
            width: 302
            Text{
                id: text_btn_drop
                color:"#ffffff"
                font.pixelSize: 24
                font.letterSpacing:0
                font.bold: true
                font.family:fontStyle.extrabold
                anchors.left:parent.left
                anchors.leftMargin: 99
                anchors.top: parent.top
                anchors.topMargin:80
                text: qsTr("Drop Paket")
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    // my_stack_view.push(courier_id_page,{})
                    my_stack_view.push(courier_home_page,{})
                }
            }
        }
    }

    Rectangle{
        id: btn_pickup
        x: 517
        y: 297
        visible : true
        Image{
            source: path_button + "paket_pickup.png"
            height: 188
            width: 302
            Text{
                id: text_btn_pickup
                color:"#ffffff"
                font.pixelSize: 24
                font.letterSpacing:0
                font.bold: true
                font.family:fontStyle.extrabold
                anchors.left:parent.left
                anchors.leftMargin: 99
                anchors.top: parent.top
                anchors.topMargin:80
                text: qsTr("Pickup Paket")
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    // my_stack_view.push(courier_id_page,{})
                    my_stack_view.push(courier_home_page,{})
                }
            }
        }
    }

    Rectangle{
        id: btn_logout
        x: 205
        y: 495
        visible : true
        Image{
            source: path_button + "login_info.png"
            height: 100
            width: 614
            Text{
                id: text_btn_logout
                color:"#ffffff"
                font.pixelSize: 22
                font.family:fontStyle.extrabold
                anchors.left:parent.left
                anchors.leftMargin: 120
                anchors.top: parent.top
                anchors.topMargin:37
                text: qsTr("Keluar Dari akun")
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {

                }
            }
        }
    }

    Rectangle{
        id: warning_info
        x: 50
        y: 669
        visible : true
        Image{
            source: path_button + "warning_infolong.png"
            height: 54
            width: 924
            Text{
                id: text_warning_info
                color:"#25213b"
                font.pixelSize: 18
                font.letterSpacing:0.5
                font.family:fontStyle.regular
                anchors.left:parent.left
                anchors.leftMargin: 84
                anchors.top: parent.top
                anchors.topMargin:15
                text: qsTr("Untuk keamanan pastikan kamu keluar akun setelah selesai")
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {

                }
            }
        }
    }


    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:notif_available
        // source_img: "asset/home_page/" + "background/full.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text:qsTr("Yahh ,, Lokernya Sudah Penuh")
        body_text:qsTr("Silakan mencoba lagi beberapa saat lagi.")

        NotifButton{
            id:ok_yes
            x:50
            y:351
            r_radius:2
            buttonText:qsTr("YA")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_available.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    ok_yes.modeReverse = true
                }
                onExited:{
                    ok_yes.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_available.start();
                    dimm_display.visible=false;
                }
            }
        }
    }

    NumberAnimation {
        id:hide_available
        targets: notif_available
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_available
        targets: notif_available
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NotifSwipe{
        id:notif_logout
        source_img: asset_path + "/button/logout.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text:qsTr("Keluar dari Menu Logistik ?")
        // body_text:"Jika kamu membatalkan proses, maka akan diarahkan <br>ke halaman awal."
        NotifButton{
            id:ok_logout
            x:310
            y:170
            r_radius:0
            modeReverse:true
            buttonText:qsTr("YA")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    slot_handler.set_logout_user()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
                onEntered:{
                    ok_logout.modeReverse = false
                }
                onExited:{
                    ok_logout.modeReverse = true
                }
            }
        }
        NotifButton{
            id:no_logout
            x:50
            y:170
            r_radius:0
            buttonText:qsTr("TIDAK")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_logout.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    no_logout.modeReverse = true
                }
                onExited:{
                    no_logout.modeReverse = false
                }
            }
        }


        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_logout.start();
                    dimm_display.visible=false;
                }

            }
        }
    }

    NumberAnimation {
        id:hide_logout
        targets: notif_logout
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_logout
        targets: notif_logout
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    function detail_user(text){
        var result = JSON.parse(text)

        role_level = result.role_level;
        courier_name = result.name;
        courier_type = result.company_name;
        select_courier = courier_type;
        if (role_level==7){
            ambil_paket.enabled = false;
            ambil_paket.source = asset_path + "/button/ambil_inactive.png" ;
        }
        if(result.company_name.indexOf("GRAB") > -1){
            courier_type = "GRAB Indonesia"
        }
    }

    function show_free_mouth_num(locker_size){
        var obj = JSON.parse(locker_size)
        var size_XS = 0
        var size_S = 0
        var size_M = 0
        var size_L = 0
        var size_XL = 0
        for(var i in obj){
            if(i == "XL"){
                size_XL = obj[i]
            }
            if(i == "L"){
                size_L = obj[i]
            }
            if(i == "M"){
                size_M = obj[i]
            }
            if(i == "S"){
                size_S = obj[i]
            }
            if(i == "MINI"){
                size_XS = obj[i]
            }
        }
        if (size_XL <= 0 && size_L <= 0 && size_M <= 0 && size_S <= 0 && size_XS <=0 ){
            availableUse = false;
        }else{
            availableUse = true;
        }
    }
}