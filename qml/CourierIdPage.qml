import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: courier_id_page
    show_img: ""
    property var press: '0'
    property var asset_global: 'asset/global/'
    property var asset_path: 'asset/logistic/'
    property int character:26
    property var ch:1 
    property var count:0
    property var identity:""
    property var level_user
    property var level_name
    property var txt_time: ""
    property bool flagInsulator: false
    

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 90
            timer_note.restart()
            press = '0';
            show_key.start();
            input_username.bool = true
            password_input.bool = false
            console.log(identity)
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    Component.onCompleted: {
        root.user_login_result.connect(handle_result)
        root.user_info_result.connect(detail_user)
        root.start_internet_status_result.connect(internet_status_result);
    }

    Component.onDestruction: {
        root.user_login_result.disconnect(handle_result)
        root.user_info_result.disconnect(detail_user)
        root.start_internet_status_result.disconnect(internet_status_result);
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("Login ID Kurir")
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }
    
    Text {
        id: username_txt
        x:50
        y:145
        text: qsTr("ID Kurir")
        font.pixelSize:32
        color:"#25213b"
        font.family: fontStyle.medium
    }

    Text {
        id: password_txt
        x:390
        y:145
        text: qsTr("Password")
        font.pixelSize:32
        color:"#25213b"
        font.family: fontStyle.medium
    }

    Text {
        id: login_info_text
        x:51
        y:324
        text: qsTr("Tidak bisa masuk ke akun ? hubungi Customer PopBox <u>Tekan disini<i>")
        font.pixelSize:22
        color:"#25213b"
        font.family: fontStyle.regular
    }

    InputText{
        id:input_username
        x:50
        y:230
        text_width:220
        r_width:310
        borderColor : "#8c8c8c"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                ch=1
                press=1
                input_username.bool = true
                password_input.bool = false
            }
        }
        Image{
            id: del_username
            x:252
            y:22
            width:36
            height:36
            visible:false
            source:asset_global + "button/delete.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    input_username.show_text = "";
                    count=0;
                    ch=1
                    touch_keyboard.input_text("")
                    del_username.visible=false
                }
            }
        }
    }

    InputText{
        id:password_input
        text_password:true
        x:390
        y:230
        r_width:310
        text_width:220
        borderColor : "#8c8c8c"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                ch=2;
                press=1;
                password_input.bool = true
                input_username.bool = false
            }
        }
        Image{
            id: del_password
            x:252
            y:22
            width:36
            height:36
            visible:false
            source:asset_global + "button/delete.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    password_input.show_text = "";
                    count=0;
                    ch=2
                    touch_keyboard.input_text("")
                    del_password.visible=false
                }
            }
        }
    }

    NotifButton{
        id:btn_lanjut
        x:727
        y:230
        enabled: false
        buttonText:qsTr("LANJUT")
        buttonColor: "#8c8c8c"
        r_radius:0
        modeReverse:false
        font_name: fontStyle.extrabold
        MouseArea{
            anchors.fill: parent
            onClicked: {
                slot_handler.start_get_internet_status()
                dimm_display.visible = true
                waiting.visible = true
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    KeyBoardAlphanumeric{
        id:touch_keyboard
        property var validate_c
        closeButton: false
        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(input_text)
        }

        function input_text(str){
            if(str == "OKE" || str == "OK"){
                str = "";
                if(press != "1"){
                    return
                }
                press = "0";
                slot_handler.start_video_capture(input_username.show_text)
                slot_handler.background_login(input_username.show_text,password_input.show_text,identity)
                dimm_display.visible = true
                waiting.visible = true
            }
            if(ch==1){
                if (str == "" && input_username.show_text.length > 0){
                    input_username.show_text.length--
                    input_username.show_text=input_username.show_text.substring(0,input_username.show_text.length-1)
                    count--;                 
                }
                if (str != "" && input_username.show_text.length< character){
                    input_username.show_text.length++
                    count++;
                }
                if (input_username.show_text.length>=character){
                    str=""
                    input_username.show_text.length=character
                }else{
                    input_username.show_text += str
                }
                if(count>0){
                    if (password_input.show_text!=""){
                        btn_lanjut.buttonColor = "#ff524f"
                        btn_lanjut.enabled = true
                    }
                    input_username.borderColor = "#ff7e7e"
                    del_username.visible=true
                } else{
                    btn_lanjut.buttonColor = "#c4c4c4"
                    btn_lanjut.enabled = false
                    input_username.borderColor = "#8c8c8c"
                    del_username.visible=false
                }
            }
            if(ch==2){
                if (str == "" && password_input.show_text.length > 0){
                    password_input.show_text.length--
                    password_input.show_text=password_input.show_text.substring(0,password_input.show_text.length-1)
                    count--;                 
                }
                if (str != "" && password_input.show_text.length< character){
                    password_input.show_text.length++
                    count++;
                }
                if (password_input.show_text.length>=character){
                    str=""
                    password_input.show_text.length=character
                }else{
                    password_input.show_text += str
                }
                if(count>0){
                    if (input_username.show_text!=""){
                        btn_lanjut.buttonColor = "#ff524f"
                        btn_lanjut.enabled = true
                    }
                    password_input.borderColor = "#ff7e7e"
                    del_password.visible=true
                } else{
                    btn_lanjut.buttonColor = "#c4c4c4"
                    btn_lanjut.enabled = false
                    password_input.borderColor = "#8c8c8c"
                    del_password.visible=false
                }

            }
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        // source: 'asset/global/animation/loading.gif'
    }

    NotifSwipe{
        id:notif_failed
        // source_img: asset_path + "background/failed_login.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text:qsTr("Data Login Tidak Dikenal")
        body_text:qsTr("Data Login yang kamu masukkan salah atau tidak <br>dikenal. Mohon masukkan kembali data login <br>anda")

        NotifButton{
            id:ok_yes
            x:310
            y:350
            r_radius:0
            modeReverse:true    
            buttonText:qsTr("ULANGI")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_fail.start();
                    dimm_display.visible=false;
                    waiting.visible=false;
                    clear_all();
                }
                onEntered:{
                    ok_yes.modeReverse = false
                }
                onExited:{
                    ok_yes.modeReverse = true
                }
            }
        }

        NotifButton{
            id:ok_no
            x:50
            y:350
            r_radius:0
            buttonText:qsTr("BATAL")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_fail.start()
                    dimm_display.visible=false;
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true}))
                }
                onEntered:{
                    ok_no.modeReverse = true
                }
                onExited:{
                    ok_no.modeReverse = false
                }
            }
        }


        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_fail.start();
                    dimm_display.visible=false;
                    waiting.visible=false;
                    clear_all();
                }

            }
        }
    }

    NotifSwipe{
        id:notif_nointernet
        // source_img: asset_global + "handle/internet.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:130
        title_text:qsTr("Yah, Internet tidak stabil")
        body_text:qsTr("Maaf jaringan internet sedang tidak stabil, <br>silakan coba kembali")

        NotifButton{
            id:coba_nointernet
            x:50
            y:221
            r_radius:0
            buttonText:qsTr("COBA KEMBALI")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_nointernet.start();
                    dimm_display.visible = false
                }
                onEntered:{
                    coba_nointernet.modeReverse = true
                }
                onExited:{
                    coba_nointernet.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_nointernet.start();
                    dimm_display.visible = false
                }
            }
        }
    }

    NumberAnimation {
        id:hide_key
        targets: touch_keyboard
        properties: "y"
        from:368
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_key
        targets: touch_keyboard
        properties: "y"
        from:768
        to:368
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_fail
        targets: notif_failed
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_fail
        targets: notif_failed
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_nointernet
        targets: notif_nointernet
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:show_nointernet
        targets: notif_nointernet
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    function handle_result(result){
        // main_page.enabled = true
        // waiting.close()      
        console.log( "RESULT : " + result)      
        if(result == "Success"){
            console.log(identity)
            waiting.visible = false
            dimm_display.visible = false
            if(identity == "LOGISTICS_COMPANY_USER"){
                slot_handler.get_user_info()
                if(level_user==6 || level_user==7 || level_user==8 || level_user==9){
                    my_stack_view.push(courier_home_page, {c_access:"limited",pref_login_user:input_username.show_text.substring(0,3), courier_userlogin:input_username.show_text, flagInsulator: flagInsulator, show_img: show_img})
                }else if(level_user==1 || level_user==2 || level_user==3 || level_user==4 || level_user==5){
                    console.log("LEVEL USER : " + level_user)
                    my_stack_view.push(courier_service_view, {c_access:"limited",pref_login_user:input_username.show_text.substring(0,3), name:input_username.show_text, flagInsulator: flagInsulator, show_img: show_img})
                }else{
                    dimm_display.visible = true;
                    show_fail.start();
                }
            }
            if(identity == "OPERATOR_USER"){
                // my_stack_view.push(manager_service_view)
            }
        }
        if(result == "Failure"){
            // wrong_user.visible=true
            // hide_key.start();
            dimm_display.visible = true;
            show_fail.start();
            // my_stack_view.push(courier_psw_error_view)
        }
        if(result == "NoPermission"){
            // my_stack_view.push(courier_psw_error_view)
        }
        if(result == "NetworkError"){
            // main_page.enabled = false
            // network_error.open()
        }
    }

    function detail_user(text){
        console.log("DETAIL USER : "+ JSON.stringify(text))
        var result = JSON.parse(text)
        level_user = result.role_level
        level_name = result.role_name
        var insulator_company = result.insulator_company
        if(insulator_company=="true") flagInsulator = true
        else flagInsulator = false
        console.log("LEVEL USER : "+ level_user + " LEVEL NAME : " + level_name)
    }

    function hide_animation(param){
        if(param=="keyboard") hide_key.start();
        dimm_display.visible=false
        press=0;
    }

    
    function clear_all() {
        press = 1;
        count = 0;
        // input_username.bool = true
        password_input.bool = true
        password_input.show_text = "";
        // input_username.show_text = "";
        touch_keyboard.input_text("")
        // del_username.visible=false;
        del_password.visible=false;
        // ch=1;
        ch=2;
    }

    function internet_status_result(param) {
        console.log("INTERNET STATUS : ", param)
        if(param=="SUCCESS"){
            slot_handler.start_video_capture(input_username.show_text)
            slot_handler.background_login(input_username.show_text,password_input.show_text,identity)
        }else{
            show_nointernet.start();
            dimm_display.visible = true
            waiting.visible = false
        }
    }

}