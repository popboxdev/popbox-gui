import QtQuick 2.4
import QtQuick.Controls 1.2
import "configQML.js" as CONFIG


Rectangle {
    id:root
    width: 1024
    height: 768
    color: 'transparent'
    // NEW GUI

    signal barcode_result(string str)
    signal start_get_mode_insulator_result(string str)
    signal start_get_gui_version_result(string str)
    signal start_get_file_banner_result(string str)
    signal start_get_locker_name_result(string str)
    signal free_mouth_result(string str)
    signal free_mouth_result_insulator(string str)
    signal free_mouth_result_all(string str)
    signal start_get_tvc_timer_result(int i)
    signal maintenance_status_result(string str)
    signal start_get_language_result(string str)
    signal start_get_theme_result(string str)
    signal start_get_contactless_result(string str)
    signal start_get_openmode_mode_result(string str)
    signal get_scanner_version_result(string str)
    signal start_internet_status_result(string str)
    signal user_login_result(string str)
    signal user_info_result(string str)
    



    //==================================================================================================//
    Image{
        id: base_background
        anchors.fill: parent
        // source: "asset/base/background/bg.png"
        source: ""
        fillMode: Image.Stretch
        Background{
            logo_vis:true
        }
    }
    //==================================================================================================//

    property var reference: "none"
    property int media_idx: 0
    property bool isEmergency: (CONFIG.isEmergency=='0') ? false : true
    property var staticFile: CONFIG.staticFile
    property int screenSize: 15

    StackView {
        id: my_stack_view
        // anchors.fill: root
        anchors.fill: parent
        initialItem: select_service
//        initialItem: simcard_ppob_view

        delegate: StackViewDelegate {
            function transitionFinished(properties)
            {
                properties.exitItem.opacity = 1
            }

            pushTransition: StackViewTransition {
                PropertyAnimation {
                    target: enterItem
                    property: "opacity"
                    from: 0
                    to: 1
                }
                PropertyAnimation {
                    target: exitItem
                    property: "opacity"
                    from: 1
                    to: 0
                }
            }
        }
    }

    //Start Andalas
    // kirim_view
    Component{id:select_service
        SelectService{}
    }
    Component{id:courier_id_page
        CourierIdPage{}
    }
    Component{id:courier_qr_page
        CourierQrPage{}
    }
    Component{id:courier_finger_page
        CourierFingerPage{}
    }
    Component{id:courier_home_page
        CourierHomePage{}
    }
}




