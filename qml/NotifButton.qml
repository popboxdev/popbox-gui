import QtQuick 2.4

Rectangle {
    id: main_rectangle
    property var r_width: 230
    property var r_height: 80
    width: r_width
    height: r_height
    property var buttonColor: '#ff524f'
    // property var font_type: "SourceSansPro-Regular"
    property int r_radius: 2
    property bool insertImage: false
    property bool modeReverse: false
    property bool bold: false
    property var buttonText: ""
    property var show_source: ""
    property int font_size: 24
    property int img_width: 82
    property int img_height: 75
    property int img_x: 19
    property int img_y: 8
    property bool visibleflag: true
    property var font_name: fontStyle.medium
    color: (modeReverse==true) ? "#FFFFFF" : buttonColor
    radius: r_radius
    border.width: (modeReverse==true) ? 3 : 0
    border.color: buttonColor
    visible: (visibleflag) ? true:false
    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }
    Text {
        id: button_text
        anchors.fill: parent
        text: (insertImage==true) ? qsTr("     "+ buttonText) : qsTr(buttonText)
        verticalAlignment: Text.AlignVCenter 
        horizontalAlignment: Text.AlignHCenter
        font.family: font_name
        // font.bold: bold
        font.pixelSize: font_size
        color: (modeReverse==true) ? buttonColor : "#FFFFFF"
    }
    Image{
        id: img_button
        width:img_width
        height:img_height
        x:img_x
        y:img_y
        visible: (insertImage==true) ? true : false
        source:show_source
    }
}
