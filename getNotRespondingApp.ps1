[CmdletBinding()]            
Param(            
 [string[]]$ComputerName = $env:ComputerName            
)

foreach($Computer in $ComputerName) {            
    if(!(Test-Connection -ComputerName $Computer -Count 1 -quiet)) {             
    Write-Host "$Computer : OFFLINE (wrong PC`s name)"            
    Continue            
    }            
    try {            
        $Processes = Get-Process -ComputerName $Computer -EA Stop            
        $nProcesses = @($Processes | ? { $_.Responding -eq $false })
        $LogFile = "log_crash.txt"
        $DateTime = "[{0:MM/dd/yy} {0:HH:mm:ss}]" -f (Get-Date)
        $scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
        # delete file : log_crash.txt if creation time more than $param days
        $deletefilesolderthan = 1
        try {
            $file = Get-Item "$scriptPath\log_crash.txt"
            if ($file.CreationTime -lt (Get-Date).AddDays(-$deletefilesolderthan)){
                Remove-Item -Path "$scriptPath\log_crash.txt"
            }
        }
        catch{
            Write-Error "File : log_crash.txt not FOUND"
        }
    } 
    catch {            
        Write-Error "Failed to query processes. $_"            
    }            
    if($nProcesses) {            
        foreach($nProcess in $nProcesses) {            
            $gotName =  $nProcess.Name
            $gotId = $nProcess.id
            Write-Host "$gotName : is CRASHED"
            $Message = "$DateTime $gotName : is CRASHED";
            taskkill /F /PID $gotId
        }                  
    } else {            
        Write-host "Not Found NON-Responding Processes"
        $Message = "$DateTime Not Found NON-Responding Processes";           
    }
    $Message >> $LogFile;         
}
