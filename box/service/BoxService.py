import json
import logging
import random
import time
from PyQt5.QtCore import QObject, pyqtSignal
import ClientTools
import Configurator
import box.repository.BoxDao as BoxDao
import company.repository.CompanyDao as CompanyDao
import company.service.CompanyService as CompanyService
import express.service.ExpressService as ExpressService
import express.repository.ExpressDao as ExpressDao
import user.service.UserService as UserService
import alert.service.AlertService as AlertService
from database import ClientDatabase
from device import LockMachine
from network import HttpClient
from return_rules.repository import ReturnRulesDao
import sys, os


class BoxSignalHandler(QObject):
    mouth_status_signal = pyqtSignal(str)
    init_client_signal = pyqtSignal(str)
    load_mouth_list_signal = pyqtSignal(str)
    manager_mouth_count_signal = pyqtSignal(int)
    mouth_list_signal = pyqtSignal(str)
    manager_set_mouth_signal = pyqtSignal(str)
    manager_open_mouth_by_id_signal = pyqtSignal(str)
    manager_open_all_mouth_signal = pyqtSignal(str)
    choose_mouth_signal = pyqtSignal(str)
    mouth_number_signal = pyqtSignal(str)
    free_mouth_num_signal = pyqtSignal(str)
    free_mouth_num_signal_insulator = pyqtSignal(str)
    free_mouth_num_signal_all = pyqtSignal(str)
    free_mouth_by_size_result = pyqtSignal(str)
    get_version_signal = pyqtSignal(str)
    manager_get_box_info_signal = pyqtSignal(str)
    open_main_box_signal = pyqtSignal(str)
    start_open_door_id_express_signal = pyqtSignal(str)
    start_choose_mouth_for_change_size_signal = pyqtSignal(str)


_LOG_ = logging.getLogger()
version = '1.30412.006.160729'
_BOX_ = BoxSignalHandler()
box_config = ''


def start_get_version():
    ClientTools.get_global_pool().apply_async(get_version)


def get_version():
    global version
    _BOX_.get_version_signal.emit(version)


def start_manager_get_box_info():
    ClientTools.get_global_pool().apply_async(manager_get_box_info)


def manager_get_box_info():
    box_info = get_box()
    box_info['operator_name'] = CompanyDao.get_company_by_id({'id': box_info['operator_id']})[0]['name']
    box_info['order_no'] = Configurator.get_value('ClientInfo', 'OrderNo')
    box_info['token'] = Configurator.get_value('ClientInfo', 'Token')
    if Configurator.get_value('Scanner', 'port'):
        box_info['scanner_port'] = Configurator.get_value('Scanner', 'port')
    else:
        box_info['scanner_port'] = 'None'
    if Configurator.get_value('LockMachine', 'port'):
        box_info['lock_machine_port'] = Configurator.get_value('LockMachine', 'port')
    else:
        box_info['lock_machine_port'] = 'None'
    if Configurator.get_value('Ups', 'port'):
        box_info['ups_port'] = Configurator.get_value('Ups', 'port')
    else:
        box_info['ups_port'] = 'None'
    _BOX_.manager_get_box_info_signal.emit(json.dumps(box_info))


def start_init_client():
    ClientTools.get_global_pool().apply_async(init_client)


def init_client():
    global box_config
    message, status_code = HttpClient.get_message('box/init')
    box_config = message
    # print("box_config : ", json.dumps(box_config))
    _LOG_.info(('box_config:', box_config))
    if status_code != 200:
        return message['statusMessage']
    try:
        ClientDatabase.init_database()
        message = json.loads(json.dumps(message))
        box_param = {'id': message['id'],
                     'name': message['name'],
                     'orderNo': message['orderNo'],
                     'validateType': message['validateType'],
                     'operator_id': message['operator']['id'],
                     'syncFlag': message['syncFlag'],
                     'currencyUnit': message['currencyUnit'],
                     'overdueType': message['overdueType'],
                     'freeHours': ClientTools.get_value('freeHours', message),
                     'freeDays': ClientTools.get_value('freeDays', message),
                     'receiptNo': ClientTools.get_value('receiptNo', message, 0)}
        company_msg = message['operator']
        company_param = {'id': company_msg['id'],
                         'companyType': company_msg['companyType'],
                         'name': company_msg['name'],
                         'parentCompany_id': '',
                         'deleteFlag': company_msg['deleteFlag']}
        CompanyService.init_company(company_param)
        BoxDao.init_box(box_param)
        cabinets = message['cabinets']
        try:
            for cabinet in cabinets:
                BoxDao.init_cabinet(cabinet)
                mouths = cabinet['mouths']
                for m in mouths:
                    __mouth = {'id': m['id'],
                               'deleteFlag': m['deleteFlag'],
                               'number': m['number'],
                               'usePrice': m['usePrice'],
                               'overduePrice': m['overduePrice'],
                               'status': m['status'],
                            #    'is_insulator': 0,
                               'is_insulator': m['is_insulator'], # Bermasalah
                               'box_id': message['id'],
                               'cabinet_id': cabinet['id'],
                               'numberInCabinet': m['numberInCabinet'],
                               'syncFlag': m['syncFlag'],
                               'openOrder': None}
                    if 'express' in m.keys():
                        __mouth['express_id'] = m['express']['id']
                        ExpressService.init_express(m['express'], __mouth, box_param)
                    else:
                        __mouth['express_id'] = None
                    __mouth['mouthType_id'] = m['mouthType']['id']
                    mouth_type_param = m['mouthType']
                    mouth_type = {'id': mouth_type_param['id'],
                                  'name': mouth_type_param['name'],
                                  'defaultUsePrice': mouth_type_param['defaultUsePrice'],
                                  'defaultOverduePrice': mouth_type_param['defaultOverduePrice'],
                                  'deleteFlag': mouth_type_param['deleteFlag']}
                    BoxDao.init_mouth_type(mouth_type)
                    BoxDao.init_mouth(__mouth)
        except Exception as e:
            _LOG_.debug(("Parsing Cabinet & Mouth ERROR : ", e))
        # init for return_rules
        try:
            msg, stat_code = HttpClient.get_message('box/return-rules/list')
            if stat_code != (200 or 202):
                return msg['message'] #balikan 202
            try:
                msg = json.loads(json.dumps(msg))
                list_return_rule = msg['data']
                for return_rule in list_return_rule:
                    rule_params = {
                        'id' : return_rule['id_rules'],
                        'regularContent' : return_rule['regularContent'],
                        'groupName' : return_rule['groupName'],
                        'operator_id': return_rule['operator_id'],
                        'logistic_id': return_rule['logistic_id'],
                        'ecommerce_id': return_rule['ecommerce_id'],
                        'deleteFlag': return_rule['deleteFlag']
                    }
                    ReturnRulesDao.insert_return_rule(rule_params)
            except Exception as er:
                _LOG_.debug(("Init ReturnRules FAILED : ", er))
        except Exception as err:
            _LOG_.debug(("Hit ReturnRules API FAILED : ", err))
    except Exception as e:
        _LOG_.debug(("Parsing Inititation DB ERROR : ", e))
    _BOX_.init_client_signal.emit('Success')


def start_update_box(message_result):
    ClientTools.get_global_pool().apply_async(update_box, (message_result,))


def update_box(message_result):
    _LOG_.info(('init_box_info:', message_result))
    try:
        box_param = {'id': message_result['box']['id'],
                     'name': message_result['box']['name'],
                     'orderNo': message_result['box']['orderNo'],
                     'validateType': message_result['box']['validateType'],
                     'currencyUnit': message_result['box']['currencyUnit'],
                     'overdueType': message_result['box']['overdueType'],
                     'freeHours': message_result['box']['freeHours'],
                     'freeDays': message_result['box']['freeDays']}
        BoxDao.update_box(box_param)
        box_post_message(message_result['id'], 'UPDATE BOX SUCCESS', 'SUCCESS')
    except Exception as e:
        _LOG_.debug(('update_box ERROR :', e))
        box_post_message(message_result['id'], 'UPDATE BOX ERROR', 'ERROR')


def start_free_mouth_by_size(mouth_size):
    free_mouth_by_size(mouth_size)


def free_mouth_by_size(mouth_size):
    mouth_type_param = {'name': mouth_size}
    mouth_type_list = BoxDao.get_mouth_type(mouth_type_param)
    if len(mouth_type_list) != 1:
        _BOX_.free_mouth_by_size_result.emit('Failure')
    mouth_type = mouth_type_list[0]
    mouth_param = {'mouthType_id': mouth_type['id'],
                   'deleteFlag': 0,
                   'status': 'ENABLE',
                   'is_insulator':0}
    mouth_list = BoxDao.get_free_mouth_by_type(mouth_param)
    if len(mouth_list) == 0:
        _BOX_.free_mouth_by_size_result.emit('Failure')
    _BOX_.free_mouth_by_size_result.emit('Success')


def get_free_mouth_by_size(mouth_size, is_insulator=False):
    mouth_type_param = {'name': mouth_size}
    mouth_type_list = BoxDao.get_mouth_type(mouth_type_param)
    if len(mouth_type_list) != 1:
        return False
    mouth_type = mouth_type_list[0]
    if is_insulator is True:
        is_insulator = 1
    else:
        is_insulator = 0

    mouth_param = {'mouthType_id': mouth_type['id'],
                   'deleteFlag': 0,
                   'is_insulator': is_insulator,
                   'status': 'ENABLE'}
    print('mouth_param_insulator : ', str(mouth_param))
    mouth_list = BoxDao.get_free_mouth_by_type(mouth_param)
    if len(mouth_list) == 0:
        return False
    mouth_no = random.randint(0, len(mouth_list) - 1)
    return mouth_list[mouth_no]


def get_box():
    order_no = Configurator.get_value('ClientInfo', 'OrderNo')
    tb = ClientDatabase.check_tb()
    box_list = []
    if tb is None or not tb:
        print('table is not exist')
        ClientDatabase.init_database()
        init_client()
        param = {'orderNo': order_no, 'deleteFlag': 0}
        box_list = BoxDao.get_box_by_order_no(param)
        if len(box_list) != 1:
            return False
        return box_list[0]
    else:
        param = {'orderNo': order_no, 'deleteFlag': 0}
        box_list = BoxDao.get_box_by_order_no(param)
        if len(box_list) != 1:
            return False
        return box_list[0]


def get_mouth(express__):
    return BoxDao.get_mouth(express__)


def free_mouth(mouth__):
    BoxDao.free_mouth(mouth__)


def use_mouth(mouth_param__):
    BoxDao.use_mouth(mouth_param__)


last_open_mouth_id = None
last_mouth_number = ''
last_mouth_number_size = ''


def start_open_mouth():
    ClientTools.get_global_pool().apply_async(open_mouth)

def open_mouth(mouth_id=None):
    global last_open_mouth_id
    global last_mouth_number
    global last_mouth_number_size
    try:
        if mouth_id is None:
            _LOG_.info(('open mouth id again from last mouth!', last_open_mouth_id))
            mouth_id = last_open_mouth_id
        else:
            _LOG_.info(('open mouth id :', mouth_id))
            last_open_mouth_id = mouth_id
        mouth_list = BoxDao.get_mouth_by_id({'id': mouth_id})
        if len(mouth_list) != 1:
            _LOG_.debug(('mouth list error!', str(mouth_list)))
            return
        _mouth = mouth_list[0]
        cabinet_list = BoxDao.get_cabinet_by_id({'id': _mouth['cabinet_id']})
        if len(cabinet_list) != 1:
            _LOG_.warning(('cabinet list error!', str(cabinet_list)))
            return
        cabinet = cabinet_list[0]
        last_mouth_number = _mouth['number']
        last_mouth_number_size = _mouth['name']
        # LockMachine.start_open_door(int(cabinet['number']), int(_mouth['numberInCabinet']))
        try:
            status_board = LockMachine.open_door(int(cabinet['number']), int(_mouth['numberInCabinet']))
            return status_board
        except Exception as e:
            return False
    except Exception as e:
        _LOG_.warning(('open_mouth ', e))
        return False

def open_mouth_by_id_express_id(mouth_id=None, express_id=None):
    try:
        if mouth_id is None or express_id is None:
            _LOG_.warning(('open_mouth_by_id_express_and_mouth_id ', 'Express or Mouth id is None'))            
            return False
        else:
            param = {
                'mouth_id': mouth_id,
                'express_id': express_id
            }
            checking_data_mouth = BoxDao.get_data_by_express_id_mouth_id(param)
            if len(checking_data_mouth) < 1:
                param_check_express = {'id': express_id}
                check_express = ExpressDao.get_express_by_id(param_check_express)
                if len(check_express) > 0 and (check_express[0]['status'] == "CUSTOMER_TAKEN" or check_express[0]['status'] == "COURIER_TAKEN"):
                    mouth_check_param = {
                        "mouth_id": check_express[0]['mouth_id'], 
                        "status": "ENABLE"
                    }
                    check_mouth = BoxDao.check_mouth_for_open_again(mouth_check_param)
                    if len(check_mouth) < 1:
                        _LOG_.warning(('[MOUTH_ID_BASED_ON_EXPRESS_ID] is None'))
                        return False
                    else:
                        try:
                            data_mouth_opn_agn = check_mouth[0]
                            _mouth, cabinet = check_cabinet_mouth(data_mouth_opn_agn['id'])
                            _LOG_.info(('[OPEN_DOOR_BY_EXPRESS_ID_WITH_MOUTH_ID]', data_mouth_opn_agn['id'], ' <LOCKER_NUMBER > ', cabinet['number']))
                            status_board = LockMachine.open_door(int(cabinet['number']), int(_mouth['numberInCabinet']))
                            return status_board
                        except Exception as e:
                            return False
                        return
                else:
                    _LOG_.warning(('[MOUTH_ID_BASED_ON_EXPRESS_ID] is None'))
                    return False
            else:
                try:
                    _mouth, cabinet = check_cabinet_mouth(mouth_id)
                    _LOG_.info(('[OPEN_DOOR_BY_EXPRESS_ID_WITH_MOUTH_ID]', mouth_id, ' <LOCKER_NUMBER > ', cabinet['number']))
                    status_board = LockMachine.open_door(int(cabinet['number']), int(_mouth['numberInCabinet']))
                    return status_board
                except Exception as e:
                    return False
                return
    except Exception as e:
        _LOG_.warning(('open_mouth_by_id_express_and_mouth_id ', e))
        return False

def check_cabinet_mouth(mouth_id):
    try:
        mouth_list = BoxDao.get_mouth_by_id({'id': mouth_id})
        if len(mouth_list) != 1:
            _LOG_.debug(('mouth list error!', str(mouth_list)))
            return False
        _mouth = mouth_list[0]
        cabinet_list = BoxDao.get_cabinet_by_id({'id': _mouth['cabinet_id']})
        if len(cabinet_list) != 1:
            _LOG_.warning(('cabinet list error!', str(cabinet_list)))
            return False
        cabinet = cabinet_list[0]
        return _mouth, cabinet
    except Exception as e:
        _LOG_.warning(('check_mouth_id_cabinet ', e))
        return False

def start_open_mouth_again_by_id(express_id):
    ClientTools.get_global_pool().apply_async(open_mouth_again, (express_id,))

def open_mouth_again(express_id):
    try:
        param = {
            'id': express_id
        }
        check_express = ExpressDao.get_express_by_id(param)
        if len(check_express) != 1:
            return False
        else:
            data_express = check_express[0]
            result_open_door_again = open_mouth_by_id_express_id(data_express['mouth_id'], data_express['id'])
            print('open_mouth_again_status :', result_open_door_again)
            return 
    except Exception as e:
        _LOG_.warning(('[ERROR] open_mouth_again ', e))
        return False

def start_get_mouth_status():
    ClientTools.get_global_pool().apply_async(get_mouth_status)

def get_mouth_status():
    result = ''
    for name in ['XL', 'L', 'M', 'S', 'MINI']:
        param = {'status': 'ENABLE', 'name': name}
        free_mouth_count = BoxDao.get_free_mouth_count_by_mouth_type_name(param)[0]
        if free_mouth_count['count'] > 0:
            result += 'T'
        else:
            result += 'F'

    _BOX_.mouth_status_signal.emit(result)


def start_load_manager_mouth_count():
    ClientTools.get_global_pool().apply_async(load_manager_mouth_count)


def load_manager_mouth_count():
    count_list = BoxDao.get_all_mouth_count({'deleteFlag': 0})
    _LOG_.debug(('load_manager_mouth_count : ', len(count_list)))
    _BOX_.manager_mouth_count_signal.emit(count_list[0]['count'])


def start_load_mouth_list(page):
    ClientTools.get_global_pool().apply_async(manager_load_mouth_list, (page,))
    _BOX_.load_mouth_list_signal.emit('Success')


user_bypass_status = False


def start_user_bypass():
    global user_bypass_status
    user_bypass_status = True


def stop_user_bypass():
    global user_bypass_status
    user_bypass_status = False


def manager_load_mouth_list(page):
    global user_bypass_status
    if user_bypass_status is True:
        __user = {'role': 'OPERATOR_USER'}
    else:
        __user = UserService.get_user()
    get_mouth_param = {'deleteFlag': 0,
                       'startLine': (int(page) - 1) * 25}
    __user_access = __user['user_role']['level_role']
    if __user_access == 1 or __user_access == 2 or __user_access == 3 or __user_access == 4 or __user_access == 5 or __user_access == 6:
        mouth_list_ = BoxDao.get_mouth_list(get_mouth_param)
        _BOX_.mouth_list_signal.emit(json.dumps(mouth_list_))


def start_manager_set_mouth(box_id, box_status):
    ClientTools.get_global_pool().apply_async(manager_set_mouth, (box_id, box_status))


def manager_set_mouth(mouth_id, box_status):
    set_mouth_param = {'id': mouth_id,
                       'status': box_status,
                       'syncFlag': 0}
    BoxDao.manage_set_mouth(set_mouth_param)
    _BOX_.manager_set_mouth_signal.emit('Success')
    message, status_code = HttpClient.post_message('box/mouth/sync', set_mouth_param)
    if status_code == 200:
        BoxDao.mark_sync_success(set_mouth_param)


def start_manager_open_mouth(mouth_id):
    ClientTools.get_global_pool().apply_async(manager_open_mouth, (mouth_id,), callback=return_signal)


def start_manager_open_empty_mouth():
    ClientTools.get_global_pool().apply_async(manager_open_empty_mouth)

def manager_open_empty_mouth():
    mouth_list = BoxDao.get_empty_mouth({'deleteFlag': 0, 'status': 'ENABLE'})
    _LOG_.info("MOUTH KOSONG : " + str(mouth_list))
    for _mouth in mouth_list:
        manager_open_mouth(_mouth['id'])
        time.sleep(1)

    _BOX_.manager_open_all_mouth_signal.emit('Success')


def start_manager_open_all_mouth():
    ClientTools.get_global_pool().apply_async(manager_open_all_mouth)


def manager_open_all_mouth():
    mouth_list = BoxDao.get_all_mouth({'deleteFlag': 0})
    for _mouth in mouth_list:
        manager_open_mouth(_mouth['id'])
        time.sleep(1)

    _BOX_.manager_open_all_mouth_signal.emit('Success')


def manager_open_mouth(mouth_id):
    global user_bypass_status
    if user_bypass_status is True:
        __user = {'role': 'OPERATOR_USER'}
    else:
        __user = UserService.get_user()
    if __user['role'] != 'OPERATOR_USER' and __user['role'] != 'OPERATOR_ADMIN':
        _BOX_.manager_open_mouth_by_id_signal.emit('Error')
        return False
    open_mouth(mouth_id)
    return True


def return_signal(result):
    _LOG_.debug(result)
    if not result:
        return
    _BOX_.manager_open_mouth_by_id_signal.emit('Success')


def start_manager_set_free_mouth(box_id):
    ClientTools.get_global_pool().apply_async(manager_set_free_mouth, (box_id,))


def manager_set_free_mouth(box_id):
    pass


mouth = None


def start_choose_mouth_size(mouth_size, method_type):
    ClientTools.get_global_pool().apply_async(choose_mouth_size, (mouth_size, method_type))

def choose_mouth_size(mouth_size, method_type):
    global mouth
    try:
        _LOG_.info(('choose_mouth_size:', mouth_size, ' type:', method_type))
        mouth = get_free_mouth_by_size(mouth_size, False)
        if not mouth:
            mouth = None
            # logger.warning('not ' + mouth_size + ' mouth!')
            if method_type == 'change_size':
                _BOX_.start_choose_mouth_for_change_size_signal.emit('NotMouth')
            else:
                _BOX_.choose_mouth_signal.emit('NotMouth')
            return
        # user_result = UserService.get_user()
        # if method_type == 'staff_store_express' and user_result['wallet']['balance'] < mouth['usePrice']:
        # if method_type == 'staff_store_express':
        #     # logger.warning('not enough money!')
        #     _BOX_.choose_mouth_signal.emit('NotBalance')
        #     return
        set_mouth_number(mouth['id'])
        # open_mouth(mouth['id'])
        _LOG_.info('open mouth id:' + str(mouth['id']) + ' number:' + str(mouth['number']))
        if method_type == 'change_size':
            _BOX_.start_choose_mouth_for_change_size_signal.emit('Success')
        else:
            _BOX_.choose_mouth_signal.emit('Success')
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] choose_mouth_size : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _BOX_.choose_mouth_signal.emit('false')


def start_choose_mouth_size_insulator(mouth_size, method_type):
    ClientTools.get_global_pool().apply_async(choose_mouth_size_insulator, (mouth_size, method_type))

def choose_mouth_size_insulator(mouth_size, method_type):
    global mouth
    try:
        _LOG_.info(('choose_mouth_size_insulator:', mouth_size, ' type:', method_type))
        mouth = get_free_mouth_by_size(mouth_size, True)
        print('mouth_insulator : ', str(mouth))
        if not mouth:
            mouth = None
            # logger.warning('not ' + mouth_size + ' mouth!')
            if method_type == 'change_size':
                _BOX_.start_choose_mouth_for_change_size_signal.emit('NotMouth')
            else:
                _BOX_.choose_mouth_signal.emit('NotMouth')
            return False
        # user_result = UserService.get_user()
        # if method_type == 'staff_store_express' and user_result['wallet']['balance'] < mouth['usePrice']:
        # if method_type == 'staff_store_express':
        #     # logger.warning('not enough money!')
        #     _BOX_.choose_mouth_signal.emit('NotBalance')
        #     return
        set_mouth_number(mouth['id'])
        # open_mouth(mouth['id'])
        _LOG_.info('open mouth id:' + str(mouth['id']) + ' number:' + str(mouth['number']))
        if method_type == 'change_size':
            _BOX_.start_choose_mouth_for_change_size_signal.emit('Success')
        else:
            _BOX_.choose_mouth_signal.emit('Success')
        return True
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] choose_mouth_size : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _BOX_.choose_mouth_signal.emit('false')
        return False

def set_mouth_number(mouth_id):
    global last_open_mouth_id
    global last_mouth_number
    global last_mouth_number_size
    try:
        if mouth_id is None:
            _LOG_.info(('open mouth again!', last_open_mouth_id))
            mouth_id = last_open_mouth_id
        else:
            _LOG_.info(('open mouth id :', mouth_id))
            last_open_mouth_id = mouth_id
        mouth_list = BoxDao.get_mouth_by_id({'id': mouth_id})
        if len(mouth_list) != 1:
            _LOG_.debug(('mouth list error!', str(mouth_list)))
            return
        _mouth = mouth_list[0]
        cabinet_list = BoxDao.get_cabinet_by_id({'id': _mouth['cabinet_id']})
        if len(cabinet_list) != 1:
            _LOG_.warning(('cabinet list error!', str(cabinet_list)))
            return
        cabinet = cabinet_list[0]
        last_mouth_number = _mouth['number']
        last_mouth_number_size = _mouth['name']
    except Exception as e:
        print('error_mouth_size', str(e))


def get_express_mouth_number():
    res = {}
    res['isSuccess'] = 'true'
    res['locker_number'] = last_mouth_number
    res['locker_number_size'] = last_mouth_number_size
    _BOX_.mouth_number_signal.emit(str(json.dumps(res)))

def start_get_express_mouth_number_by_express_id(express_id):
    try:
        res = {}
        param_express = {'id': express_id}
        get_express = ExpressDao.get_express_by_id(param_express)

        if len(get_express) < 1:
            res['isSuccess'] = 'true'
            res['locker_number'] = 0
            res['locker_number_size'] = '-'
            _BOX_.mouth_number_signal.emit(str(json.dumps(res)))
            return
        else:
            express = get_express[0]
            param = {'id': express['mouth_id']}
            get_data_mouth = BoxDao.get_mouth_by_id(param)

            if len(get_data_mouth) < 1:
                res['isSuccess'] = 'true'
                res['locker_number'] = 0
                res['locker_number_size'] = '-'
                _BOX_.mouth_number_signal.emit(str(json.dumps(res)))
                return
            else:
                mouth = get_data_mouth[0]
                res['isSuccess'] = 'true'
                res['locker_number'] = mouth['number']
                res['locker_number_size'] = mouth['name']
                _BOX_.mouth_number_signal.emit(str(json.dumps(res)))
                return
    except Exception as e:
        print('error_get_mouth', str(e))

def update_free_time(param):
    box_list = BoxDao.get_box_by_order_no({'orderNo': param['orderNo'], 'deleteFlag': 0})
    if len(box_list) == 0:
        # logger.warning('box not found!')
        return False
    param['overdueType'] = ClientTools.get_value('overdueType', param)
    param['freeDays'] = ClientTools.get_value('freeDays', param)
    param['freeHours'] = ClientTools.get_value('freeHours', param)
    BoxDao.update_free_time(param)
    return True

def start_get_free_mouth_mun_insulator():
    ClientTools.get_global_pool().apply_async(get_free_mouth_num_insulator)

def start_get_free_mouth_mun( flagInsulator ):
    ClientTools.get_global_pool().apply_async(get_free_mouth_num, (flagInsulator))

def get_free_mouth_num( flagInsulator ):
    __result = {}
    __mouth_type_list = BoxDao.get_all_mouth_type({'deleteFlag': 0})
    if flagInsulator == 1 or flagInsulator == "1":
        insulator = 1
    else:
        insulator = 0
    try:
        print( flagInsulator, insulator )
        for __mouth_type in __mouth_type_list:
            param = {'status': 'ENABLE',
                    'mouthType_id': __mouth_type['id'],
                    'is_insulator': insulator,
                    'deleteFlag': 0}
            if flagInsulator == "2" or flagInsulator == 2:
                param = {'status': 'ENABLE',
                    'mouthType_id': __mouth_type['id'],
                    'deleteFlag': 0}
                __free_mouth_count = BoxDao.get_count_by_status_and_mouth_type_id_all_insulator_noninsulator(param)[0]
                print( 'param_get_free : ', str(param))
                
            else:
                __free_mouth_count = BoxDao.get_count_by_status_and_mouth_type_id(param)[0]            
            __result[__mouth_type['name']] = __free_mouth_count['mouth_count']

        if flagInsulator == "2" or flagInsulator == 2:
            _BOX_.free_mouth_num_signal_all.emit(json.dumps(__result))
        else:
            _BOX_.free_mouth_num_signal.emit(json.dumps(__result))
        return
    except Exception as e:
        print('error_availability : ', e)

def get_free_mouth_num_insulator():
    __result = {}
    __mouth_type_list = BoxDao.get_all_mouth_type({'deleteFlag': 0})
    counter_mouth = 0
    for __mouth_type in __mouth_type_list:
        param = {'status': 'ENABLE',
                 'is_insulator': 1,
                 'mouthType_id': __mouth_type['id'],
                 'deleteFlag': 0}
        __free_mouth_count = BoxDao.get_count_by_status_and_mouth_type_id(param)[0]
        __result[__mouth_type['name']] = __free_mouth_count['mouth_count']
        counter_mouth = counter_mouth + int(__free_mouth_count['mouth_count'])
    __result['total'] = counter_mouth
    _BOX_.free_mouth_num_signal_insulator.emit(json.dumps(__result))


def get_scan_not_sync_mouth_list():
    return BoxDao.get_not_sync_mouth_list({'syncFlag': 0})


def mark_sync_success(mouth):
    return BoxDao.mark_sync_success(mouth)


def mark_box_sync_success():
    return BoxDao.mark_box_sync_success({'deleteFlag': 0})


last_pull_open_mouth_id = None
last_pull_mouth_number = ''


def pull_open_mouth(message_result):
    global last_pull_mouth_number
    global last_pull_open_mouth_id
    if message_result['mouth']['id'] is None:
        box_post_message(message_result['id'], 'MOUTH NOT AVAILABLE!', 'ERROR')
        return
    _LOG_.info(('Server_pull open mouth id :', message_result['mouth']['id']))
    last_pull_open_mouth_id = message_result['mouth']['id']
    mouth_list = BoxDao.get_mouth_by_id({'id': message_result['mouth']['id']})
    if len(mouth_list) != 1:
        box_post_message(message_result['id'], 'MOUTH LIST ERROR!', 'ERROR')
        return
    _mouth = mouth_list[0]
    cabinet_list = BoxDao.get_cabinet_by_id({'id': _mouth['cabinet_id']})
    if len(cabinet_list) != 1:
        box_post_message(message_result['id'], 'CABINET LIST ERROR!', 'ERROR')
        return
    cabinet = cabinet_list[0]
    last_pull_mouth_number = _mouth['number']
    LockMachine.start_open_door(int(cabinet['number']), int(_mouth['numberInCabinet']))
    box_post_message(message_result['id'], 'SUCCESSFULLY OPEN!', 'SUCCESS')


def box_post_message(task_id, result, reset_status):
    reset_express_result = {'id': task_id, 'result': result, 'statusType': reset_status}
    HttpClient.post_message('task/finish', reset_express_result)


def start_update_mouth_status(message_result):
    ClientTools.get_global_pool().apply_async(update_mouth_status, (message_result,))


def update_mouth_status(message_result):
    try:
        _LOG_.info(('update_mouth_status:', message_result))
        mouth_result = BoxDao.get_mouth_by_id({'id': message_result['mouth']['id']})
        _LOG_.debug(('mouth_result is :', mouth_result))
        if len(mouth_result) == 0:
            box_post_message(message_result['id'], 'MOUTH NOT AVAILABLE', 'ERROR')
        else:
            mouth_param = {'id': message_result['mouth']['id'],
                           'status': message_result['mouth']['status']}
            BoxDao.update_mouth_status(mouth_param)
            box_post_message(message_result['id'], 'MOUTH SUCCESSFULLY UPDATED!', 'SUCCESS')
    except Exception as e:
        _LOG_.debug(('update_mouth_status ERROR!!', e))
        box_post_message(message_result['id'], 'UPDATE MOUTH FAILED', 'ERROR')


def start_open_main_box():
    ClientTools.get_global_pool().apply_async(open_main_box)


def open_main_box():
    try:
        main_box_cabinet = Configurator.get_value('MainBox', 'cabinet')
        main_box_number_in_cabinet = Configurator.get_value('MainBox', 'numberInCabinet')
        result = LockMachine.open_door(int(main_box_cabinet), int(main_box_number_in_cabinet))
        if result:
            _LOG_.info('main box open Success')
            AlertService.main_box_open_alert()
            _BOX_.open_main_box_signal.emit('Success')
        else:
            _LOG_.info('main box open Failed')
            _BOX_.open_main_box_signal.emit('Failed')
    except Exception as e:
        _LOG_.debug('open_main_box ERROR :', e)

def get_return_rules():
    param = {'deleteFlag': 0}
    return_list = BoxDao.get_rules(param)
    if len(return_list) == 0:
        return False
    return return_list

def get_data_door(mouth_id):
    data = BoxDao.get_mouth_by_id({'id': mouth_id})
    return data[0]

def start_init_insulator():
    ClientTools.get_global_pool().apply_async(init_insulator)

def init_insulator():
    global box_config
    box_info = BoxDao.get_box_info()[0]
    message, status_code = HttpClient.get_message('insulator/list/pricing/' + box_info['id'])
    # _LOG_.info(('box_config:', status_code))
    # print( " result_config_insulator : " + str(message) + "box_info : " + box_info['id'])

    if status_code != (200 or 202):
        return message['response']['message']
    else:
        check_tb_exist = BoxDao.check_table_info({'table': 'table', 'table_name': 'InsulatorPricing'})
        if not check_tb_exist:
            try:
                tb_paid_overdue = """
                CREATE TABLE IF NOT EXISTS InsulatorPricing
                (
                    locker_id   VARCHAR(255)   NOT NULL,
                    locker_name           VARCHAR(255),
                    order_no              VARCHAR(255),
                    overdue_day           INT NOT NULL DEFAULT(1),
                    overdue_hour          INT NOT NULL DEFAULT(1),
                    logisticsCompany_id   VARCHAR(100),
                    company_name          VARCHAR(100),
                    S                     VARCHAR(100),
                    M                     VARCHAR(100),
                    L                     VARCHAR(100),
                    XL                    VARCHAR(100),
                    is_active             INT DEFAULT(1),
                    deleteFlag            INT DEFAULT(0),
                    abort_charging        VARCHAR(2) DEFAULT(0)
                );"""
                
                ClientDatabase.create_table(tb_paid_overdue)
                insert_data_to_insulator_pricing(message)
                return
            except Exception as e:
                print('[ERROR_CREATE_INSULATOR_PRICING]:', str(e))
                _LOG_.warning(('[ERROR_CREATE_INSULATOR_PRICING]:', str(e)))
                return
        else:
            print('tb_insulator_pricing_is_exist')
            BoxDao.delete_rows_insulator_pricing({})
            insert_data_to_insulator_pricing(message)
            return

def insert_data_to_insulator_pricing(message):
    try:
        list_company_pricing = message['data']
        for company_pricing in list_company_pricing:
            BoxDao.init_insulator_pricing(company_pricing)
        return
    except Exception as e:
        _LOG_.warning(('[ERROR_INIT_INSULATOR_PRICING]:', str(e)))
        return

def start_open_door_by_express_id(id_express):
    ClientTools.get_global_pool().apply_async(open_door_by_id_express, (id_express,))

def open_door_by_id_express(id_express):
    try:
        check_express = {'id': id_express}
        inquiry = ExpressDao.get_express_by_id(check_express)
        res = {}
        if len(inquiry) < 1:
            res['isSuccess'] = "false"
            _BOX_.start_open_door_id_express_signal.emit(str(json.dumps(res)))
            return

        mouth_id = inquiry[0]['mouth_id']
        
        if mouth_id is None:
            res['isSuccess'] = "false"
            _BOX_.start_open_door_id_express_signal.emit(str(json.dumps(res)))
            return
        else:
            # open_mouth = open_mouth(mouth_id)
            open_mouth = open_mouth_by_id_express_id(mouth_id, id_express)
            # print('hasil_open_door', str(open_mouth), ' >mouth_id: ', mouth_id, ' >express_id', id_express)
            # disabled for a moments
            # if open_mouth is not False:
            #     res['isSuccess'] = "true"
            #     _BOX_.start_open_door_id_express_signal.emit(str(json.dumps(res)))
            #     return
            # else:
            #     res['isSuccess'] = "false"
            #     _BOX_.start_open_door_id_express_signal.emit(str(json.dumps(res)))
            #     return

            res['isSuccess'] = "true"
            _BOX_.start_open_door_id_express_signal.emit(str(json.dumps(res)))
            return

    except Exception as e:
        res['isSuccess'] = "false"
        _BOX_.start_open_door_id_express_signal.emit(str(json.dumps(res)))
        _LOG_.debug('open_mouth_error ERROR :', e)
        
def start_init_paid_overdue():
    ClientTools.get_global_pool().apply_async(init_paid_overdue)

def init_paid_overdue():
    global box_config
    order_no = Configurator.get_value('ClientInfo', 'OrderNo')
    message, status_code = HttpClient.get_message('company/paying-overdue/' + order_no)
    box_config = message
    
    if status_code != 200:
        return message['response']['message']
    else:

        check_tb_exist = BoxDao.check_table_info({'table': 'table', 'table_name': 'PaidOverdue'})
        if not check_tb_exist:
            try:
                tb_paid_overdue = """
                CREATE TABLE IF NOT EXISTS PaidOverdue
                (
                    logisticsCompany_id   VARCHAR(255)   NOT NULL,
                    company_name          VARCHAR(255),
                    costs_size_mini       INT NOT NULL DEFAULT(1),
                    costs_size_small      INT NOT NULL DEFAULT(1),
                    costs_size_medium     INT NOT NULL DEFAULT(1),
                    costs_size_large      INT NOT NULL DEFAULT(1),
                    costs_size_extra_large     INT NOT NULL DEFAULT(1),
                    costs_overdue_day     INT NOT NULL DEFAULT(1),
                    costs_overdue_hour    INT NOT NULL DEFAULT(1),
                    cost_per_day         INT NOT NULL DEFAULT(1),
                    flat_costs_instore    INT NOT NULL DEFAULT(1),
                    flat_costs_overdue    INT NOT NULL DEFAULT(1),
                    tier_percentage       VARCHAR(10),
                    payment_type          VARCHAR(100),
                    calculate_price_by    VARCHAR(10),
                    free_days             VARCHAR(2),
                    max_day_costs         VARCHAR(2),
                    abort_charging        VARCHAR(2) DEFAULT(0),
                    is_active             INT NOT NULL DEFAULT(0),
                    deleteFlag            INT DEFAULT(0)
                );"""
                ClientDatabase.create_table(tb_paid_overdue)
                insert_data_to_paid_overdue(message)
                return
            except Exception as e:
                _LOG_.warning(('[ERROR_CREATE_PAID_OVERDUE]:', str(e)))
                return
        else:
            try:
                check_column = ExpressDao.check_column_exist({}, 'PaidOverdue')
                checking = 0
                columns = ["flat_costs_instore", "cost_per_day"]
                list_add = []
                for list_ in check_column:
                    for colmun in columns:
                        if list_['name'] == colmun:
                            checking += 1
                        else:
                            list_add.append(colmun) if len( list_add ) < len( columns ) else columns

                if checking <  len( columns ):
                    for add in list_add:
                        adding_column = ExpressDao.adding_column({}, add , 0, 'PaidOverdue')

                BoxDao.delete_rows_paid_overdue({})
                insert_data_to_paid_overdue(message)
                return
            except Exception as e:
                print("[*] [ERROR] insert_data_paid_pricing : %r" % e )
                _LOG_.warning(('[ERROR] insert_data_paid_pricing :', str(e)))
                return

def insert_data_to_paid_overdue(message):
    try:
        list_company_paid = message['data']
        for company_paid in list_company_paid:
            BoxDao.init_paid_overdue(company_paid)
        return
    except Exception as e:
        _LOG_.warning(('[ERROR_INIT_PAID_OVERDUE]:', str(e)))
        return

def start_set_online_offline_gui():
    ClientTools.get_global_pool().apply_async(set_online_offline_gui)

def set_online_offline_gui():
    try:
        param = {
            'alertType': 'locker-status'
        }
        result = BoxDao.get_alert_locker_status( param )
        if not result:
            box = BoxDao.get_box_info()
            print("[*] box-info %r" % box)

            param = {
                'id': ClientTools.get_uuid(),
                'alertType': 'locker-status',
                'box_id': box[0]['id'],
                'operator_id': box[0]['operator_id'],
                'createTime': ClientTools.now(),
                'alertStatus': 'ONLINE',
                'syncFlag': 1,
                'value_id': '0',
                'alertvalue': 'ONLINE'
            }
            create = BoxDao.insert_alert_locker_status( param )
        else:
            while True:
                param = {
                    'id': result[0]['id'],
                    'createTime': ClientTools.now(),
                    'alertStatus': 'ONLINE',
                    'alertvalue': 'ONLINE'
                }
                update = BoxDao.update_alert_locker_status( param )
                # print("[*] update_status_gui_to : ONLINE")
                time.sleep( 30 )
                
    except Exception as e:
        _LOG_.warning(('[set_online_offline_gui]:', str(e)))
        return


def start_init_popsafe_price():
    ClientTools.get_global_pool().apply_async(init_popsafe_price)

def init_popsafe_price():
    global box_config
    order_no = Configurator.get_value('ClientInfo', 'OrderNo')
    message, status_code = HttpClient.get_message('locker/popsafe/pricing/' + order_no)
    box_config = message
    
    if status_code != 200:
        return message['response']['message']
    else:
        check_tb_exist = BoxDao.check_table_info({'table': 'table', 'table_name': 'PopsafePricing'})
        if not check_tb_exist:
            try:
                tb_popsafe_pricing = """
                CREATE TABLE IF NOT EXISTS PopsafePricing
                (
                    popsafe_pricing_id  VARCHAR(255) NOT NULL,
                    pricing_name        VARCHAR(255),
                    size_xs             INT NOT NULL DEFAULT(1),
                    size_s              INT NOT NULL DEFAULT(1),
                    size_m              INT NOT NULL DEFAULT(1),
                    size_l              INT NOT NULL DEFAULT(1),
                    size_xl             INT NOT NULL DEFAULT(1),
                    type                VARCHAR(100),
                    duration            VARCHAR(10),
                    is_active           INT NOT NULL DEFAULT(1)
                );"""
                ClientDatabase.create_table(tb_popsafe_pricing)
                insert_data_to_popsafe_price(message)
                return
            except Exception as e:
                _LOG_.warning(('[ERROR_CREATE_POPSAFE_PRICE]:', str(e)))
                return
        else:
            try:
                BoxDao.delete_rows_popsafe_price({})
                insert_data_to_popsafe_price(message)
                return
            except Exception as e:
                print("[*] [ERROR] insert_data_popsafe_price : %r" % e )
                _LOG_.warning(('[ERROR] insert_data_popsafe_price :', str(e)))
                return

def insert_data_to_popsafe_price(message):
    try:
        list_popsafe_paid = message['data']
        for popsafe_paid in list_popsafe_paid:
            BoxDao.init_popsafe_price(popsafe_paid)
        return
    except Exception as e:
        _LOG_.warning(('[ERROR_INIT_POPSAFE_PRICE]:', str(e)))
        return
