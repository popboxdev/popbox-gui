from database import ClientDatabase
__author__ = 'popbox@popbox.asia'


def get_mouth(msg):
    sql = 'SELECT * FROM Mouth WHERE express_id = :id'
    return ClientDatabase.get_result_set(sql, msg)[0]


def init_box(msg):
    sql = 'INSERT INTO Box (id, deleteFlag, name, orderNo, operator_id,validateType,syncFlag,currencyUnit,overdueType,freeDays,freeHours,receiptNo) VALUES (:id,0,:name,:orderNo,:operator_id,:validateType,:syncFlag,:currencyUnit,:overdueType,:freeDays,:freeHours,:receiptNo)'
    ClientDatabase.insert_or_update_database(sql, msg)


def update_box(msg):
    sql = 'UPDATE Box SET name=:name, orderNo=:orderNo,validateType=:validateType,currencyUnit=:currencyUnit,overdueType=:overdueType,freeDays=:freeDays,freeHours=:freeHours WHERE id=:id'
    ClientDatabase.insert_or_update_database(sql, msg)

def init_cabinet(cabinet):
    sql = 'INSERT INTO Cabinet(id, deleteFlag, number)VALUES (:id,:deleteFlag,:number)'
    ClientDatabase.insert_or_update_database(sql, cabinet)


def init_mouth(mouth):
    sql = 'INSERT INTO Mouth(id, deleteFlag, number,usePrice ,overduePrice, status, is_insulator, box_id, cabinet_id, express_id, mouthType_id,numberInCabinet,syncFlag,openOrder)VALUES (:id,:deleteFlag,:number,:usePrice,:overduePrice,:status, :is_insulator,:box_id,:cabinet_id,:express_id,:mouthType_id,:numberInCabinet,:syncFlag,:openOrder)'
    ClientDatabase.insert_or_update_database(sql, mouth)


def init_mouth_type(mouth_type):
    sql = 'SELECT * FROM MouthType WHERE id=:id'
    result_set = ClientDatabase.get_result_set(sql, mouth_type)
    if len(result_set) != 0:
        return
    sql = 'INSERT INTO MouthType (id, defaultUsePrice,defaultOverduePrice, name,deleteFlag)VALUES (:id,:defaultUsePrice,:defaultOverduePrice,:name,:deleteFlag)'
    ClientDatabase.insert_or_update_database(sql, mouth_type)


def free_mouth(mouth):
    sql = "UPDATE mouth SET express_id = NULL , status = 'ENABLE' WHERE id = :id"
    ClientDatabase.insert_or_update_database(sql, mouth)


def get_mouth_type(mouth_type_param):
    sql = 'SELECT * FROM MouthType WHERE name = :name AND deleteFlag = 0'
    return ClientDatabase.get_result_set(sql, mouth_type_param)


def get_free_mouth_by_type(mouth_param):
    sql = 'SELECT * FROM Mouth WHERE mouthType_id=:mouthType_id AND deleteFlag = :deleteFlag AND status = :status AND is_insulator = :is_insulator'
    return ClientDatabase.get_result_set(sql, mouth_param)


def get_box_by_order_no(order_no):
    sql = 'SELECT * FROM Box WHERE orderNo = :orderNo AND deleteFlag = :deleteFlag'
    return ClientDatabase.get_result_set(sql, order_no)


def get_box_by_box_id(box_id):
    sql = 'SELECT * FROM Box WHERE id = :id AND deleteFlag = :deleteFlag'
    return ClientDatabase.get_result_set(sql, box_id)


def use_mouth(mouth_param__):
    sql = 'UPDATE mouth SET express_id=:express_id ,Status=:status WHERE id = :id'
    ClientDatabase.insert_or_update_database(sql, mouth_param__)


def get_mouth_by_id(mouth_param):
    sql = 'SELECT Mouth.*, MouthType.name FROM Mouth INNER JOIN MouthType on Mouth.mouthType_id = MouthType.id WHERE Mouth.id = :id'
    return ClientDatabase.get_result_set(sql, mouth_param)


def get_cabinet_by_id(param):
    sql = 'SELECT * FROM Cabinet WHERE id=:id'
    return ClientDatabase.get_result_set(sql, param)


def get_free_mouth_count_by_mouth_type_name(param):
    sql = 'SELECT count(1) AS count FROM Mouth  INNER JOIN MouthType ON Mouth.mouthType_id = MouthType.id AND Mouth.status = :status AND MouthType.name = :name'
    return ClientDatabase.get_result_set(sql, param)


def update_free_time(param):
    sql = 'UPDATE Box SET freeHours=:freeHours,freeDays=:freeDays,overdueType=:overdueType WHERE id=:id'
    ClientDatabase.insert_or_update_database(sql, param)


def update_mouth_status(param):
    sql = 'UPDATE mouth SET status=:status WHERE id=:id '
    ClientDatabase.insert_or_update_database(sql, param)


def get_mouth_list(param):
    sql = 'SELECT mouth.id, mouth.deleteFlag, Mouth.number, Mouth.syncFlag, Mouth.status, MouthType.name FROM Mouth INNER JOIN MouthType ON Mouth.mouthType_id = MouthType.id AND Mouth.deleteFlag = :deleteFlag ORDER BY Mouth.number LIMIT :startLine,25 '
    return ClientDatabase.get_result_set(sql, param)


def get_all_mouth(param):
    sql = 'SELECT * FROM Mouth WHERE deleteFlag=:deleteFlag ORDER BY Mouth.number'
    return ClientDatabase.get_result_set(sql, param)


def get_empty_mouth(param):
    sql = 'SELECT * FROM Mouth WHERE deleteFlag=:deleteFlag and status=:status ORDER BY Mouth.number'
    return ClientDatabase.get_result_set(sql, param)


def get_all_mouth_count(param):
    sql = 'SELECT count(1) AS count FROM Mouth WHERE deleteFlag = :deleteFlag'
    return ClientDatabase.get_result_set(sql, param)


def manage_set_mouth(param):
    sql = 'UPDATE mouth SET status=:status, syncFlag=:syncFlag WHERE id=:id '
    return ClientDatabase.insert_or_update_database(sql, param)


def mark_sync_success(param):
    sql = 'UPDATE mouth SET syncFlag = 1 WHERE id = :id'
    ClientDatabase.insert_or_update_database(sql, param)


def mark_box_sync_success(param):
    sql = 'UPDATE Box SET syncFlag = 1'
    ClientDatabase.insert_or_update_database(sql, param)


def get_all_mouth_type(param):
    sql = 'SELECT * FROM MouthType WHERE deleteFlag = :deleteFlag'
    return ClientDatabase.get_result_set(sql, param)


def get_count_by_status_and_mouth_type_id(param):
    sql = 'SELECT count(1) AS mouth_count FROM Mouth WHERE status=:status AND is_insulator=:is_insulator AND mouthType_id=:mouthType_id AND deleteFlag=:deleteFlag'
    return ClientDatabase.get_result_set(sql, param)

def get_count_by_status_and_mouth_type_id_all_insulator_noninsulator(param):
    sql = 'SELECT count(1) AS mouth_count FROM Mouth WHERE status=:status AND is_insulator IN (0,1) AND mouthType_id=:mouthType_id AND deleteFlag=:deleteFlag'
    return ClientDatabase.get_result_set(sql, param)


def get_not_sync_mouth_list(param):
    sql = 'select * from Mouth WHERE deleteFlag=0 and syncFlag=:syncFlag'
    return ClientDatabase.get_result_set(sql, param)


def get_free_mouth_by_id(param):
    sql = 'SELECT * FROM Mouth WHERE deleteFlag=0 AND id=:id AND status =:status'
    return ClientDatabase.get_result_set(sql, param)

def get_detail_mouth_by_id(param):
    sql = 'SELECT a.number, b.name FROM Mouth a, MouthType b WHERE a.mouthType_id=b.id AND a.id=:id'
    return ClientDatabase.get_result_set(sql, param)

def get_rules(param):
    sql = 'SELECT regularContent, groupName FROM ReturnRules WHERE deleteFlag=:deleteFlag'
    return ClientDatabase.get_result_set(sql, param)

def get_mouth_type_name(mouth_type_param):
    sql = 'SELECT * FROM MouthType WHERE id = :id AND deleteFlag = 0'
    return ClientDatabase.get_result_set(sql, mouth_type_param)[0]

def update_price_for_poptitip(price_param):
    sql = 'UPDATE MouthType SET defaultOverduePrice=:defaultOverduePrice WHERE id=:id'
    return ClientDatabase.insert_or_update_database(sql, price_param)

def get_list_size(param):
    sql = 'SELECT * FROM MouthType WHERE deleteFlag=:deleteFlag'
    return ClientDatabase.get_result_set(sql, param)

def get_box_info():
    sql = 'SELECT * FROM Box'
    return ClientDatabase.get_result_void(sql)

def get_box_counter(param):
    sql = 'SELECT * FROM Box'
    return ClientDatabase.get_result_telegram(sql, param)

def check_table_info(param):
    sql = 'SELECT name FROM sqlite_master WHERE type =:table AND name=:table_name'
    return ClientDatabase.check_table(sql, param)

def update_box_to_used(param):
    sql = 'UPDATE Mouth SET status=:status, express_id=:express_id, syncFlag=:syncFlag WHERE id=:id'
    return ClientDatabase.insert_or_update_database(sql, param)

def init_insulator_pricing(msg):
    sql = 'INSERT INTO InsulatorPricing(locker_id, locker_name, order_no, overdue_day, overdue_hour, logisticsCompany_id, company_name, S, M, L, XL , is_active, deleteFlag, abort_charging) VALUES (:locker_id, :locker_name, :order_no, :overdue_day, :overdue_hour, :logisticsCompany_id, :company_name, :S, :M, :L, :XL , :is_active, :deleteFlag, :abort_charging)'
    ClientDatabase.insert_or_update_database(sql, msg)

def delete_rows_insulator_pricing(msg):
    sql = 'DELETE FROM InsulatorPricing WHERE deleteFlag = 0 OR deleteFlag = 1'
    ClientDatabase.delete_record(sql)

def insulator_overdue_by_logistics_company_id(param):
    sql = 'SELECT * FROM InsulatorPricing WHERE logisticsCompany_id=:logisticsCompany_id AND is_active=:is_active'
    return ClientDatabase.get_result_set(sql, param)

def get_size_name_by_express_id(param):
    sql = 'SELECT MouthType.name as locker_size FROM Express LEFT JOIN Mouth ON Express.mouth_id = Mouth.id LEFT JOIN MouthType ON Mouth.mouthType_id = MouthType.id WHERE Express.id=:id'
    return ClientDatabase.get_result_set(sql, param)

def get_pricing_by_size( param ):
    sql = 'SELECT * From InsulatorPricing WHERE logisticsCompany_id=:logisticsCompany_id AND is_active=:is_active'
    return ClientDatabase.get_result_set(sql, param)

def update_box_to_available(msg):
    sql = 'UPDATE Mouth SET status=:status, express_id = NULL, syncFlag=:syncFlag WHERE id=:id'
    return ClientDatabase.insert_or_update_database(sql, msg)

def check_logistics_overdue_paid(msg):
    sql = 'SELECT * FROM PaidOverdue WHERE is_active=:is_active AND logisticsCompany_id=:logisticsCompany_id AND deleteFlag=:deleteFlag'
    return ClientDatabase.get_result_set(sql, msg)

def init_paid_overdue(msg):
    sql = 'INSERT INTO PaidOverdue(logisticsCompany_id, company_name, costs_size_mini, costs_size_small, costs_size_medium, costs_size_large, costs_size_extra_large,costs_overdue_day, costs_overdue_hour, cost_per_day, flat_costs_instore, flat_costs_overdue, tier_percentage, payment_type, calculate_price_by, free_days, max_day_costs, abort_charging, is_active, deleteFlag) VALUES (:logisticsCompany_id, :company_name, :costs_size_mini, :costs_size_small, :costs_size_medium, :costs_size_large, :costs_size_extra_large, :costs_overdue_day, :costs_overdue_hour, :cost_per_day, :flat_costs_instore, :flat_costs_overdue, :tier_percentage, :payment_type, :calculate_price_by, :free_days, :max_day_costs, :abort_charging, :is_active, :deleteFlag)'
    ClientDatabase.insert_or_update_database(sql, msg)

def delete_rows_paid_overdue(msg):
    sql = 'DELETE FROM PaidOverdue WHERE deleteFlag = 0 OR deleteFlag = 1'
    ClientDatabase.delete_record(sql)
    
def get_data_by_express_id_mouth_id(param):
    sql = 'SELECT * FROM Mouth WHERE id=:mouth_id AND express_id=:express_id'
    return ClientDatabase.get_result_set(sql, param)

def check_mouth_for_open_again(param):
    sql = 'SELECT * FROM Mouth WHERE id=:mouth_id AND status=:status'
    return ClientDatabase.get_result_set(sql, param)

def check_otp(param):
    sql = 'SELECT * FROM UserOtp WHERE phone_number=:phone_number AND deleteFlag = 0'    
    return ClientDatabase.get_result_set(sql, param)

def insert_otp(param):
    sql = 'INSERT INTO UserOtp (phone_number, valid_date, used_times) VALUES (:phone_number, :valid_date, 1)'
    return ClientDatabase.insert_or_update_database(sql, param)

def update_otp(param):
    sql = 'UPDATE UserOtp SET valid_date=:valid_date, used_times = used_times + 1 WHERE phone_number=:phone_number'
    return ClientDatabase.insert_or_update_database(sql, param)

def get_alert_locker_status(param):
    sql = 'SELECT * FROM Alert WHERE alertType=:alertType'    
    return ClientDatabase.get_result_set(sql, param)

def insert_alert_locker_status(param):
    sql = 'INSERT INTO Alert (id, alertType, box_id, operator_id, createTime, alertStatus, syncFlag, value_id, alertvalue) VALUES (:id, :alertType, :box_id, :operator_id, :createTime, :alertStatus, :syncFlag, :value_id, :alertvalue)'
    return ClientDatabase.insert_or_update_database(sql, param)

def update_alert_locker_status(param):
    sql = 'UPDATE Alert SET createTime=:createTime, alertStatus=:alertStatus, alertvalue=:alertvalue WHERE id=:id'
    return ClientDatabase.insert_or_update_database(sql, param)

def get_mouth_by_id_status(mouth_param):
    sql = 'SELECT Mouth.*, MouthType.name FROM Mouth INNER JOIN MouthType on Mouth.mouthType_id = MouthType.id WHERE Mouth.id = :id AND Mouth.status =: status'
    return ClientDatabase.get_result_set(sql, mouth_param)

def init_popsafe_price(msg):
    sql = 'INSERT INTO PopsafePricing(popsafe_pricing_id, pricing_name, size_xs, size_s, size_m, size_l, size_xl,type, duration) VALUES (:popsafe_pricing_id, :pricing_name, :size_xs, :size_s, :size_m, :size_l, :size_xl, :type, :duration)'
    ClientDatabase.insert_or_update_database(sql, msg)

def get_pricing_popsafe(msg):
    sql = 'SELECT * FROM PopsafePricing WHERE is_active = :is_active ORDER BY type'
    return ClientDatabase.get_result_set(sql, msg)

def get_pricing_popsafe_by_type(msg):
    sql = 'SELECT * FROM PopsafePricing WHERE is_active = :is_active AND type = :type'
    return ClientDatabase.get_result_set(sql, msg)

def delete_rows_popsafe_price(msg):
    sql = 'DELETE FROM PopsafePricing'
    ClientDatabase.delete_record(sql)