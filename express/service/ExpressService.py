import re #Library for Regex Pattern (exp.Lazada)
import json
import logging
import math
import random
import datetime
import time
from PyQt5.QtCore import QObject, pyqtSignal
import ClientTools
import Configurator
import box.repository.BoxDao as BoxDao
import company.service.CompanyService as CompanyService
import company.repository.CompanyDao as CompanyDao
import express.repository.ExpressDao as ExpressDao
import return_rules.repository.ReturnRulesDao as ReturnRulesDao
# import express.popbox.PopboxService as PopboxService
import box.service.BoxService
import user.service.UserService as UserService
from network import HttpClient
from device import Scanner
from device import Camera
from device import QP3000S
import sys, os
import platform
bit_platform =  platform.architecture()[0]
os_platform = platform.system()
if(bit_platform=="64bit") and (os_platform != 'Linux'):
    from device import NFC
    if Configurator.get_value('ClientInfo','language')=='PH':
        from device import LocalClient

dummy_beep_trans = int(Configurator.get_value('popbox','dummy^transaction'))
if (bit_platform=="32bit"):
    if dummy_beep_trans ==1:
        if Configurator.get_value('ClientInfo','language')=='PH':
            from device import LocalClient


__author__ = 'wahyudi@popbox.asia'


class ExpressSignalHandler(QObject):
    customer_take_express_signal = pyqtSignal(str)
    overdue_cost_signal = pyqtSignal(str)
    barcode_signal = pyqtSignal(str)
    store_express_signal = pyqtSignal(str)
    phone_number_signal = pyqtSignal(str)
    paid_amount_signal = pyqtSignal(str)
    customer_store_express_signal = pyqtSignal(str)
    overdue_express_list_signal = pyqtSignal(str)
    popsafe_express_list_signal = pyqtSignal(str)
    overdue_express_count_signal = pyqtSignal(int)
    return_express_count_signal = pyqtSignal(int)
    popsafe_express_count_signal = pyqtSignal(int)
    popsend_express_count_signal = pyqtSignal(int)
    staff_take_overdue_express_signal = pyqtSignal(str)
    load_express_list_signal = pyqtSignal(str)
    customer_store_express_cost_signal = pyqtSignal(str)
    store_customer_express_result_signal = pyqtSignal(str)
    customer_express_cost_insert_coin_signal = pyqtSignal(str)
    send_express_list_signal = pyqtSignal(str)
    send_express_count_signal = pyqtSignal(int)
    staff_take_send_express_signal = pyqtSignal(str)
    imported_express_result_signal = pyqtSignal(str)
    customer_reject_express_signal = pyqtSignal(str)
    reject_express_signal = pyqtSignal(str)
    reject_express_list_signal = pyqtSignal(str)
    product_file_signal = pyqtSignal(str)
    start_get_express_info_result_signal = pyqtSignal(str)
    start_get_pakpobox_express_info_signal = pyqtSignal(str)
    start_get_customer_info_by_card_signal = pyqtSignal(int)
    start_payment_by_card_signal = pyqtSignal(str)
    start_get_cod_status_signal = pyqtSignal(str)
    start_update_mouth_id_express_signal = pyqtSignal(str)
    set_value_change_size_signal = pyqtSignal(str)
    start_update_insulator_payment_signal = pyqtSignal(str)
    store_booking_express_signal = pyqtSignal(str)

    #ANDALAS V2
    # load_express_all_signal = pyqtSignal(str)
    all_express_list_signal = pyqtSignal(str)
    cancel_order_signal = pyqtSignal(str)
    update_paid_parcels_signal = pyqtSignal(str)
    store_booking_express_signal = pyqtSignal(str)

    #nfcpayment CHECK
    nfc_card_signal=pyqtSignal(str)
    nfc_valid_signal=pyqtSignal(str)
    nfc_balance_signal = pyqtSignal(str)

    #grpc update
    reader_init_signal=pyqtSignal(str)
    grpc_getbalance_signal=pyqtSignal(str)
    grpc_debit_signal=pyqtSignal(str)

_LOG_ = logging.getLogger()
_EXPRESS_ = ExpressSignalHandler()
barcode_run_flag = False
barcode_start = False
overdue_cost = 0
identification_number = ''
is_passport = 0

# SET OVERDUE
overdueTime = float(Configurator.get_value('OverdueTime', 'hours'))

if 'pr0x' not in Configurator.get_value('ClientInfo', 'serveraddress'):
    still_ebox = True
else:
    still_ebox = False


def init_express(param, mouth, box_param):
    express = {'id': param['id'],
               'expressType': param['expressType'],
               'status': param['status'],
               'storeTime': param['storeTime'],
               'syncFlag': 1,
               'version': param['version'],
               'box_id': mouth['box_id'],
               'logisticsCompany_id': ClientTools.get_value('id', ClientTools.get_value('logisticsCompany', param)),
               'operator_id': box_param['operator_id'],
               'mouth_id': mouth['id'],
               'storeUser_id': ClientTools.get_value('id', ClientTools.get_value('storeUser', param)),
               'groupName': load_param_or_default('groupName', param),
               'expressNumber': load_param_or_default('expressNumber', param),
               'customerStoreNumber': load_param_or_default('customerStoreNumber', param),
               'overdueTime': load_param_or_default('overdueTime', param),
               'takeUserPhoneNumber': load_param_or_default('takeUserPhoneNumber', param),
               'storeUserPhoneNumber': ClientTools.get_value('phoneNumber', ClientTools.get_value('storeUser', param)),
               'validateCode': load_param_or_default('validateCode', param),
               'designationSize': load_param_or_default('designationSize', param),
               'chargeType': load_param_or_default('chargeType', param),
               'continuedHeavy': load_param_or_default('continuedHeavy', param),
               'endAddress': load_param_or_default('endAddress', param),
               'continuedPrice': load_param_or_default('continuedPrice', param),
               'startAddress': load_param_or_default('startAddress', param),
               'firstHeavy': load_param_or_default('firstHeavy', param),
               'firstPrice': load_param_or_default('firstPrice', param),
               'payOfAmount': load_param_or_default('payOfAmount', param),
               'electronicCommerce_id': ClientTools.get_value('id', ClientTools.get_value('electronicCommerce', param)),
               'takeUser_id': ClientTools.get_value('id', ClientTools.get_value('takeUser', param)),
               'is_insulator': load_param_or_default('is_insulator', param)}
    ExpressDao.init_express(express)
    if express['electronicCommerce_id'] is not None:
        company_list = CompanyDao.get_company_by_id({'id': express['electronicCommerce_id']})
        electronic_commerce = {'id': express['electronicCommerce_id'],
                               'companyType': 'ELECTRONIC_COMMERCE',
                               'name': param['electronicCommerce']['name'],
                               'deleteFlag': 0,
                               'parentCompany_id': ClientTools.get_value('parentCompany_id',
                                                                         param['electronicCommerce'], None)}
        if len(company_list) == 0:
            CompanyDao.insert_company(electronic_commerce)
        else:
            CompanyDao.update_company(electronic_commerce)


def load_param_or_default(key, dict_, default_value=None):
    if key in dict_.keys():
        return dict_[key]
    return default_value


def start_barcode_take_express():
    global barcode_start
    if barcode_start:
        return
    barcode_start = True
    Scanner._SCANNER_.barcode_result.connect(get_express_barcode_text)
    Scanner.start_get_text_info()


def stop_barcode_take_express():
    global barcode_start
    if not barcode_start:
        return
    barcode_start = False
    Scanner._SCANNER_.barcode_result.disconnect(get_express_barcode_text)
    Scanner.start_stop_scanner()


def start_customer_take_express(validate_code):
    ClientTools.get_global_pool().apply_async(customer_take_express, (validate_code,))


AP_EXPRESS = None


def customer_take_express(data_take):
    global AP_EXPRESS
    # _LOG_.info("VALIDATEEE CODE : " + str(len(validate_code)))
    res = {}
    try:
        checkIsExtend = False
        dataExtend = ""
        checkDataExist = False

        tempValidateCode = data_take.split('||')
        checkIsJson = is_json(tempValidateCode[0])
        _LOG_.info(('check_is_json', checkIsJson, tempValidateCode[0]))
        if checkIsJson==True:
            getValidateCode = json.loads(tempValidateCode[0])
            _LOG_.info(('check_is_json', checkIsJson))
            validate_code = getValidateCode['extendPincode']
            dataExtend = tempValidateCode[0]
            checkIsExtend = True if getValidateCode["isExtend"] == "true" else False
        else:
            validate_code_tmp = data_take.split('||')
            validate_code = validate_code_tmp[0]

        param = {'validateCode': validate_code,
                 'status': 'IN_STORE'}
        
        if len(validate_code) == 2:
            try:
                check_express = ExpressDao.get_record_appexress(param)
                _LOG_.info(('[check_express]', check_express))
                if len(check_express) != 0:
                    AP_EXPRESS = check_express[0]
            except Exception as e:
                _LOG_.warning(('customer_take_express for AP_EXPRESS', e))
                _EXPRESS_.customer_take_express_signal.emit('Error')
        else:
            _LOG_.debug(('[DEBUG] not AP_EXPRESS parcel : ', str(validate_code)))

        expresses = ExpressDao.get_express_by_validate(param)
        if not expresses:
            res['isSuccess'] = "false"
            res['message'] = "Express not found"
            _EXPRESS_.customer_take_express_signal.emit(str(json.dumps(res)))
            
        else:
            # if expresses[0]['groupName'] == 'POPTITIP':
            #     AP_EXPRESS = expresses[0]
            _LOG_.info(('[expresses_customer_take]', expresses[0], "[DATA_EXTEND]", dataExtend))

            check_tb_exist = BoxDao.check_table_info({'table': 'table', 'table_name': 'PaidOverdue'})
            data_overdue_paid = []
            flagLastmilePaid = 0
            if check_tb_exist:
                param = {
                    'is_active': 1,
                    'logisticsCompany_id': expresses[0]['logisticsCompany_id'],
                    'deleteFlag': 0
                }
                check_logistics_paid = BoxDao.check_logistics_overdue_paid(param)
                if check_logistics_paid:
                    flagLastmilePaid = 1

            print("extend_status : ", checkIsExtend)
            print("flagLastmilePaid : ", flagLastmilePaid)
            if checkIsExtend is True and flagLastmilePaid == 0:
                pre_take_express(expresses, dataExtend)
            else:
                if dataExtend != "":
                    pre_take_express(expresses, dataExtend, flagLastmilePaid)
                else:
                    pre_take_express(expresses)

    except Exception as e:
        res['isSuccess'] = "false"
        res['message'] = str(e)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] customer_take_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.customer_take_express_signal.emit(str(json.dumps(res)))


def get_express_barcode_text(text_info):
    global _EXPRESS_
    global barcode_start
    if not barcode_start:
        return
    barcode_start = False
    if text_info == 'ERROR':
        return
    if text_info == '':
        _EXPRESS_.customer_take_express_signal.emit('NotInput')
        return
    if text_info.find('|') == -1:
        _EXPRESS_.customer_take_express_signal.emit('Error')
        return
    express_info = text_info.split('|')
    param = {'id': express_info[0].strip(),
             'validateCode': express_info[1].strip(),
             'status': 'IN_STORE'}
    expresses = ExpressDao.get_express_by_id_and_validate(param)
    pre_take_express(expresses)


def pre_take_express(expresses, dataExtend="", flagLastmilePaid=0):
    if len(expresses) < 1:
        _EXPRESS_.customer_take_express_signal.emit('Error')
        return
    express = expresses[0]
    if flagLastmilePaid == 1:
        take_express(express, False, dataExtend)
    else:
        if express['overdueTime'] > ClientTools.now():
            take_express(express, False, dataExtend)
        else:
            pre_take_overdue_express(express)


def take_express(param, overdue=False, dataExtend=""):
    global recipeint_name
    global is_passport
    global identification_number
    multipleOpendDoor = False
    res = {}
    recipient = ''
    if recipeint_name != '':
        recipient = recipeint_name
    
    express = {'takeTime': ClientTools.now(),
               'status': 'CUSTOMER_TAKEN',
               'syncFlag': 0,
               'version': param['version'] + 1,
               'id': param['id'],
               'staffTakenUser_id': None,
               'recipientName': recipient}

    mouth_param = box.service.BoxService.get_mouth(express)
    # flagDoor = box.service.BoxService.open_mouth(mouth_param['id']) // disable for a moments

    flagDoor = box.service.BoxService.open_mouth_by_id_express_id(mouth_param['id'], param['id'])
    flagDoorIs = Configurator.get_value('panel', 'gui')
    flagDoorIs = "disabled" # disabled for a moment till issue fixed

    if flagDoorIs == "public":
        if flagDoor is False:
            res['isSuccess'] = "false"
            res['doorFlag'] = "false"
            _EXPRESS_.customer_take_express_signal.emit(str(json.dumps(res)))
            return
        else:
            box.service.BoxService.free_mouth(mouth_param)
    else:
        box.service.BoxService.free_mouth(mouth_param)
        
    try:
        ExpressDao.take_express(express)
    except Exception as e:
        res['isSuccess'] = "false"
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] take_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.customer_take_express_signal.emit(str(json.dumps(res)))
    
    try:
        express['expressNumber'] = param['expressNumber']
        express['takeUserPhoneNumber'] = param['takeUserPhoneNumber']
        express['overdueTime'] = param['overdueTime']
        express['storeTime'] = param['storeTime']
        express['validateCode'] = param['validateCode']
        express['box_id'] = param['box_id']
        express['logisticsCompany_id'] = param['logisticsCompany_id']
        express['mouth_id'] = param['mouth_id']
        express['operator_id'] = param['operator_id']
        express['storeUser_id'] = param['storeUser_id']
        # express['dataInsulator'] = param['dataInsulator']
        data_ins = {"data": param}	
        if "dataInsulator" in data_ins['data']:	
            express['dataInsulator'] = param['dataInsulator']	
        else:	
            express['dataInsulator'] = ""
        express['paymentParam'] = param['paymentParam']
        express['groupName'] = param['groupName']


        if recipeint_name != '':
            express['recipientName'] = recipient

        if dataExtend != "":
            
            data_extend = json.loads(dataExtend)
            data_trans = {"data": param}
            if "transactionRecord" in data_trans['data']:
                express['transactionRecord'] = param['transactionRecord']
            else:
                express['transactionRecord'] = ""
            # express['transactionRecord'] = express['transactionRecord']
            express['extendTimes'] = data_extend['extendTimes']
            express['paymentMethod'] = data_extend['paymentMethod']
            express['overdueTime'] = param['overdueTime']
            express['isExtend'] = data_extend['isExtend']
            express['duration'] = data_extend['duration']
            express['minutes_duration'] = data_extend['minutes_duration']
            express['cost_overdue'] = data_extend['cost_overdue']
            express['timestamp_duration'] = param['storeTime'] if overdue == False else data_extend['timestamp_duration']

            _LOG_.warning(("[DATA_EXTEND_SAVE]", str(dataExtend)))
        
        express['is_passport'] = is_passport
        express['identification_number'] = identification_number
        express['receive_name'] = recipient
        # transaction_record = get_scan_sync_express_transaction_record({'express_id': express['id']})
        # if len(transaction_record) != 0:
        #     express['transactionRecords'] = transaction_record
        #     if len(transaction_record) > 1:
        #         multipleOpendDoor = True
        # express['multipleOpenDoor'] = multipleOpendDoor
        

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] PREPARE_DATA_EXTEND_TO_SEND : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

    try:
        if AP_EXPRESS is None:
            express_data = json.dumps(express)
            res['isSuccess'] = "true"
            res['is_overdue'] = "false"
            res['data'] = express_data
            _EXPRESS_.customer_take_express_signal.emit(str(json.dumps(res)))
        else:
            express_data = json.dumps(AP_EXPRESS)
            res['isSuccess'] = "true"
            res['data'] = express_data
            _EXPRESS_.customer_take_express_signal.emit(str(json.dumps(res)))
        if overdue:
            record_list = ExpressDao.get_record({'express_id': express['id']})
            if len(record_list) == 1:
                record = record_list[0]
                record_param = {'id': record['id'],
                                'createTime': record['createTime'],
                                'amount': record['amount']}
                express['transactionRecords'] = [record_param]

        dataExtend = json.dumps(express)
        paramExtend = {
            'dataExtend' : dataExtend,
            'validateCode' : express['validateCode']
        }
        transaction_record = get_scan_sync_express_transaction_record({'express_id': express['id']})
        if len(transaction_record) != 0:
            express['transactionRecords'] = transaction_record
            if len(transaction_record) > 1:
                multipleOpendDoor = True
        express['multipleOpenDoor'] = multipleOpendDoor
        
        ExpressDao.update_data_extend(paramExtend)
        _LOG_.warning(("[take_express] SEND DATA TO PROX : ", str(express)))
        result, status_code = HttpClient.post_message('express/customerTakeExpress', express)
        if status_code == 200 and result['id'] == express['id']:
            ExpressDao.mark_sync_success(express)
            recipeint_name = ''
            identification_number = ''
            is_passport = 0

            dataExtend = json.dumps(express)
            paramExtend = {
                'dataExtend' : dataExtend,
                'validateCode' : express['validateCode']
            }
            ExpressDao.update_data_extend(paramExtend)
            result, status_code = HttpClient.post_message('express/customerTakeExpress', express)
            if status_code == 200 and result['id'] == express['id']:
                ExpressDao.mark_sync_success(express)
                recipeint_name = ''
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] TAKE EXPRESS : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

overdue_express = None


def pre_take_overdue_express(express_param):
    global overdue_express
    global paid_amount
    global overdue_cost
    global AP_EXPRESS
    overdue_express = express_param
    res = {}
    try:
        _LOG_.debug(("express_param_pretake_overdue : ", express_param))
        # mouth_param = box.service.BoxService.get_mouth(express_param)
        mouth = BoxDao.get_mouth_by_id({'id':overdue_express['mouth_id']})
        mouthTypeId = mouth[0]['mouthType_id']
        mouth_param = BoxDao.get_mouth_type_name({'id': mouthTypeId})
        box_result = box.service.BoxService.get_box()

        price_size = ''
        price_duration = 0
        db_overdue = 0
        size_box = mouth_param['name']
        if(overdue_express['groupName']=="POPTITIP"):
            if size_box == 'L':
                price_size = 'size_l'
            elif size_box == 'M':
                price_size = 'size_m'
            elif size_box == 'S':
                price_size = 'size_s'
            elif size_box == 'MINI':
                price_size = 'size_xs'
            elif size_box == 'XL':
                price_size = 'size_xl'
            param_get = {
                'is_active': 1,
                'type': overdue_express['chargeType']
            }
            popsafe_pricing = BoxDao.get_pricing_popsafe_by_type(param_get)
            if len(popsafe_pricing) < 1:
                price_duration = 0
                db_overdue = 0
            else:
                if((popsafe_pricing[0][price_size])<=0):
                    db_overdue = 0
                    price_duration = 0
                else:
                    db_overdue = popsafe_pricing[0][price_size]
                    price_duration = int(popsafe_pricing[0]['duration'])

        time_span = ClientTools.now() - express_param['overdueTime']
        # day_span = math.ceil(time_span / 1000.0 / 60.0 / 60.0 / overdueTime)
        day_span = math.ceil(time_span / 1000.0 / 60.0 / 60.0 / overdueTime)
        if(price_duration != 0):
            day_span = math.ceil(day_span / price_duration)
        else :
            day_span = math.ceil(day_span / 24)
        # overdue_cost = day_span * mouth_param['overduePrice']
        # db_overdue = mouth_param['defaultOverduePrice']
        if (db_overdue == 0):
            js = open(sys.path[0]+"/qml/door_price.js", "r").readlines()[3]
            jslist = js.split()
            jslistval = jslist[3]
            jstr = jslistval.strip('""')
            db_overdue = int (jstr)
        
        pricing = 100
        is_insulator = 0
        if "is_insulator" in express_param: 
            if express_param['is_insulator']==1 or express_param['is_insulator']=='1':
                is_insulator = 1
                param = {'id': express_param['id']}
                get_size = BoxDao.get_size_name_by_express_id(param)
                if len(get_size) < 1:
                    pricing = 1000
                else:
                    size = get_size[0]['locker_size']
                    param_price = {
                        'column': size,
                        'logisticsCompany_id': express_param['logisticsCompany_id'],
                        'is_active': 1
                    }
                    get_pricing = BoxDao.get_pricing_by_size(param_price)
                    if len(get_pricing) < 1:
                        param_price = {
                            'column': size,
                            'logisticsCompany_id': '145b2728140f11e5bdbd0242ac110001',
                            'is_active': 1
                        }
                        get_pricing = BoxDao.get_pricing_by_size(param_price)
                        if len(get_pricing) > 0:
                            if(get_pricing[0]['abort_charging']=='1'):
                                pricing = 0
                            else:
                                pricing = get_pricing[0][size]
                        else:
                            pricing = 0
                        # print('pricing : ' + str(get_pricing[0]) + ' param : ' + str(param_price))
                    else:
                        pricing = get_pricing[0][size]
                    # size = get_size[0]['locker_size']
                    # param_price = {
                    #     'column': size,
                    #     'logisticsCompany_id': express_param['logisticsCompany_id'],
                    #     'is_active': 1,
                    #     'abort_charging': '0'
                    # }
                    # get_pricing = BoxDao.get_pricing_by_size(param_price)
                    # if len(get_pricing) < 1:
                    #     param_price = {
                    #         'column': size,
                    #         'logisticsCompany_id': '145b2728140f11e5bdbd0242ac110001',
                    #         'is_active': 1,
                    #         'abort_charging': '0'
                    #     }
                    #     get_pricing = BoxDao.get_pricing_by_size(param_price)
                    #     pricing = get_pricing[0][size]
                    #     # print('pricing : ' + str(get_pricing[0]) + ' param : ' + str(param_price))
                    # else:
                    #     pricing = get_pricing[0][size]
            else:
                is_insulator = 0
        else:
            is_insulator = 0

        if is_insulator == 1:
            # print("FROM PYTHON : "+ str(is_insulator))
            overdue_cost = day_span * int(pricing)
        else:
            # print("FROM PYTHON : "+ str(is_insulator))
            overdue_cost = day_span * db_overdue
        time_now = datetime.datetime.now().timestamp()
        times = int(express_param['overdueTime']) / 1000
        slice_time = slice(10)
        epoch_timeduration = time_now
        time_difference = math.trunc((time_now - int(times)) / 3600)
        minutes_difference = round((time_now - times) / 60) - 60 * time_difference
        day_span_temp = (time_span / 1000.0 / 60.0 / 60.0 / overdueTime)
        flagLastmilePaid = 0

        check_tb_exist = BoxDao.check_table_info({'table': 'table', 'table_name': 'PaidOverdue'})
        data_overdue_paid = []
        if check_tb_exist:
            param = {
                'is_active': 1,
                'logisticsCompany_id': express_param['logisticsCompany_id'],
                'deleteFlag': 0
            }
            check_logistics_paid = BoxDao.check_logistics_overdue_paid(param)
            if len(check_logistics_paid) > 0:
                data_overdue_paid = check_logistics_paid[0]
                if data_overdue_paid['abort_charging'] == 0 or data_overdue_paid['abort_charging'] == "0":
                    flagLastmilePaid = 1

            else:
                param = {
                    'is_active': 1,
                    'logisticsCompany_id': '161e5ed1140f11e5bdbd0242ac110001',
                    'deleteFlag': 0
                }
                check_logistics_paid = BoxDao.check_logistics_overdue_paid(param)
                if len(check_logistics_paid) > 0:
                    data_overdue_paid = check_logistics_paid[0]
                    if data_overdue_paid['abort_charging'] == 0 or data_overdue_paid['abort_charging'] == "0":
                        flagLastmilePaid = 1

        if AP_EXPRESS is None:
            _LOG_.info("APEXPRESS NONE : " + str(overdue_express))
            if overdue_express['expressNumber'][:3] != 'PSL' and flagLastmilePaid != 1 and overdue_express['expressNumber'][:3] != 'PFB':
                if ((overdue_express['expressNumber'][:3] != 'PDS') and (overdue_express['groupName'] != 'UNDEFINED')):
                    res['isSuccess'] = "true"
                    res['is_overdue'] = "true"
                    res['parcelType'] = "POPTITIP"
                    # _EXPRESS_.customer_take_express_signal.emit('Overdue')
                    _EXPRESS_.customer_take_express_signal.emit(str(json.dumps(res)))
                else:
                    res['isSuccess'] = "true"
                    res['is_overdue'] = "true"
                    res['parcelType'] = "POPSAFE"
                    _EXPRESS_.customer_take_express_signal.emit(str(json.dumps(res)))
            else:
                # Get Details Mouth
                used_mouth = BoxDao.get_detail_mouth_by_id({"id": overdue_express['mouth_id']})
                overdue_express['lockerNo'] = str(used_mouth[0]["number"])
                overdue_express['lockerSize'] = used_mouth[0]["name"]
                used_locker = BoxDao.get_box_by_box_id({"id": overdue_express['box_id'], "deleteFlag": 0})
                overdue_express['lockerName'] = used_locker[0]["name"]
            
                if flagLastmilePaid == 1:
                    time_now = datetime.datetime.now().timestamp()
                    times = int(express_param['storeTime']) / 1000
                    day_spand = math.trunc((time_now - int(times)) / 84000)
                    
                    freeDays = int( data_overdue_paid["free_days"] )
                    maxDays = int( data_overdue_paid["max_day_costs"] )

                    if data_overdue_paid['payment_type'] == "fixed":
                        max_price = ( data_overdue_paid['flat_costs_instore'] + data_overdue_paid['flat_costs_overdue'] ) * maxDays
                        overdue_express['cost_overdue'] = int( data_overdue_paid['flat_costs_instore'] ) + int( data_overdue_paid['flat_costs_overdue'] )

                    else:

                        if data_overdue_paid["calculate_price_by"] == "DAY":
                            max_price = data_overdue_paid['cost_per_day'] * maxDays
                            overdue_express['cost_overdue'] = data_overdue_paid['cost_per_day'] * ( day_spand - freeDays )
        
                        elif data_overdue_paid["calculate_price_by"] == "HOUR":
                            max_price = data_overdue_paid['costs_overdue_hour'] * maxDays
                            overdue_express['cost_overdue'] = data_overdue_paid['costs_overdue_hour'] * time_difference
                        elif data_overdue_paid["calculate_price_by"] == "SIZE":
                            if overdue_express['lockerSize'] == "MINI":
                                max_price = maxDays * data_overdue_paid['costs_size_mini']
                                overdue_express['cost_overdue'] = data_overdue_paid['costs_size_mini'] * ( day_spand - freeDays )

                            elif overdue_express['lockerSize'] == "S":
                                max_price = maxDays * data_overdue_paid['costs_size_small']
                                overdue_express['cost_overdue'] = data_overdue_paid['costs_size_small'] * ( day_spand - freeDays )
                            elif overdue_express['lockerSize'] == "M":
                                max_price = maxDays * data_overdue_paid['costs_size_medium']
                                overdue_express['cost_overdue'] = data_overdue_paid['costs_size_medium'] * ( day_spand - freeDays )

                            elif overdue_express['lockerSize'] == "L":
                                max_price = maxDays * data_overdue_paid['costs_size_large']
                                overdue_express['cost_overdue'] = data_overdue_paid['costs_size_large'] * ( day_spand - freeDays )

                            elif overdue_express['lockerSize'] == "XL":
                                max_price = maxDays * data_overdue_paid['costs_size_extra_large']
                                overdue_express['cost_overdue'] = data_overdue_paid['costs_size_extra_large'] * ( day_spand - freeDays )

                            else:
                                overdue_express['cost_overdue'] = data_overdue_paid['cost_per_day'] * ( day_spand - freeDays )
                        else:
                            overdue_express['cost_overdue'] = data_overdue_paid['cost_per_day'] * ( day_spand - freeDays )

                        
                    overdue_express['cost_overdue'] = overdue_express['cost_overdue'] if ( overdue_express['cost_overdue'] < max_price ) else max_price

                else:
                    overdue_express['cost_overdue'] = str(overdue_cost)

                overdue_express['extend_overdue'] = str(day_span)
                overdue_express['parcel_duration'] = ( time_difference )
                overdue_express['parcel_minutes_duration'] = ( minutes_difference )
                overdue_express['timestamp_duration'] = ( epoch_timeduration )
                overdue_express['date_overdue'] =  express_param['overdueTime']
                overdue_express['validateCode'] =  express_param['validateCode']
                overdue_data = json.dumps(overdue_express)
                _LOG_.debug(("overdue_express : ", overdue_express))
                res['isSuccess'] = "true"
                res['is_overdue'] = "true"
                res['parcelType'] = express_param["groupName"]
                res['data'] = overdue_data
                _EXPRESS_.customer_take_express_signal.emit(str(json.dumps(res)))
        else:
            overdue_data = json.dumps(AP_EXPRESS)
            AP_EXPRESS = None
            _EXPRESS_.customer_take_express_signal.emit('Overdue||'+overdue_data)
    except Exception as e:
        res['isSuccess'] = "false"
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] customer_take_express_signal : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.customer_take_express_signal.emit(str(json.dumps(res)))



paid_amount = 0
coin_machine_connect_flag = False


def take_overdue_express():
    pass


def get_overdue_cost():
    _EXPRESS_.overdue_cost_signal.emit(str(math.ceil(overdue_cost / 100)))


scanner_signal_connect_flag = False


def start_get_express_number_by_barcode():
    global scanner_signal_connect_flag
    if not scanner_signal_connect_flag:
        Scanner._SCANNER_.barcode_result.connect(get_express_number_by_barcode)
        scanner_signal_connect_flag = True
    Scanner.start_get_text_info()


express_id = ''
imported_express = None

def inquiry_preload_data(express_no):
    global express_id
    global phone_number
    try:
        message, status_code = HttpClient.get_message('express/imported/' + express_no )
        if 'expressNumber' in message:
            express_id = message['id']
            phone_number = message['takeUserPhoneNumber']
        return message, status_code
    except Exception as e:
        return 404, 'err'

def get_express_number_by_barcode(text):
    # global imported_express
    # global express_id
    # pattern_barcode = '^[a-zA-Z0-9-_]{1,24}$'
    # check_pattern_barcode = (re.search(pattern_barcode, text))
    if text == '' or text == 'ERROR':
        return
    _EXPRESS_.barcode_signal.emit(text)
    #customer_store_number = text
    #get_customer_store_express_info()

    # express_message, status_code = HttpClient.get_message('express/imported/' + text)
    # if status_code == 200:
    #     if "takeUserPhoneNumber" in str(express_message):
    #         imported_express = express_message
    #         express_id = imported_express['id']
    #         _EXPRESS_.phone_number_signal.emit(imported_express['takeUserPhoneNumber'])
    #     else:
    #         _EXPRESS_.imported_express_result_signal.emit('no_imported')
    #         imported_express = None
    #         express_id = ''
    # else:
    #     imported_express = None
    #     express_id = ''
    # res = {}
    # info_user = UserService.get_user_info()
    # _company_id = info_user['company_id']
    # id_lazada = "2c9180c151a8c1a70151ccfc1cc109ca"

    # express_message, status_code = HttpClient.get_message('express/imported/' + text)
    # if status_code == 200:
    #     if "takeUserPhoneNumber" in str(express_message):
    #         imported_express = express_message
    #         express_id = imported_express['id']
    #         res['isSuccess'] = "true"
    #         res['data'] = "imported"
    #         res['phone_number'] = imported_express['takeUserPhoneNumber']
    #         _LOG_.info(('[MERCHANT_INFO]', imported_express['merchant_info']))
    #         if len(imported_express['merchant_info']) == 0:
    #             res['merchant_info'] = ''
    #         else:
    #             res['merchant_info'] = imported_express['merchant_info'][0]

    #         _EXPRESS_.barcode_signal.emit(str(json.dumps(res)))
    #     else:
    #         if _company_id == id_lazada:
    #             check_pattern_forbidden = check_pattern(text)
    #             if check_pattern_forbidden is False:
    #                 res['isSuccess'] = "false"
    #                 res['data'] = "forbidden_pattern"
    #                 _EXPRESS_.barcode_signal.emit(str(json.dumps(res)))
    #             else:
    #                 express_message, status_code = HttpClient.get_message('express/imported/' + text)
    #                 # response = json.loads(express_message)
    #                 if status_code == 200:
    #                     if "takeUserPhoneNumber" not in str(express_message):
    #                         res['isSuccess'] = "false"
    #                         res['data'] = "forbidden_pattern"
    #                         _EXPRESS_.barcode_signal.emit(str(json.dumps(res)))
    #                 else:
    #                     res['isSuccess'] = "true"
    #                     res['data'] = "no_imported"
    #                     _EXPRESS_.barcode_signal.emit(str(json.dumps(res)))
    #                     imported_express = None
    #                     express_id = ''

    #         else:
    #             res['isSuccess'] = "true"
    #             res['data'] = "no_imported"
    #             _EXPRESS_.barcode_signal.emit(str(json.dumps(res)))
    #             imported_express = None
    #             express_id = ''
    # else:
    #     if _company_id == id_lazada:
    #         check_pattern_forbidden = check_pattern(text)
    #         if check_pattern_forbidden is False:
    #             res['isSuccess'] = "false"
    #             res['data'] = "forbidden_pattern"
    #             _EXPRESS_.barcode_signal.emit(str(json.dumps(res)))
    #         else:
    #             express_message, status_code = HttpClient.get_message('express/imported/' + text)
    #             # response = json.loads(express_message)
    #             if status_code == 200:
    #                 if "takeUserPhoneNumber" not in str(express_message):
    #                     res['isSuccess'] = "false"
    #                     res['data'] = "forbidden_pattern"
    #                     _EXPRESS_.barcode_signal.emit(str(json.dumps(res)))
    #             else:
    #                 res['isSuccess'] = "true"
    #                 res['data'] = "no_imported"
    #                 _EXPRESS_.barcode_signal.emit(str(json.dumps(res)))
    #                 imported_express = None
    #                 express_id = ''

    #     else:
    #         res['isSuccess'] = "true"
    #         res['data'] = "no_imported"
    #         _EXPRESS_.barcode_signal.emit(str(json.dumps(res)))
    #         imported_express = None
    #         express_id = ''



def stop_get_express_number_by_barcode():
    global scanner_signal_connect_flag
    if scanner_signal_connect_flag:
        Scanner._SCANNER_.barcode_result.disconnect(get_express_number_by_barcode)
        scanner_signal_connect_flag = False
    Scanner.start_stop_scanner()

temp_data_redrop = ''

def start_store_redrop_express():
    ClientTools.get_global_pool().apply_async(store_redrop_express)

def store_redrop_express():
    global temp_data_redrop
    user_id = UserService.get_user()
    box_result = box.service.BoxService.get_box()
    overdue_time = get_overdue_timestamp(box_result)
    data_redrop_ = temp_data_redrop['data'][0]
    mouth_result = box.service.BoxService.mouth
    # redrop_message['data'][0]
    res = {}
    try:
        express_check = ExpressDao.get_express_by_id({'id': data_redrop_['id_express']})
        if(len(express_check)!=1):
            express_param = {'id': data_redrop_['id_express'],
                            'expressNumber': data_redrop_['expressNumber'],
                            'expressType': data_redrop_['expressType'],
                            'overdueTime': overdue_time,
                            'status': 'IN_STORE',
                            'storeTime': ClientTools.now(),
                            'syncFlag': 0,
                            'takeUserPhoneNumber': data_redrop_['takeUserPhoneNumber'],
                            'validateCode': data_redrop_['validateCode'],
                            'version': data_redrop_['version'],
                            'box_id': box_result['id'],
                            'logisticsCompany_id': user_id['company']['id'],
                            'mouth_id': mouth_result['id'], #butuh mouth result
                            'operator_id': box_result['operator_id'],
                            'storeUser_id': user_id['id'], #butuh yang store
                            'groupName': data_redrop_['groupName'],
                            'other_company_name': other_company_name,
                            'courier_phone': courier_phone,
                            'transactionRecord': '',
                            'drop_by_courier_login': '',
                            'payment_param': '',
                            'flagRedrop': 1,
                            'is_insulator':0}
            # ========================================
            while True:
                save_redrop_express_db = ExpressDao.save_express(express_param)
                if save_redrop_express_db is True:
                    _LOG_.info(('[SUCCESS] store express data to DB', data_redrop_['customerStoreNumber']))
                    break
            # ========================================

            if save_redrop_express_db is True:
                mouth_param = {'id': mouth_result['id'],
                            'express_id': express_param['id'],
                            'status': 'USED'}
                box.service.BoxService.use_mouth(mouth_param)
        else:
            express_param = {'id': data_redrop_['id_express'],
                            'overdueTime': overdue_time,
                            'status': 'IN_STORE',
                            'storeTime': ClientTools.now(),
                            'syncFlag': 0,
                            'validateCode': data_redrop_['validateCode'],
                            'box_id': box_result['id'],
                            'logisticsCompany_id': user_id['company']['id'],
                            'mouth_id': mouth_result['id'],
                            'operator_id': box_result['operator_id'],
                            'storeUser_id': user_id['id'],
                            'flagRedrop': 1}
            # ========================================
            while True:
                # _LOG_.info(('[ATTEMPT] store express data to DB', express_param))
                update_redrop_express_db = ExpressDao.update_express(express_param)
                if update_redrop_express_db is True:
                    # _LOG_.info(('[SUCCESS] store express data to DB', express_param))
                    break
            # ========================================
            if update_redrop_express_db is True:
                mouth_param = {'id': mouth_result['id'],
                            'express_id': express_param['id'],
                            'status': 'USED'}
                box.service.BoxService.use_mouth(mouth_param)
        
        res['isSuccess'] = "true"
        res['data'] = "re_drop"
        res['status_size'] = "true"
        res['id_express'] = express_param['id']
        _EXPRESS_.store_express_signal.emit(str(json.dumps(res)))
        # _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] update_redrop_function : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))


def save_redrop_express(data_express):
    try:
        # _LOG_.info("REDROP : "+ str(redrop_message) + "status Code : " + str(_status_code))
        print("redrop true")
        res = {}
        redrop_message = data_express
        data_redrop = redrop_message['data'][0]
        # data_redrop['lockerSize'] = "XL"
        _LOG_.info("Data redrop : = " + str(data_redrop['mouth_id']))
        _LOG_.info("Size redrop : = " + str(data_redrop['lockerSize']))

        #==============================================#
        user_id = UserService.get_user()
        box_result = box.service.BoxService.get_box()
        if not box_result:
            # res['isSuccess'] = 'false'
            # res['message'] = e
            # _EXPRESS_.store_express_signal.emit(str(json.dumps(res)))
            _LOG_.info("[ERROR] store_deposit_express box_result")
            return
        overdue_time = get_overdue_timestamp(box_result)
        _LOG_.info("Data redrop : overdue_time = " + str(overdue_time))
        #==============================================#

        mouth_param = {'mouthType_id': data_redrop['id'],
                    'deleteFlag': 0,
                    'status': 'ENABLE',
                    'is_insulator':0}
        mouth_check = BoxDao.get_free_mouth_by_type(mouth_param)
        # mouth_check = BoxDao.get_mouth_by_id_status({'id':data_redrop['mouth_id'], 'status': 'ENABLE'})
        check_size = box.service.BoxService.choose_mouth_size(data_redrop['lockerSize'], "redrop")
        express_check = ExpressDao.get_express_by_id({'id': data_redrop['id_express']})
        _LOG_.info("LEN MOUTH CHECK : "+ str(len(mouth_check)))
        # _LOG_.info("LEN Express CHECK : "+ str(len(check_size)))
        if check_size==True:
            mouth_result = box.service.BoxService.mouth
            print("OKE SIZE : " + str(mouth_result['id']))
            if(len(express_check)!=1):
                express_param = {'id': data_redrop['id_express'],
                                'expressNumber': data_redrop['expressNumber'],
                                'expressType': data_redrop['expressType'],
                                'overdueTime': overdue_time,
                                'status': 'IN_STORE',
                                'storeTime': ClientTools.now(),
                                'syncFlag': 0,
                                'takeUserPhoneNumber': data_redrop['takeUserPhoneNumber'],
                                'validateCode': data_redrop['validateCode'],
                                'version': data_redrop['version'],
                                'box_id': box_result['id'],
                                'logisticsCompany_id': user_id['company']['id'],
                                'mouth_id': mouth_result['id'], #butuh mouth result
                                'operator_id': box_result['operator_id'],
                                'storeUser_id': user_id['id'], #butuh yang store
                                'groupName': data_redrop['groupName'],
                                'other_company_name': other_company_name,
                                'courier_phone': courier_phone,
                                'transactionRecord': '',
                                'drop_by_courier_login': '',
                                'payment_param': '',
                                'flagRedrop': 1,
                                'is_insulator': 0}
                # ========================================
                while True:
                    save_redrop_express_db = ExpressDao.save_express(express_param)
                    if save_redrop_express_db is True:
                        _LOG_.info(('[SUCCESS] store express data to DB', data_redrop['customerStoreNumber']))
                        break
                # ========================================

                if save_redrop_express_db is True:
                    mouth_param = {'id': mouth_result['id'],
                                'express_id': express_param['id'],
                                'status': 'USED'}
                    box.service.BoxService.use_mouth(mouth_param)
            else:
                express_param = {'id': data_redrop['id_express'],
                                'overdueTime': overdue_time,
                                'status': 'IN_STORE',
                                'storeTime': ClientTools.now(),
                                'syncFlag': 0,
                                'validateCode': data_redrop['validateCode'],
                                'box_id': box_result['id'],
                                'logisticsCompany_id': user_id['company']['id'],
                                'mouth_id': mouth_result['id'],
                                'operator_id': box_result['operator_id'],
                                'storeUser_id': user_id['id'],
                                'flagRedrop': 1}
                # ========================================
                while True:
                    # _LOG_.info(('[ATTEMPT] store express data to DB', express_param))
                    update_redrop_express_db = ExpressDao.update_express(express_param)
                    if update_redrop_express_db is True:
                        # _LOG_.info(('[SUCCESS] store express data to DB', express_param))
                        break
                # ========================================
                if update_redrop_express_db is True:
                    mouth_param = {'id': mouth_result['id'],
                                'express_id': express_param['id'],
                                'status': 'USED'}
                    box.service.BoxService.use_mouth(mouth_param)
            
            res['isSuccess'] = "true"
            res['data'] = "re_drop"
            res['status_size'] = "true"
            res['express_id'] = express_param['id']
            _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))

        else:
            print("FALSE SIZE : No Mouth")
            if(len(express_check)!=1):
                res['isSuccess'] = "true"
                res['data'] = "re_drop"
                res['status_size'] = "false"
                res['express_id'] = data_redrop['id_express']
                _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
            else:
                res['isSuccess'] = "true"
                res['data'] = "re_drop"
                res['mouth_size'] = "false"
                res['express_id'] = data_redrop['id_express']
                _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] update_redrop_function : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))


def start_get_imported_express(text):
    ClientTools.get_global_pool().apply_async(get_express_number_by_text, (text,))

def get_express_number_by_text(text):
    global imported_express
    global express_id
    global temp_data_redrop
    locker_country = Configurator.get_value('ClientInfo', 'language')
    res = {}
    if text == '' or text == 'ERROR':
        res['isSuccess'] = "false"
        res['data'] = "express Number cannot null"
        _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
        # return
    print('info_awb_inquired', str(text))
    info_user = UserService.get_user_info()
    _company_id = info_user['company_id']
    id_lazada = "2c9180c151a8c1a70151ccfc1cc109ca"

    if (locker_country=="MY"): 
        check_pattern_forbidden = check_pattern_my(text, _company_id)
        if _company_id == "402880825ea763b8015fd71178de212b":   #PROD
        # if _company_id == "800640693001567595171qEYKp7s4CTR":     #DEV
            company_forbidden = "forbidden_pattern_poslaju"
        else:
            company_forbidden = "forbidden_pattern" 

        if check_pattern_forbidden == True:  
            res['isSuccess'] = "false"
            res['data'] = company_forbidden
            _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
        else:
            express_message, status_code = HttpClient.get_message('express/imported/' + text)
    else:
        express_message, status_code = HttpClient.get_message('express/imported/' + text)

    if status_code == 200:
        if "takeUserPhoneNumber" in str(express_message):
            imported_express = express_message
            express_id = imported_express['id']
            res['isSuccess'] = "true"
            res['data'] = "imported"
            res['phone_number'] = imported_express['takeUserPhoneNumber']
            _LOG_.info(('[MERCHANT_INFO]', imported_express['merchant_info']))
            if len(imported_express['merchant_info']) == 0:
                res['merchant_info'] = ''
            else:
                res['merchant_info'] = imported_express['merchant_info'][0]

            _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
        else:
            redrop_message, _status_code = HttpClient.get_message('redrop/inquiry/' + text)

        if _status_code == 200:
            #disini
            temp_data_redrop = redrop_message
            save_redrop_express(redrop_message)
        elif _status_code == 402:
            res['isSuccess'] = "false"
            res['data'] = "payment_required"
            _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
        else:
            print("redrop false")
            if _company_id == id_lazada:
                check_pattern_forbidden = check_pattern(text)
                if check_pattern_forbidden is False:
                    res['isSuccess'] = "false"
                    res['data'] = "forbidden_pattern"
                    _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
                else:
                    express_message, status_code = HttpClient.get_message('express/imported/' + text)
                    # response = json.loads(express_message)
                    #DIMATIKAN KARENA TIDAK KERJA SAMA DENGAN LAZADA LAGI (PENGECHECKAN PRELOAD)
                    # if status_code == 200:
                    #     if "takeUserPhoneNumber" not in str(express_message):
                    #         res['isSuccess'] = "false"
                    #         res['data'] = "forbidden_pattern"
                    #         _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
                    # else:
                    #     res['isSuccess'] = "true"
                    #     res['data'] = "no_imported"
                    #     _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
                    #     imported_express = None
                    #     express_id = ''
                    res['isSuccess'] = "true"
                    res['data'] = "no_imported"
                    _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
                    imported_express = None
                    express_id = ''

            else:
                if re.search('\s', text):
                    res['isSuccess'] = "false"
                    res['data'] = "forbidden_space"
                else:
                    res['isSuccess'] = "true"
                    res['data'] = "no_imported"
                
                imported_express = None
                express_id = ''
                _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
    else:
    #     redrop_message, _status_code = HttpClient.get_message('redrop/inquiry/' + text)

    # if _status_code == 200:
    #     _LOG_.info("REDROP : "+ str(redrop_message) + "status Code : " + str(_status_code))
    #     print("redrop true")
    #     # res['isSuccess'] = "true"
    #     # res['data'] = "no_imported"
    # else:
        print("redrop false")
        if _company_id == id_lazada:
            check_pattern_forbidden = check_pattern(text)
            if check_pattern_forbidden is False:
                res['isSuccess'] = "false"
                res['data'] = "forbidden_pattern"
                _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
            else:
                express_message, status_code = HttpClient.get_message('express/imported/' + text)
                # response = json.loads(express_message)
                if status_code == 200:
                    if "takeUserPhoneNumber" not in str(express_message):
                        res['isSuccess'] = "false"
                        res['data'] = "forbidden_pattern"
                        _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
                else:
                    res['isSuccess'] = "true"
                    res['data'] = "no_imported"
                    _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))
                    imported_express = None
                    express_id = ''

        else:
            if re.search('\s', text):
                res['isSuccess'] = "false"
                res['data'] = "forbidden_space"
            else:
                res['isSuccess'] = "true"
                res['data'] = "no_imported"
            
            imported_express = None
            express_id = ''
            _EXPRESS_.imported_express_result_signal.emit(str(json.dumps(res)))


express_number = ''


def set_express_number(express_number__):
    global express_number
    express_number = express_number__


phone_number = ''
courier_phone = ''
other_company_name =  ''
client_popex_overdue = ''
recipeint_name = ''
drop_by_courier = 0

def set_phone_number(phone_number__):
    global phone_number
    phone_number = phone_number__

def set_drop_by_courier(drop_courier_by__):
    print('drop_by_courier_express_service', str(drop_courier_by__))
    global drop_by_courier
    drop_by_courier = drop_courier_by__

def set_client_overdue_popex_info(overdue_time__, overdue_type__, company_name__):
    global client_popex_overdue
    client_popex_overdue = {}
    _LOG_.info(('[CLIENT_INFO_OVERDUE]', overdue_time__))
    client_popex_overdue['overdueType'] = overdue_type__
    client_popex_overdue['company_name'] = company_name__
    if overdue_type__ == "DAY":
        client_popex_overdue['freeDays'] = overdue_time__
    else:
        client_popex_overdue['freeHours'] = overdue_time__

def set_recipient_name(__recipient_name):
    global recipeint_name
    # _LOG_.info(('INFO [recipeint_name] ', __recipient_name))
    recipeint_name = __recipient_name
    # _LOG_.info(('INFO [recipeint_name02] ', recipeint_name))


def set_courier_phone(phone_courier):
    global courier_phone
    courier_phone = phone_courier

def set_other_company_name(company_name):
    global other_company_name
    other_company_name = company_name
    _LOG_.debug(('set_company_other_name:', company_name))

id_logistic_company = ''

def set_id_logistic_company(id_logistic):
    global id_logistic_company
    id_logistic_company = id_logistic

def start_change_mouth_id_by_express_id(express_id):
    # ClientTools.get_global_pool().apply_async(update_mouth_id_express, (express_id,))
    update_mouth_id_express(express_id)

def update_mouth_id_express(express_id):
    res = {}
    try:
        print('express_id_for_change', express_id)
        param = {'id': express_id}
        check_express_by_id = ExpressDao.get_express_by_id(param)
        if len(check_express_by_id) < 1:
            res['isSuccess'] = "false"
            _EXPRESS_.start_update_mouth_id_express_signal.emit(str(json.dumps(res)))
        else:
            _LOG_.info(('[DEBUG] mouth_id_change : ', check_express_by_id[0]['mouth_id']))

            # change mouth_id available
            print('mouth_id_to_change', str(check_express_by_id[0]['mouth_id']))
            change_mouth_id_free(check_express_by_id[0]['mouth_id'])

            # get mouth after change
            mouth_result = box.service.BoxService.mouth
            update_express = {
                'id': express_id,
                'mouth_id': mouth_result['id'],
                'syncFlag': 0
            }
            ExpressDao.update_mouth_id_by_express_id(update_express)

            set_mouth_used_after_change_size(mouth_result['id'], express_id)

            res['isSuccess'] = 'true'
            res['id_express'] = express_id
            res['message'] = 'success updated'
            _EXPRESS_.start_update_mouth_id_express_signal.emit(str(json.dumps(res)))
    except Exception as e:
        res['isSuccess'] = 'false'
        res['message'] = str(e)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] update_mouth_id_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.start_update_mouth_id_express_signal.emit(str(json.dumps(res)))

def change_mouth_id_free(mouth_id):
    try:
        _LOG_.info(('[DEDUG] change_mouth_id_free : ', mouth_id))
        param = {'id': mouth_id, 'status': 'ENABLE', 'syncFlag': 0}
        change_mouth_to_available = BoxDao.update_box_to_available(param)
        return True
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] change_mouth_free_signal : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        return False

def set_mouth_used_after_change_size(mouth_id, express_id):
    try:
        _LOG_.info(('[DEDUG] change_mouth_id_used : ', mouth_id))
        param = {'id': mouth_id, 'status': 'USED', 'express_id': express_id, 'syncFlag': 0}
        change_mouth_to_available = BoxDao.update_box_to_used(param)
        return True
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] change_mouth_used_signal : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def start_store_express():
    ClientTools.get_global_pool().apply_async(store_express)


def store_express():
    global imported_express
    global express_id
    global drop_by_courier
    global client_popex_overdue
    global courier_phone
    global id_logistic_company
    
    res = {}
    try:
        if len(express_number) == 0 or express_number == "":
            _EXPRESS_.store_express_signal.emit('Error')
            _LOG_.warning('[ERROR] store_express express_number is null')
            return
        mouth_result = box.service.BoxService.mouth
        _LOG_.debug(('mouth_result:', mouth_result))
        user_result = UserService.get_user()
        _LOG_.debug(('[USER_LOGIN_RESULT]', user_result))
        
        is_need_pod = 0
        
        if user_result == '':
            user_result = {}
            if(id_logistic_company==''):
                id_logistic_company = 'f54530d1a9ae4983b3871fd43a187fa9'
            else:
                logistic_id_company = id_logistic_company
            user_result['id'] = ClientTools.get_uuid()
        else:
            logistic_id_company = user_result['company']['id']

        box_result = box.service.BoxService.get_box()
        if not box_result:
            _EXPRESS_.store_express_signal.emit('Error')
            _LOG_.warning('[ERROR] store_express box_result is null')
            return

        _LOG_.info(('[OVERDUE_CLIENT]', client_popex_overdue))

        if client_popex_overdue != '':
            overdue_time = get_overdue_timestamp(client_popex_overdue)
            
            check_column = ExpressDao.check_column_exist({}, 'Express')
            checking = 0
            for list_ in check_column:
                if list_['name'] == 'is_needed_pod':
                    checking += 1

            if checking == 0:
                adding_column = ExpressDao.adding_column({},'is_needed_pod', 0, 'Express')
            
            list_company_need_pod = ['DHL']
            if client_popex_overdue['company_name'] in list_company_need_pod:
                is_need_pod = 1
            else:
                is_need_pod = 0
        elif mouth_result['is_insulator'] == "1" or mouth_result['is_insulator'] == 1:
            check_column = ExpressDao.check_column_exist({}, 'Express')
            checking = 0
            for list_ in check_column:
                if list_['name'] == 'dataInsulator':
                    checking += 1

            if checking == 0:
                adding_column = ExpressDao.adding_column({},'dataInsulator', 0, 'Express')

            param = {
                'logisticsCompany_id': id_logistic_company,
                'is_active': 1
            }
            get_data_overdue = BoxDao.insulator_overdue_by_logistics_company_id(param)
            if len(get_data_overdue) < 1:
                param = {
                    'logisticsCompany_id': '145b2728140f11e5bdbd0242ac110001',
                    'is_active': 1
                }
                get_data_overdue = BoxDao.insulator_overdue_by_logistics_company_id(param)

                if len(get_data_overdue) < 1:
                    overdue_time = get_overdue_timestamp(box_result)
                else:
                    result_overdue = get_data_overdue[0]
                    insulator_overdue = {
                        'overdueType': box_result['overdueType'],
                        'freeHours': result_overdue['overdue_hour'], 
                        'freeDays': result_overdue['overdue_day'] 
                    }
                    overdue_time = get_overdue_timestamp(insulator_overdue)
            else:
                result_overdue = get_data_overdue[0]
                insulator_overdue = {
                    'overdueType': box_result['overdueType'],
                    'freeHours': result_overdue['overdue_hour'], 
                    'freeDays': result_overdue['overdue_day'] 
                }
                overdue_time = get_overdue_timestamp(insulator_overdue)
        else:
            overdue_time = get_overdue_timestamp(box_result)

        # operator_result = CompanyService.get_company_by_id(box_result['operator_id'])
        # if not operator_result:
        #     _EXPRESS_.store_express_signal.emit('Error')
        #     _LOG_.warning('[ERROR] store_express operator_result is null')
        #     return
        # transactionRecord = express.popbox.PopboxService.get_transaction_record()
        # _LOG_.info("transactionRecord :: == " + str(transactionRecord))

        express_param = {'expressNumber': express_number,
                         'expressType': 'COURIER_STORE',
                         'overdueTime': overdue_time,
                         'status': 'IN_STORE',
                         'storeTime': ClientTools.now(),
                         'syncFlag': 0,
                         'takeUserPhoneNumber': phone_number,
                         'courier_phone': courier_phone,
                         'other_company_name': other_company_name,
                         'validateCode': random_validate(box_result['validateType']),
                         'version': 0,
                         'box_id': box_result['id'],
                         'logisticsCompany_id': logistic_id_company,
                         'mouth_id': mouth_result['id'],
                         'operator_id': box_result['operator_id'],
                         'storeUser_id': user_result['id'],
                         'transactionRecord': '',
                         'payment_param': '',
                         'flagRedrop': 0,
                         'chargeType': ''}

        express_param['drop_by_courier_login'] = drop_by_courier
        _LOG_.info(('[EXPRESS_PARAM]', express_param))
        if ClientTools.get_value('groupName', imported_express) is not None:
            express_param['groupName'] = imported_express['groupName']
            express_param['is_insulator'] = 0 #Tambahan
        else:
            if mouth_result['is_insulator'] == "1" or mouth_result['is_insulator'] == 1:
                express_param['groupName'] = "FOOD_BEVERAGE"
                express_param['is_insulator'] = 1
            else:
                express_param['groupName'] = "UNDEFINED"
                express_param['is_insulator'] = 0
        if express_id == '':
            express_param['id'] = ClientTools.get_uuid()
        else:
            param_check = {'id': express_id}
            check_express_by_id = ExpressDao.get_express_by_id(param_check)
            if len(check_express_by_id) > 0:
                express_param['id'] = ClientTools.get_uuid()
            else:
                express_param['id'] = express_id


        # ========================================
        while True:
            _LOG_.info(('[ATTEMPT] store express data to DB', express_number))
            store_express_db = ExpressDao.save_express(express_param)
            if store_express_db is True:
                _LOG_.info(('[SUCCESS] store express data to DB', express_number))
                break
        # ========================================

        if store_express_db is True:

            update_pod = {'is_needed_pod': is_need_pod, 'id': express_param['id']}
            updating_express_pod = ExpressDao.update_set_flag_pod(update_pod)

                            
            mouth_param = {'id': mouth_result['id'],
                        'express_id': express_param['id'],
                        'status': 'USED'}
            box.service.BoxService.use_mouth(mouth_param)
            express_param['box'] = {'id': express_param['box_id']}
            express_param.pop('box_id')
            express_param['logisticsCompany'] = {'id': express_param['logisticsCompany_id']}
            express_param.pop('logisticsCompany_id')
            express_param['mouth'] = {'id': express_param['mouth_id']}
            express_param.pop('mouth_id')
            express_param['operator'] = {'id': express_param['operator_id']}
            express_param.pop('operator_id')
            express_param['storeUser'] = {'id': express_param['storeUser_id']}
            express_param.pop('storeUser_id')
            imported_express = None
            Camera.start_video_capture('courier_finish_drop_'+express_number)
            _LOG_.debug(('[DEBUG] system_capture', 'courier_finish_drop_'+express_number))

            res['isSuccess'] = "true"
            res['id_express'] = express_param['id']
            res['expressNumber'] = express_param['expressNumber']
            _EXPRESS_.store_express_signal.emit(str(json.dumps(res)))
            _LOG_.debug(('[REQUEST_STORE] system_capture : ' + str(express_param)))

            time.sleep(60) # delay for 1 minutes & 30 seconds
            param_get_detail = {'id': express_param['id']}
            data_updated = ExpressDao.get_express_by_id(param_get_detail)[0]
            
            if data_updated['status'] != express_param['status']:
                express_param['status'] = data_updated['status']
                if data_updated['reason'] != "":
                    express_param['reason'] = data_updated['reason']
            if data_updated['mouth_id'] != express_param['mouth']['id']:
                change_mouth_id_free(express_param['mouth']['id'])
                set_mouth_used_after_change_size(data_updated['mouth_id'], express_param['id'])
                express_param['mouth']['id'] = data_updated['mouth_id']

            message, status_code = HttpClient.post_message('express/staffStoreExpress', express_param)
            if status_code == 200 and message['id'] == express_param['id']:
                express_id = ''
                ExpressDao.mark_sync_success(express_param)
            is_need_pod = 0
            client_popex_overdue = ''
            return
        else:
            is_need_pod = 0
            client_popex_overdue = ''
            res = {}
            res['isSuccess'] = 'false'
            _LOG_.warning(('[ERROR] store_express_signal : ', str(store_express_db)))
            _EXPRESS_.store_express_signal.emit(str(json.dumps(res)))
            return
    except Exception as e:
        # hasil_check = save_data_to_db_when_error(express_param)
        # print('hasil_check', str(hasil_check))
        is_need_pod = 0
        res = {}
        res['isSuccess'] = 'false'
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] store_express_signal : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.store_express_signal.emit(str(json.dumps(res)))
        return


def save_data_to_db_when_error(express_param):

    try:
        param = {
            'id': express_param['id'],
            'deleteFlag': 0
        }

        check_data = ExpressDao.check_parcel(param)
        print('check_data', str(check_data))
        if not check_data:
            ExpressDao.save_data_temp(express_param)
        else:
            return False
        _LOG_.warning(('[SUCCESS] save_data_to_temp : ', str(express_param)))
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] save_data_to_db_when_error : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        return False

def get_overdue_timestamp(box_result):
    try:
        start_time = datetime.datetime.now()
        overdue_flag = ClientTools.get_value('overdueFlag', imported_express, default_value=None)
        if overdue_flag is None:
            overdue_type = box_result['overdueType']
            if overdue_type == 'HOUR':
                overdue_time = box_result['freeHours']
            else:
                overdue_time = box_result['freeDays']
        else:
            overdue_type = overdue_flag.split(':')[0]
            overdue_time = overdue_flag.split(':')[1]
        if overdue_type == 'HOUR':
            end_time = start_time + datetime.timedelta(hours=int(overdue_time))
        else:
            end_time = (start_time + datetime.timedelta(days=int(overdue_time))).replace(hour=23, minute=0, second=0)
            # print('start_time', str(start_time))
            # print('overdue_time', str(overdue_time))
            # tmp_date = date_by_adding_business_days(start_time, int(overdue_time))
            # end_time = tmp_date.replace(hour=23, minute=0, second=0)
            
        return int(time.mktime(time.strptime(end_time.strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')) * 1000)
    except Exception as e:
        _LOG_.debug(('get_overdue_timestamp', imported_express, str(e)))

def date_by_adding_business_days(from_date, add_days):
    business_days_to_add = add_days
    current_date = from_date
    while business_days_to_add > 0:
        current_date += datetime.timedelta(days=1)
        weekday = current_date.weekday()
        if weekday >= 5: # sunday = 6
            continue
        business_days_to_add -= 1
    return current_date

def random_validate(validate_type, chars='ABCDEFGHJKLMNPQRSTUVWXYZ23456789'):
    while True:
        if validate_type == 'LETTER_AND_NUMBER_VALIDATE_CODE':
            validate_code = ''
            i = 0
            while i < 6:
                validate_code += random.choice(chars)
                i += 1
            if len(ExpressDao.get_express_by_validate({'validateCode': validate_code, 'status': 'IN_STORE'})) == 0:
                return validate_code
        else:
            return ''


customer_store_number = ''
customer_store_express = ''


def start_get_customer_store_express_info(text):
    global customer_store_number
    customer_store_number = text
    ClientTools.get_global_pool().apply_async(get_customer_store_express_info)


def get_customer_store_express_info():
    global customer_store_express
    locker_info = box.service.BoxService.get_box()
    message, status_code = HttpClient.get_message('express/customerExpress/' + customer_store_number)
    if status_code == 200:
        if ClientTools.get_value("statusCode", message) == 404:
            customer_store_express = {}
            customer_store_express['isSuccess'] = "false"
            _EXPRESS_.customer_store_express_signal.emit(str(json.dumps(customer_store_express)))
            return
        if ClientTools.get_value("status", message) == "IMPORTED":
            customer_store_express = message
            customer_store_express['isSuccess'] = "true"
            customer_store_express['box_name'] = locker_info['name']
            _EXPRESS_.customer_store_express_signal.emit(str(json.dumps(customer_store_express)))
        else:
            customer_store_express = {}
            customer_store_express['isSuccess'] = "false"
            _EXPRESS_.customer_store_express_signal.emit(str(json.dumps(customer_store_express)))
            return
    else:
        customer_store_express = {}
        customer_store_express['isSuccess'] = "false"
        _EXPRESS_.customer_store_express_signal.emit(str(json.dumps(customer_store_express)))


customer_reject_number = ''
customer_reject_express = ''


def start_get_customer_reject_express_info(text):
    global customer_reject_number
    customer_reject_number = text
    ClientTools.get_global_pool().apply_async(get_customer_reject_express_info)


def get_customer_reject_express_info():
    global customer_reject_express
    message, status_code = HttpClient.get_message('express/rejectExpress/' + customer_reject_number)
    if status_code == 200:
        customer_reject_express = message
        _EXPRESS_.customer_reject_express_signal.emit(str(json.dumps(customer_reject_express)))
    else:
        _EXPRESS_.customer_reject_express_signal.emit('False')


customer_scanner_signal_connect_flag = False


def start_customer_scan_qr_code():
    global customer_scanner_signal_connect_flag
    if not customer_scanner_signal_connect_flag:
        Scanner._SCANNER_.barcode_result.connect(customer_scan_qr_code)
        customer_scanner_signal_connect_flag = True
    Scanner.start_get_text_info()


def stop_customer_scan_qr_code():
    global customer_scanner_signal_connect_flag
    if not customer_scanner_signal_connect_flag:
        return
    customer_scanner_signal_connect_flag = False
    Scanner._SCANNER_.barcode_result.disconnect(customer_scan_qr_code)
    Scanner.start_stop_scanner()


def customer_scan_qr_code(text):
    global customer_scanner_signal_connect_flag
    global customer_store_number
    if text == '' or text == 'ERROR':
        customer_scanner_signal_connect_flag = False
        return
    _EXPRESS_.barcode_signal.emit(text)
    customer_store_number = text
    ClientTools.get_global_pool().apply_async(get_customer_store_express_info)
    customer_scanner_signal_connect_flag = False


def start_load_courier_overdue_express_count():
    ClientTools.get_global_pool().apply_async(load_courier_overdue_express_count)


def load_courier_overdue_express_count():
    try:
        __user = UserService.get_user()
        param = {'status': 'IN_STORE',
                'overdueTime': ClientTools.now(),
                'logisticsCompany_id': __user['company']['id'],
                'expressType': 'COURIER_STORE',
                'groupName_1': 'POPDEPOSIT',
                'groupName_2': 'POPTITIP'}
        __user_access = __user['user_role']['level_role']
        if __user_access == 1 or __user_access == 2 or __user_access == 3 or __user_access == 4 or __user_access == 5 or __user_access == 6:
            param.pop('logisticsCompany_id')
            count_list = ExpressDao.get_overdue_express_count_by_manager(param)
        elif __user_access == 9:
            param = {'status': 'IN_STORE',
                    'overdueTime': ClientTools.now(),
                    'logisticsCompany_id': '161e5ed1140f11e5bdbd0242ac110001',
                    'expressType': 'COURIER_STORE',
                    'groupName_1': 'POPDEPOSIT',
                    'groupName_2': 'POPTITIP'}
            _LOG_.info("PARAMM COUNT : " + str(param))
            count_list = ExpressDao.get_overdue_express_count_by_centralization(param)
        else:
            count_list = ExpressDao.get_overdue_express_count_by_logistics_id(param)
        _EXPRESS_.overdue_express_count_signal.emit(count_list[0]['count'])
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] load_courier_overdue_express_count : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        return False


def start_load_courier_return_express_count():
    ClientTools.get_global_pool().apply_async(load_courier_return_express_count)


def load_courier_return_express_count():
    __user = UserService.get_user()
    param = {'status': 'IN_STORE',
             'expressType': 'CUSTOMER_REJECT'}
    __user_access = __user['user_role']['level_role']
    if __user_access == 1 or __user_access == 2 or __user_access == 3 or __user_access == 4 or __user_access == 5 or __user_access == 6:
        return_list = ExpressDao.get_return_express_count_by_manager(param)
        _LOG_.info(("RETURN LIST TEST 1  :", str(return_list)))
    # elif __user_access == 7 or __user_access == 8 or __user_access == 9:
    #     return_list = ExpressDao.get_popsend_express_count_by_logistics_id(param)
    else:
        return_list = [{'count':0}]
        _LOG_.info(("RETURN LIST TEST 2  :", str(return_list)))
    _EXPRESS_.return_express_count_signal.emit(return_list[0]['count'])


def start_load_courier_popsafe_express_count():
    ClientTools.get_global_pool().apply_async(load_courier_popsafe_express_count)


def load_courier_popsafe_express_count():
    __user = UserService.get_user()
    param = {'status': 'IN_STORE',
             'overdueTime': ClientTools.now(),
             'logisticsCompany_id': __user['company']['id'],
             'groupName_1': 'POPDEPOSIT',
             'groupName_2': 'POPTITIP'}
    __user_access = __user['user_role']['level_role']
    if __user_access == 1 or __user_access == 2 or __user_access == 3 or __user_access == 4 or __user_access == 5 or __user_access == 6:
        popsafe_list = ExpressDao.get_popsafe_express_count_by_manager(param)
    elif __user_access == 9:
        param = {'status': 'IN_STORE',
                 'overdueTime': ClientTools.now(),
                 'logisticsCompany_id': '161e5ed1140f11e5bdbd0242ac110001',
                 'groupName_1': 'POPDEPOSIT',
                 'groupName_2': 'POPTITIP'}
        popsafe_list = ExpressDao.get_popsafe_express_count_by_centralization(param)
    else:
        popsafe_list = ExpressDao.get_popsafe_express_count_by_logistics_id(param)
    _EXPRESS_.popsafe_express_count_signal.emit(popsafe_list[0]['count'])


def start_load_courier_popsend_express_count():
    ClientTools.get_global_pool().apply_async(load_courier_popsend_express_count)


def load_courier_popsend_express_count():
    __user = UserService.get_user()
    param = {'status': 'IN_STORE',
             'logisticsCompany_id': __user['company']['id'],
             'expressType': 'CUSTOMER_STORE',
             'storeUser_id': 'POPSEND'}
    __user_access = __user['user_role']['level_role']
    if __user_access == 1 or __user_access == 2 or __user_access == 3 or __user_access == 4 or __user_access == 5 or __user_access == 6:
        popsend_list = ExpressDao.get_popsend_express_count_by_manager(param)
    # elif __user_access == 7 or __user_access == 8 or __user_access == 9:
    #     popsend_list = ExpressDao.get_popsend_express_count_by_logistics_id(param)
    else:
        popsend_list = [{'count':0}]
    _EXPRESS_.popsend_express_count_signal.emit(popsend_list[0]['count'])


def start_courier_load_popsafe_express_list():
    ClientTools.get_global_pool().apply_async(courier_load_popsafe_express)
    _EXPRESS_.load_express_list_signal.emit('Success')


def courier_load_popsafe_express():
    try:
        __user = UserService.get_user()
        param = {'status': 'IN_STORE',
                 'overdueTime': ClientTools.now(),
                 'logisticsCompany_id': __user['company']['id'],
                 'groupName_1': 'POPDEPOSIT',
                 'groupName_2': 'POPTITIP'}

        __user_access = __user['user_role']['level_role']
        if __user_access == 1 or __user_access == 2 or __user_access == 3 or __user_access == 4 or __user_access == 5 or __user_access == 6:
            popsafe_express_list_ = ExpressDao.get_popsafe_express_by_manager(param)

        elif __user_access == 9:
            param = {'status': 'IN_STORE',
                    'overdueTime': ClientTools.now(),
                    'logisticsCompany_id': '161e5ed1140f11e5bdbd0242ac110001',
                    'groupName_1': 'POPDEPOSIT',
                    'groupName_2': 'POPTITIP'}
            # _LOG_.info("PARAMM LIST : " + str(param))
            popsafe_express_list_ = ExpressDao.get_popsafe_express_by_centralization(param)
        else:
            popsafe_express_list_ = ExpressDao.get_popsafe_express_by_logistics_id(param)

        for popsafe_express_ in popsafe_express_list_:
            popsafe_express_['mouth'] = box.service.BoxService.get_mouth(popsafe_express_)

        _EXPRESS_.overdue_express_list_signal.emit(json.dumps(popsafe_express_list_))
        _LOG_.debug(('courier_load_popsafe_express', popsafe_express_list_))

    except Exception as e:
        res = {}
        res['isSuccess'] = "false"
        res['message'] = str(e)
        _EXPRESS_.overdue_express_list_signal.emit(str(json.dumps(res)))
        popsafe_express_['mouth'] = box.service.BoxService.get_mouth(popsafe_express_)


def start_courier_load_overdue_express_list():
    ClientTools.get_global_pool().apply_async(courier_load_overdue_express)
    _EXPRESS_.load_express_list_signal.emit('Success')


def courier_load_overdue_express():
    try:
        __user = UserService.get_user()
        param = {'status': 'IN_STORE',
                 'overdueTime': ClientTools.now(),
                 'logisticsCompany_id': __user['company']['id'],
                 'expressType': 'COURIER_STORE',
                 'groupName_1': 'POPDEPOSIT',
                 'groupName_2': 'POPTITIP'}
        __user_access = __user['user_role']['level_role']
        if __user_access == 1 or __user_access == 2 or __user_access == 3 or __user_access == 4 or __user_access == 5 or __user_access == 6:
            overdue_express_list_ = ExpressDao.get_overdue_express_by_manager(param)

        elif __user_access == 9:
            param = {'status': 'IN_STORE',
                    'overdueTime': ClientTools.now(),
                    'logisticsCompany_id': '161e5ed1140f11e5bdbd0242ac110001',
                    'expressType': 'COURIER_STORE',
                    'groupName_1': 'POPDEPOSIT',
                    'groupName_2': 'POPTITIP'}
            # _LOG_.info("PARAMM LIST : " + str(param))
            overdue_express_list_ = ExpressDao.get_overdue_express_by_centralization(param)
        else:
            overdue_express_list_ = ExpressDao.get_overdue_express_by_logistics_id(param)

        # for overdue_express_ in overdue_express_list_:
        #     overdue_express_['mouth'] = box.service.BoxService.get_mouth(overdue_express_)

        _EXPRESS_.overdue_express_list_signal.emit(json.dumps(overdue_express_list_))
        _LOG_.debug(('courier_load_overdue_express', overdue_express_list_))

    except Exception as e:
        res = {}
        res['isSuccess'] = "false"
        res['message'] = str(e)
        _LOG_.warning(('[ERROR] courier_load_overdue_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.overdue_express_list_signal.emit(str(json.dumps(res)))


def start_staff_take_all_overdue_express():
    ClientTools.get_global_pool().apply_async(staff_take_all_overdue_express)


def staff_take_all_overdue_express():
    __user = UserService.get_user()
    __express_list = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        __param = {'status': 'IN_STORE',
                   'overdueTime': ClientTools.now(),
                   'expressType': 'COURIER_STORE',
                   'logisticsCompany_id': __user['company']['id']}
        __express_list = ExpressDao.get_all_overdue_express_by_logistics_id(__param)
    __user_access = __user['user_role']['level_role']
    if __user_access == 1 or __user_access == 2 or __user_access == 3 or __user_access == 4 or __user_access == 5 or __user_access == 6:
        __param = {'status': 'IN_STORE',
                   'overdueTime': ClientTools.now(),
                   'expressType': 'COURIER_STORE'}
        __express_list = ExpressDao.get_all_overdue_express_by_manager(__param)
    _LOG_.info('overdue_express_count:' + str(len(__express_list)))
    if len(__express_list) == 0:
        _EXPRESS_.staff_take_overdue_express_signal.emit('None')
        return
    for __express in __express_list:
        staff_take_overdue_express(__user, __express)
        time.sleep(2)

    _EXPRESS_.staff_take_overdue_express_signal.emit('Success')


def start_staff_take_overdue_express_list(express_id_list):
    ClientTools.get_global_pool().apply_async(staff_take_overdue_express_list, (express_id_list,))


def staff_take_overdue_express_list(express_id_list):
    _LOG_.info(('express_id_list :', express_id_list))
    res = {}
    try:
        doors = []
        __express_list = json.loads(express_id_list)
        if len(__express_list) == 0:
            res['isSuccess'] = "false"
            res['message'] = "Data cannot empty"
            _LOG_.info(('staff_take_overdue_express_list : false'))
            _EXPRESS_.staff_take_overdue_express_signal.emit(str(json.dumps(res)))
            return
        __user = UserService.get_user()
        for __express in __express_list:
            express_result_list = ExpressDao.get_express_by_id({'id': __express})
            express = express_result_list[0]
            box_service = box.service.BoxService.get_data_door(express['mouth_id'])
            door_info = {
                'size' : box_service['name'],
                'number' : box_service['number'],
                'id_express': express['id']
            }
            doors.append(door_info)
            _LOG_.info("express_result_list : " + str(express))
            staff_take_overdue_express(__user, express)
            time.sleep(2)

        res['isSuccess'] = "true"
        res['door'] = doors
        _LOG_.info(('staff_take_overdue_express_list : true'))
        _EXPRESS_.staff_take_overdue_express_signal.emit(str(json.dumps(res)))
    except Exception as e:
        res['isSuccess'] = "false"
        res['message'] = str(e)
        _EXPRESS_.staff_take_overdue_express_signal.emit(str(json.dumps(res)))



def staff_take_overdue_express(__user, express):
    try:
        express['takeTime'] = ClientTools.now()
        _LOG_.info("USER ROLE : " + str(__user['role']))
        if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
            express['status'] = 'COURIER_TAKEN'
        if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
            express['status'] = 'OPERATOR_TAKEN'
        express['staffTakenUser_id'] = __user['id']
        express['version'] += 1
        express['syncFlag'] = 0
        express['recipientName'] = ""
        ExpressDao.take_express(express)
        box.service.BoxService.free_mouth({'id': express['mouth_id']})
        box.service.BoxService.open_mouth(express['mouth_id'])
        express['staffTakenUser'] = {'id': express['staffTakenUser_id']}
        ClientTools.get_global_pool().apply_async(sync_staff_take_overdue_express, (express,))
    except Exception as e:
        res = {}
        res['isSuccess'] = "false"
        res['err_message'] = str(e)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] staff_take_overdue_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.staff_take_overdue_express_signal.emit(str(json.dumps(res)))


def sync_staff_take_overdue_express(express):
    message, status_code = HttpClient.post_message('express/staffTakeOverdueExpress', express)
    print('response-take:', str(message))
    if status_code == 200 and message['id'] == express['id']:
        ExpressDao.mark_sync_success(express)


def get_scan_not_sync_express_list():
    return ExpressDao.get_not_sync_express_list({'syncFlag': 0})


def mark_sync_success(express_):
    ExpressDao.mark_sync_success(express_)


calculate_customer_express_cost_flag = False


def stop_calculate_customer_express_cost():
    global calculate_customer_express_cost_flag
    if calculate_customer_express_cost_flag:
        calculate_customer_express_cost_flag = False


customer_express_weight = 0
customer_express_cost = 0


def calculate_customer_express_cost(weight):
    global customer_express_weight
    global customer_express_cost
    if not calculate_customer_express_cost_flag:
        return
    customer_express_weight = weight
    range_price_ = customer_store_express
    cost = 0
    cost += range_price_['firstPrice']
    continued_weight = weight - range_price_['firstHeavy']
    if continued_weight < 0:
        continued_weight = 0
    cost += math.ceil(continued_weight / range_price_['continuedHeavy']) * range_price_['continuedPrice']
    cost_result_ = {'heavy': weight, 'cost': cost}
    customer_express_cost = cost
    _EXPRESS_.customer_store_express_cost_signal.emit(json.dumps(cost_result_))


def start_pull_pre_pay_cash_for_customer_express(express_cost):
    global customer_express_cost
    customer_express_cost = express_cost


customer_paid_amount = 0
calculate_customer_reject_express_cost_flag = False


def stop_calculate_customer_reject_express_cost():
    global calculate_customer_reject_express_cost_flag
    if calculate_customer_reject_express_cost_flag:
        calculate_customer_reject_express_cost_flag = False


customer_reject_express_weight = 0
customer_reject_express_cost = 0


def calculate_customer_reject_express_cost(weight):
    global customer_reject_express_cost
    global customer_reject_express_weight
    if not calculate_customer_reject_express_cost_flag:
        return
    customer_reject_express_weight = weight
    range_price_ = customer_reject_express
    cost = 0
    cost += range_price_['firstPrice']
    continued_weight = weight - range_price_['firstHeavy']
    if continued_weight < 0:
        continued_weight = 0
    cost += math.ceil(continued_weight / range_price_['continuedHeavy']) * range_price_['continuedPrice']
    cost_result_ = {'heavy': weight, 'cost': cost}
    customer_reject_express_cost = cost
    _EXPRESS_.customer_store_express_cost_signal.emit(json.dumps(cost_result_))


def start_pull_pre_pay_cash_for_customer_reject_express(express_cost):
    global customer_reject_express_cost
    customer_reject_express_cost = express_cost


store_customer_express_flag = False


def start_store_customer_express():
    global store_customer_express_flag
    _LOG_.info(("store_customer_express_flag : ", str(store_customer_express_flag)))
    if not store_customer_express_flag:
        _LOG_.info(("store_customer_express_flag_next : ", str(store_customer_express_flag)))

        store_customer_express_flag = True
        ClientTools.get_global_pool().apply_async(store_customer_express)

popsend_data = customer_store_express


def store_customer_express():
    global store_customer_express_flag
    try:
        _LOG_.info(("[popsend_data] : ", str(popsend_data)))
        box_info = box.service.BoxService.get_box()
        customer_store_express['box_id'] = box_info['id']
        customer_store_express['logisticsCompany_id'] = customer_store_express['logisticsCompany']['id']
        mouth_result = box.service.BoxService.mouth
        mouth_param = {'id': mouth_result['id'],
                    'express_id': customer_store_express['id'],
                    'status': 'USED'}
        box.service.BoxService.use_mouth(mouth_param)
        customer_store_express['mouth'] = mouth_result
        customer_store_express['mouth_id'] = mouth_result['id']
        customer_store_express['operator_id'] = box_info['operator_id']
        customer_store_express['storeUser_id'] = "POPSEND"
        customer_store_express['weight'] = customer_express_weight
        customer_store_express['storeTime'] = ClientTools.now()
        customer_store_express['chargeType'] = customer_store_express['chargeType']
        store_to_db = ExpressDao.save_customer_express(customer_store_express)
        res = {}
        if store_to_db is True:
            _LOG_.info(('[SUCCESS] store popsend/ondemand express data to DB', str(store_to_db)))

            res['isSuccess'] = "true"
            res['id_express'] = customer_store_express['id']
            res['expressNumber'] = customer_store_express['customerStoreNumber']
            _EXPRESS_.store_customer_express_result_signal.emit(str(json.dumps(res)))
            record_list = ExpressDao.get_record({'express_id': customer_store_express['id']})
            if len(record_list) == 1:
                record = record_list[0]
                record_param = {'id': record['id'],
                                'createTime': record['createTime'],
                                'amount': record['amount']}
                customer_store_express['transactionRecords'] = [record_param]
            customer_store_express['box'] = {'id': customer_store_express['box_id']}
            _LOG_.debug(("customer_store_express : ", str(customer_store_express)))
            message, status_code = HttpClient.post_message('express/customerStoreExpress', customer_store_express)
            if status_code == 200 and message['id'] == customer_store_express['id']:
                ExpressDao.mark_sync_success(customer_store_express)
            store_customer_express_flag = False

        else:
            res['isSuccess'] = "false"
            _LOG_.info(('[ERROR] store popsend/ondemand data to DB', str(store_to_db)))
            _EXPRESS_.store_customer_express_result_signal.emit(str(json.dumps(res)))



    except Exception as e:
        res = {}
        res['isSuccess'] = "false"
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] store popsend/ondemand : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.store_customer_express_result_signal.emit(str(json.dumps(res)))



store_customer_reject_express_flag = False


def start_store_customer_reject_express():
    global store_customer_reject_express_flag
    if not store_customer_reject_express_flag:
        store_customer_reject_express_flag = True
        ClientTools.get_global_pool().apply_async(store_customer_reject_express)


def store_customer_reject_express():
    global store_customer_reject_express_flag
    customer_reject_express['box_id'] = box.service.BoxService.get_box()['id']
    customer_reject_express['logisticsCompany_id'] = customer_reject_express['logisticsCompany']['id']
    mouth_result = box.service.BoxService.mouth
    mouth_param = {'id': mouth_result['id'],
                   'express_id': customer_reject_express['id'],
                   'status': 'USED'}
    box.service.BoxService.use_mouth(mouth_param)
    customer_reject_express['mouth'] = mouth_result
    customer_reject_express['mouth_id'] = mouth_result['id']
    customer_reject_express['operator_id'] = box.service.BoxService.get_box()['operator_id']
    customer_reject_express['storeUser_id'] = ClientTools.get_value('id', ClientTools.get_value('storeUser',
                                                                                                customer_reject_express,
                                                                                                {}))
    customer_reject_express['weight'] = customer_reject_express_weight
    customer_reject_express['endAddress'] = ClientTools.get_value('endAddress', customer_reject_express, None)
    customer_reject_express['startAddress'] = ClientTools.get_value('startAddress', customer_reject_express, None)
    customer_reject_express['recipientName'] = ClientTools.get_value('recipientName', customer_reject_express, None)
    customer_reject_express['recipientUserPhoneNumber'] = ClientTools.get_value('recipientUserPhoneNumber',
                                                                                customer_reject_express, None)
    customer_reject_express['storeTime'] = ClientTools.now()
    customer_reject_express['chargeType'] = customer_reject_express['chargeType']
    ExpressDao.save_customer_express(customer_reject_express)
    _EXPRESS_.store_customer_express_result_signal.emit('Success')
    record_list = ExpressDao.get_record({'express_id': customer_reject_express['id']})
    if len(record_list) == 1:
        record = record_list[0]
        record_param = {'id': record['id'],
                        'createTime': record['createTime'],
                        'amount': record['amount']}
        customer_reject_express['transactionRecords'] = [record_param]
    customer_reject_express['box'] = {'id': customer_reject_express['box_id']}
    message, status_code = HttpClient.post_message('express/customerRejectExpress', customer_reject_express)
    if status_code == 200 and message['id'] == customer_reject_express['id']:
        ExpressDao.mark_sync_success(customer_reject_express)
    store_customer_reject_express_flag = False


def start_get_customer_express_cost():
    ClientTools.get_global_pool().apply_async(get_customer_express_cost)


def get_customer_express_cost():
    _EXPRESS_.customer_express_cost_insert_coin_signal.emit(str(math.ceil(customer_express_cost / 100)))


def start_get_customer_reject_express_cost():
    ClientTools.get_global_pool().apply_async(get_customer_reject_express_cost)


def get_customer_reject_express_cost():
    _EXPRESS_.customer_express_cost_insert_coin_signal.emit(
        str(math.ceil(customer_reject_express_cost / 100)))


def get_scan_sync_express_transaction_record(param):
    return ExpressDao.get_record(param)


def start_load_customer_send_express_count():
    ClientTools.get_global_pool().apply_async(load_customer_send_express_count)


def load_customer_send_express_count():
    __user = UserService.get_user()
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_STORE',
                 'logisticsCompany_id': __user['company']['id']}
        count_list = ExpressDao.get_send_express_count_by_logistics_id(param)
        _EXPRESS_.send_express_count_signal.emit(count_list[0]['count'])
    elif __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_STORE'}
        count_list = ExpressDao.get_send_express_count_by_operator(param)
        _EXPRESS_.send_express_count_signal.emit(count_list[0]['count'])


def start_load_customer_reject_express_count():
    ClientTools.get_global_pool().apply_async(load_customer_reject_express_count)


def load_customer_reject_express_count():
    __user = UserService.get_user()
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_REJECT',
                 'logisticsCompany_id': __user['company']['id']}
        count_list = ExpressDao.get_send_express_count_by_logistics_id(param)
        _EXPRESS_.send_express_count_signal.emit(count_list[0]['count'])
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_REJECT'}
        count_list = ExpressDao.get_send_express_count_by_operator(param)
        _EXPRESS_.send_express_count_signal.emit(count_list[0]['count'])


def get_send_amount(express__):
    return ExpressDao.get_mouth(express__)


def start_customer_load_send_express_list(page):
    ClientTools.get_global_pool().apply_async(customer_load_send_express, (page,))
    _EXPRESS_.load_express_list_signal.emit('Success')


def customer_load_send_express(page):
    __user = UserService.get_user()
    _LOG_.debug(('customer_load_send_express __user is : ', __user))
    send_express_list_ = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_STORE',
                 'logisticsCompany_id': __user['company']['id'],
                 'startLine': (int(page) - 1) * 5}
        send_express_list_ = ExpressDao.get_send_express_by_logistics_id(param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_STORE',
                 'startLine': (int(page) - 1) * 5}
        send_express_list_ = ExpressDao.get_send_express_by_operator(param)
    for send_express_ in send_express_list_:
        send_express_['mouth'] = box.service.BoxService.get_mouth(send_express_)
        send_express_['amount'] = get_send_amount(send_express_)

    _EXPRESS_.send_express_list_signal.emit(json.dumps(send_express_list_))


def start_customer_load_reject_express_list(page):
    ClientTools.get_global_pool().apply_async(customer_load_reject_express, (page,))
    _EXPRESS_.load_express_list_signal.emit('Success')


def customer_load_reject_express(page):
    __user = UserService.get_user()
    _LOG_.debug(('customer_load_reject_express __user is : ', __user))
    send_express_list_ = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_REJECT',
                 'logisticsCompany_id': __user['company']['id'],
                 'startLine': (int(page) - 1) * 5}
        send_express_list_ = ExpressDao.get_send_express_by_logistics_id(param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_REJECT',
                 'startLine': (int(page) - 1) * 5}
        send_express_list_ = ExpressDao.get_send_express_by_operator(param)
    for send_express_ in send_express_list_:
        send_express_['mouth'] = box.service.BoxService.get_mouth(send_express_)
        send_express_['amount'] = get_send_amount(send_express_)
        send_express_['electronicCommerce'] = \
        CompanyDao.get_company_by_id({'id': send_express_['electronicCommerce_id']})[0]

    _EXPRESS_.reject_express_list_signal.emit(json.dumps(send_express_list_))


def start_staff_take_all_send_express():
    ClientTools.get_global_pool().apply_async(staff_take_all_send_express)


def staff_take_all_send_express():
    __user = UserService.get_user()
    __express_list = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        __param = {'status': 'IN_STORE',
                   'expressType': 'CUSTOMER_STORE',
                   'logisticsCompany_id': __user['company']['id']}
        __express_list = ExpressDao.get_all_send_express_by_logistics_id(__param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        __param = {'status': 'IN_STORE',
                   'expressType': 'CUSTOMER_STORE',
                   'overdueTime': ClientTools.now()}
        __express_list = ExpressDao.get_all_send_express_by_manager(__param)
    _LOG_.info('send_express_count:' + str(len(__express_list)))
    if len(__express_list) == 0:
        _EXPRESS_.staff_take_send_express_signal.emit('None')
        return
    for __express in __express_list:
        staff_take_send_express(__user, __express)

    _EXPRESS_.staff_take_send_express_signal.emit('Success')


def start_staff_take_send_express_list(send_express_id_list):
    ClientTools.get_global_pool().apply_async(staff_take_send_express_list, (send_express_id_list,))


def staff_take_send_express_list(send_express_id_list):
    _LOG_.info(('express_id_list:', send_express_id_list))
    res = {}
    try:
        __express_list = json.loads(send_express_id_list)
        if len(__express_list) == 0:
            res['isSuccess'] = "false"
            _EXPRESS_.staff_take_send_express_signal.emit(str(json.dumps(res)))
            return
        __user = UserService.get_user()
        door = []
        for __express in __express_list:
            express_result_list = ExpressDao.get_express_by_id({'id': __express})

            express = express_result_list[0]
            box_service = box.service.BoxService.get_data_door(express['mouth_id'])
            print("data_box_service",box_service['number'])
            door_info = {
                'size' : box_service['name'],
                'number' : box_service['number'],
                'id_express': express['id']
            }
            door.append(door_info)
            staff_take_send_express(__user, express)

        res['isSuccess'] = "true"
        res['door'] = door
        _EXPRESS_.staff_take_send_express_signal.emit(str(json.dumps(res)))
        return
    except Exception as e:
        res['isSuccess'] = "false"
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] staff_take_send_express_list : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.staff_take_send_express_signal.emit(str(json.dumps(res)))
        return


def staff_take_send_express(__user, express):
    express['takeTime'] = ClientTools.now()
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        express['status'] = 'COURIER_TAKEN'
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        express['status'] = 'OPERATOR_TAKEN'
    express['staffTakenUser_id'] = __user['id']
    express['version'] += 1
    express['syncFlag'] = 0
    express['recipientName'] = ""
    ExpressDao.take_express(express)
    box.service.BoxService.free_mouth({'id': express['mouth_id']})
    box.service.BoxService.open_mouth(express['mouth_id'])
    express['staffTakenUser'] = {'id': express['staffTakenUser_id']}
    ClientTools.get_global_pool().apply_async(sync_staff_take_send_express, (express,))


def sync_staff_take_send_express(express):
    message, status_code = HttpClient.post_message('express/staffTakeUserSendExpress', express)
    if status_code == 200 and message['id'] == express['id']:
        ExpressDao.mark_sync_success(express)


def start_staff_take_all_reject_express():
    ClientTools.get_global_pool().apply_async(staff_take_all_reject_express)


def staff_take_all_reject_express():
    __user = UserService.get_user()
    __express_list = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        __param = {'status': 'IN_STORE',
                   'expressType': 'CUSTOMER_REJECT',
                   'logisticsCompany_id': __user['company']['id']}
        __express_list = ExpressDao.get_all_send_express_by_logistics_id(__param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        __param = {'status': 'IN_STORE',
                   'expressType': 'CUSTOMER_REJECT',
                   'overdueTime': ClientTools.now()}
        __express_list = ExpressDao.get_all_send_express_by_manager(__param)
    _LOG_.info('reject_express_count:' + str(len(__express_list)))
    if len(__express_list) == 0:
        _EXPRESS_.staff_take_send_express_signal.emit('None')
        return
    for __express in __express_list:
        staff_take_reject_express(__user, __express)

    _EXPRESS_.staff_take_send_express_signal.emit('Success')


def start_staff_take_reject_express_list(reject_express_id_list):
    ClientTools.get_global_pool().apply_async(staff_take_reject_express_list, (reject_express_id_list,))


def staff_take_reject_express_list(reject_express_id_list):
    _LOG_.info(('express_id_list:', reject_express_id_list))
    res = {}
    try:
        __express_list = json.loads(reject_express_id_list)
        if len(__express_list) == 0:
            _EXPRESS_.staff_take_send_express_signal.emit(str(json.dumps(res)))
            return
        __user = UserService.get_user()
        door = []
        for __express in __express_list:
            res['isSuccess'] = "false"
            express_result_list = ExpressDao.get_express_by_id({'id': __express})
            express = express_result_list[0]
            box_service = box.service.BoxService.get_data_door(express['mouth_id'])
            print("data_box_service",box_service['number'])
            door_info = {
                'size' : box_service['name'],
                'number' : box_service['number'],
                'id_express': express['id']
            }
            door.append(door_info)
            staff_take_reject_express(__user, express)

        res['isSuccess'] = "true"
        res['door'] = door
        _EXPRESS_.staff_take_send_express_signal.emit(str(json.dumps(res)))
        return
    except Exception as e:
        res['isSuccess'] = "false"
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] staff_take_reject_express_list : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.staff_take_send_express_signal.emit(str(json.dumps(res)))
        return


def staff_take_reject_express(__user, express):
    express['takeTime'] = ClientTools.now()
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        express['status'] = 'COURIER_TAKEN'
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        express['status'] = 'OPERATOR_TAKEN'
    express['staffTakenUser_id'] = __user['id']
    express['version'] += 1
    express['syncFlag'] = 0
    express['recipientName'] = ""
    ExpressDao.take_express(express)
    box.service.BoxService.free_mouth({'id': express['mouth_id']})
    box.service.BoxService.open_mouth(express['mouth_id'])
    express['staffTakenUser'] = {'id': express['staffTakenUser_id']}
    ClientTools.get_global_pool().apply_async(sync_staff_take_reject_express, (express,))


def sync_staff_take_reject_express(express):
    message, status_code = HttpClient.post_message('express/staffTakeUserRejectExpress', express)
    if status_code == 200 and message['id'] == express['id']:
        ExpressDao.mark_sync_success(express)


def start_service_pull_store_express(express_massage, timestamp=0):
    _LOG_.info(('Server_pull timestamp is ', timestamp))
    if ClientTools.now() - timestamp >= 30000:
        return
    service_pull_store_express(express_massage)


def service_pull_store_express(express_massage):
    _LOG_.info(('service_pull_store_express_info:', express_massage))
    param = {'id': express_massage['mouth']['box']['id'],
             'deleteFlag': 0}
    pull_box_info = BoxDao.get_box_by_box_id(param)
    if len(pull_box_info) != 1:
        # logger.debug('bad box_id')
        return False
    check_free_mouth_param = {'id': express_massage['mouth']['id'],
                              'status': 'ENABLE'}
    # logger.debug('start BoxDao')
    pull_mouth_result = BoxDao.get_free_mouth_by_id(check_free_mouth_param)
    if len(pull_mouth_result) != 1:
        # logger.debug('no free mouth')
        return False
    box.service.BoxService.pull_open_mouth(express_massage['mouth']['id'])
    # logger.debug('opened')
    express_param = {'id': express_massage['id'],
                     'expressNumber': express_massage['expressNumber'],
                     'expressType': express_massage['expressType'],
                     'overdueTime': express_massage['overdueTime'],
                     'status': express_massage['status'],
                     'storeTime': express_massage['storeTime'],
                     'syncFlag': 1, 'takeUserPhoneNumber': express_massage['takeUserPhoneNumber'],
                     'validateCode': express_massage['validateCode'],
                     'version': 0, 'box_id': express_massage['mouth']['box']['id'],
                     'logisticsCompany_id': express_massage['logisticsCompany']['id'],
                     'mouth_id': express_massage['mouth']['id'],
                     'operator_id': pull_box_info[0]['operator_id'],
                     'storeUser_id': express_massage['storeUser']['id'],
                     'groupName': ClientTools.get_value('groupName', express_massage),
                     'transactionRecord': '',
                     'drop_by_courier_login': '',
                     'payment_param': '',
                     'flagRedrop': 0,
                     'chargeType': ''}
    _LOG_.debug(('express_param is :', express_param))
    ExpressDao.save_express(express_param)
    # logger.debug('save express end')
    mouth_param = {'id': express_massage['mouth']['id'],
                   'express_id': express_massage['id'],
                   'status': 'USED'}
    box.service.BoxService.use_mouth(mouth_param)
    # logger.debug('use_mouth end')
    return True


def start_reset_express(express_message):
    ClientTools.get_global_pool().apply_async(reset_express, (express_message,))


def reset_express(express_message):

    try:
        _LOG_.debug("reset_express : " + str(express_message))
        param = {'id': express_message['box']['id'],
                'deleteFlag': 0}
        pull_box_info = BoxDao.get_box_by_box_id(param)
        if len(pull_box_info) != 1:
            reset_express_post_message(express_message['id'], 'ERROR BOX_ID', 'ERROR')
            return
        express_info_list = ExpressDao.get_express_by_id({'id': express_message['express']['id']})
        if len(express_info_list) != 1:
            reset_express_post_message(express_message['id'], 'NO SUCH EXPRESS', 'ERROR')
            return
        express_info = express_info_list[0]
        if express_message['express']['overdueTime'] <= express_message['express']['storeTime']:
            reset_express_post_message(express_message['id'], 'OVERDUE TIME ERROR', 'ERROR')
            return
        
        express_param = {'id': express_message['express']['id'],
                        'overdueTime': express_message['express']['overdueTime'],
                        'storeTime': express_message['express']['storeTime'],
                        'status': express_message['express']['status'],
                        'syncFlag': 0,
                        'takeTime': None,
                        'takeUserPhoneNumber': express_message['express']['takeUserPhoneNumber'],
                        'validateCode': express_message['express']['validateCode'],
                        'version': express_info['version'] + 1}
        
        if express_message['express']['status'] == "CUSTOMER_TAKEN":
            express_param['takeTime'] = express_message['express']['takeTime']
        
        if express_info['status'] == 'IN_STORE':
            # logger.debug('the express in box')
            express_param['lastModifiedTime'] = ClientTools.now()
            ExpressDao.reset_express(express_param)
            if express_message['express']['status'] == "CUSTOMER_TAKEN":
                mouth = express_message['express']['mouth']
                param = {
                    'status': 'ENABLE',
                    'id': mouth['id']
                }
                BoxDao.update_mouth_status( param )
            reset_express_post_message(express_message['id'], 'IN_STORE EXPRESS RESET DONE', 'SUCCESS')
            return
        if express_info['status'] == 'CUSTOMER_TAKEN':
            # logger.debug('the express taken by customer')
            mouth_info_list = BoxDao.get_mouth_by_id({'id': express_message['express']['mouth']['id']})
            if len(mouth_info_list) != 1:
                reset_express_post_message(express_message['id'], 'MOUTH_ID ERROR', 'ERROR')
                return
            mouth_info = mouth_info_list[0]
            if mouth_info['status'] != 'ENABLE':
                reset_express_post_message(express_message['id'], 'MOUTH_STATUS ERROR', 'ERROR')
                return
            if mouth_info['status'] == 'ENABLE':
                mouth_param = {'status': 'USED',
                            'id': mouth_info['id'],
                            'express_id': express_message['express']['id']}
                BoxDao.use_mouth(mouth_param)
                express_param['lastModifiedTime'] = ClientTools.now()
                ExpressDao.reset_express(express_param)
                reset_express_post_message(express_message['id'], 'CUSTOMER_TAKEN EXPRESS DONE', 'SUCCESS')
                return
        if express_info['status'] == 'OPERATOR_TAKEN' or express_info['status'] == 'COURIER_TAKEN':
            # logger.debug('the express taken by staff')
            mouth_info_list = BoxDao.get_mouth_by_id({'id': express_message['express']['mouth']['id']})
            if len(mouth_info_list) != 1:
                reset_express_post_message(express_message['id'], 'MOUTH_ID ERROR', 'ERROR')
                return
            mouth_info = mouth_info_list[0]
            if mouth_info['status'] != 'ENABLE':
                reset_express_post_message(express_message['id'], 'MOUTH_STATUS ERROR', 'ERROR')
                return
            if mouth_info['status'] == 'ENABLE':
                mouth_param = {'status': 'USED',
                            'id': mouth_info['id'],
                            'express_id': express_message['express']['id']}
                BoxDao.use_mouth(mouth_param)
                express_param['lastModifiedTime'] = ClientTools.now()
                ExpressDao.reset_express(express_param)
                reset_express_post_message(express_message['id'], 'STAFF_TAKEN EXPRESS DONE', 'SUCCESS')
                return
        reset_express_post_message(express_message['id'], 'EXPRESS STATUS ERROR', 'ERROR')
    except Exception as e:
        _LOG_.debug(('reset_express ERROR :', e))
        reset_express_post_message(express_message['id'], 'EXPRESS STATUS ERROR', 'ERROR')


def reset_express_post_message(task_id, result, reset_status):
    reset_express_result = {'id': task_id,
                            'result': result,
                            'statusType': reset_status}
    HttpClient.post_message('task/finish', reset_express_result)
    # logger.debug("reset_express result : ", str(reset_express_result))


reject_merchant_name = ''


def start_customer_reject_select_merchant(merchant_name):
    global reject_merchant_name
    reject_merchant_name = merchant_name


electronic_commerce_reject_number = ''
electronic_reject_express = ''


def start_customer_reject_for_electronic_commerce(barcode):
    global electronic_commerce_reject_number
    electronic_commerce_reject_number = barcode
    ClientTools.get_global_pool().apply_async(customer_reject_for_electronic_commerce)


def customer_reject_for_electronic_commerce():
    global electronic_reject_express
    message, status_code = HttpClient.get_message('express/reject/checkRule/' + electronic_commerce_reject_number + '?type=' + reject_merchant_name)
    if status_code == 200:
        if ClientTools.get_value("statusCode", message) == 404:
            _EXPRESS_.customer_reject_express_signal.emit('False')
        else:
            electronic_reject_express = message
            electronic_reject_express['chargeType'] = 'NOT_CHARGE'
            electronic_reject_express['package_id'] = electronic_commerce_reject_number
            _EXPRESS_.customer_reject_express_signal.emit(str(json.dumps(electronic_reject_express)))
    else:
        _EXPRESS_.customer_reject_express_signal.emit('False')


def start_get_electronic_commerce_reject_express():
    ClientTools.get_global_pool().apply_async(get_electronic_commerce_reject_express)


def get_electronic_commerce_reject_express():
    global electronic_reject_express
    box_info = box.service.BoxService.get_box()
    electronic_reject_express['phone_number'] = phone_number
    electronic_reject_express['box_name'] = box_info['name']
    _LOG_.debug("reject_express_return : " + str(electronic_reject_express))
    _EXPRESS_.reject_express_signal.emit(str(json.dumps(electronic_reject_express)))


store_customer_reject_for_electronic_commerce_flag = False


def start_store_customer_reject_for_electronic_commerce():
    global store_customer_reject_for_electronic_commerce_flag
    if not store_customer_reject_for_electronic_commerce_flag:
        store_customer_reject_for_electronic_commerce_flag = True
        ClientTools.get_global_pool().apply_async(store_customer_reject_for_ecommerce)


def store_customer_reject_for_ecommerce():
    res = {}
    try:
        global store_customer_reject_for_electronic_commerce_flag
        global electronic_reject_express
        box_info = box.service.BoxService.get_box()

        _LOG_.info(('[electronic_reject_express]', electronic_reject_express))
        reject_express = dict()

        if electronic_reject_express == "":
            param = {
                'merchant_name': reject_merchant_name
            }
            data = ReturnRulesDao.get_data_rules(param)
            if data is not None:
                electronic_reject_express = data
                electronic_reject_express['groupName'] = data['groupName']
                electronic_reject_express['id'] = ClientTools.get_uuid()
                electronic_reject_express['chargeType'] = 'NOT_CHARGE'
                electronic_reject_express['package_id'] = electronic_commerce_reject_number
                electronic_reject_express['logisticsCompany'] = {
                    'id': data['logistic_id']
                }
                electronic_reject_express['electronicCommerce'] = {
                    'id': data['ecommerce_id'],
                    'name': data['groupName']
                }
            else:
                return_not_exist = {
                    'id': ClientTools.get_uuid(),
                    'logisticsCompany' : {
                        'id': '161e5ed1140f11e5bdbd0242ac110001'
                    },
                    'electronicCommerce' : {
                        'id': ClientTools.get_uuid(),
                        'name': reject_merchant_name
                    },
                    'groupName': reject_merchant_name,
                    'chargeType': 'NOT_CHARGE',
                    'package_id': electronic_commerce_reject_number,
                    'storeUserPhoneNumber': phone_number

                }
                electronic_reject_express = return_not_exist

        print('DATA_RETURN_AFTER_RE_ASSIGNMENT', str(electronic_reject_express))

        if ClientTools.get_value('id', electronic_reject_express) is not None:
            reject_express['id'] = electronic_reject_express['id']
        else:
            reject_express['id'] = ClientTools.get_uuid()
        reject_express['box_id'] = box_info['id']
        reject_express['logisticsCompany_id'] = electronic_reject_express['logisticsCompany']['id']
        reject_express['groupName'] = ClientTools.get_value('groupName', electronic_reject_express, reject_merchant_name)
        mouth_result = box.service.BoxService.mouth
        _LOG_.debug(('[MOUTH_RESULT_store_customer_reject_for_ecommerce]', mouth_result))
        mouth_param = {'id': mouth_result['id'],
                    'express_id': reject_express['id'],
                    'status': 'USED'}
        box.service.BoxService.use_mouth(mouth_param)
        reject_express['mouth'] = mouth_result
        reject_express['mouth_id'] = mouth_result['id']
        reject_express['operator_id'] = box_info['operator_id']
        reject_express['storeUser_id'] = 'C_' + reject_express['id']
        reject_express['endAddress'] = reject_express['groupName'] + "_WAREHOUSE"
        reject_express['startAddress'] = 'PopBox @ ' + box_info['name']
        reject_express['recipientName'] = reject_express['groupName'] + "_CUSTOMER"
        reject_express['weight'] = 0
        if ClientTools.get_value('storeUserPhoneNumber', electronic_reject_express) is not None:
            reject_express['storeUserPhoneNumber'] = electronic_reject_express['storeUserPhoneNumber']
        else:
            reject_express['storeUserPhoneNumber'] = phone_number
        reject_express['storeTime'] = ClientTools.now()
        reject_express['chargeType'] = electronic_reject_express['chargeType']
        reject_express['customerStoreNumber'] = electronic_commerce_reject_number
        reject_express['expressType'] = 'CUSTOMER_REJECT'
        reject_express['electronicCommerce_id'] = electronic_reject_express['electronicCommerce']['id']

        store_express_db = ExpressDao.save_customer_reject_express(reject_express)

        if store_express_db is True:
            _LOG_.info(('[SUCCESS] store reject express data to DB', electronic_commerce_reject_number))

            company_list = CompanyDao.get_company_by_id({'id': reject_express['electronicCommerce_id']})
            electronic_commerce = {
                'id': electronic_reject_express['electronicCommerce']['id'],
                'companyType': 'ELECTRONIC_COMMERCE',
                'name': electronic_reject_express['electronicCommerce']['name'],
                'deleteFlag': 0,
                'parentCompany_id': ""
            }
            if len(company_list) == 0:
                CompanyDao.insert_company(electronic_commerce)
            else:
                CompanyDao.update_company(electronic_commerce)

            reject_express['box'] = {'id': reject_express['box_id']}
            reject_express['logisticsCompany'] = electronic_reject_express['logisticsCompany']
            reject_express['electronicCommerce'] = electronic_reject_express['electronicCommerce']
            _LOG_.debug("reject_express : " + str(reject_express))
            message, status_code = HttpClient.post_message('express/rejectExpressNotImported', reject_express)
            if status_code == 200 and message['id'] == reject_express['id']:
                ExpressDao.mark_sync_success(reject_express)
                res['isSuccess']="true"
                res['id_express'] = reject_express['id']
                res['expressNumber'] = reject_express['customerStoreNumber']
                _EXPRESS_.store_customer_express_result_signal.emit(str(json.dumps(res)))
            else:
                res['isSuccess']="false"
                _EXPRESS_.store_customer_express_result_signal.emit(str(json.dumps(res)))

            store_customer_reject_for_electronic_commerce_flag = False
        else:
            res['isSuccess'] = "false"
            _LOG_.warning(('[ERROR_REJECT_EXPRESS]', str(store_express_db)))
            _EXPRESS_.store_customer_express_result_signal.emit(str(json.dumps(res)))


    except Exception as e:
        res['isSuccess'] = "false"
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] store_customer_reject_for_ecommerce : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.store_customer_express_result_signal.emit(str(json.dumps(res)))
        pass


def start_get_product_file():
    ClientTools.get_global_pool().apply_async(get_product_file)


def get_product_file():
    box_info = box.service.BoxService.get_box()
    _EXPRESS_.product_file_signal.emit(box_info['orderNo'])


other_service_scanner_signal_connect_flag = False


def start_get_express_info_by_barcode():
    global other_service_scanner_signal_connect_flag
    if not other_service_scanner_signal_connect_flag:
        Scanner._SCANNER_.barcode_result.connect(get_express_info)
        other_service_scanner_signal_connect_flag = True
    Scanner.start_get_text_info()


def stop_get_express_info_by_barcode():
    global other_service_scanner_signal_connect_flag
    if other_service_scanner_signal_connect_flag:
        Scanner._SCANNER_.barcode_result.disconnect(get_express_info)
        other_service_scanner_signal_connect_flag = False
    Scanner.start_stop_scanner()


global_express_info = ''


def start_get_express_info(express_text):
    ClientTools.get_global_pool().apply_async(get_express_info, (express_text,))


def get_express_info(text):
    global pakpobox_order_number
    global global_express_info
    if text == '':
        return
    global_express_info = ''
    pakpobox_info = dict()
    pakpobox_info['token'] = Configurator.get_value('popbox', 'Token')
    start_url = Configurator.get_value('popbox', 'serveraddress')
    pakpobox_order_number = text
    pakpobox_info['order_number'] = text
    express_message, status_code = HttpClient.pakpobox_get_message(start_url, pakpobox_info)
    if status_code == 200:
        if express_message['response']['message'] == 'AVAILABLE':
            if len(express_message['data'][0]) != 0:
                _LOG_.debug(('get_express_info express_message: ', express_message['data']))
                global_express_info = express_message['data'][0]
                QP3000S.price = express_message['data'][0]['order_amount']
                _EXPRESS_.start_get_express_info_result_signal.emit('success')
            else:
                _EXPRESS_.start_get_express_info_result_signal.emit('Error')
        elif express_message['response']['message'] == 'NOT AVAILABLE':
            _EXPRESS_.start_get_express_info_result_signal.emit('Error')
        elif express_message['response']['message'] == 'PAID':
            if len(express_message['data'][0]) != 0:
                _LOG_.debug(('get_express_info express_message: ', express_message['data']))
                global_express_info = express_message['data'][0]
                _EXPRESS_.start_get_express_info_result_signal.emit('PAID')
            else:
                _EXPRESS_.start_get_express_info_result_signal.emit('Error')
    else:
        _EXPRESS_.start_get_express_info_result_signal.emit('Error')


def start_get_pakpobox_express_info():
    ClientTools.get_global_pool().apply_async(get_pakpobox_express_info)


def get_pakpobox_express_info():
    _EXPRESS_.start_get_pakpobox_express_info_signal.emit(json.dumps(global_express_info))


def start_get_customer_info_by_card():
    ClientTools.get_global_pool().apply_async(get_customer_info_by_card)


def get_customer_info_by_card():
    global card_info
    _LOG_.debug('start_get_customer_info_by_card')
    flag = QP3000S.init_serial(x=1)
    _LOG_.debug(('start_get_customer_info_by_card flag: ', flag))
    if flag:
        card_info = QP3000S.balanceInfo()
        _LOG_.debug(('start_get_customer_info_by_card customer_card_amount: ', card_info))
    else:
        card_info = '0'
    _EXPRESS_.start_get_customer_info_by_card_signal.emit(int(card_info))


def start_payment_by_card():
    ClientTools.get_global_pool().apply_async(payment_by_card)


def payment_by_card():
    global time_stamp
    payment_result_info = dict()
    _LOG_.debug('start_payment_by_card')
    flag = QP3000S.init_serial(x=1)
    _LOG_.debug(('start_get_card_info flag: ', flag))
    if flag:
        payment_info_flag = QP3000S.purcDeb()
        _LOG_.debug(('start_get_card_info : ', payment_info_flag))
        if payment_info_flag == '0000':
            payment_info = QP3000S.getReport()
            _LOG_.debug(('start_get_card_info getReport result : ', payment_info))
            payment_result_info['last_balance'] = payment_info[32:40].lstrip('0')
            payment_result_info['card_no'] = payment_info[4:20]
            time_stamp = int(time.time())
            payment_result_info['show_date'] = time_stamp * 1000
            payment_result_info['terminal_id'] = Configurator.get_value('popbox', 'terminalID')
            box_info = box.service.BoxService.get_box()
            payment_result_info['locker'] = box_info['name']
            _EXPRESS_.start_payment_by_card_signal.emit(json.dumps(payment_result_info))
        else:
            _EXPRESS_.start_payment_by_card_signal.emit('ERROR')
    else:
        _EXPRESS_.start_payment_by_card_signal.emit('ERROR')


def start_get_cod_status():
    ClientTools.get_global_pool().apply_async(get_cod_status)


def get_cod_status():
    cod_status = Configurator.get_value('COP', 'status')
    _EXPRESS_.start_get_cod_status_signal.emit(cod_status)


def start_finish_payment_operate():
    ClientTools.get_global_pool().apply_async(finish_payment_operate)


def finish_payment_operate():
    pakpobox_finish_info = dict()
    box_result = box.service.BoxService.get_box()
    _LOG_.debug('start_finish_payment_operate')
    flag = QP3000S.init_serial(x=1)
    _LOG_.debug(('start_finish_payment_operate flag: ', flag))
    if flag:
        settlement = QP3000S.settlement()
        _LOG_.debug(('start_finish_payment_operate settlement: ', settlement))
        pakpobox_finish_info['settle_code'] = settlement
        pakpobox_finish_info['order_number'] = pakpobox_order_number
        pakpobox_finish_info['token'] = Configurator.get_value('popbox', 'Token')
        pakpobox_finish_info['order_amount'] = int(global_express_info['order_amount'])
        time_array = time.localtime(time_stamp)
        other_style_time = time.strftime('%Y-%m-%d %H:%M:%S', time_array)
        pakpobox_finish_info['settle_timestamp'] = other_style_time
        pakpobox_finish_info['settle_place'] = box_result['name']
        _LOG_.debug(('start_finish_payment_operate info: ', pakpobox_finish_info))
        start_retry_push_customer_info(pakpobox_finish_info)


def start_retry_push_customer_info(customer_info):
    ClientTools.get_global_pool().apply_async(retry_push_customer_info, (customer_info,))


def retry_push_customer_info(customer_info):
    end_url = Configurator.get_value('popbox', 'endserveraddress')
    while True:
        express_message, status_code = HttpClient.pakpobox_get_message(end_url, customer_info)
        if status_code == 200 and express_message['response']['message'] == 'SUCCESS':
            return
        time.sleep(2)


def start_deposit_express():
    ClientTools.get_global_pool().apply_async(store_deposit_express)


popdeposit_express = customer_store_express


def store_deposit_express():
    res = {}
    try:
        print('masuk start deposit express service')
        mouth_result = box.service.BoxService.mouth
        _LOG_.debug(('mouth_result:', mouth_result))
        box_result = box.service.BoxService.get_box()
        if not box_result:
            res['isSuccess'] = 'false'
            # res['message'] = str(e)
            _EXPRESS_.store_express_signal.emit(str(json.dumps(res)))
            _LOG_.info("[ERROR] store_deposit_express box_result")
            return
        overdue_time = get_overdue_timestamp(box_result)
        operator_result = CompanyService.get_company_by_id(box_result['operator_id'])
        if not operator_result:
            res['isSuccess'] = 'false'
            # res['message'] = str(e)
            _EXPRESS_.store_express_signal.emit(str(json.dumps(res)))
            _LOG_.info("[ERROR] store_deposit_express operator_result")
            return
        express_param = {'expressNumber': popdeposit_express['customerStoreNumber'],
                        'expressType': 'COURIER_STORE',
                        'overdueTime': overdue_time,
                        'status': 'IN_STORE',
                        'storeTime': ClientTools.now(),
                        'syncFlag': 0,
                        'takeUserPhoneNumber': popdeposit_express['takeUserPhoneNumber'],
                        'validateCode': random_validate(box_result['validateType']),
                        'version': 1,
                        'box_id': box_result['id'],
                        'logisticsCompany_id': popdeposit_express['logisticsCompany']['id'],
                        'mouth_id': mouth_result['id'],
                        'operator_id': box_result['operator_id'],
                        'storeUser_id': '402880825dbcd4c3015de54d98c5518e',
                        'groupName': 'POPDEPOSIT',
                        'id': popdeposit_express['id'],
                        'transactionRecord': '',
                        'drop_by_courier_login': '',
                        'payment_param': '',
                        'is_insulator':0,
                        'flagRedrop': 0,
                        'chargeType': ''}
        ExpressDao.save_express(express_param)
        mouth_param = {'id': express_param['mouth_id'],
                    'express_id': express_param['id'],
                    'status': 'USED'}
        box.service.BoxService.use_mouth(mouth_param)
        express_param['box'] = {'id': express_param['box_id']}
        express_param.pop('box_id')
        express_param['logisticsCompany'] = {'id': express_param['logisticsCompany_id']}
        express_param.pop('logisticsCompany_id')
        express_param['mouth'] = {'id': express_param['mouth_id']}
        express_param.pop('mouth_id')
        express_param['operator'] = {'id': express_param['operator_id']}
        express_param.pop('operator_id')
        express_param['storeUser'] = {'id': express_param['storeUser_id']}
        express_param.pop('storeUser_id')
        _EXPRESS_.store_express_signal.emit('Success')
        logging.info('POPDEPOSIT express stored : ' + str(json.dumps(express_param)))
        message, status_code = HttpClient.post_message('express/staffStoreExpress', express_param)
        if status_code == 200 and message['id'] == express_param['id']:
            ExpressDao.mark_sync_success(express_param)
    except Exception as e:
        save_data_to_db_when_error(express_param)
        res['isSuccess'] = 'false'
        res['message'] = str(e)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] store_deposit_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.store_express_signal.emit(str(json.dumps(res)))

# UPDATE ANDALAS V2.0

def start_courier_load_popsend_list():
    ClientTools.get_global_pool().apply_async(courier_load_popsend_list)
    _EXPRESS_.load_express_list_signal.emit('Success')


def courier_load_popsend_list():
    __user = UserService.get_user()
    # _LOG_.info("USERRAAA : " + str(__user))
    param = {'status': 'IN_STORE',
             'logisticsCompany_id': __user['company']['id'],
             'expressType': 'CUSTOMER_STORE',
             'storeUser_id': 'POPSEND'}
    __user_access = __user['user_role']['level_role']
    if __user_access == 1 or __user_access == 2 or __user_access == 3 or __user_access == 4 or __user_access == 5 or __user_access == 6:
        overdue_express_list_ = ExpressDao.get_popsend_list_by_manager(param)
    else:
        overdue_express_list_ = ExpressDao.get_popsend_list_by_logistic_id(param)
    # _LOG_.info('Param Express List:' + str(param))
    for overdue_express_ in overdue_express_list_:
        overdue_express_['mouth'] = box.service.BoxService.get_mouth(overdue_express_)
    _EXPRESS_.overdue_express_list_signal.emit(json.dumps(overdue_express_list_))
    _LOG_.info('load_popsend_list:' + str(overdue_express_list_))

def start_courier_load_reject_express_list():
    ClientTools.get_global_pool().apply_async(courier_load_reject_express_list)
    _EXPRESS_.load_express_list_signal.emit('Success')

def courier_load_reject_express_list():
    __user = UserService.get_user()
    param = {'status': 'IN_STORE',
             'logisticsCompany_id': __user['company']['id'],
             'expressType': 'CUSTOMER_REJECT'}
    __user_access = __user['user_role']['level_role']
    if __user_access == 1 or __user_access == 2 or __user_access == 3 or __user_access == 4 or __user_access == 5 or __user_access == 6:
        param.pop('logisticsCompany_id')
        overdue_express_list_ = ExpressDao.get_reject_list_by_manager(param)
    else:
        overdue_express_list_ = ExpressDao.get_reject_list_by_logistic_id(param)
    for overdue_express_ in overdue_express_list_:
        overdue_express_['mouth'] = box.service.BoxService.get_mouth(overdue_express_)
    _EXPRESS_.overdue_express_list_signal.emit(json.dumps(overdue_express_list_))
    _LOG_.info('load_reject_express_list:' + str(overdue_express_list_))

def start_operator_taken(op_param):
    ClientTools.get_global_pool().apply_async(operator_take_express, (op_param,))

def operator_take_express(param_op):
    op_param = json.loads(param_op)
    __user = UserService.get_user()
    express_id = op_param['express_id']
    _LOG_.info(("[express_id_operator_taken boss]", express_id))
    if express_id == '':
        _LOG_.warning(("[operator_taken] express_id null"))
    else:
        try:
            express = {'takeTime': ClientTools.now(),
               'status': 'OPERATOR_TAKEN',
               'syncFlag': 0,
               'version': 1,
               'id': express_id,
               'staffTakenUser_id': __user['id']}

            data_operator = {
                'dataOperator': json.dumps(param_op),
                'id': express_id
            }
            express['recipientName'] = ""
            ExpressDao.take_express(express)
            ExpressDao.save_data_operator_taken(data_operator)
            mouth_param = box.service.BoxService.get_mouth(express)
            box.service.BoxService.free_mouth(mouth_param)
            box.service.BoxService.open_mouth(mouth_param['id'])
            param = {
                'id': express_id,
            }
            data_express = ExpressDao.getdata_express_operator_taken(param)
            _LOG_.info(("[data_express_operator_taken]", data_express))

            try:
                express['expressNumber'] = data_express['expressNumber']
                express['takeUserPhoneNumber'] = data_express['takeUserPhoneNumber']
                express['overdueTime'] = data_express['overdueTime']
                express['storeTime'] = data_express['storeTime']
                express['validateCode'] = data_express['validateCode']
                express['box_id'] = data_express['box_id']
                express['logisticsCompany_id'] = data_express['logisticsCompany_id']
                express['mouth_id'] = data_express['mouth_id']
                express['operator_id'] = data_express['operator_id']
                express['storeUser_id'] = data_express['storeUser_id']
                express['staffTakenUser'] = {'id': data_express['staffTakenUser_id']}
                express['dataOperator'] = data_express['dataOperator']
                express['isExtend'] = False

            except Exception as e:
                # _CAP_(e)
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                _LOG_.warning(('[ERROR] operator_take_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

            _LOG_.warning(("[operator_taken] SEND DATA TO PROX : ", express))
            result, status_code = HttpClient.post_message('express/customerTakeExpress', express)
            if status_code == 200 and result['id'] == express['id']:
                ExpressDao.mark_sync_success(express)
        except Exception as e:
            # _CAP_(e)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            _LOG_.warning(('[ERROR] operator_take_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
            # _EXPRESS_.operator_take_express_signal.emit('Error')

def start_operator_load_overdue_express_list(page):
    ClientTools.get_global_pool().apply_async(operator_load_overdue_express, (page,))
    _EXPRESS_.load_express_list_signal.emit('Success')

def operator_load_overdue_express(page):
    try:
        __user = UserService.get_user()
        param = {'status': 'IN_STORE',
                'overdueTime': ClientTools.now(),
                'logisticsCompany_id': __user['company']['id'],
                'expressType': 'COURIER_STORE'}
        __user_access = __user['user_role']['level_role']
        if __user_access == 1 or __user_access == 2 or __user_access == 3 or __user_access == 4 or __user_access == 5 or __user_access == 6:
            param.pop('logisticsCompany_id')
            overdue_express_list_ = ExpressDao.get_overdue_express_by_operator(param)
        else:
            overdue_express_list_ = ExpressDao.get_overdue_express_by_logistics_id(param)

        _LOG_.warning(('[LIST] operator_load_overdue_express : ',str(overdue_express_list_)))

        # for overdue_express_ in overdue_express_list_:
        #     overdue_express_['mouth'] = box.service.BoxService.get_mouth(overdue_express_)
        #     print('express_id',str(overdue_express_['express_id']))

        _EXPRESS_.overdue_express_list_signal.emit(json.dumps(overdue_express_list_))
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] operator_load_overdue_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.overdue_express_list_signal.emit(json.dumps(overdue_express_list_))

def start_operator_logging(title, op_param):
    ClientTools.get_global_pool().apply_async(operator_logging, (title, op_param,))

def operator_logging(title, op_param):
    _LOG_.info(str(title) + " : " + str(op_param))

def start_load_express_all():
    ClientTools.get_global_pool().apply_async(load_express_all)
    # _EXPRESS_.load_express_all_signal.emit('Success')

def load_express_all():
    param = {'status': 'IN_STORE'}
    all_express_list_ = ExpressDao.get_all_express_by_operator(param)
    _EXPRESS_.all_express_list_signal.emit(json.dumps(all_express_list_))

def check_pattern(awb):
    print('masuk_ke_check_pattern')
    # info_user = UserService.get_user_info()
    # _company_id = info_user['company_id']
    # id_lazada = "2c9180c151a8c1a70151ccfc1cc109ca"
    # forbidden_pattern_lazada_orderno = "^[0-9]{15}$"
    # forbidden_pattern_lazada_hub = "^[a-zA-Z]{1}-[a-zA-Z]{3}-[a-zA-Z1-9]{1,8}$"

    pattern_one = '^LXRP-[0-9]{10}$'
    pattern_two = '^LXRT-[0-9]{10}$'
    pattern_three = '^ID[0-9]{10}$'
    pattern_four = '^LXAT-[0-9]{10}$'
    pattern_five = '^LXXB-[0-9]{10}$'
    pattern_six = '^LXEE-[0-9]{10}$'
    pattern_seven = '^LXAP-[0-9]{10}$'

    check_awb_pattern = (re.search(pattern_one, awb)) or (re.search(pattern_two, awb)) or (re.search(pattern_three, awb)) or (re.search(pattern_four, awb)) or (re.search(pattern_five, awb)) or (re.search(pattern_six, awb))  or (re.search(pattern_seven, awb))
    # if check_awb_pattern:
    #     return True
    # else:
    #     return False
    return True

def check_pattern_my(awb, company_id):
    # if company_id == "800640693001567595171qEYKp7s4CTR": # DEV
    if company_id == "402880825ea763b8015fd71178de212b": # PROD
        # pos laju pattern 
        forbidden_pattern_one = '^E[A-Z]{1,2}[0-9]{9}MY$'
        forbidden_pattern_two = '^PS[0-9]{12}$'
        forbidden_pattern_three = '^PL[0-9]{12}$'
        forbidden_pattern_four = '^PLFF[0-9]{8}$'
        check_awb_pattern = (re.search(forbidden_pattern_one, awb)) or (re.search(forbidden_pattern_two, awb)) or (re.search(forbidden_pattern_three, awb)) or (re.search(forbidden_pattern_four, awb))
        if check_awb_pattern:
            return False
        else:
            return True
    else:
        forbidden_pattern = '^[a-zA-Z]{1}-[a-zA-Z]{3}-[a-zA-Z]{1,8}$'
        check_awb_pattern = re.search(forbidden_pattern, awb)
        if check_awb_pattern:
            return True
        else:
            return False

change_locker = ''
express_id = ''
opendoor_status = ''

def set_value_change_mouth_size(change_locker, express_id, opendoor_status):
    ClientTools.get_global_pool().apply_async(set_value_mouth_for_change_size, (change_locker, express_id, opendoor_status,))

def set_value_mouth_for_change_size(change_locker_, express_id_, opendoor_status_):
    global change_locker
    global express_id
    global opendoor_status
    res = {}
    try:
        change_locker = change_locker_
        express_id = express_id_
        opendoor_status = opendoor_status_
        res['isSuccess'] = "true"
        res['data'] = {
            'change_locker': "true",
            'express_id': express_id,
            'opendoor_status': opendoor_status
        }
        _EXPRESS_.set_value_change_size_signal.emit(json.dumps(res))

    except Exception as e:
        res['isSuccess'] = "false"
        res['message'] = str(e)
        _EXPRESS_.set_value_change_size_signal.emit(json.dumps(res))


def is_json(myjson):
  try:
    json_object = json.loads(myjson)
    try:
        int(json_object)
        return False
    except Exception as e:
        # _CAP_(e)
        return True
  except ValueError as e:
    return False
  return True

def start_cancel_express_by_id(id_express, reason):
    ClientTools.get_global_pool().apply_async(cancel_order_loker_by_id_express, (id_express, reason,))

def cancel_order_loker_by_id_express(id_express, reason):
    try:
        check_column = ExpressDao.check_column_exist({}, 'Express')
        checking = 0
        for list_ in check_column:
            if list_['name'] == 'reason':
                checking += 1

        if checking == 0:
            adding_column = ExpressDao.adding_column_reason({})
            cancel_order(id_express, reason)
        else:
            cancel_order(id_express, reason)

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] cancel_order_loker_by_id_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))


def cancel_order(id_express, reason):
    res  = {}
    try:
        # change to canceled order by id
        param_ = {
            'id': id_express,
            'reason': reason,
            'status': 'CANCELED',
            'syncFlag': 0
        }
        
        canceled = ExpressDao.cancel_order(param_)

        if cancel_order is not None:

            # change mouth to available
            detail_express = {'id': id_express}
            data_express = ExpressDao.get_express_by_id(detail_express)[0]

            list = []
            if data_express is not None or data_express is not list:
                change_status_mouth = change_mouth_id_free(data_express['mouth_id'])
                if change_status_mouth is True:
                    res['isSuccess'] = "true"
                else:
                    res['isSuccess'] = "false"

        else:
            res['isSuccess'] = "false"

        _EXPRESS_.cancel_order_signal.emit(str(json.dumps(res)))
        # _EXPRESS_.customer_take_express_signal.emit(str(json.dumps(res)))
    except Exception as e:
        res['isSuccess'] = "false"
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] cancel_order : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        # _EXPRESS_.customer_take_express_signal.emit(str(json.dumps(res)))
        _EXPRESS_.cancel_order_signal.emit(str(json.dumps(res)))


def set_identification_number(is_passport_,identification_number_):
    global is_passport
    global identification_number
    is_passport = is_passport_
    identification_number = identification_number_

# UPDATE FOR INSULATOR

def start_upadate_payment_insulator(validate_code, payment_result, data_taken):
    ClientTools.get_global_pool().apply_async(update_payment_insulator, (validate_code, payment_result, data_taken, ))

def update_payment_insulator(validate_code, payment_result, data_taken):
    try:
        res = {}
        payment = json.dumps(payment_result)
        taken_payload = json.dumps(data_taken)
        data_load_payment = json.loads(payment_result)
        print('transction_id', str(data_load_payment['transaction_id']))
        insulator_update = {
            'syncFlag': 0,
            'validateCode': validate_code,
            'paymentParam': payment,
            'transactionId': str(data_load_payment['transaction_id']),
            'dataInsulator': taken_payload
        }
        _LOG_.warning(('[PARAM_UPDATE] update_payment_insulator : ', str(insulator_update)))

        ExpressDao.update_payment_insulator(insulator_update)
        _EXPRESS_.start_update_insulator_payment_signal.emit('Success')
        return
    except Exception as e:
        res['isSuccess'] = "false"
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] update_payment_insulator : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.start_update_insulator_payment_signal.emit('Failed')
        return

def start_update_paid_parcels(validateCode, paymentOverdue):
    ClientTools.get_global_pool().apply_async(update_paid_parcel, (validateCode,paymentOverdue,))

def update_paid_parcel(validateCode, paymentOverdue):
    try:
        overdue = json.dumps(paymentOverdue)
        data_load_overdue = json.loads(paymentOverdue)
        paid_parcel_param = {
            'syncFlag': 0,
            'validateCode': validateCode,
            'status': 'IN_STORE',
            'paymentOverdue': overdue,
            'paymentParam': overdue,
            'transactionId': data_load_overdue['transaction_id']
        }
        ExpressDao.update_paid_parcel(paid_parcel_param)
        _EXPRESS_.update_paid_parcels_signal.emit('Success')
        return
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] update_apexpress : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.update_paid_parcels_signal.emit('Failed')

def start_store_booking_express(express_id):
    ClientTools.get_global_pool().apply_async(store_booking_express, (express_id,))

# popdeposit_express = customer_store_express

def store_booking_express(express_id):
    try:
        box_result = box.service.BoxService.get_box()
        res = {}
        if not box_result:
            res['isSuccess'] = 'false'
            res['message'] = e
            _EXPRESS_.store_booking_express_signal.emit(str(json.dumps(res)))
            _LOG_.info("[ERROR] store_booking_express box_result")
            return
        overdue_time = get_overdue_timestamp(box_result)
        express_param = {'express_id' : express_id,
                        'overdueTime': overdue_time,
                        'status': 'IN_STORE',
                        'storeTime': ClientTools.now(),
                        'syncFlag': 0,
                        'validateCode': random_validate(box_result['validateType'])
                        }

        ExpressDao.update_express_booking(express_param)
        res['isSuccess'] = "true"
        res['expressId'] = express_param['express_id']
        _EXPRESS_.store_booking_express_signal.emit(str(json.dumps(res)))
        _LOG_.info('BOOKING express stored : ' + str(json.dumps(express_param)))
        # message, status_code = HttpClient.post_message('express/staffStoreExpress', express_param)
        # if status_code == 200 and message['id'] == express_param['id']:
        #     ExpressDao.mark_sync_success(express_param)
    except Exception as e:
        # save_data_to_db_when_error(express_param)
        res['isSuccess'] = 'false'
        res['message'] = e
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] store_booking_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.store_booking_express_signal.emit(str(json.dumps(res)))

#UPDATE nfcpayment PH
def start_read_nfc():
    try:
        get_nfc=NFC.get_nfc_status()
        _EXPRESS_.nfc_card_signal.emit(str(get_nfc))
        #NFC._READER_.CardAvailable.connect(get_nfc_status_avail)
    except Exception as e:
        print("EXPRESS READ NFC FAILED"+ str(e))

def start_get_nfc_data():
    try:
        nfc_data,nfc_balance=NFC.get_nfc_data()
        _EXPRESS_.nfc_valid_signal.emit(str(nfc_data))
        _EXPRESS_.nfc_balance_signal.emit(str(nfc_balance))
    except Exception as e:
        print("EXPRESS GET NFC FAILED"+ str(e))

def start_init_reader():
    global reader_init
    reader_init = False
    try:
        if dummy_beep_trans == 1:
            reader_init="True"
            _EXPRESS_.reader_init_signal.emit(reader_init)
            print("pyt: start init reader DUMMY")
            grpc_getbalance()
        else:   
            reader_init=NFC.init_reader()
            _EXPRESS_.reader_init_signal.emit(str(reader_init))
    except Exception as e:
        print("Express failed start init reader : "+ str(e))

def start_grpc_getbalance():
    ClientTools.get_global_pool().apply_async(grpc_getbalance)

def grpc_getbalance():
    try:
        res={}
        card_can,card_status,card_balance,uid= LocalClient.grpc_getbalance()
        print("pyt : card_can :" + str(card_can))
        print("pyt : card_status :" + str(card_status))
        print("pyt : card_balance:" + str(card_balance))
        print("pyt : uid :" + str(uid))

        if card_status == "Card Valid":
            res["CAN"] = card_can
            res["isValid"] = "true"
            res["balance"] = card_balance
            res["uid"]=uid
            _EXPRESS_.grpc_getbalance_signal.emit(str(json.dumps(res)))
            _LOG_.info('GRPC GetBalance  : ' + str(json.dumps(res)))
            print("pyt: CARD VALID!!!")
            print("result :" + str(res))
        elif card_status == False:
            res["CAN"] = card_can
            res["isValid"] = "timeout"
            res["balance"] = card_balance
            res["uid"]=uid
            _EXPRESS_.grpc_getbalance_signal.emit(str(json.dumps(res)))
            _LOG_.info('GRPC GetBalance  : ' + str(json.dumps(res)))
            print("pyt: Server TIMEOUT GETBALANCE")
        else:
            res["CAN"] = card_can
            res["isValid"] = "false"
            res["balance"] = card_balance
            res["uid"]=uid
            _EXPRESS_.grpc_getbalance_signal.emit(str(json.dumps(res)))
            _LOG_.info('GRPC GetBalance  : ' + str(json.dumps(res)))
    except Exception as e:
        print("Express failed start grpc getbalance : "+ str(e))
        _LOG_.warning('[ERROR]GRPC GetBalance  : ' + str(e))

def start_grpc_debit():
    ClientTools.get_global_pool().apply_async(grpc_debit)

def grpc_debit():
    try:
        res={}
        card_can,card_status,card_balance,uid = LocalClient.grpc_debit()
        if card_status == "Card Valid":
            res["CAN"] = card_can
            res["isValid"] = "true"
            res["balance"] = card_balance
            res["uid"]=uid
            _EXPRESS_.grpc_debit_signal.emit(str(json.dumps(res)))
            _LOG_.info('GRPC debit  : ' + str(json.dumps(res)))
        elif card_status == False:
            res["CAN"] = card_can
            res["isValid"] = "timeout"
            res["balance"] = card_balance
            res["uid"]=uid
            _EXPRESS_.grpc_debit_signal.emit(str(json.dumps(res)))
            _LOG_.info('GRPC debit  : ' + str(json.dumps(res)))
            print("pyt: Server TIMEOUT DEBIT!!")
        else:
            res["CAN"] = card_can
            res["isValid"] = "false"
            res["balance"] = card_balance
            res["uid"]=uid
            _EXPRESS_.grpc_debit_signal.emit(str(json.dumps(res)))
            _LOG_.info('GRPC debit  : ' + str(json.dumps(res)))
    except Exception as e:
        print("Express failed start grpc debit : "+ str(e))
        _LOG_.warning('[ERROR]GRPC debit  : ' + str(e))