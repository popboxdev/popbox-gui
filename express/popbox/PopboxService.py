import requests
import json
import logging
import os
import datetime
import time
import base64
import glob
import random
import sys
import re
import math
import urllib.request
import math
import sys, os
import ClientTools
import Configurator
import box.service.BoxService as BoxService
import box.repository.BoxDao as BoxDao
import express.repository.ExpressDao as ExpressDao
import company.service.CompanyService as CompanyService
import express.service.ExpressService as ExpressService
from PyQt5.QtCore import QObject, pyqtSignal
from network import HttpClient
from device import QP3000S
from device import Scanner
from return_rules.repository import ReturnRulesDao
# from device import Detector
from database import ClientDatabase
from urllib.request import urlopen #Internet Status urlopen
import pyscreenshot as ImageGrab
from PIL import Image
import ssl #SSL issue test
import platform

os_platform = platform.system()
__author__ = 'wahyudi@popbox.asia'
startFlag = True

if os_platform == 'Linux':      #changed
    pass
else:   #windows
    from win32 import win32api
    import wmi 
    import pythoncom


class PopSignalHandler(QObject):
    __qualname__ = 'PopSignalHandler'
    start_get_express_info_result_signal = pyqtSignal(str)
    start_get_popbox_express_info_signal = pyqtSignal(str)
    start_get_customer_info_by_card_signal = pyqtSignal(str)
    start_payment_by_card_signal = pyqtSignal(str)
    start_get_cod_status_signal = pyqtSignal(str)
    start_reset_partial_transaction_signal = pyqtSignal(str)
    start_post_capture_file_signal = pyqtSignal(str)
    start_get_capture_file_location_signal = pyqtSignal(str)
    start_get_locker_name_signal = pyqtSignal(str)
    start_global_payment_emoney_signal = pyqtSignal(str)
    start_get_sepulsa_product_signal = pyqtSignal(str)
    start_sepulsa_transaction_signal = pyqtSignal(str)
    start_get_gui_version_signal = pyqtSignal(str)
    start_customer_scan_qr_code_signal = pyqtSignal(str)
    stop_customer_scan_qr_code_signal = pyqtSignal(str)
    get_popshop_product_signal = pyqtSignal(str)
    start_push_data_settlement_signal = pyqtSignal(str)
    get_query_member_popsend_signal = pyqtSignal(str)
    start_popsend_topup_signal = pyqtSignal(str)
    get_locker_data_signal = pyqtSignal(str)
    start_popshop_transaction_signal = pyqtSignal(str)
    get_popsend_button_signal = pyqtSignal(str)
    sepulsa_trx_check_signal = pyqtSignal(str)
    box_start_migrate_signal = pyqtSignal(str)
    start_get_ads_images_signal = pyqtSignal(str)
    start_delete_express_by_range_signal = pyqtSignal(str)
    start_post_gui_info_signal = pyqtSignal(str)
    start_post_subscribe_data_signal = pyqtSignal(str)
    start_get_file_dir_signal = pyqtSignal(str)
    start_get_detection_signal = pyqtSignal(str)
    start_create_payment_signal = pyqtSignal(str)
    start_check_trans_global_signal = pyqtSignal(str)
    start_check_awb_signal = pyqtSignal(str)
    start_checking_return_signal = pyqtSignal(str)
    start_checking_popsend_signal = pyqtSignal(str)
    start_send_otp_signal = pyqtSignal(str)
    ovo_transaction_no_signal = pyqtSignal(str)
    start_send_checking_otp_signal = pyqtSignal(str)

    # UPDATE SINCE ANDALAS V2
    start_hour_overdue_result_signal = pyqtSignal(str)
    start_time_overdue_result_signal = pyqtSignal(str)
    start_get_locker_type_signal = pyqtSignal(str)
    start_internet_status_result_signal = pyqtSignal(str)
    get_parcel_data_bypin_signal = pyqtSignal(str)
    start_get_tvc_timer_signal = pyqtSignal(int)
    start_get_tvc32_timer_signal = pyqtSignal(int)
    start_get_file_banner_signal = pyqtSignal(str)
    get_locker_resolution_signal = pyqtSignal(str)
    start_get_language_signal = pyqtSignal(str)
    start_capture_signature_signal = pyqtSignal(str)
    start_post_signature_signal = pyqtSignal(str)
    start_get_mode_insulator_signal = pyqtSignal(str)
    start_get_contactless_signal = pyqtSignal(str)
    start_get_booking_express_signal = pyqtSignal(str)
    get_parcel_data_idnumber_signal = pyqtSignal(str)
    generate_free_transaction_signal = pyqtSignal(str)
    nfc_transaction_no_signal = pyqtSignal(str)
    popsafe_price_signal = pyqtSignal(str)
    start_get_opendoor_mode_signal = pyqtSignal(str)
    start_get_theme_signal = pyqtSignal(str)
    get_scanner_version_signal = pyqtSignal(str)


_POP_ = PopSignalHandler()
_LOG_ = logging.getLogger()
img_path = sys.path[0] + '/video_capture/'
NOT_INTERNET_ = {
    'statusCode': -1,
    'statusMessage': 'Not Internet'}
customer_scanner_signal_connect_flag = False
pin_code = ''
bad_chars = ['D:', '\\', '.jpg', 'video_capture', '/', 'popboxgui_webkit2', 'popboxclient_mly01', 'signature', '##GUI_PROJECT##']
base_url = Configurator.get_value('ClientInfo', 'serveraddress')

# Initial Version Information
version_path = os.path.join(os.getcwd(), 'version.txt')
version = open(version_path, 'r').read().strip()
Configurator.set_value('version', 'version', version)
bypass_server = 'http://127.0.0.1:8000/post-data'

# print('version : ', version_path, version)


##################################################################
# BoxService = box.service.BoxService
# ExpressDao = express.repository.ExpressDao
# CompanyService = company.service.CompanyService
# ExpressService = express.service.ExpressService
##################################################################

#######################TOP SUPPORTING FUNCTION####################
def development_progress():
    if Configurator.get_value('panel', 'setserver') == "dev":
        return True
    else:
        _LOG_.info('Public live GUI is Running')
        return False


##################################################################
def uat_mode():
    uat_link = ""
    try:
        if Configurator.get_value('panel', 'uat') == "yes":
            uat_link = "UAT"
    except:
        pass
    finally:
        return uat_link
##################################################################
is_UAT = uat_mode()

# Get Status of Development
is_dev = development_progress()
if is_dev:
    global_url = "http://api-dev.popbox.asia/"
    global_token = "767ae89fec88a53dbb465adb4d0c76c294a8a28e"
else:
    global_url = Configurator.get_value('popbox', 'popserver')
    global_token = Configurator.get_value('popbox', 'poptoken')

dev_token = "767ae89fec88a53dbb465adb4d0c76c294a8a28e"
dev_url = "http://api-dev.popbox.asia/"
##################################################################
# Get Locker Information
locker_info = BoxService.get_box()
gui_version = version
BoxService.version = version
SSL_VERIFY = True if Configurator.get_or_set_value('panel', 'ssl^verify', '1') == '1' else False


##################################################################
###SUPPORTING_FUNCTIONS###
##################################################################

def take_capture_file_signature(path):
    global latest_img
    img_name = None
    img_b64 = None
    try:
        latest_img = max(glob.iglob(img_path + path + '*.jpg'), key=os.path.getctime)
        img_name = rename_file(latest_img, bad_chars, '')
        with open(latest_img, 'rb') as imageFile:
            img_b64 = base64.b64encode(imageFile.read())
    finally:
        return img_name, img_b64

def take_capture_file():
    global latest_img
    img_name = None
    img_b64 = None
    xchars = ['D:', '\\', '.jpg', 'video_capture', '/', 'popboxgui_webkit2']
    try:
        latest_img = max(glob.iglob(img_path + '*.jpg'), key=os.path.getctime)
        img_name = rename_file(latest_img, xchars, '')
        with open(latest_img, 'rb') as imageFile:
            img_b64 = base64.b64encode(imageFile.read())
    finally:
        return img_name, img_b64


##################################################################
def capture_photo_post(url_post, param):
    _LOG_.info('url:' + str(url_post) + '; json: ' + str(param))
    try:
        r = requests.post(url_post, json=param, timeout=50)
    except requests.RequestException:
        _LOG_.debug((NOT_INTERNET_, url_post, -1))
        return NOT_INTERNET_, -1
    try:
        r_response = r.json()
        r_status = r_response['response']['code']
    except ValueError:
        _LOG_.debug(('ValueError on : ', url_post))
        return NOT_INTERNET_, -1
    _LOG_.info((r_status, r_response))
    return r_status, r_response


##################################################################
def rename_file(filename, list, x):
    for char in list:
        filename = filename.replace(char, x)
    return filename


##################################################################
def get_list_popshop(url_param, param=None):
    _LOG_.info('url:' + str(url_param) + '; json: ' + str(param))
    headers = {'content-type': "application/json"}
    try:
        r = requests.post(url_param, json=param, timeout=50, headers=headers)
    except requests.RequestException:
        _LOG_.debug((NOT_INTERNET_, -1))
        return NOT_INTERNET_, -1
    try:
        r_json = r.json()
        r_status = 0
        if "data" in r_json:
            r_status = 200
        elif "response" in r_json:
            r_status = 400
    except ValueError:
        _LOG_.debug(('ValueError on : ', url_param))
        return NOT_INTERNET_
    _LOG_.info((r_json, r_status))
    return r_json, r_status


##################################################################
def global_post(url_param, param=None):
    global SSL_VERIFY
    headers = {'content-type': "application/json"}
    r_status = '404'
    _LOG_.debug('---<URL> : ' + str(url_param) + ', <POST> : ' + str(param))

    params = {
        'url': url_param,
        'param':param,
        'header':headers
    }
    payload = json.loads(json.dumps(params))
    _LOG_.warning(('[HTTP_CLIENT_POST_SERVICE]', payload))
    try:
        if 'https' in url_param:
            r = requests.post(bypass_server, json=payload, timeout=60, headers=headers, verify=SSL_VERIFY)
        else:
            r = requests.post(bypass_server, json=payload, timeout=60, headers=headers)
    except requests.RequestException:
        try:
            payload = param
            _LOG_.info("[PARAMETER SEND] : " + str(payload))
            if 'https' in url_param:
                r = requests.post(url_param, json=payload, timeout=60, headers=headers, verify=SSL_VERIFY)
            else:
                r = requests.post(url_param, json=payload, timeout=60, headers=headers)
        except requests.RequestException:
            _LOG_.debug((NOT_INTERNET_, -1))
            return NOT_INTERNET_, -1

    try:
        r_json = r.json()
        r_status = r_json["response"]["code"]
    except ValueError:
        _LOG_.warning(('ValueError on ', url_param, str(r.content)))
        return NOT_INTERNET_, r_status
    _LOG_.debug('***<URL> : ' + str(url_param) + ' | <STATUS> : ' + str(r_status) + ' | <POST> : ' + str(param) +
                ' | <RESP> : ' + str(r_json))
    return r_json, r_status


##################################################################
def get_return_value(value):
    new_value = str(value)
    return new_value


##################################################################
def random_chars(type_of, length_of=6):
    while True:
        if type_of == 'LETTER_AND_NUMBER_VALIDATE_CODE':
            chars = 'ABCDEFGHJKMNPQRSTUVWXYZ23456789'
            validate_code = ''
            i = 0
            while i < length_of:
                validate_code += random.choice(chars)
                i += 1

            if len(ExpressDao.get_express_by_validate({'validateCode': validate_code,
                                                       'status': 'IN_STORE'})) == 0:
                return validate_code
        else:
            return ''


##################################################################
def resync_table(sql, param):
    ClientDatabase.insert_or_update_database(sql=sql, parameter=param)
    _LOG_.debug(("resync_table (sql => param) : " + sql + " => " + param))


##################################################################
def force_rename(file1, file2):
    from shutil import move
    try:
        move(file1, file2)
        return True
    except:
        return False


##################################################################
def get_overdue_hours(h):
    try:
        start_time = datetime.datetime.now()
        end_time = start_time + datetime.timedelta(hours=int(h))
        return int(time.mktime(time.strptime(end_time.strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')) * 1000)
    except Exception as e:
        _LOG_.debug(('get_overdue_hours', str(e)))


##################################################################

###CALLABLE_FUNCTIONS###
def start_get_express_info(express_text):
    ClientTools.get_global_pool().apply_async(get_express_info, (express_text,))


def get_express_info(text):
    global popbox_express_info
    global cop_order_number
    try:
        popbox_express_info = ''
        cop_order_number = text
        popbox_info = dict()
        popbox_info["order_number"] = text
        popbox_info["token"] = global_token
        query_url = global_url + 'cop/detail'
        _LOG_.debug(('url_query_cop : ', query_url))
        express_message, status_code = HttpClient.pakpobox_get_message(query_url, popbox_info)
        if status_code == 200:
            if express_message["response"]["message"] == 'AVAILABLE':
                if len(express_message["data"][0]) != 0:
                    _LOG_.debug(('get_express_info express_message: ', express_message['data']))
                    popbox_express_info = express_message['data'][0]
                    QP3000S.price = express_message['data'][0]['order_amount']
                    _POP_.start_get_express_info_result_signal.emit('SUCCESS')
                else:
                    _POP_.start_get_express_info_result_signal.emit('ERROR')
            elif express_message["response"]["message"] == 'NOT AVAILABLE':
                _POP_.start_get_express_info_result_signal.emit('ERROR')
            elif express_message["response"]["message"] == 'PAID':
                if len(express_message["data"][0]) != 0:
                    _LOG_.debug(('get_express_info express_message: ', express_message['data']))
                    popbox_express_info = express_message['data'][0]
                    _POP_.start_get_express_info_result_signal.emit('PAID')
                else:
                    _POP_.start_get_express_info_result_signal.emit('ERROR')
        else:
            _POP_.start_get_express_info_result_signal.emit('ERROR')
    except Exception as e:
        _POP_.start_get_express_info_result_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] get_express_info : ', str(e)))


def start_get_popbox_express_info():
    ClientTools.get_global_pool().apply_async(get_popbox_express_info)


def get_popbox_express_info():
    _POP_.start_get_popbox_express_info_signal.emit(json.dumps(popbox_express_info))


def start_get_customer_info_by_card():
    ClientTools.get_global_pool().apply_async(get_customer_info_by_card)


def get_customer_info_by_card():
    global card_info
    try:
        flag = QP3000S.init_serial(x=1)
        _LOG_.debug(('start_get_customer_info_by_card flag: ', flag))
        if flag:
            card_info = QP3000S.balanceInfo()
            _LOG_.debug(('start_get_customer_info_by_card flag: ', card_info))
            if card_info == 'ERROR':
                _POP_.start_get_customer_info_by_card_signal.emit('ERROR')
            elif card_info == 'EMPTY':
                _POP_.start_get_customer_info_by_card_signal.emit('EMPTY')
            elif card_info == 'WRONG-CARD':
                _POP_.start_get_customer_info_by_card_signal.emit('WRONG-CARD')
            else:
                _POP_.start_get_customer_info_by_card_signal.emit(card_info)
            return card_info
        else:
            card_info = 'ERROR'
            _POP_.start_get_customer_info_by_card_signal.emit(card_info)
            return card_info
    except Exception as e:
        card_info = 'ERROR'
        _POP_.start_get_customer_info_by_card_signal.emit(card_info)
        _LOG_.warning(('[ERROR] get_customer_info_by_card : ', str(e)))
        return card_info


UNFINISHED_CODE = ['6F00', '1004', '6700', '1015']
WRONGCARD_CODE = ['6A82', 'FFFE', '6999', '1024']


def start_payment_by_card():
    ClientTools.get_global_pool().apply_async(payment_by_card)


def payment_by_card():
    global time_stamp
    try:
        payment_result_info = dict()
        flag = QP3000S.init_serial(x=1)
        _LOG_.debug(('start_get_card_info flag: ', flag))
        response_purcdeb = ""
        if flag:
            response_purcdeb = QP3000S.purcDeb()
            _LOG_.debug(('init_status_payment : ', response_purcdeb))
            if response_purcdeb == 'SUCCESS':
                payment_info = QP3000S.paymentSuccess()
                _LOG_.debug(('start_get_card_info getReport result : ', payment_info))
                payment_result_info["card_no"] = payment_info[4:20]
                payment_result_info["last_balance"] = payment_info[32:40].lstrip('0')
                time_stamp = int(time.time())
                payment_result_info["show_date"] = time_stamp * 1000
                payment_result_info["terminal_id"] = Configurator.get_value('popbox', 'terminalID')
                payment_result_info["locker"] = locker_info["name"]
                _POP_.start_payment_by_card_signal.emit(json.dumps(payment_result_info))
                _LOG_.info(('status_payment: ', response_purcdeb, ' get signal : SUCCESS'))
            elif response_purcdeb in UNFINISHED_CODE:
                _POP_.start_payment_by_card_signal.emit('UNFINISHED')
                _LOG_.debug(('status_payment: ', response_purcdeb, ' get signal : UNFINISHED'))
            elif response_purcdeb in WRONGCARD_CODE:
                _POP_.start_payment_by_card_signal.emit('WRONG-CARD')
                _LOG_.debug(('status_payment: ', response_purcdeb, ' get signal : WRONG-CARD'))
            else:
                _POP_.start_payment_by_card_signal.emit('ERROR')
                _LOG_.warning(('status_payment: ', response_purcdeb, ' get signal : ERROR'))
        else:
            _POP_.start_payment_by_card_signal.emit('FAILED')
            _LOG_.warning(('status_payment: ', response_purcdeb, ' get signal : FAILED'))
    except Exception as e:
        _POP_.start_payment_by_card_signal.emit('FAILED')
        _LOG_.warning(('[ERROR] payment_by_card: ', str(e)))


def start_get_cod_status():
    ClientTools.get_global_pool().apply_async(get_cod_status)


def get_cod_status():
    res = {}
    emoney_status = Configurator.get_value('panel', 'emoney')
    ovo_status = Configurator.get_value('QrOvo', 'merchant_id')
    if(emoney_status=="enabled"):
        emoney_status = "enabled"
    else:
        emoney_status = "disable"
    if(ovo_status!="ovo"):
        ovo_status = "enabled"
    else:
        ovo_status = "disable"
    
    param = {'emoney_status': emoney_status,
            'ovo_status': ovo_status}
    payment_status = json.dumps(param)
    _POP_.start_get_cod_status_signal.emit(payment_status)

def start_get_locker_type():
    ClientTools.get_global_pool().apply_async(get_locker_type)

def get_locker_type():
    locker_type = Configurator.get_value('LockerType', 'type')
    length_locker = Configurator.get_value('LockerType', 'length')

    param = {'locker_type': {'type': locker_type, 'length': length_locker}}
    locker_json = json.dumps(param)
    _LOG_.info("LOCKER_TYPE_OKE : " + str(param))
    _POP_.start_get_locker_type_signal.emit(locker_json)

def start_get_gui_version():
    ClientTools.get_global_pool().apply_async(get_gui_version)


def get_gui_version():
    global version, gui_version
    version_path = os.path.join(os.getcwd(), 'version.txt')
    version = open(version_path, 'r').read().strip()
    gui_version = version
    BoxService.version = version
    Configurator.set_value('version', 'version', version)
    _POP_.start_get_gui_version_signal.emit(version)


def start_reset_partial_transaction():
    ClientTools.get_global_pool().apply_async(reset_partial_transaction)


def reset_partial_transaction():
    try:
        flag = QP3000S.init_serial(x=1)
        _LOG_.debug(('reset_partial_transaction flag: ', flag))
        reset_partial = ""
        if flag:
            reset_partial = QP3000S.resetPartial()
            _LOG_.debug(('init_reset_partial_status: ', reset_partial))
            if reset_partial == '0000':
                _POP_.start_reset_partial_transaction_signal.emit('RESET-SUCCESS')
                _LOG_.info(('reset_partial_status: ', reset_partial))
            else:
                _POP_.start_reset_partial_transaction_signal.emit('RESET-FAILED')
                _LOG_.warning(('reset_partial_status: ', reset_partial))
        else:
            _POP_.start_reset_partial_transaction_signal.emit('RESET-FAILED')
            _LOG_.warning(('reset_partial_status: ', reset_partial))
    except Exception as e:
        _POP_.start_reset_partial_transaction_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] reset_partial_transaction: ', str(e)))


def start_post_capture_file():
    ClientTools.get_global_pool().apply_async(post_capture_file)


def post_capture_file():
    photo_param = dict()
    result_name, result_b64 = take_capture_file()
    photo_param['token'] = global_token
    photo_param['locker_name'] = locker_info['name']
    photo_param['event_name'] = result_name
    photo_param['datetime'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
    photo_param['base64_image'] = result_b64.decode('ascii')
    photo_url = global_url + 'locker/photo'
    _LOG_.debug(('url_photo_upload : ', photo_url))
    while True:
        status, response = capture_photo_post(photo_url, photo_param)
        _LOG_.info(('start_post_capture_file flag: ', photo_url, status))
        if status == '200' and response['response']['message'] == 'OK':
            return
        time.sleep(2)


def start_get_capture_file_location():
    ClientTools.get_global_pool().apply_async(get_capture_file_location)


def get_capture_file_location():
    chars = ['D:\\popboxgui_webkit2/video_capture\\']
    filename = None
    try:
        captured_image = max(glob.iglob(img_path + '*.jpg'), key=os.path.getctime)
        _LOG_.info(('value check :', captured_image))
        filename = rename_file(captured_image, chars, '../video_capture/')
        _POP_.start_get_capture_file_location_signal.emit(filename)
        _LOG_.info(('filename check :', filename))
    except Exception as e:
        _LOG_.warning(('get_capture_file_location failed:', filename, e))
        _POP_.start_get_capture_file_location_signal.emit('ERROR')


def start_get_locker_name():
    ClientTools.get_global_pool().apply_async(get_locker_name)


def get_locker_name():
    if len(locker_info["name"]) > 40 :
        temp_name = locker_info["name"]
        loc_name = temp_name[0:38] + ".."
    else:
        loc_name = locker_info["name"]
    _POP_.start_get_locker_name_signal.emit(loc_name)


def start_global_payment_emoney(amount):
    ClientTools.get_global_pool().apply_async(global_payment_emoney, (amount,))
    print("pyt: [TRIGGER] global_payment_emoney("+ str(amount)+") | " + str(int(time.time()) * 1000))


DUMMY_RESPONSE = False if Configurator.get_value('popbox', 'dummy^transaction') == '1' else False ##DUMMY_RESPONSE FALSE
print('DUMMY_RESPONSE ', str(DUMMY_RESPONSE))

def global_payment_emoney(amount):
    global global_payment_info
    global timestamp_payment
    global_payment_info = ''
    response_purcdeb = None
    timestamp_payment = int(time.time()) * 1000
    res = {}
    print("pyt: [TRIGGER] global_payment_emoney("+ str(amount)+") | DUMMY_RESPONSE : " + str(DUMMY_RESPONSE))
    if DUMMY_RESPONSE is True:
        dummy = {
            "show_date": timestamp_payment,
            "locker": locker_info['name'],
            "last_balance": str(100000-int(amount)),
            "card_no": "12345678987654321",
            "terminal_id": Configurator.get_value('popbox', 'terminalID'),
            "raw": "60000123123123123123123123123123123123123" + "|" + str(timestamp_payment)
            }
        print("pyt: [RESULT] global_payment_emoney("+ str(amount)+") | " + str(timestamp_payment) + " | " + str(dummy))
        # _POP_.start_global_payment_emoney_signal.emit(json.dumps(dummy))
        res['isSuccess'] = "true"
        res['getAmount'] = "true"
        res['message'] = json.dumps(dummy)
        _POP_.start_global_payment_emoney_signal.emit(str(json.dumps(res)))
        return
    try:
        getSaldo = get_customer_info_by_card()
        # getSaldo = "23000"
        print("pyt: [TRIGGER] global_payment_emoney| DUMMY_RESPONSE : " + str(getSaldo))
        if(getSaldo == "WRONG-CARD"):
            res['isSuccess'] = "false"
            res['message'] = "wrong-card"
            _POP_.start_global_payment_emoney_signal.emit(str(json.dumps(res)))
        elif(getSaldo == "EMPTY"):
            res['isSuccess'] = "false"
            res['message'] = "empty"
            _POP_.start_global_payment_emoney_signal.emit(str(json.dumps(res)))
        elif(getSaldo == "ERROR"):
            res['isSuccess'] = "false"
            res['message'] = "error"
            _POP_.start_global_payment_emoney_signal.emit(str(json.dumps(res)))
        elif(int(getSaldo)< int(amount)):
            res['isSuccess'] = "true"
            res['getAmount'] = "false"
            res['message'] = int(getSaldo)
            _POP_.start_global_payment_emoney_signal.emit(str(json.dumps(res)))
        else:
            ###TESTING
            # payment_result_info = {
            #     "show_date": timestamp_payment,
            #     "locker": locker_info['name'],
            #     "last_balance": int(getSaldo),
            #     "card_no": "12345678987654321",
            #     "terminal_id": Configurator.get_value('popbox', 'terminalID'),
            #     "raw": "60000123123123123123123123123123123123123" + "|" + str(timestamp_payment)
            # }
            # res['isSuccess'] = "true"
            # res['getAmount'] = "true"
            # res['message'] = json.dumps(payment_result_info)
            # _POP_.start_global_payment_emoney_signal.emit(str(json.dumps(res)))

            ##TESTING
            flag = QP3000S.init_serial(x=1)
            time.sleep(1)
            if flag:
                response_purcdeb = QP3000S.purcDebGlobal(amountX=amount)
                _LOG_.debug(('init_status_payment : ', response_purcdeb + ', with price :', amount))
                if response_purcdeb == 'SUCCESS':
                    global_payment_info = QP3000S.paymentSuccess()
                    _LOG_.debug(('start_global_payment_emoney getReport result : ', global_payment_info))
                    payment_result_info = {
                        "card_no": global_payment_info[4:20],
                        "last_balance": global_payment_info[32:40].lstrip('0'),
                        "show_date": timestamp_payment,
                        "terminal_id": Configurator.get_value('popbox', 'terminalID'),
                        "locker": locker_info["name"], 
                        "raw" : global_payment_info + "|" + str(timestamp_payment),
                    }
                    res['isSuccess'] = "true"
                    res['getAmount'] = "true"
                    res['message'] = json.dumps(payment_result_info)
                    _POP_.start_global_payment_emoney_signal.emit(str(json.dumps(res)))
                    _LOG_.info(('status_global_payment: ', response_purcdeb, ' param: ', payment_result_info))
                elif response_purcdeb in UNFINISHED_CODE:
                    res['isSuccess'] = "false"
                    res['message'] = "unfinished"
                    _POP_.start_global_payment_emoney_signal.emit(str(json.dumps(res)))
                    _LOG_.debug(('status_global_payment: ', response_purcdeb, ' get signal : UNFINISHED'))
                elif response_purcdeb in WRONGCARD_CODE:
                    res['isSuccess'] = "false"
                    res['message'] = "wrong-card"
                    _POP_.start_global_payment_emoney_signal.emit(str(json.dumps(res)))
                    _LOG_.debug(('status_global_payment: ', response_purcdeb, ' get signal : WRONG-CARD'))
                    _POP_.start_global_payment_emoney_signal.emit('WRONG-CARD')
                else:
                    res['isSuccess'] = "false"
                    res['message'] = "error"
                    _POP_.start_global_payment_emoney_signal.emit(str(json.dumps(res)))
                    _LOG_.warning(('status_global_payment: ', response_purcdeb, ' get signal : ERROR'))
            else:
                res['isSuccess'] = "false"
                res['message'] = "failed"
                _POP_.start_global_payment_emoney_signal.emit(str(json.dumps(res)))
                _LOG_.warning(('status_global_payment: ', response_purcdeb, ' get signal : FAILED'))
    except Exception as e:
        res['isSuccess'] = "false"
        res['message'] = "failed"
        _POP_.start_global_payment_emoney_signal.emit(str(json.dumps(res)))
        _LOG_.warning(str(e))


def start_get_sepulsa_product(phone):
    ClientTools.get_global_pool().apply_async(get_sepulsa_product, (phone,))


def get_sepulsa_product(phone):
    try:
        sepulsa_express_info = dict()
        sepulsa_info = dict()
        sepulsa_info["phone"] = str(phone)
        sepulsa_info["token"] = global_token
        if is_dev:
            query_url = global_url + 'service/sepulsa/getPulsaProduct2' + is_UAT
        else:
            query_url = global_url + 'service/sepulsa/getPulsaProduct' + is_UAT
        _LOG_.debug(('url_get_sepulsa : ', query_url))
        express_message, status_code = global_post(query_url, sepulsa_info)
        if status_code == 200 and express_message["response"]["message"] == 'OK':
            if len(express_message["data"]) != 0:
                _LOG_.info(('get_sepulsa_product_info : ', status_code, express_message['data']))
                sepulsa_express_info["data"] = express_message["data"]["pulsa_list"]
                sepulsa_express_info["operator"] = express_message["data"]["operator"]
                sepulsa_express_info["avail_prod"] = len(sepulsa_express_info["data"])
                _POP_.start_get_sepulsa_product_signal.emit(json.dumps(sepulsa_express_info))
            else:
                _POP_.start_get_sepulsa_product_signal.emit('ERROR')
                _LOG_.warning(('get_sepulsa_product_info : ', status_code, express_message["response"]["message"]))
        else:
            _POP_.start_get_express_info_result_signal.emit('ERROR')
            _LOG_.warning(('[ERROR] get_sepulsa_product_info : ', str(express_message)))
    except Exception as e:
        _POP_.start_get_express_info_result_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] get_sepulsa_product : ', str(e)))


def start_sepulsa_transaction(phone_no, prod_id, prod_type, pay_type):
    ClientTools.get_global_pool().apply_async(sepulsa_transaction, (phone_no, prod_id, prod_type, pay_type,))


def sepulsa_transaction(phone_no, prod_id, prod_type, pay_type):
    global transaction_param
    try:
        if phone_no == "" or prod_id == "" or prod_type == "" or pay_type == "":
            _LOG_.warning('[ERROR] missing parameter')
            return
        transaction_param = dict()
        if is_dev:
            query_url = global_url + 'service/sepulsa/postTransaction2'
        else:
            query_url = global_url + 'service/sepulsa/postTransaction'
        _LOG_.debug(('url_postTransaction_sepulsa : ', query_url))
        if pay_type == 'emoney' or pay_type == 'MANDIRI-EMONEY':
            if global_payment_info != "":
                sepulsa_trans_info = global_payment_info
            else:
                sepulsa_trans_info = QP3000S.getReport()
            _LOG_.debug(('sepulsa_trans_info param : ', sepulsa_trans_info))
            transaction_param["product_amount"] = sepulsa_trans_info[24:32].lstrip('0')
            transaction_param["card_number"] = sepulsa_trans_info[4:20] +"-"+ str(phone_no)
            transaction_param["payment_type"] = str(pay_type)
            try:
                transaction_param["last_balance"] = int(sepulsa_trans_info[32:40].lstrip('0'))
            except Exception as e:
                _LOG_.debug(('Empty Last Balance, Assuming 0', str(e)))
                transaction_param["last_balance"] = 0
        else:
            # Except emoney the pay_tipe format will be "name|trx_no|amount"
            p = pay_type.split('|')
            _LOG_.debug(('pay_type_param : ', pay_type))
            transaction_param["payment_type"] = p[0].lower()
            transaction_param["card_number"] = p[1]
            transaction_param["product_amount"] = p[2]
            transaction_param["last_balance"] = 0
        transaction_param["token"] = global_token
        transaction_param["phone"] = str(phone_no)
        transaction_param["product_type"] = str(prod_type).lower()
        transaction_param["product_id"] = str(prod_id)
        transaction_param["locker_name"] = locker_info["name"]
        _LOG_.info(('[response_internal_api_url]', query_url))
        _LOG_.info(('[response_internal_api_param]', transaction_param))

        express_message, status_code = global_post(query_url, transaction_param)
        _LOG_.info(('[response_internal_api]', express_message))
        if status_code == 200 and express_message["response"]["message"] == 'OK':
            _POP_.start_sepulsa_transaction_signal.emit(express_message["data"][0]["invoice_id"])
            _LOG_.info(('start_sepulsa_transaction result : ', status_code, express_message["data"]))
        else:
            _POP_.start_sepulsa_transaction_signal.emit("ERROR")
            _LOG_.warning(('[ERROR] sepulsa_transaction : ', str(express_message)))
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] sepulsa_transaction_postransaction : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _POP_.start_sepulsa_transaction_signal.emit("ERROR")
        _LOG_.warning(str(e))


def start_push_data_settlement(type__):
    ClientTools.get_global_pool().apply_async(push_data_settlement, (type__,))


def push_data_settlement(type__):
    _LOG_.debug(('push_data_settlement settlement for :', type__))
    if DUMMY_RESPONSE is True:
        _POP_.start_push_data_settlement_signal.emit('NO SETTLE DUMMY')
        _LOG_.info(('push_data_settlement info: ', 'No Settlement for Dummy Transaction'))
        return
    order_transaction_info = dict()
    flag = QP3000S.init_serial(x=1)
    if flag:
        current_time = time.localtime(int(time.time()))
        settlement = QP3000S.settlement()
        _LOG_.debug(('push_data_settlement settlement: ', settlement + ', for :', type__))
        order_transaction_info["token"] = global_token
        order_transaction_info["order_number"] = type__
        order_transaction_info["settle_code"] = settlement
        order_transaction_info["order_amount"] = settlement[46:54].lstrip('0')
        order_transaction_info["settle_timestamp"] = time.strftime('%Y-%m-%d %H:%M:%S', current_time)
        order_transaction_info["settle_place"] = locker_info["name"]
        _LOG_.info(('push_data_settlement info: ', order_transaction_info))
        start_post_payment_transaction(order_transaction_info)
        _POP_.start_push_data_settlement_signal.emit(order_transaction_info)
    else:
        _POP_.start_push_data_settlement_signal.emit('SETTLE FAILED')


def start_post_payment_transaction(payment_info):
    ClientTools.get_global_pool().apply_async(post_payment_transaction, (payment_info,))


def post_payment_transaction(payment_info):
    settle_url = global_url + 'cop/submit'
    _LOG_.debug(('settlement_server_url : ', settle_url))
    while True:
        express_message, status_code = global_post(settle_url, payment_info)
        if (status_code == "200" or status_code == 200) and express_message is not None:
            return
        time.sleep(3)


def start_customer_scan_qr_code():
    global customer_scanner_signal_connect_flag
    if not customer_scanner_signal_connect_flag:
        Scanner._SCANNER_.barcode_result.connect(customer_scan_qr_code)
        customer_scanner_signal_connect_flag = True
    Scanner.start_get_text_info()


def stop_customer_scan_qr_code():
    global customer_scanner_signal_connect_flag
    if not customer_scanner_signal_connect_flag:
        return
    customer_scanner_signal_connect_flag = False
    Scanner._SCANNER_.barcode_result.disconnect(customer_scan_qr_code)
    Scanner.start_stop_scanner()
    _POP_.stop_customer_scan_qr_code_signal.emit('CLOSED')


def customer_scan_qr_code(result):
    global pin_code
    global customer_scanner_signal_connect_flag
    if result == '':
        customer_scanner_signal_connect_flag = False
        _POP_.start_customer_scan_qr_code_signal.emit(result)
        return
    pin_code = str(result)
    customer_scanner_signal_connect_flag = False
    _POP_.start_customer_scan_qr_code_signal.emit(pin_code)


def get_popshop_product(category, maxcount):
    ClientTools.get_global_pool().apply_async(popshop_product, (category, maxcount,))


def popshop_product(category, maxcount):
    try:
        popshop_express_info = dict()
        popshop_info = dict()
        popshop_info["token"] = global_token
        popshop_info["page"] = 1
        popshop_info["category"] = int(category)
        popshop_info["pagesize"] = int(maxcount)
        query_url = global_url + 'paybyqr/list'
        _LOG_.debug(('url_get_popshop : ', query_url))
        express_message, status_code = get_list_popshop(query_url, popshop_info)
        if status_code == 200:
            popshop_express_info["prod_list"] = express_message["data"][0]
            popshop_express_info["prod_total"] = len(express_message["data"][0])
            _POP_.get_popshop_product_signal.emit(str(json.dumps(popshop_express_info)))
        else:
            _POP_.get_popshop_product_signal.emit('ERROR')
            _LOG_.warning(('[ERROR] popshop_product : ', str(express_message)))
    except Exception as e:
        _POP_.get_popshop_product_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] popshop_product : ', str(e)))


def get_query_member_popsend(phone):
    ClientTools.get_global_pool().apply_async(query_member_popsend, (phone,))


def query_member_popsend(phone):
    global post_data_popsend
    try:
        post_data_popsend = {"token": global_token, "phone": str(phone)}
        query_end = global_url + 'member/detail'
        _LOG_.debug(('url_query_member_popsend : ', query_end))
        express_message, status_code = global_post(query_end, post_data_popsend)
        if status_code == 200 and express_message["total_page"] != 0:
            get_data_popsend = express_message["data"][0]
            get_data_popsend['balance'] = get_member_balance(param=post_data_popsend)
            _POP_.get_query_member_popsend_signal.emit(str(json.dumps(get_data_popsend)))
            _LOG_.info(('get_query_member_popsend result : ', get_data_popsend))
        else:
            _POP_.get_query_member_popsend_signal.emit('NOT FOUND')
            _LOG_.warning(('[ERROR] query_member_popsend : ', str(express_message)))
    except Exception as e:
        _POP_.get_query_member_popsend_signal.emit('NOT FOUND')
        _LOG_.warning(('[ERROR] query_member_popsend : ', str(e)))


def get_member_balance(param):
    try:
        url = global_url + 'balance/info'
        _LOG_.debug(('get_member_balance : ', url))
        response, status = global_post(url, param)
        if (status == 200 or status == "200") and response['data'][0]['current_balance'] is not None:
            member_balance = response['data'][0]['current_balance']
        else:
            member_balance = 0
        print(member_balance)
        return member_balance
    except Exception as e:
        _LOG_.warning(('[ERROR] get_member_balance : ', str(e)))
        return None


def start_popsend_topup(amount):
    ClientTools.get_global_pool().apply_async(popsend_topup, (amount,))


def popsend_topup(amount):
    try:
        post_popsend_topup = dict()
        post_popsend_topup["token"] = global_token
        post_popsend_topup["phone"] = post_data_popsend["phone"]
        post_popsend_topup["top_up_amount"] = str(amount)
        timestamp_topup = int(time.time()) * 1000
        post_popsend_topup["transid"] = post_data_popsend["phone"] + "BAL_LOC" + str(timestamp_topup)
        query_end_ = global_url + "balance/topup"
        _LOG_.debug(('url_popsend_topup : ', query_end_))
        express_message, status_code = global_post(query_end_, post_popsend_topup)
        if status_code == "200":
            get_popsend_balance = str(express_message["data"][0]["current_balance"])
            _POP_.start_popsend_topup_signal.emit(get_popsend_balance)
            _LOG_.info(('start_popsend_topup result : ', get_popsend_balance))
        else:
            _POP_.start_popsend_topup_signal.emit('NOT FOUND')
            _LOG_.warning(('start_popsend_topup ERROR : ', str(express_message)))
    except Exception as e:
        _POP_.start_popsend_topup_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] locker_data : ', str(e)))


def get_locker_data(province, city):
    ClientTools.get_global_pool().apply_async(locker_data, (province, city,))


def locker_data(province, city):
    try:
        param = {
            "token": global_token,
            "country": "Indonesia",
            "zip_code": "",
            "province": str(province),
            "city": str(city)
        }
        url = global_url + 'locker/location'
        _LOG_.debug(('url_locker_data : ', url))
        response, status = global_post(url, param)
        if response["response"]["message"] == "OK":
            result = {"data": response["data"], "total": response["total_data"]}
            _POP_.get_locker_data_signal.emit(str(json.dumps(result)))
        else:
            _POP_.get_locker_data_signal.emit('ERROR')
            _LOG_.warning(('get_locker_data ERROR : ', str(response), str(status)))
    except Exception as e:
        _POP_.get_locker_data_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] locker_data : ', str(e)))


def start_popshop_transaction(product, customer, purchase, address):
    ClientTools.get_global_pool().apply_async(popshop_transaction, (product, customer, purchase, address,))


def popshop_transaction(product, customer, purchase, address):
    global timestamp_payment
    try:
        timestamp_payment = int(time.time()) * 1000
        time_payment = datetime.datetime.fromtimestamp(timestamp_payment/1000).strftime('%Y-%m-%d %H:%M')
        post_popshop_transaction = dict()
        post_popshop_transaction["token"] = global_token
        post_popshop_transaction["product_info"] = str(product)
        post_popshop_transaction["customer_info"] = str(customer)
        post_popshop_transaction["purchase_info"] = str(purchase) + "|" + str(time_payment)
        post_popshop_transaction["delivery_address"] = str(address)
        end_url = global_url + "ordershop/submit"
        _LOG_.debug(('url_popshop_transaction : ', end_url))
        express_message, status_code = global_post(end_url, post_popshop_transaction)
        if status_code == 200 and express_message["response"]["message"] == "SUCCESS":
            get_popshop_transaction = express_message["data"]["invoice_id"]
            _POP_.start_popshop_transaction_signal.emit(str(get_popshop_transaction))
        else:
            _POP_.start_popshop_transaction_signal.emit("ERROR")
            _LOG_.warning(('[ERROR] popshop_transaction : ', str(express_message)))
    except Exception as e:
        _POP_.start_popshop_transaction_signal.emit("ERROR")
        _LOG_.warning(('[ERROR] popshop_transaction : ', str(e)))


def get_popsend_button():
    ClientTools.get_global_pool().apply_async(popsend_button)


def popsend_button():
    try:
        post_popsend_button = dict()
        get_popsend_button = dict()
        post_popsend_button["token"] = global_token
        end_url = global_url + "balance/topupinfo"
        _LOG_.debug(('url_get_popsend_button : ', end_url))
        express_message, status_code = global_post(end_url, post_popsend_button)
        if status_code == 200 and express_message["response"]["message"] == "ok":
            get_popsend_button["data_button"] = express_message["data"]
            get_popsend_button["total_button"] = len(express_message["data"])
            _POP_.get_popsend_button_signal.emit(str(json.dumps(get_popsend_button)))
        else:
            _POP_.get_popsend_button_signal.emit("ERROR")
            _LOG_.warning(('[ERROR] popsend_button: ', str(express_message)))
    except Exception as e:
        _POP_.get_popsend_button_signal.emit("ERROR")
        _LOG_.warning(('[ERROR] popsend_button : ', str(e)))


def sepulsa_trx_check(trx_id):
    ClientTools.get_global_pool().apply_async(get_trx_record, (trx_id,))


def get_trx_record(trx_id):
    try:
        post_trx_record = dict()
        post_trx_record["token"] = global_token
        post_trx_record["invoice_id"] = str(trx_id)
        end_url = global_url + "service/sepulsa/getTransaction"
        _LOG_.debug(('url_get_trx_record : ', end_url))
        express_message, status_code = global_post(end_url, post_trx_record)
        if status_code == "200":
            get_trx_data = express_message["data"][0]
            _POP_.sepulsa_trx_check_signal.emit(str(json.dumps(get_trx_data)))
            _LOG_.info(str(json.dumps(express_message)))
        elif status_code == "400":
            get_trx_data = str(express_message["response"]["message"])
            _POP_.sepulsa_trx_check_signal.emit(get_trx_data)
        else:
            _POP_.sepulsa_trx_check_signal.emit("ERROR")
            _LOG_.warning('url_get_trx_record result : ERROR')
    except Exception as e:
        _POP_.sepulsa_trx_check_signal.emit("ERROR")
        _LOG_.warning(('[ERROR] get_trx_record : ', str(e)))


def start_deposit_express():
    ClientTools.get_global_pool().apply_async(store_deposit_express)


def store_direct_popsafe(param):
    _LOG_.debug(("store_direct_popsafe", param))
    ClientTools.get_global_pool().apply_async(store_deposit_express, (param,))


def store_deposit_express(extra_param=None):
    try:
        box_result = BoxService.get_box()
        _LOG_.info(('[BOX_RESULT] BOX', box_result))
        res = {}
        if not box_result:
            print('no_box', str(box_result))
            res['isSuccess'] = 'false'
            res['message'] = 'box_error'
            ExpressService._EXPRESS_.store_express_signal.emit(str(json.dumps(res)))
        mouth_result = BoxService.mouth
        if extra_param is None:
            overdue_time = ExpressService.get_overdue_timestamp(box_result)
            popdeposit_express = ExpressService.customer_store_express
        else:
            overdue_time = get_overdue_hours(24)
            popdeposit_express = json.loads(extra_param)
            popdeposit_express['customerStoreNumber'] = 'PDSL'+random_chars(box_result['validateType'], 5)
            popdeposit_express['logisticsCompany'] = {'id': '161e5ed1140f11e5bdbd0242ac110001'}
        # operator_result = CompanyService.get_company_by_id(box_result['operator_id'])
        # if not operator_result:
        #     ExpressService._EXPRESS_.store_express_signal.emit('Error')
        #     return
        print('masuk start deposit popbox service')
        express_param = {'expressNumber': popdeposit_express['customerStoreNumber'],
                         'expressType': 'COURIER_STORE',
                         'overdueTime': overdue_time,
                         'status': 'IN_STORE',
                         'storeTime': ClientTools.now(),
                         'syncFlag': 0,
                         'takeUserPhoneNumber': popdeposit_express['takeUserPhoneNumber'],
                         'validateCode': ExpressService.random_validate(box_result['validateType']),
                         'version': 0,
                         'box_id': box_result['id'],
                         'logisticsCompany_id': popdeposit_express['logisticsCompany']['id'],
                        #  'logisticsCompany_id': '161e5ed1140f11e5bdbd0242ac110001',
                         'mouth_id': mouth_result['id'],
                         'operator_id': box_result['operator_id'],
                         'storeUser_id': '402880825dbcd4c3015de54d98c5518e',
                         'groupName': 'POPDEPOSIT',
                         'courier_phone': '0',
                         'other_company_name': '',
                         'id': ClientTools.get_uuid(),
                         'transactionRecord': '',
                         'drop_by_courier_login': '',
                         'payment_param': '',
                         'is_insulator':0,
                         'flagRedrop': 0,
                         'chargeType': ''}

        print('express_param', str(express_id))
        _LOG_.debug(("store_direct_popsafe_new", extra_param))


        if extra_param is not None:
            express_param['transactionRecord'] = popdeposit_express['transactionRecord']
            express_param['paymentMethod'] = popdeposit_express['paymentMethod']
            express_param['paymentAmount'] = popdeposit_express['paymentAmount']
            express_param['lockerSize'] = popdeposit_express['lockerSize']
            express_param['lockerNo'] = str(mouth_result['number'])
        while True:
            _LOG_.info(('[ATTEMPT] store_popsafe_db', express_param['expressNumber']))
            store_popsafe_db = ExpressDao.save_express(express_param)
            if store_popsafe_db is True:
                _LOG_.info(('[SUCCESS] store_popsafe_db', express_param['expressNumber']))
                break
        mouth_param = {'id': express_param['mouth_id'],
                       'express_id': express_param['id'],
                       'status': 'USED'}
        BoxService.use_mouth(mouth_param)
        express_param['box'] = {'id': express_param['box_id']}
        express_param.pop('box_id')
        express_param['logisticsCompany'] = {'id': express_param['logisticsCompany_id']}
        express_param.pop('logisticsCompany_id')
        express_param['mouth'] = {'id': express_param['mouth_id']}
        express_param.pop('mouth_id')
        express_param['operator'] = {'id': express_param['operator_id']}
        express_param.pop('operator_id')
        express_param['storeUser'] = {'id': express_param['storeUser_id']}
        express_param.pop('storeUser_id')
        res['isSuccess'] = 'true'
        res['id_express'] = express_param['id']
        res['expressNumber'] = express_param['expressNumber']
        ExpressService._EXPRESS_.store_express_signal.emit(str(json.dumps(res)))
        message, status_code = HttpClient.post_message('express/staffStoreExpress', express_param)
        if status_code == 200 and message['id'] == express_param['id']:
            ExpressDao.mark_sync_success(express_param)
    except Exception as e:
        ExpressService.save_data_to_db_when_error(express_param)
        res['isSuccess'] = 'false'
        res['message'] = str(e)
        print('[ERROR] store_deposit_express')
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.info(('[ERROR] store_deposit_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        ExpressService._EXPRESS_.store_express_signal.emit(str(json.dumps(res)))


def delete_custore_by_id(id):
    try:
        header = {"usertoken": "142487bdbb774060b4711debe39577f4"}
        url = Configurator.get_value("ClientInfo", "serveraddress")
        url_d = url + "express/deleteImportedExpress/" + str(id)
        # logger.debug("URL delete CustomerStoreNumber : " + url_d)
        d = requests.post(url_d, headers=header, json=None, timeout=50)
        d_resp = d.json()
        _LOG_.info("Deletion of CustomerStoreNumber " + str(id) + " : " + str(d_resp))
    except Exception as e:
        _LOG_.warning(("[ERROR] delete_custore_by_id : ", str(e)))


def get_comp_stats():
    if os_platform == 'Linux':      
        # import wmi
        import psutil
        comp_stats = dict()
        # c = wmi.WMI()
        try:
            mem = psutil.virtual_memory()
            disk = psutil.disk_usage('/')
            temp = psutil.sensors_temperatures()
            # memory_free = "%.2f" % (mem.available/1024/1024)
            # disk_free = "%.2f" % (disk.free/1024/1024)
            # print("RAM : ",str(memory_free))
            # print("DISK : ",str(disk_free))
            # print("TEMP : ",str())
            comp_stats["disk_space"] = "%.2f" % (disk.free/1024/1024)
            comp_stats["memory_space"] = "%.2f" % (mem.available/1024/1024)
            comp_stats["cpu_temp"] = "%.2f" % (temp['coretemp'][0].current)
        except Exception as e:
            _LOG_.warning(("[ERROR] get_comp_stats : ", str(e)))

        # try:
        #     disk_space = []
        #     for d in c.Win32_LogicalDisk(Caption="D:"):
        #         disk_space.append(int(d.FreeSpace.strip()) / 1024 / 1024)
        #         comp_stats["disk_space"] = "%.2f" % disk_space[0]
        # except Exception as e:
        #     # logger.error("Error in getting Disk Space : ", e)
        #     comp_stats["disk_space"] = "%.2f" % -1

        # try:
        #     memory_space = []
        #     for e in c.Win32_OperatingSystem():
        #         memory_space.append(int(e.FreePhysicalMemory.strip()) / 1024)
        #         comp_stats["memory_space"] = "%.2f" % memory_space[0]
        # except Exception as e:
        #     # logger.error("Error in getting Memory Space : ", e)
        #     comp_stats["memory_space"] = "%.2f" % -1

        # try:
        #     cpu_temp = []
        #     f = wmi.WMI(namespace="root\wmi")
        #     common = 30
        #     variance = random.uniform(0.09, 1.09)
        #     for g in f.MSAcpi_ThermalZoneTemperature():
        #         cpu_temp.append((int(g.CurrentTemperature) / 10) - 273.15 + variance)
        #         comp_stats["cpu_temp"] = "%.2f" % cpu_temp[0]
        # except Exception as e:
        #     # logger.error("Error in getting CPU Temperature : ", e)
        #     comp_stats["cpu_temp"] = "%.2f" % (common - variance)

        # try:
        #    vss_path = 'D://VSS/ads/'
        #    comp_stats["tvc_list"] = [v for v in os.listdir(vss_path) if os.path.isfile(v)]
        # except Exception as e:
        #    logger.error("Error in getting list of TVC Videos : ", e)
        #    comp_stats["tvc_list"] = "undefined"
        return comp_stats

    else:       #windows
        import wmi
        pythoncom.CoInitialize()
        comp_stats = dict()
        c = wmi.WMI()
        try:
            disk_space = []
            for d in c.Win32_LogicalDisk(Caption="D:"):
                disk_space.append(int(d.FreeSpace.strip()) / 1024 / 1024)
                comp_stats["disk_space"] = "%.2f" % disk_space[0]
        except Exception as e:
            # logger.error("Error in getting Disk Space : ", e)
            comp_stats["disk_space"] = "%.2f" % -1

        try:
            memory_space = []
            for e in c.Win32_OperatingSystem():
                memory_space.append(int(e.FreePhysicalMemory.strip()) / 1024)
                comp_stats["memory_space"] = "%.2f" % memory_space[0]
        except Exception as e:
            # logger.error("Error in getting Memory Space : ", e)
            comp_stats["memory_space"] = "%.2f" % -1

        try:
            cpu_temp = []
            f = wmi.WMI(namespace="root\wmi")
            common = 30
            variance = random.uniform(0.09, 1.09)
            for g in f.MSAcpi_ThermalZoneTemperature():
                cpu_temp.append((int(g.CurrentTemperature) / 10) - 273.15 + variance)
                comp_stats["cpu_temp"] = "%.2f" % cpu_temp[0]
        except Exception as e:
            # logger.error("Error in getting CPU Temperature : ", e)
            comp_stats["cpu_temp"] = "%.2f" % (common - variance)

        # try:
        #    vss_path = 'D://VSS/ads/'
        #    comp_stats["tvc_list"] = [v for v in os.listdir(vss_path) if os.path.isfile(v)]
        # except Exception as e:
        #    logger.error("Error in getting list of TVC Videos : ", e)
        #    comp_stats["tvc_list"] = "undefined"
        return comp_stats


def box_start_migrate():
    ClientTools.get_global_pool().apply_async(start_migrate)


def start_migrate():
    if 'pr0x' in Configurator.get_value('ClientInfo', 'serveraddress'):
        _POP_.box_start_migrate_signal.emit('NO NEED')
        return
    else:
        new_url = 'http://pr0x-dev.popbox.asia/'
        new_db = 'popboxclient.db'
        try:
            param_ = BoxService.box_config
            if param_ is not None or param_ != '':
                response, status_code = global_post(new_url + 'task/start/migrate', param_)
                if status_code == 200 and response is not None:
                    path_db = sys.path[0] + '/database/'
                    try:
                        resync_table('UPDATE express SET syncFlag =:syncFlag WHERE id not null', {'syncFlag': 0})
                    except Exception as e:
                        _LOG_.debug(("Force resync_table for : ", e))
                    force_rename(path_db + 'pakpobox.db', path_db + new_db)
                    Configurator.set_value('ClientInfo', 'serveraddress', new_url)
                    Configurator.set_value('ClientInfo', 'dbname', new_db)
                    _POP_.box_start_migrate_signal.emit('SUCCESS')
                    _LOG_.info(("Locker Migration SUCCESS : ", status_code))
                else:
                    _POP_.box_start_migrate_signal.emit('FAILED')
                    _LOG_.warning(("Locker Migration FAILED : ", status_code))
            else:
                _POP_.box_start_migrate_signal.emit('ERROR')
                _LOG_.warning("Locker Migration ERROR..!")
        except Exception as e:
            _LOG_.warning(("start_migrate FAILED : ", e))


def force_resync_all():
    # 7 Days Data For Roll-Back Sync
    rangeTime = (int(time.time()) - (7 * 604800)) * 1000
    try:
        resync_table('UPDATE express SET syncFlag =:syncFlag WHERE id not null AND storeTime >:limitTime',
                     {'syncFlag': 0, 'limitTime': rangeTime})
    except Exception as e:
        _LOG_.debug(("Force resync_table express exception : ", e))
    try:
        resync_table('UPDATE mouth SET syncFlag =:syncFlag WHERE id not null', {'syncFlag': 0})
    except Exception as e:
        _LOG_.debug(("Force resync_table mouth exception : ", e))


def start_get_ads_images(zpath):
    ClientTools.get_global_pool().apply_async(get_ads_images, (zpath,))


def get_ads_images(zpath):
    get_file_dir(directory_path=zpath)


def start_delete_express_by_range(limit, duration):
    ClientTools.get_global_pool().apply_async(delete_express_by_range, (limit, duration,))


def delete_express_by_range(limit, duration):
    if limit is None or duration is None:
        return
    ExpressDao.delete_express(limit=int(limit), duration=duration)
    _LOG_.info(('Delete Express If More Than ', str(limit), str(duration)))


def execute_command(command):
    try:
        os.system(command)
        _LOG_.info(('Remote Executing : ', command))
    except Exception as e:
        _LOG_.warning(('Remote Executing : ', str(e)))


def start_post_gui_info():
    ClientTools.get_global_pool().apply_async(post_gui_info)


def post_gui_info():
    global version
    try:
        message, status_code = HttpClient.post_message('box/guiInfo', {"gui_version": str(version)})
        _LOG_.info(('post_gui_info : ', str(status_code), str(message)))
    except Exception as e:
        _LOG_.warning(('[ERROR] post_gui_info: ', str(e)))


def start_post_subscribe_data(name, cust_data):
    ClientTools.get_global_pool().apply_async(subscribe_data, (name, cust_data,))


def subscribe_data(name, cust_data):
    try:
        post_subscribe_data = dict()
        if "||" in cust_data:
            cust_email = cust_data.split("||")[0]
            cust_phone = cust_data.split("||")[1]
            post_subscribe_data["phone"] = cust_phone
        else:
            cust_email = cust_data
        post_subscribe_data["token"] = global_token
        post_subscribe_data["name"] = str(name)
        post_subscribe_data["email"] = cust_email
        post_subscribe_data["register_place"] = locker_info["name"]
        query_url = global_url + 'member/newsletter'
        _LOG_.info(('subscribe_data url : ' + query_url + ", cust_data : " + cust_data))
        express_message, status_code = global_post(query_url, post_subscribe_data)
        if status_code == "200":
            result = express_message["response"]["message"]
            _POP_.start_post_subscribe_data_signal.emit(result)
        else:
            _POP_.start_post_subscribe_data_signal.emit("ERROR")
            _LOG_.warning(("start post subscribe_data result : ", status_code))
    except Exception as e:
        _POP_.start_post_subscribe_data_signal.emit("ERROR")
        _LOG_.warning(("[ERROR] subscribe_data : ", str(e)))


def start_get_file_dir(dir_):
    ClientTools.get_global_pool().apply_async(get_file_dir, (dir_,))


def get_file_dir(directory_path):
    if directory_path == "" or directory_path is None:
        _POP_.start_get_file_dir_signal.emit("ERROR")
        return
    if "video" in str(directory_path):
        ext_files = ('.mp4', '.mov', '.avi', '.mpg', '.mpeg')
    elif "image" in str(directory_path):
        ext_files = ('.png', '.jpeg', '.jpg')
    elif "music" in str(directory_path):
        ext_files = ('.mp3', '.ogg', '.wav')
    else:
        ext_files = ('.png', '.jpeg', '.jpg')

    try:
        _dir_ = directory_path.replace(".", "")
        _LOG_.info(("getting files from : ", _dir_))
        _tvclist = [xyz for xyz in os.listdir(sys.path[0] + _dir_) if xyz.endswith(ext_files)]
        _LOG_.info(("getting files list : ", _tvclist))
        post_tvclist(json.dumps(_tvclist))
        files = {"output": _tvclist}
        _POP_.start_get_file_dir_signal.emit(json.dumps(files))
    except Exception as e:
        _POP_.start_get_file_dir_signal.emit("ERROR")
        _LOG_.warning(("[ERROR] get_file_dir : ", str(e)))


def start_get_file_banner(dir_):
    ClientTools.get_global_pool().apply_async(get_file_banner, (dir_,))


def get_file_banner(directory_path):
    if directory_path == "" or directory_path is None:
        _POP_.start_get_file_banner_signal.emit("ERROR")
        return
    if "video" in str(directory_path):
        ext_files = ('.mp4', '.mov', '.avi', '.mpg', '.mpeg')
    elif "image" in str(directory_path):
        ext_files = ('.png', '.jpeg', '.jpg')
    elif "music" in str(directory_path):
        ext_files = ('.mp3', '.ogg', '.wav')
    else:
        ext_files = ('.png', '.jpeg', '.jpg')

    try:
        _dir_ = directory_path.replace(".", "")
        _LOG_.info(("getting files from : ", _dir_))
        _bannerlist = [xyz for xyz in os.listdir(sys.path[0] + _dir_) if xyz.endswith(ext_files)]
        files = {"output": _bannerlist}
        _POP_.start_get_file_banner_signal.emit(json.dumps(files))
    except Exception as e:
        _POP_.start_get_file_banner_signal.emit("ERROR")
        _LOG_.warning(("[ERROR] get_file_dir : ", str(e)))


# TODO Activate this function if required only, (missing imutils module issue)
# def start_get_detection(method):
#     if Detector.START_DETECTION is False:
#         ClientTools.get_global_pool().apply_async(get_detection, (method,))
#
#
# def get_detection(method):
#     if method is None or method == "all":
#         Detector.setting["face_detector"] = True
#         Detector.setting["motion_detector"] = True
#     elif method == "face":
#         Detector.setting["face_detector"] = True
#         Detector.setting["motion_detector"] = False
#     elif method == "motion":
#         Detector.setting["face_detector"] = False
#         Detector.setting["motion_detector"] = True
#
#     try:
#         Detector.setting["status"] = True if (Configurator.get_value('panel', 'detector') == "enabled") else False
#         Detector.setting["prompt"] = True
#         Detector.setting["printable"] = False
#         print("Detector Setting : " + str(Detector.setting))
#         status, response = Detector.start_detection()
#         # print("Detector Result : ", str(response))
#         if status is True and 'error' not in response:
#             _POP_.start_get_detection_signal.emit(json.dumps(response))
#         else:
#             _POP_.start_get_detection_signal.emit("ERROR")
#     except Exception as e:
#         _POP_.start_get_detection_signal.emit("FAILED")
#         logger.warning(("get_detection ERROR : ", e))
#


payment_token = "PHVVH4KXB9QJUK6NZ6EKGQNFQOHLVGVFX87RUYBV"
payment_url = "https://payment.popbox.asia/api"


def define_dev_payment():
    global payment_token, payment_url
    if Configurator.get_value('popbox', 'dummy^transaction') == '1':
        payment_url = "http://paymentdev.popbox.asia/api"
        payment_token = "ZNZNEONSYG6SUAFEPYNACR1H2AMJ29DS7D3630AH"


define_dev_payment()

def get_trans_id():
    return "LKR" + time.strftime("%y%m%d%H%M") + ClientTools.get_random_chars(length=3)


PAYMENT_ID_GLOBAL = ""
TRANS_ID_GLOBAL = ""
param_id = ""
param_order = ""
status_payment_express = False


def start_create_payment_yap(amount, cust_name, item, locker_name):
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name,))


def start_create_payment_tcash(amount, cust_name, item, locker_name):
    _channel = 'TCASH'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel,))


def start_create_payment_ottoqr(amount, cust_name, item, locker_name):
    _channel = 'OTTO-QR'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel,))

def start_create_payment_gopay(amount, cust_name, item, locker_name):
    _channel = 'MID-GOPAY'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel,))

def start_create_payment_ovo(amount, cust_name, item, locker_name, transaction_id):
    _channel = 'OVO'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel, "",transaction_id,))

def generate_ovo_transaction():
    transaction_id = get_trans_id()
    _POP_.ovo_transaction_no_signal.emit(str(transaction_id))

def start_create_payment_emoney(amount, cust_name, item, locker_name, data_emoney):
    _channel = 'MANDIRI-EMONEY'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel, data_emoney, ))

def start_create_payment_boost(amount, cust_name, item, locker_name):
    _channel = 'BOOST'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel,))

def start_create_payment_grabpay(amount, cust_name, item, locker_name):
    _channel = 'GRABPAY'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel,))

def start_create_payment_dana(amount, cust_name, item, locker_name):
    _channel = 'DANA'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel,))

def start_create_payment_beepcard(amount, cust_name, item, locker_name,data_emoney):
    _channel = 'BEEPCARD'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel, data_emoney, ))

def start_create_payment_qrpayment(amount, cust_name, item, locker_name):
    _channel = 'QR-PAYMENT'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel,))
def generate_free_transaction():
    transaction_id = get_trans_id()
    _POP_.generate_free_transaction_signal.emit(str(transaction_id))

def generate_nfc_transaction():
    transaction_id = get_trans_id()
    _POP_.nfc_transaction_no_signal.emit(str(transaction_id))
    print("nfc transaction EMITTED")
    

def get_payment_session(clientid="001"):
    try:
        param = {
            "token": payment_token,
            "client_id": clientid
        }
        url = payment_url + "/v1/createSession"
        _LOG_.debug(('get_payment_session : ', url))
        response, status = global_post(url, param)
        if status == 200 and response['data'][0]['session_id'] is not None:
            return response['data'][0]['session_id']
        else:
            return None
    except Exception as e:
        _LOG_.warning(('[ERROR] get_payment_session : ', str(e)))
        return None

def create_payment_global(amount, cust_name, item, locker_name, channel='BNI-YAP', emoney="", transaction_id=""):
    global PAYMENT_ID_GLOBAL, TRANS_ID_GLOBAL
    res = {}
    res['isSuccess'] = "false"
    try:
        if amount is None or cust_name is None:
            _POP_.start_create_payment_signal.emit(str(json.dumps(res)))
            return
        session_id = get_payment_session()
        if channel == 'MANDIRI-EMONEY' or channel == 'BEEPCARD':
            if session_id is None:
                pass
        else:
            if session_id is None:
                _POP_.start_create_payment_signal.emit(str(json.dumps(res)))
                return

        paymentRequest = True
        trans_id = (transaction_id if channel == "OVO" else get_trans_id())
        TRANS_ID_GLOBAL = trans_id
        cust_name = cust_name.split('||')
        param = {
            "token": payment_token,
            "session_id": session_id,
            "method_code": channel,
            "amount": int(amount),
            "billing_type": "fixed",
            "transaction_id": trans_id,
            "customer_name": cust_name[0],
            "customer_phone": cust_name[1],
            "customer_email": cust_name[2],
            "description": item if item is not None else "N/A",
            "location_type": "locker",
            "location_name": locker_name if locker_name is not None else locker_info['name'],
            "data_emoney": emoney
        }
        box = BoxDao.get_box_info()
        param['locker_id'] = box[0]['id']
        # print(("status_payment_express PY " + str(status_payment_express)))
        if status_payment_express == True :
            param['id_order_number'] = param_id
            param['locker_order_number'] = param_order

        if channel == 'OVO':
            merchant_id = Configurator.get_value('QrOvo', 'merchant_id')
            store_id = Configurator.get_value('QrOvo', 'store_id')
            tid = Configurator.get_value('QrOvo', 'tid')
            mid = Configurator.get_value('QrOvo', 'mid')

            param['store_id'] = store_id
            param['tid'] = tid
            param['mid'] = mid
        
        if session_id is None:
            paymentRequest = False
        else:
            url = payment_url + "/v1/payment/createPayment"
            response, status = global_post(url, param)

        if channel == 'MANDIRI-EMONEY' or channel == 'BEEPCARD' :
            if paymentRequest is not False:
                try:
                    checkResponse = True
                    response['response']['code']
                except Exception as e:
                    # _CAP_(e)
                    checkResponse = False

                _LOG_.debug(("[CHECKING_RESPONSE] : ", response))
                if checkResponse is not False:
                    if status == 200 and (response['response']['code'] == 200 or len(response['data']) > 0):
                        PAYMENT_ID_GLOBAL = response['data'][0]['payment_id']
                        res = response['data'][0]
                        res['isSuccess'] = "true"
                        _POP_.start_create_payment_signal.emit(json.dumps(res))
                    elif status == 500 and response['response']['code'] == 500:
                        _LOG_.debug(("[PAYMENT_RESPONSE] : FALSE"))
                        response = {
                            "token": payment_token,
                            "session_id": "",
                            "method_code": channel,
                            "amount": int(amount),
                            "billing_type": "fixed",
                            "transaction_id": trans_id,
                            "customer_name": cust_name[0],
                            "customer_phone": cust_name[1],
                            "customer_email": cust_name[2],
                            "description": item if item is not None else "N/A",
                            "location_type": "locker",
                            "location_name": locker_name if locker_name is not None else locker_info['name'],
                            "data_emoney": emoney,
                            "payment_offline": "true",
                            "isSuccess": "true"
                        }
                        _POP_.start_create_payment_signal.emit(json.dumps(response))

                    else:
                        res = response['data'][0]
                        res['isSuccess'] = "true"
                        _POP_.start_create_payment_signal.emit(json.dumps(res))

                elif response['statusCode'] == -1:
                    _LOG_.debug(("[PAYMENT_RESPONSE] : FALSE"))
                    response = {
                        "token": payment_token,
                        "session_id": "",
                        "method_code": channel,
                        "amount": int(amount),
                        "billing_type": "fixed",
                        "transaction_id": trans_id,
                        "customer_name": cust_name[0],
                        "customer_phone": cust_name[1],
                        "customer_email": cust_name[2],
                        "description": item if item is not None else "N/A",
                        "location_type": "locker",
                        "location_name": locker_name if locker_name is not None else locker_info['name'],
                        "data_emoney": emoney,
                        "payment_offline": "true"
                    }
                    _LOG_.warning(("[PAYMENT_ONLINE_OFFLINE] create_payment_global : ", response))
                    res = response
                    res['isSuccess'] = "true"
                    _POP_.start_create_payment_signal.emit(json.dumps(res))
            else:
                response = {
                    "token": payment_token,
                    "session_id": "",
                    "method_code": channel,
                    "amount": int(amount),
                    "billing_type": "fixed",
                    "transaction_id": trans_id,
                    "customer_name": cust_name[0],
                    "customer_phone": cust_name[1],
                    "customer_email": cust_name[2],
                    "description": item if item is not None else "N/A",
                    "location_type": "locker",
                    "location_name": locker_name if locker_name is not None else locker_info['name'],
                    "data_emoney": emoney,
                    "payment_offline": "true"
                }
                _LOG_.warning(("[PAYMENT_OFFLINE] create_payment_global : ", response))
                res = response
                res['isSuccess'] = "true"
                _POP_.start_create_payment_signal.emit(json.dumps(res))
        else:
            if status == 200 and (response['response']['code'] == 200 or len(response['data']) > 0):
                PAYMENT_ID_GLOBAL = response['data'][0]['payment_id']
                if channel != 'OVO':
                    ssl._create_default_https_context = ssl._create_unverified_context #SSL issue test
                    urllib.request.urlretrieve(response['data'][0]['url'], "qr-payment/"+PAYMENT_ID_GLOBAL+".png")

                res = response['data'][0]
                res['isSuccess'] = "true"
                _LOG_.warning(("[PAYMENT] create_payment_global : ", res))
                _POP_.start_create_payment_signal.emit(str(json.dumps(res)))
            else:
                res['isSuccess'] = "false"
                _POP_.start_create_payment_signal.emit(json.dumps(res))

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] create_payment_global : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        res = {}
        res['isSuccess'] = "false"
        _POP_.start_create_payment_signal.emit(json.dumps(res))

def start_temp_express_session(parameter):
    ClientTools.get_global_pool().apply_async(temp_express_session, (parameter,))

def temp_express_session(parameter):
    global param_id, param_order, status_payment_express
    data_json = json.loads(parameter)
    print("DATA JSON PY : " + str(data_json))
    param_id = data_json['expressId']
    param_order = data_json['expressNumber']
    status_payment_express = True



def start_clear_transaction():
    ClientTools.get_global_pool().apply_async(clear_transaction)

def clear_transaction():
    global TRANS_ID_GLOBAL, PAYMENT_ID_GLOBAL, param_id, param_order, status_payment_express
    PAYMENT_ID_GLOBAL = ""
    TRANS_ID_GLOBAL = ""
    param_id = ""
    param_order = ""
    status_payment_express = False

def start_check_trans_global():
    ClientTools.get_global_pool().apply_async(check_trans_global)

def check_trans_global():
    global TRANS_ID_GLOBAL, PAYMENT_ID_GLOBAL
    if DUMMY_RESPONSE is True:
        dummy_response = {
            'status': 'PAID',
            'transaction_id': TRANS_ID_GLOBAL,
            'payment_id': PAYMENT_ID_GLOBAL,
            'expired_datetime': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'url': 'http://dummy.url.img',
            'isSuccess': 'true'
        }
        _POP_.start_check_trans_global_signal.emit(json.dumps(dummy_response))
        return
    try:
        param = {
            "token": payment_token,
            "session_id": get_payment_session(),
            "trans_id": TRANS_ID_GLOBAL,
            "payment_id": PAYMENT_ID_GLOBAL
        }
        url = payment_url + "/v1/payment/inquiryPayment"
        response, status = global_post(url, param)
        if status == 200 and len(response['data']) > 0:
            res = response['data'][0]
            res['isSuccess'] = "true"
            _POP_.start_check_trans_global_signal.emit(json.dumps(res))
        else:
            res = {}
            res['isSuccess'] = "false"
            _POP_.start_check_trans_global_signal.emit(str(json.dumps(res)))
    except Exception as e:
        res = {}
        res['isSuccess'] = "false"
        res['error'] = str(e)
        _POP_.start_check_trans_global_signal.emit(str(json.dumps(res)))
        _LOG_.warning(("[ERROR] check_trans_global: ", str(e)))
        PAYMENT_ID_GLOBAL = ""
        TRANS_ID_GLOBAL = ""


def post_tvclist(list_):
    try:
        HttpClient.post_message('box/tvcList', {"tvclist": list_})
        # logger.info(('post_tvclist RESULT : ', response))
    except Exception as e:
        _LOG_.warning(("[ERROR] post_tvclist: ", str(e)))


def start_post_tvclog(media):
    ClientTools.get_global_pool().apply_async(post_tvclog, (media,))


def post_tvclog(media):
    try:
        param = {
            "filename": media,
            "country": "ID",
            "playtime": time.strftime("%Y-%m-%d %H")
        }
        HttpClient.post_message('box/tvcLog', param)
        # logger.info(("post_tvclog RESULT : ", status, response))
    except Exception as e:
        _LOG_.warning(("[ERROR] post_tvclog : ", str(e)))


def start_post_activity(activity):
    ClientTools.get_global_pool().apply_async(post_activity, (activity,))


def post_activity(activity):
    try:
        param = {
            "activity": activity,
            "country": "ID",
            "recordtime": time.strftime("%Y-%m-%d %H")
        }
        HttpClient.post_message('box/activityLog', param)
        # logger.info(("post_activity RESULT : ", status, response))
    except Exception as e:
        _LOG_.warning(("[ERROR] post_activity : ", str(e)))


def start_extend_express(express_no):
    ClientTools.get_global_pool().apply_async(extend_express, (express_no,))


def extend_express(express_no):
    if express_no is None:
        return
    try:
        __express_no = json.loads(express_no)
        param = {
            "expressNumber": __express_no["expressNumber"],
            "syncFlag": 0,
            "overdueTime": get_overdue_hours(24)
        }
        ExpressDao.local_extend_express(param)
        _LOG_.debug(("extend_express", param))
        send_param = {
            "token": "4EI5COIOWBVPF6JVXTFPP4TMC8LZYPZ5J5HYUA1SKXEQQXA10Q",
            "session_id": __express_no["userSession"],
            "invoice_id": __express_no["expressNumber"],
            "remarks": __express_no["trxRemarks"]
            }
        global_post("http://popsendv2.popbox.asia/popsafe/extend", send_param)
    except Exception as e:
        _LOG_.warning(str(e))

def start_checking_popsend(express_no):
    ClientTools.get_global_pool().apply_async(checking_popsend, (express_no,))

def checking_popsend(express_no):
    try:
        global express_id
        message, status_code = HttpClient.get_message('express/customerExpress/' + express_no )
        _LOG_.info(("[CHECK_POPSAFE_OR_POPSEND_OR_DEMAND]", str(message)))
        if status_code == 200:
            if ClientTools.get_value("statusCode", message) == 404:
                electronic_reject_express = {}
                electronic_reject_express['isSuccess'] = "false"
                _POP_.start_checking_popsend_signal.emit(str(json.dumps(electronic_reject_express)))        
            else:
                _LOG_.info(("[CHECK_DATA_IMPORTED] 03"))
                electronic_reject_express = message
                electronic_reject_express['chargeType'] = 'NOT_CHARGE'
                electronic_reject_express['package_id'] = express_no
                electronic_reject_express['type'] = message['parcelType']
                electronic_reject_express['box_name'] = locker_info['name']
                electronic_reject_express['isSuccess'] = 'true'
                electronic_reject_express['takeUserPhoneNumber'] = ""
                _POP_.start_checking_popsend_signal.emit(str(json.dumps(electronic_reject_express)))
        else:
            electronic_reject_express = {}
            electronic_reject_express['isSuccess'] = "false"
            _POP_.start_checking_popsend_signal.emit(str(json.dumps(electronic_reject_express)))
                
    except Exception as e:
        res = {
            'isSuccess' : 'false',
            'message' : str(e)
        }
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] start_checking_popsend : ','<errMessage>:', str(e), '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _POP_.start_checking_popsend_signal.emit(str(res))


def start_checking_return(express_no):
    ClientTools.get_global_pool().apply_async(checking_return, (express_no,))

def checking_return(express_no):
    try:
        global express_id
        global electronic_reject_express
        check_return = False
        rules = BoxService.get_return_rules()
        uniqueKey = '{{p0pb0x_4514}}' 
        _LOG_.info(("pattern_awb_checking_return", rules))
        _LOG_.info(("locker_info_checking_return", str(locker_info)))
        for rtn_list in rules:
            pattern = rtn_list['regularContent']
            preg_replace = re.sub(pattern.replace('\\','\\'), '{{p0pb0x_4514}}', express_no)
            if preg_replace == uniqueKey :
                check_return = True
                message, status_code = HttpClient.get_message('express/reject/checkRule/' + express_no + '?type=' + rtn_list['groupName'])
                if status_code == 200:
                    if ClientTools.get_value("statusCode", message) == 404:
                        electronic_reject_express = {}
                        electronic_reject_express['isSuccess'] = "false"
                        _POP_.start_checking_return_signal.emit(str(json.dumps(electronic_reject_express)))
                    else:
                        electronic_reject_express = message
                        electronic_reject_express['chargeType'] = 'NOT_CHARGE'
                        electronic_reject_express['package_id'] = express_no
                        electronic_reject_express['type'] = 'RETURN'
                        electronic_reject_express['box_name'] = locker_info['name']
                        electronic_reject_express['isSuccess'] = 'true'
                        _POP_.start_checking_return_signal.emit((json.dumps(electronic_reject_express)))
                else:
                    electronic_reject_express = {}
                    electronic_reject_express['isSuccess'] = "false"
                    _POP_.start_checking_return_signal.emit(str(json.dumps(electronic_reject_express)))
           
        if check_return == False :
            electronic_reject_express = {}
            electronic_reject_express['isSuccess'] = "false"
            _LOG_.info(("checking_return", str(json.dumps(electronic_reject_express))))
            _POP_.start_checking_return_signal.emit(str(json.dumps(electronic_reject_express)))
            # _POP_.start_checking_return_signal.emit(str(json.dumps(electronic_reject_express)))

    except Exception as e:
        res = {
            'isSuccess' : 'false',
            'message' : str(e)
        }
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] start_checking_return : ','<errMessage>:', str(e), '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _POP_.start_checking_return_signal.emit(str(res))



def checking_awb(express_no):
    ClientTools.get_global_pool().apply_async(checking_pattern_awb, (express_no,))

express_id = ''

def checking_pattern_awb(express_no=None):
    try:
        if express_no is None:
            return
            
        global express_id
        global electronic_reject_express
        print('checking_awb_python', str(express_no))
        parcel_return = False
        rules = BoxService.get_return_rules()
        uniqueKey = '{{p0pb0x_4514}}'
        _LOG_.info(("pattern_awb", rules))
        _LOG_.info(("locker_info_checking_pattern", str(locker_info)))
        if (rules==False):
            electronic_reject_express = {}
            electronic_reject_express['isSuccess'] = "false"
            _POP_.start_check_awb_signal.emit(str(json.dumps(electronic_reject_express)))
            return
        else:
            for rtn_list in rules:
                pattern = rtn_list['regularContent']
                preg_replace = re.sub(pattern.replace('\\','\\'), '{{p0pb0x_4514}}', express_no)
                if preg_replace == uniqueKey :
                    parcel_return = True
                    message, status_code = HttpClient.get_message('express/reject/checkRule/' + express_no + '?type=' + rtn_list['groupName'])
                    if status_code == 200:
                        if ClientTools.get_value("statusCode", message) == 404:
                            electronic_reject_express = {}
                            electronic_reject_express['isSuccess'] = "true"
                            electronic_reject_express['type'] = 'LASTMILE'
                            electronic_reject_express['box_name'] = locker_info['name']
                            _POP_.start_check_awb_signal.emit(str(json.dumps(electronic_reject_express)))
                            return
                        else:
                            electronic_reject_express = message
                            electronic_reject_express['chargeType'] = 'NOT_CHARGE'
                            electronic_reject_express['package_id'] = express_no
                            electronic_reject_express['type'] = 'RETURN'
                            electronic_reject_express['box_name'] = locker_info['name']
                            electronic_reject_express['isSuccess'] = 'true'
                            _POP_.start_check_awb_signal.emit(str(json.dumps(electronic_reject_express)))
                            return
                    else:
                        electronic_reject_express = {}
                        electronic_reject_express['isSuccess'] = "false"
                        _POP_.start_check_awb_signal.emit(str(json.dumps(electronic_reject_express)))
                        return
           
        if parcel_return == False :
            # POPSEND, POPSAFE AND ONDEMAND
            # CHECKING LOCATION IF NOT SAME WITH SERVER
            message, status_code = HttpClient.get_message('express/customerExpress/' + express_no )
            _LOG_.info(("[CHECK_POPSAFE_OR_POPSEND_OR_DEMAND]", str(message)))
            if status_code == 200:
                if ClientTools.get_value("statusCode", message) == 404:
                    # message, status_code = HttpClient.get_message('express/imported/' + express_no )
                    message, status_code = ExpressService.inquiry_preload_data(express_no)
                    _LOG_.info(("[CHECK_DATA_IMPORTED]", str(message)))
                    _LOG_.info(("[LOCKER_INFO]", str(locker_info)))
                    if status_code == 200:
                        if 'expressNumber' in message:
                            express_id = message['id']
                            electronic_reject_express = message
                            electronic_reject_express['chargeType'] = 'NOT_CHARGE'
                            electronic_reject_express['type'] = 'LASTMILE'
                            electronic_reject_express['takeUserPhoneNumber'] = message['takeUserPhoneNumber']
                            # electronic_reject_express['box_name'] = locker_info['name']
                            electronic_reject_express['isSuccess'] = 'true'
                            _POP_.start_check_awb_signal.emit(str(json.dumps(electronic_reject_express)))
                            return
                        else:
                            electronic_reject_express = {}
                            electronic_reject_express['isSuccess'] = "true"
                            electronic_reject_express['type'] = 'LASTMILE'
                            electronic_reject_express['takeUserPhoneNumber'] = ""
                            electronic_reject_express['box_name'] = locker_info['name']
                            _POP_.start_check_awb_signal.emit(str(json.dumps(electronic_reject_express)))
                            return
                else:
                    electronic_reject_express = message
                    electronic_reject_express['chargeType'] = 'NOT_CHARGE'
                    electronic_reject_express['package_id'] = express_no
                    electronic_reject_express['type'] = message['parcelType']
                    electronic_reject_express['box_name'] = locker_info['name']
                    electronic_reject_express['isSuccess'] = 'true'
                    electronic_reject_express['takeUserPhoneNumber'] = ""
                    _POP_.start_check_awb_signal.emit(str(json.dumps(electronic_reject_express)))
                    return
            else:
                electronic_reject_express = {}
                electronic_reject_express['isSuccess'] = "true"
                electronic_reject_express['type'] = 'LASTMILE'
                electronic_reject_express['box_name'] = locker_info['name']
                electronic_reject_express['takeUserPhoneNumber'] = ""
                _POP_.start_check_awb_signal.emit(str(json.dumps(electronic_reject_express)))
                return
    except Exception as e:
        res = {
            'isSuccess' : 'false',
            'message' : str(e)
        }
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] checking_pattern_awb : ','<errMessage>:', str(e), '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _POP_.start_check_awb_signal.emit(str(res))

def start_hour_overdue():
    ClientTools.get_global_pool().apply_async(get_hour_overdue)

def get_hour_overdue():
    hour_overdue = Configurator.get_value('OverdueTime', 'hours')
    hour_overdue = int(float(hour_overdue))
    _POP_.start_hour_overdue_result_signal.emit(str(hour_overdue))

def start_checking_otp_validality(phone):
    ClientTools.get_global_pool().apply_async(checking_otp_validality, (phone,))

def checking_otp_validality(phone):
    try:
        res = {}
        otp_session = check_otp_valid(phone)
        if otp_session is True:
            res['isSuccess'] = "true"
            res['session_valid'] = "true"
            ExpressService.set_courier_phone(phone)
            _POP_.start_send_checking_otp_signal.emit(str(json.dumps(res)))
            return
        else:
            res['isSuccess'] = "false"
            res['session_valid'] = "false"
            _POP_.start_send_checking_otp_signal.emit(str(json.dumps(res)))
            return
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR_CHECKING_VALIDALITY_OTP] :','<errMessage>:', str(e), '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        res['isSuccess'] = "false"
        _POP_.start_send_checking_otp_signal.emit(str(json.dumps(res)))   
        return       

def send_request_otp(phone, req_type, otp):
    ClientTools.get_global_pool().apply_async(send_otp_user, (phone,req_type,otp,))

def send_otp_user(phone, req_type,otp=None):
    _LOG_.info(("[SEND_OTP_USER_RES]", phone))
    res = {}
    param = {}
    try:
        param['token'] = global_token
        param['phone'] = phone
        res['validation']="false"
        res['type']= req_type
        res['session_valid'] = "false"

        
        # set courier phone
        ExpressService.set_courier_phone(phone)
        otp_session = check_otp_valid(phone)
        if otp_session is True:
            res['isSuccess'] = "true"
            res['session_valid'] = "true"
            _POP_.start_send_otp_signal.emit(str(json.dumps(res)))
        else:
            if req_type == "request":
                query_url = global_url + 'member/register'
            elif req_type == "resend":
                query_url = global_url + 'member/resendotp'
            else:
                param['pin'] = otp
                query_url = global_url + 'member/validation'

            request = param
            otp_response, status_code = global_post(query_url, request)
            _LOG_.info(("[REQUEST_OTP]", str(otp_response)))

            if otp_response['response']['code'] == '200' or otp_response['response']['code'] == 200:
                if req_type == 'validation':
                    res['validation']="true"
                    # date = (datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
                    otp_data = otp_response['data'][0]
                    record_otp(phone, otp_data['valid_date'])

                
                res['isSuccess'] = "true"           
                res['code'] = otp_response['response']['code']
                res['message'] = otp_response['response']['message']
                _LOG_.info(("[RESULT_OTP]", request))
                _POP_.start_send_otp_signal.emit(str(json.dumps(res)))
                return
            else:
                res['isSuccess'] = "false"
                res['code'] = otp_response['response']['code']
                res['message'] = "Failed to request otp"
                _POP_.start_send_otp_signal.emit(str(json.dumps(res)))  
                return

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR_SEND_OTP] :','<errMessage>:', str(e), '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        res['isSuccess'] = "false"
        _POP_.start_send_otp_signal.emit(str(json.dumps(res)))   
        return     

## UPDATE SINCE ANDALAS V2
def start_time_overdue():
    ClientTools.get_global_pool().apply_async(get_time_overdue)

def get_time_overdue():
    time_overdue = Configurator.get_value('OverdueTime', 'take^overdue')
    _POP_.start_time_overdue_result_signal.emit(str(time_overdue))


def is_internet_available():
    global connectionStatus
    try:
        internet_url = Configurator.get_value('ClientInfo', 'serveraddress')
        urlopen(internet_url, timeout=1)
        connectionStatus = True
        # time.sleep(1)
        return connectionStatus
    except:
        connectionStatus = False
        # time.sleep(1)
        return connectionStatus


def internet_status():
    counter = 0
    for i in range(3):
        if is_internet_available() == True:
            counter += 1
        elif is_internet_available() == False:
            counter -= 1
    if counter >= 3:
        return True
    else:
        return False
	
def start_get_internet_status():
    ClientTools.get_global_pool().apply_async(get_internet_status)

def get_internet_status():
    # statusConnection = is_internet_available()
    statusConnection = internet_status()
    if(statusConnection):
        _POP_.start_internet_status_result_signal.emit("SUCCESS")
    else:
        _POP_.start_internet_status_result_signal.emit("ERROR")

def get_parcel_data_bypin(pincode):
    ClientTools.get_global_pool().apply_async(parcel_data_bypin, (pincode,))

def parcel_data_bypin(pincode):
    global get_parcel_data
    global global_pin
    global_pin = pincode
    send = {
        'validateCode': pincode,
        'status': 'IN_STORE'
    }
    try:
        check_express = ExpressDao.get_express_by_validate(send)
        flagLastmilePaid = 0
        print("[*] parcel_data_bypin : %r " % len(check_express) )
        if len(check_express) != 0:
            expr = check_express[0]
            param = {'id': expr['id']}
            data_express = ExpressDao.get_express_by_id(param)
            express = data_express[0]

            time_now = datetime.datetime.now().timestamp()
            times = int(express['storeTime']) / 1000
            day_spend = math.trunc((time_now - int(times)) / 84000)
            hour_duration = math.trunc((time_now - int(times)) / 3600)
            minute_duration = round((time_now - times) / 60) - 60 * hour_duration

            if express['overdueTime'] < ClientTools.now():
                response = {}
                response['isSuccess'] = 'true'

                if "is_insulator" in express: 
                    if express['is_insulator']==1 or express['is_insulator']=='1':
                        is_insulator = 1
                    else:
                        is_insulator = 0
                else:
                    is_insulator = 0
                
                check_tb_exist = BoxDao.check_table_info({'table': 'table', 'table_name': 'PaidOverdue'})
                data_overdue_paid = []
                if check_tb_exist:
                    param = {
                        'is_active': 1,
                        'logisticsCompany_id': express['logisticsCompany_id'],
                        'deleteFlag': 0
                    }
                    check_logistics_paid = BoxDao.check_logistics_overdue_paid(param)
                    try:
                        data_overdue_check = check_logistics_paid[0]
                    except:
                        data_overdue_check = check_logistics_paid
                    if check_logistics_paid:
                        if(express['groupName']=="POPTITIP"):
                            flagLastmilePaid = 0
                        elif data_overdue_check['abort_charging'] == 0 or data_overdue_check['abort_charging'] == "0":
                            flagLastmilePaid = 1
                            data_overdue_paid = check_logistics_paid[0]
                        else:
                            flagLastmilePaid = 0
                            # data_overdue_paid = check_logistics_paid[0]
                # data_overdue_paid_01 = data_overdue_paid

                response['data'] = {
                    'is_overdue': 'true',
                    'groupname': express['groupName'],
                    'logistics_company': express['logisticsCompany_id'],
                    'is_insulator': is_insulator,
                    'is_overdue_paid': flagLastmilePaid,
                    'data_overdue_paid': data_overdue_paid,
                    'is_needed_pod': 0,
                    'barcode': express['expressNumber'],
                    'phone': express['takeUserPhoneNumber'],
                    'pin_code': express['validateCode'],
                    'overdue_date': express['overdueTime'],
                    'store_date': express['storeTime'],
                    'id_express': express['id']
                }

                if(is_insulator==1):
                    param_express = {'id': expr['id']}
                    get_size = BoxDao.get_size_name_by_express_id(param_express)
                    if len(get_size) < 1:
                        pricing = 1000
                    else:
                        size = get_size[0]['locker_size']
                        param_price = {
                            'column': size,
                            'logisticsCompany_id': express['logisticsCompany_id'],
                            'is_active': 1
                        }
                        get_pricing = BoxDao.get_pricing_by_size(param_price)
                        if len(get_pricing) < 1:
                            param_price = {
                                'column': size,
                                'logisticsCompany_id': '145b2728140f11e5bdbd0242ac110001',
                                'is_active': 1
                            }
                            get_pricing = BoxDao.get_pricing_by_size(param_price)
                            if len(get_pricing) > 0:
                                if(get_pricing[0]['abort_charging']=='1'):
                                    pricing = 0
                                else:
                                    pricing = get_pricing[0][size]
                            else:
                                pricing = 0
                            # print('pricing : ' + str(get_pricing[0]) + ' param : ' + str(param_price))
                        else:
                            if(get_pricing[0]['abort_charging']=='1'):
                                pricing = 0
                            else:
                                pricing = get_pricing[0][size]

                    response['data']['insulator_price'] = pricing
                    response['data']['groupname'] = express['groupName']
                    response['data']['parcel_duration'] =  hour_duration
                    response['data']['parcel_minutes_duration'] = minute_duration
                    response['data']['date_overdue'] = express['overdueTime']
                    # _LOG_.info(("response sip express : ", str(response['data'])))
                
                # if(express['groupName']=="POPTITIP"):
                    # mouth = BoxDao.get_mouth_by_id({'id':express['mouth_id']})
                    # mouthTypeId = mouth[0]['mouthType_id']
                    # mouth_param = BoxDao.get_mouth_type_name({'id': mouthTypeId})

                    # price_size = ''
                    # price_duration = ''
                    # size_box = mouth_param['name']
                    # if size_box == 'L':
                    #     price_size = 'size_l'
                    # elif size_box == 'M':
                    #     price_size = 'size_m'
                    # elif size_box == 'S':
                    #     price_size = 'size_s'
                    # elif size_box == 'MINI':
                    #     price_size = 'size_xs'
                    # elif size_box == 'XL':
                    #     price_size = 'size_xl'
                    # param_get = {
                    #     'is_active': 1,
                    #     'type': express['chargeType']
                    # }
                    # popsafe_pricing = BoxDao.get_pricing_popsafe_by_type(param_get)
                    # if len(popsafe_pricing) < 1:
                    #     price_duration = 0
                    #     db_overdue = 0
                    # else:
                    #     if((popsafe_pricing[0][price_size])<=0):
                    #         db_overdue = 0
                    #         price_duration = 0
                    #     else:
                    #         db_overdue = popsafe_pricing[0][price_size]
                    #         price_duration = int(popsafe_pricing[0]['duration'])
                    # time_span = ClientTools.now() - express['overdueTime']
                    # # day_span = math.ceil(time_span / 1000.0 / 60.0 / 60.0 / overdueTime)
                    # day_span = math.ceil(time_span / 1000.0 / 60.0 / 60.0 / overdueTime)
                    # if(price_duration != 0):
                    #     day_span = math.ceil(day_span / price_duration)
                    # else :
                    #     day_span = math.ceil(day_span / 24)

                    # overdue_cost = day_span * db_overdue
                    # response['data']['cost_overdue'] = overdue_cost

                if(flagLastmilePaid == 1):
                    param = {
                        'id': express['mouth_id']
                    }
                    _LOG_.info(("response sip express : ", str(response['data'])))
                    box = BoxDao.get_mouth_by_id(param)[0]
                    response['data']['data_overdue_paid']['details'] = {
                        'day_spend': day_spend,
                        'hour_duration': hour_duration,
                        'minute_duration': minute_duration,
                        'size': box['name']
                    }

                _POP_.get_parcel_data_bypin_signal.emit(json.dumps(response))
                return
            else:
                get_size = BoxDao.get_size_name_by_express_id(param)
                if len(get_size) < 1:
                    pricing = 1000
                else:
                    size = get_size[0]['locker_size']
                    param_price = {
                        'column': size,
                        'logisticsCompany_id': express['logisticsCompany_id'],
                        'is_active': 1
                    }
                    get_pricing = BoxDao.get_pricing_by_size(param_price)
                    if len(get_pricing) < 1:
                        param_price = {
                            'column': size,
                            'logisticsCompany_id': '145b2728140f11e5bdbd0242ac110001',
                            'is_active': 1
                        }
                        get_pricing = BoxDao.get_pricing_by_size(param_price)
                        if len(get_pricing) > 0:
                            if(get_pricing[0]['abort_charging']=='1'):
                                pricing = 0
                            else:
                                pricing = get_pricing[0][size]
                        else:
                            pricing = 0
                        # print('pricing : ' + str(get_pricing[0]) + ' param : ' + str(param_price))
                    else:
                        if(get_pricing[0]['abort_charging']=='1'):
                            pricing = 0
                        else:
                            pricing = get_pricing[0][size]

                time_now = datetime.datetime.now().timestamp()
                times = int(express['storeTime']) / 1000
                slice_time = slice(10)
                epoch_timeduration = time_now
                time_difference = math.trunc((time_now - int(times)) / 3600)
                minutes_difference = round((time_now - times) / 60) - 60 * time_difference

                response = {}
                response['isSuccess'] = 'true'
                response['data'] = {
                    'is_overdue': 'false',
                    'barcode': express['expressNumber'],
                    'phone': express['takeUserPhoneNumber'],
                    'name': express['storeUser_id'],
                    'locker_name': express['box_id'],
                    'groupname': express['groupName'],
                    'courier_feature': 'Signature',
                    'pin_code': express['validateCode'],
                    'overdue_date': express['overdueTime'],
                    'logistics_company': express['logisticsCompany_id'],
                    'id_express': express['id'],
                    'is_needed_pod': express['is_needed_pod'],
                    'is_insulator': express['is_insulator'],
                    'date_overdue': express['overdueTime'],
                    'validate_code': express['validateCode'],
                    'insulator_price': pricing,
                    'parcel_duration': time_difference,
                    'parcel_minutes_duration': minutes_difference,
                    'data_overdue_paid': [],
                    'data_pay_collect': '',
                    'store_date': express['storeTime']
                }
                
                time_now = datetime.datetime.now().timestamp()
                times = int(express['storeTime']) / 1000
                day_spend = math.trunc((time_now - int(times)) / 84000)
                hour_duration = math.trunc((time_now - int(times)) / 3600)
                minute_duration = round((time_now - times) / 60) - 60 * hour_duration

                print('[*] day_spend : ', day_spend)
                print('[*] minut_durations : ', minute_duration)
                print('[*] hour_duration : ', hour_duration)

                if express['groupName'] == 'UNDEFINED':
    
                    check_tb_exist = BoxDao.check_table_info({'table': 'table', 'table_name': 'PaidOverdue'})
                    data_overdue_paid = []
                    try:

                        if check_tb_exist:
                            param = {
                                'is_active': 1,
                                'logisticsCompany_id': express['logisticsCompany_id'],
                                'deleteFlag': 0
                            }
                            check_logistics_paid = BoxDao.check_logistics_overdue_paid(param)
                            if check_logistics_paid:
                                data_overdue_paid = check_logistics_paid[0]
                                if data_overdue_paid['abort_charging'] == 0 or data_overdue_paid['abort_charging'] == "0":
                                    flagLastmilePaid = 1
                                    response['data']['data_pay_collect'] = data_overdue_paid
                                    
                                    param = {
                                        'id': express['mouth_id']
                                    }
                                    box = BoxDao.get_mouth_by_id( param )[0]
                                    
                                    response['data']['data_pay_collect']['details'] = {
                                        'day_spend': day_spend,
                                        'hour_duration': hour_duration,
                                        'minute_duration': minute_duration,
                                        'size': box['name']
                                    }
                            else:
                                param = {
                                    'is_active': 1,
                                    'logisticsCompany_id': '161e5ed1140f11e5bdbd0242ac110001',
                                    'deleteFlag': 0
                                }
                                check_logistics_paid = BoxDao.check_logistics_overdue_paid(param)
                                if check_logistics_paid:
                                    data_overdue_paid = check_logistics_paid[0]
                                    if data_overdue_paid['abort_charging'] == 0 or data_overdue_paid['abort_charging'] == "0":
                                        flagLastmilePaid = 1
                                        response['data']['data_pay_collect'] = data_overdue_paid
                                        
                                        param = {
                                            'id': express['mouth_id']
                                        }
                                        box = BoxDao.get_mouth_by_id( param )[0]
                                        
                                        response['data']['data_pay_collect']['details'] = {
                                            'day_spend': day_spend,
                                            'hour_duration': hour_duration,
                                            'minute_duration': minute_duration,
                                            'size': box['name']
                                        }

                    except Exception as e:
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        _LOG_.info(('[ERROR] get_data_by_pin : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

                get_parcel_data = response
                _POP_.get_parcel_data_bypin_signal.emit(json.dumps(response))
                return
        else:
            paramtaken_ = {
                'validateCode': pincode,
                'status': 'CUSTOMER_TAKEN'
            }
            chck_express = ExpressDao.get_express_by_validate(paramtaken_)
            if len(chck_express) != 0:
                temp_Express = chck_express[0]
                val = temp_Express['takeTime']
                val = str(val)
                val = val[0:10]
                val = int(val)
                val = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(val))
                response = {
                    'isSuccess': 'false',
                    'status': 'CUSTOMER_TAKEN',
                    'data' : {
                        'expressNumber' : temp_Express['expressNumber'],
                        'takeTime' : val
                    }
                }
                _POP_.get_parcel_data_bypin_signal.emit(json.dumps(response))
                return
            else:
                paramtaken_ = {
                    'validateCode': pincode,
                    'status': 'COURIER_TAKEN'
                }
                chck_express = ExpressDao.get_express_by_validate(paramtaken_)
                if len(chck_express) != 0:
                    temp_Express = chck_express[0]
                    val = temp_Express['takeTime']
                    val = str(val)
                    val = val[0:10]
                    val = int(val)
                    val = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(val))
                    response = {
                        'isSuccess': 'false',
                        'status': 'COURIER_TAKEN',
                        'data' : {
                            'expressNumber' : temp_Express['expressNumber'],
                            'takeTime' : val
                        }
                    }
                    _POP_.get_parcel_data_bypin_signal.emit(json.dumps(response))
                    return
                else:
                    paramtaken_ = {
                        'validateCode': pincode,
                        'status': 'OPERATOR_TAKEN'
                    }
                    chck_express = ExpressDao.get_express_by_validate(paramtaken_)
                    if len(chck_express) != 0:
                        temp_Express = chck_express[0]
                        val = temp_Express['takeTime']
                        val = str(val)
                        val = val[0:10]
                        val = int(val)
                        val = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(val))
                        response = {
                            'isSuccess': 'false',
                            'status': 'OPERATOR_TAKEN',
                            'data' : {
                                'expressNumber' : temp_Express['expressNumber'],
                                'takeTime' : val
                            }
                        }
                        _POP_.get_parcel_data_bypin_signal.emit(json.dumps(response))
                        return
                    else:
                        response = {
                            'isSuccess': 'false',
                            'data': []
                        }
                        _POP_.get_parcel_data_bypin_signal.emit(json.dumps(response))
                        return
    except Exception as e:
        response = {
            'isSuccess': 'false',
            'data': str(e)
        }
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.info(('[ERROR] get_data_by_pin : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        # ExpressService._EXPRESS_.store_express_signal.emit(str(json.dumps(res)))
        _POP_.get_parcel_data_bypin_signal.emit(json.dumps(response))

def check_connection_time():
    ClientTools.get_global_pool().apply_async(get_time_value)

def get_time_value():
    time.sleep(600)
    while True:
        try:
            message, status_code = HttpClient.get_message('box/server-time')
        except Exception as e:
            _LOG_.info('Error Hit Time API: '+str(e))
        if status_code == 200 :
            del_localDate()
            gtime=message['data']['server_time']
            create_localDate(gtime)
            setTime(gtime)
        time.sleep(3600)

startFlag = ''

def set_localTime():
    global startFlag
    count = 0
    while startFlag:
        try:
            message, status_code = HttpClient.get_message('box/server-time')
        except Exception as e:
            _LOG_.info('Error Hit Time API: '+str(e))
        if status_code == 200 :
            startFlag = False
            del_localDate()
            gtime=message['data']['server_time']
            create_localDate(gtime)
            setTime(gtime)
        else:
            count +=1
            if count >= 300:
                startFlag = False
                localTime = read_localDate()
                setTime(localTime)
        time.sleep(1)

def setTime(param):
    tempTime = param[0:10]+ ' ' + param[11:19]
    conv = converter_strdtime_listint(tempTime)
    write_systemMachine(conv)

def converter_strdtime_listint(data_convert):
    time_tuple = time.strptime(str(data_convert), "%Y-%m-%d %H:%M:%S")
    time_str= list(time_tuple)
    time_int = list (map (int, time_str))
    temp = [0,0]
    temp[0:1] =  time_int[0:7]
    temp[2] = 0
    temp[3:7]= time_int[2:6]
    return temp

def write_systemMachine(list_temp):
    tup =  tuple(map(int, list_temp))
    try:
        win32api.SetSystemTime(tup[0], tup[1], tup[2], tup[3], tup[4], tup[5], tup[6], tup[7])
    except Exception as e:
        _LOG_.info('Error Change System Time: '+str(e))

def create_localDate(time):
    openFile = open('local_time.txt', 'a')
    openFile.write(str(time))
    openFile.close()

def read_localDate():
    readHandle = open ('local_time.txt',"r" )
    lineList = readHandle.readlines()
    readHandle.close()
    return lineList[0]

def del_localDate():
    try:
        delHandle = open("local_time.txt", "r+")
        lines = delHandle.readlines()
        del lines[0]
        delHandle.seek(0)
        delHandle.truncate()
        delHandle.writelines(lines)
    except Exception as e:
        _LOG_.info('Error Delete local time:' + str(e))

def change_overdue_price():
    file = open(sys.path[0]+"/qml/door_price.js", "r").readlines()[3]
    priceList = file.split()
    priceVal = priceList[3]
    price = priceVal.strip('""')

    param = {'deleteFlag': 0}
    listSize = BoxDao.get_list_size(param)
    print('list_size', str(listSize))

    for size in listSize:
        param_price = {
            'id': size['id'],
            'defaultOverduePrice': int(price)
        }
        print('param', str(param_price))
        update = BoxDao.update_price_for_poptitip(param_price)

def save_survey_data_byexpress(typesurvey, survey, idexpress, expressNumber):
    ClientTools.get_global_pool().apply_async(save_survey_data, (typesurvey, survey, idexpress, expressNumber))

def save_survey_data(typesurvey, survey, idexpress, expressNumber):
    day = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    if typesurvey == "TITIP" or typesurvey == 'BUKA PINTU' or typesurvey == 'RETURN' or typesurvey == 'POPSEND':
        param = {
            'id_express': idexpress,
            'expressNumber': expressNumber,
            'survey_store': survey,
            'storetime_survey': day,
            'syncFlag': 0,
            'deleteFlag': 0
        }
        ExpressDao.import_survey_store_record(param)

    elif typesurvey == "AMBIL":
        param = {
            'id_express': idexpress,
            'expressNumber': expressNumber,
            'survey_take': survey,
            'taketime_survey': day,
            'syncFlag': 0,
            'deleteFlag': 0
        }
        try:
            recorded = ExpressDao.get_survey_record(param)
            if not recorded:
                ExpressDao.import_survey_take_record(param)
            else:
                ExpressDao.update_survey_record(param)
        except Exception as e:
            _LOG_.info('Error Take Survey: '+ str(e))

def start_update_survey_express():
    ClientTools.get_global_pool().apply_async(update_survey_express)

def update_survey_express():
    param={
        'syncFlag' : 0
    }
    box = BoxDao.get_box_info()
    surveylist = ExpressDao.get_not_sync_survey_list(param)
    for survey in surveylist:
        temp_store = ''
        temp_timestore = ''
        temp_take = ''
        temp_timetake = ''
        try:
            temp_store = survey['survey_store']
            temp_timestore = survey['storetime_survey']
            temp_take = survey['survey_take']
            temp_timetake = survey['taketime_survey']
        except Exception as e:
            _LOG_.info('Survey Store and Storetime is empty: '+ str(e))

        params = {
            'id_locker': box[0]['id'],
            'locker_name': box[0]['name'],
            'country': 'ID',
            'data':[
                {
                    'id_express': survey['id_express'],
                    'expressNumber': survey['expressNumber'],
                    'survey_store': temp_store,
                    'survey_take': temp_take,
                    'storetime_survey': temp_timestore,
                    'taketime_survey': temp_timetake
                }
            ]
        }
        try:
            message, status_code = HttpClient.post_message('survey/store', params)
        except Exception as e:
            _LOG_.info('Error Hit API Survey: '+ str(e))
        if status_code == 200:
            _LOG_.info('Success HIT API Survey: '+ str(message))
            ExpressDao.mark_sync_survey_success(survey)

def start_get_tvc_timer():
    ClientTools.get_global_pool().apply_async(get_tvc_timer)

def get_tvc_timer():
    try:
        tvc_timer = 0
        locker_language = Configurator.get_value('ClientInfo', 'language')
        if(locker_language=="MY"):
            videoCounter = len(glob.glob1(sys.path[0]+"/advertisement/myvideo/", "*.mp4"))
        elif(locker_language=="PH"):
            videoCounter = len(glob.glob1(sys.path[0]+"/advertisement/phvideo/", "*.mp4"))
        else:
            videoCounter = len(glob.glob1(sys.path[0]+"/advertisement/video/", "*.mp4"))
        if(videoCounter>0):
            tvc_timer = (videoCounter*60) + 600
            #  tvc_timer = 60
        else:
            tvc_timer = 0
        _POP_.start_get_tvc_timer_signal.emit(int(tvc_timer))
        return tvc_timer
    except Exception as e:
        _LOG_.info('Error get_tvc_counter : '+str(e))


def start_get_tvc32_timer():
    ClientTools.get_global_pool().apply_async(get_tvc32_timer)

def get_tvc32_timer():
    try:
        tvc_timer = 0
        videoCounter = len(glob.glob1(sys.path[0]+"/advertisement/video32/", "*.mp4"))
        if(videoCounter>0):
            if os_platform =='Linux':       #changed
                tvc_timer = (videoCounter*60) + 600
                # tvc_timer = 20
            else:   #windows
                tvc_timer = (videoCounter*60) + 600
                # tvc_timer = 20
        else:
            tvc_timer = 0
        _POP_.start_get_tvc32_timer_signal.emit(int(tvc_timer))
        return tvc_timer
    except Exception as e:
        _LOG_.info('Error get_tvc32_counter : '+str(e))

def create_database_survey():
    try:
        param = {
            'table_name' : 'Survey',
            'table' : 'table'
        }
        tbtemp = BoxDao.check_table_info(param)
        if tbtemp is None or not tbtemp:
            sql_table_survey = """
            CREATE TABLE IF NOT EXISTS Survey
            (
                id_express        VARCHAR(255) PRIMARY KEY NOT NULL,
                expressNumber     VARCHAR(255),
                survey_store      VARCHAR(255),
                survey_take       VARCHAR(255),
                storetime_survey  VARCHAR(255),
                taketime_survey   VARCHAR(255),
                syncFlag          INT,
                deleteFlag        INT
            );"""
            ClientDatabase.create_table(sql_table_survey)
    except Exception as e:
        _LOG_.info('Error make database Survey : '+str(e))

def start_get_language():
    ClientTools.get_global_pool().apply_async(get_language)

def get_language():
    locker_language = Configurator.get_value('ClientInfo', 'language')
    _LOG_.info("LOCKER_locker_language : " + str(locker_language))
    _POP_.start_get_language_signal.emit(locker_language)

def record_otp(phone, date):
    try:
        DB_CREATE = create_database_user_otp()
        if DB_CREATE is True or DB_CREATE is not None:
            param = {
                'phone_number': phone,
                'valid_date': date
            }
            check_otp = BoxDao.check_otp(param)
            if len(check_otp) < 1:
                BoxDao.insert_otp(param)
                return True
            else:
                BoxDao.update_otp(param)
                return True
        else:
            return False
    except Exception as e:
        _LOG_.info("[ERROR_OTP] : " + str(e))
        return False

def check_otp_valid(phone):
    try:
        param = {
            'table_name' : 'UserOtp',
            'table' : 'table'
        }
        tbtemp = BoxDao.check_table_info(param)
        if tbtemp is None or not tbtemp:
            CHECK = create_database_user_otp()
            return False # TB Still NULL So return False
        else:
            param = {
                'phone_number': phone
            }
            check_otp = BoxDao.check_otp(param)
            if len(check_otp) < 1:
                return False
            else:
                otp = check_otp[0]
                local_otp = int(time.mktime(time.strptime(otp['valid_date'], '%Y-%m-%d %H:%M:%S')) * 1000)

                if ClientTools.now() < local_otp:
                    param = {
                        'phone_number': phone,
                        'valid_date': otp['valid_date']
                    }
                    BoxDao.update_otp(param)
                    return True
                else:
                    return False
    except Exception as e:
        _LOG_.info("[ERROR_CHECK_OTP] : " + str(e))
        return False

def create_database_user_otp():
    try:
        param = {
            'table_name' : 'UserOtp',
            'table' : 'table'
        }
        tbtemp = BoxDao.check_table_info(param)
        if tbtemp is None or not tbtemp:
            sql_table_survey = """
            CREATE TABLE IF NOT EXISTS UserOtp
            (
                phone_number      VARCHAR(15) PRIMARY KEY NOT NULL,
                valid_date        VARCHAR(50),
                used_times        VARCHAR(10),
                deleteFlag        VARCHAR(2) DEFAULT(0)
            );"""
            ClientDatabase.create_table(sql_table_survey)
            return True
        else:
            return tbtemp
    except Exception as e:
        _LOG_.info('[ERROR_CREATE_DB_OTP] : '+str(e))
        return
##################
def resize_picture(path, size):
    #size = (250, 170) -> 50% from Original Signature Dimension
    image = Image.open(path)
    image.thumbnail(size, Image.ANTIALIAS)
    image.save(path, quality=100)
#################

def get_last_image(path):
    new_img_name = None
    try:
        os.makedirs(img_path + path)
    except OSError:
        pass
    try:
        new_img_name = rename_file(max(glob.iglob(img_path + path + '*.jpg'), key=os.path.getctime), bad_chars, '')
    except Exception as e:
        _LOG_.warning(('getting latest image : ', e))
        pass
    finally:
        _LOG_.info(('getting latest image : ' + new_img_name))
        return new_img_name

#################

def create_db_signature():
    try:
        param = {
            'table_name' : 'Epod',
            'table' : 'table'
        }
        tb_check = BoxDao.check_table_info(param)
        if tb_check is None or not tb_check:
            table_signature = """
            CREATE TABLE IF NOT EXISTS Epod
            (
                express_id        VARCHAR(255) PRIMARY KEY NOT NULL,
                barcode           VARCHAR(255),
                groupname         VARCHAR(255),
                pincode           VARCHAR(255),
                cust_name         VARCHAR(255),
                create_at         VARCHAR(255),
                signature         BLOB,
                syncFlag          INT,
                deleteFlag        INT
            );"""
            ClientDatabase.create_table(table_signature)
    except Exception as e:
        _LOG_.info('Error make database Epod : '+str(e))

def start_capture_signature(filepath, filename):
    ClientTools.get_global_pool().apply_async(capture_signature, (filepath, filename,))


def capture_signature(filepath, filename):
    #path for digital signature is 'signature/'
    _LOG_.info(('capture_signature : ', filepath))
    global signature_path
    signature_path = filepath
    try:
        os.makedirs(img_path + filepath)
    except OSError:
        pass
    sign_time = time.strftime('%Y%m%d%H%M%S', time.localtime(time.time()))
    sign_name = img_path + filepath + filename + '_' + sign_time + '.jpg'
    #details of img_crop = (x1,y1,x2,y2) => Image Size 500 x 350
    img_crop = (262, 255, 762, 595)
    resize_dimension = (250, 170)
    img_capture = ''
    # _LOG_.info(('sign_name okk : ', sign_name))
    # _LOG_.info(('get_parcel_data okk : ', get_parcel_data))
    # _LOG_.info(('get_parcel_data[] okk : ', get_parcel_data["data"]["barcode"]))
    # _LOG_.info(('global_pin okk : ', global_pin))
    customer_name = filename.replace("_" + global_pin + "_" + get_parcel_data["data"]["barcode"], "")
    _LOG_.info(('customer_name : ', customer_name))
    try:
        img_capture = ImageGrab.grab(bbox=img_crop)
        img_capture.save(sign_name, 'JPEG')
        resize_picture(sign_name, resize_dimension)
        time.sleep(3)
        check_last_file = get_last_image(path=signature_path).replace("_" + sign_time, "")
        sign_name_temp = rename_file(sign_name, bad_chars, '')
        if check_last_file == sign_name or check_last_file in sign_name_temp:
            start_save_signature(customer_name)
            _POP_.start_capture_signature_signal.emit('SUCCESS')
        else:
            _POP_.start_capture_signature_signal.emit('ERROR')
            _LOG_.warning(("start_capture_signature result : ERROR"))
    except Exception as e:
        _POP_.start_capture_signature_signal.emit('FAILED')
        _LOG_.warning(("start_capture_signature result FAILED : ", e))
    finally:
        del img_capture


def start_save_signature(cust_name):
    ClientTools.get_global_pool().apply_async(save_signature, (cust_name,))


def save_signature(cust_name):
    _LOG_.info(('save_signature cust_name : ', cust_name))
    if cust_name == "":
        return
    create_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    signature_param = dict()
    result_name, result_b64 = take_capture_file_signature(path=signature_path)
    _LOG_.info(('save_signature result_name : ', result_name))
    # signature_param["token"] = global_token
    # signature_param["pincode"] = get_parcel_data["data"]["pin_code"]
    try:
        signature_param["express_id"] = get_parcel_data["data"]["id_express"]
        signature_param["barcode"] = get_parcel_data["data"]["barcode"]
        signature_param["groupname"] = get_parcel_data["data"]["groupname"]
    except ValueError:
        signature_param["express_id"] = "NONE"
        signature_param["barcode"] = "NONE"
        signature_param["groupname"] = "NONE"
    signature_param["pincode"] = get_parcel_data["data"]["pin_code"]
    signature_param["cust_name"] = str(cust_name)
    signature_param["create_at"] = create_time
    signature_param["signature"] = result_b64
    signature_param["syncFlag"] = 0
    signature_param["deleteFlag"] = 0
    try:
        store_signature_db = ExpressDao.save_signature(signature_param)
        if store_signature_db is True:
            _LOG_.info(('[SUCCESS] store signature data to DB', signature_param))
    except Exception as e:
        _LOG_.debug(('store signature ERROR :', e))

# def start_post_signature(cust_name):
#     ClientTools.get_global_pool().apply_async(post_signature, (cust_name,))


# def post_signature(cust_name):
#     _LOG_.info(('post_signature cust_name : ', cust_name))
#     if cust_name == "":
#         return
#     signature_param = dict()
#     _LOG_.info(('post_signature signature_param : ', signature_param))
#     result_name, result_b64 = take_capture_file_signature(path=signature_path)
#     signature_param["token"] = global_token
#     signature_param["pincode"] = get_parcel_data["data"]["pin_code"]
#     _LOG_.info(('post_signature result_name : ', result_name))
#     try:
#         signature_param["express_id"] = get_parcel_data["data"]["id_express"]
#         signature_param["barcode"] = get_parcel_data["data"]["barcode"]
#         signature_param["groupname"] = get_parcel_data["data"]["groupname"]
#     except ValueError:
#         signature_param["express_id"] = "NONE"
#         signature_param["barcode"] = "NONE"
#         signature_param["groupname"] = "NONE"
#     signature_param["customer_name"] = str(cust_name)
#     signature_param["signature"] = result_b64.decode('ascii')
#     # signature_url = global_url + 'courier/signature'
#     # signature_url = global_url + 'task/syncSignature'
#     signature_url = base_url + 'task/syncSignature'
#     _LOG_.info(('post_signature url : ', signature_url))
#     try:
#         express_message, status_code = global_post(signature_url, signature_param)
#         if status_code == "200" and express_message["response"]["message"] == "OK":
#             _POP_.start_post_signature_signal.emit("SUCCESS")
#         else:
#             _POP_.start_post_signature_signal.emit("ERROR")
#             _LOG_.warning(("start_post_signature result : ", status_code))
#     except Exception as e:
#         _POP_.start_post_signature_signal.emit("FAILED")
#         _LOG_.warning(("start_post_signature result FAILED : ", e))


# def start_sync_signature():
#     ClientTools.get_global_pool().apply_async(sync_signature)

def sync_signature():
    try:
        signature_ = dict()
        signature_list = ExpressDao.get_scan_not_sync_signature_list({'syncFlag': 0})
        if len(signature_list) == 0:
            return
        for signature_ in signature_list:
            signature_['customer_name'] = signature_['cust_name']
            signature_['barcode'] = signature_['barcode']
            signature_['express_id'] = signature_['express_id']
            signature_['signature'] = signature_['signature'].decode('ascii')
            signature_['pincode'] = signature_['pincode']
            signature_['groupname'] = signature_['groupname']
            signature_['token'] = global_token
            _LOG_.info(("syncSignature : ", signature_))
        message, status_code = HttpClient.post_message('task/syncSignature', signature_)
        if status_code == 200:
            _LOG_.info(("syncSignature RESULT : ", message))
            ExpressDao.mark_sync_signature_success(signature_)
        else:
            _LOG_.debug(("syncSignature FAILED : ", message))
    except Exception as e:
        _LOG_.debug(('syncSignature ERROR :', e))

def start_update_return_rules(message_result):
    try:
        all_return_rules = message_result['return_rules']
        ReturnRulesDao.delete_record_return_rules()
        for return_rule in all_return_rules:
            params = {
                'id' : return_rule['id_rules'],
                'regularContent' : return_rule['regularContent'],
                'groupName' : return_rule['groupName'],
                'operator_id': return_rule['operator_id'],
                'logistic_id': return_rule['logistic_id'],
                'ecommerce_id': return_rule['ecommerce_id'],
                'deleteFlag': return_rule['deleteFlag']
            }
            ReturnRulesDao.insert_return_rule(params)
    except Exception as e:
        _LOG_.debug(("Insert ReturnRules FAILED : ", e))

def get_ovo_config():
    try:
        box = BoxDao.get_box_info()
        if len(box) > 0:
            message, status_code = HttpClient.get_message('box/ovo-config/' + box[0]['id'] )
            response = message['response']
            response_code = response['code']
        
            if response_code == 202 or response_code == 200:
                data = message['data'][0]
                Configurator.set_value('QrOvo', 'merchant_id', data['merchant_id_global'])
                Configurator.set_value('QrOvo', 'mid', data['mid_locker'])
                Configurator.set_value('QrOvo', 'tid', data['tid_locker'])
                Configurator.set_value('QrOvo', 'store_id', data['store_code'])
                message, status_code = HttpClient.get_message('box/ovo-config/deployed/' + box[0]['id'] )
                response = message['response']
                response_code = response['code']
                if response_code == 202 or response_code == 200:
                    Configurator.set_value('QrOvo', 'is_deployed', "1")
                    print("pergantian config selesai")
                return
            else:
                print('gagal get config ovo')
                return
    except Exception as e:
        print('eroorr_ovo :', str(e))
        _LOG_.info(("get_ovo_config : ", str(e)))

def start_get_mode_insulator():
    ClientTools.get_global_pool().apply_async(get_mode_insulator)

def get_mode_insulator():
    got = Configurator.get_value('ClientInfo', 'insulator')
    _POP_.start_get_mode_insulator_signal.emit(got)
    
def start_get_contactless():
    ClientTools.get_global_pool().apply_async(get_contactless)

def get_contactless():
    contactless_status = Configurator.get_value('Scanner', 'contactless')
    _LOG_.info("LOCKER_locker_language : " + str(contactless_status))
    _POP_.start_get_contactless_signal.emit(contactless_status)

def create_db_contactless():
    try:
        param = {
            'table_name' : 'Contactless',
            'table' : 'table'
        }
        tb_check = BoxDao.check_table_info(param)
        if tb_check is None or not tb_check:
            table_contactless = """
            CREATE TABLE IF NOT EXISTS Contactless
            (
                express_id        VARCHAR(255),
                express_number    VARCHAR(255),
                locker_name       VARCHAR(255),
                box_id            VARCHAR(255),
                locker_size       VARCHAR(255),
                locker_number     INT,
                is_taken          INT,
                is_drop           INT,
                is_pushed         INT,
                storeTime         BIGINT,
                takeTime          BIGINT,
                push_date         BIGINT,
                is_success        VARCHAR(255),
                qr_content        VARCHAR(255),
                syncFlag          INT
            );"""
            ClientDatabase.create_table(table_contactless)
        else:
            check_column = ExpressDao.check_column_exist({}, 'Contactless')
            checking = 0
            for list_ in check_column:
                if list_['name'] == 'push_date':
                    checking += 1
            print("Checking  == ",str(checking))
            if checking == 0:
                ExpressDao.adding_column({},'push_date', 0, 'Contactless')
                ExpressDao.adding_column({},'locker_size', 0, 'Contactless')
                ExpressDao.adding_column({},'locker_number', 0, 'Contactless')

    except Exception as e:
        _LOG_.info('Error make database contactless : '+str(e))

def start_save_contactless(parameter):
    ClientTools.get_global_pool().apply_async(save_contactless, (parameter,))

def save_contactless(parameter):
    json_param = json.loads(parameter)
    data_param = {
        'is_success': json_param['is_success'],
        'qr_content': json_param['qr_content'],
        'pincode': json_param['pincode'],
        'is_drop': json_param['is_drop'],
        'is_taken': json_param['is_taken']
    }
    print("Data Param : "+ str(data_param))
    send = {
        'validateCode': data_param['pincode'] #dari param
    }
    contactless_param = dict()
    try:
        check_express = ExpressDao.get_express_by_validate_all(send)
        if len(check_express) != 0:
            expr = check_express[0]
            param = {'id': expr['id_express']}
            data_express = ExpressDao.get_express_by_id(param)
            express = data_express[0]
            response = {
                'express_id': express['id'],
                'express_number': express['expressNumber'],
                'storeTime': express['storeTime'],
                'takeTime': express['takeTime'],
                'locker_size': expr['name'],
                'locker_number': expr['number']
            }
            try:
                contactless_param["express_id"] = response["express_id"]
                contactless_param["express_number"] = response["express_number"]
                contactless_param["storeTime"] = response["storeTime"]
                contactless_param["takeTime"] = response["takeTime"]
                contactless_param["locker_size"] = response["locker_size"]
                contactless_param["locker_number"] = response["locker_number"]

            except ValueError:
                contactless_param["express_id"] = "NONE"
                contactless_param["express_number"] = "NONE"
                contactless_param["storeTime"] = 0
                contactless_param["takeTime"] = 0
                contactless_param["locker_size"] = "NONE"
                contactless_param["locker_number"] = 0
        else:
            contactless_param["express_id"] = "NONE"
            contactless_param["express_number"] = "NONE"
            contactless_param["storeTime"] = 0
            contactless_param["takeTime"] = 0
            contactless_param["locker_size"] = "NONE"
            contactless_param["locker_number"] = 0

        contactless_param["locker_name"] = locker_info['name']
        contactless_param["box_id"] = locker_info['id']
        contactless_param["is_taken"] = data_param["is_taken"]
        contactless_param["is_drop"] = data_param["is_drop"]
        contactless_param["is_success"] = data_param['is_success']
        if data_param['qr_content']=="":
            contactless_param["qr_content"] = "NONE"
        else:
            contactless_param["qr_content"] = data_param['qr_content']
        contactless_param['push_date'] = 0
        contactless_param["syncFlag"] = 0
        contactless_param["is_pushed"] = 0
        try:
            print("Data contactless_param : "+ str(contactless_param))
            store_contactless_db = ExpressDao.save_contactless(contactless_param)
            if store_contactless_db is True:
                _LOG_.info(('[SUCCESS] store_contactless_db', contactless_param))
        except Exception as e:
            _LOG_.debug(('store contactless ERROR :', e))

    except Exception as e:
        _LOG_.debug(('check_express ERROR :', e))

def sync_contactless():
    try:
        contactless_ = dict()
        collect_data = dict()
        contactless_list = ExpressDao.get_not_sync_contactless({'syncFlag': 0})
        if len(contactless_list) == 0:
            return
        else:
            for contactless_ in contactless_list:
                contactless_['express_id'] = contactless_['express_id']
                contactless_['express_number'] = contactless_['express_number']
                contactless_['storeTime'] = str(contactless_['storeTime'])
                contactless_['takeTime'] = str(contactless_['takeTime'])
                contactless_['locker_name'] = contactless_['locker_name']
                contactless_['box_id'] = contactless_['box_id']
                contactless_['locker_size'] = contactless_['locker_size']
                contactless_['locker_number'] = str(contactless_['locker_number'])
                contactless_['push_date'] = str(ClientTools.now())
                contactless_['is_taken'] = str(contactless_['is_taken'])
                contactless_['is_drop'] = str(contactless_['is_drop'])
                contactless_['is_pushed'] = str(contactless_['is_pushed'])
                contactless_['is_success'] = eval(str(contactless_['is_success']).title())
                if(contactless_['qr_content']==""):
                    contactless_['qr_content'] = "NONE"
                else:
                    contactless_['qr_content'] = contactless_['qr_content']
                contactless_['syncFlag'] = contactless_['syncFlag']

                collect_data = [contactless_ 
                            for contactless_ in contactless_list]
        message, status_code = HttpClient.post_message('contactless/store', collect_data)
        if status_code == 200:
            try:
                contactless__ = dict()
                contactless_list = ExpressDao.get_not_sync_contactless({'syncFlag': 0})
                for contactless__ in contactless_list:
                    contactless__['qr_content'] = contactless__['qr_content']
                    contactless__['push_date'] = ClientTools.now()
                    _LOG_.info(("update_sync_contactless RESULT : ", message))
                    ExpressDao.update_sync_contactless(contactless__)
            except Exception as e:
                _LOG_.debug(('update_sync_contactless ERROR :', e))
        else:
            _LOG_.debug(("sync_contactless FAILED : ", message))
    except Exception as e:
        _LOG_.debug(('sync_contactless ERROR :', e))
        

def start_get_booking_express(awb):
    ClientTools.get_global_pool().apply_async(get_booking_express, (awb,))

def get_booking_express(awb):
    awb_number = awb
    data_awb = {
        'expressNumber': awb_number, #dari param
        'status': 'BOOKING'
    }
    try:
        check_express = ExpressDao.get_express_booking(data_awb)
        data_booking = dict()
        if len(check_express) != 0:
            data_booking['expressId'] = str(check_express[0]['express_id'])
            data_booking['locker_type'] = str(check_express[0]['locker_type'])
            data_booking['locker_number'] = check_express[0]['locker_number']
            data_booking['isSuccess'] = 'true'
            _POP_.start_get_booking_express_signal.emit(str(json.dumps(data_booking)))
        else:
            data_booking['awb_number'] = awb_number
            data_booking['isSuccess'] = "false"
            _POP_.start_get_booking_express_signal.emit(str(json.dumps(data_booking)))
    except Exception as e:
        _LOG_.debug(('get_express_booking ERROR :', e))
        
def create_table_mini_insulator():
    try:
        #add table MINI 
        check_column = ExpressDao.check_column_exist({}, 'InsulatorPricing')
        checking = 0
        for list_ in check_column:
            if list_['name'] == 'MINI':
                checking += 1
        if checking == 0:
            ExpressDao.adding_column({},'MINI', 0, 'InsulatorPricing')

        for list_ in check_column:
            if list_['name'] == 'abort_charging':
                checking += 1
        if checking == 1:
            ExpressDao.adding_column({},'abort_charging', '0', 'InsulatorPricing')

    except Exception as e:
        _LOG_.info('Error make table InsulatorPricing : '+str(e))
        _LOG_.debug(('get_express_booking ERROR :', e))

def start_get_popsafe_price():
    ClientTools.get_global_pool().apply_async(get_popsafe_price)

def get_popsafe_price():
    res = {}
    pricing_result = dict()
    try:
        param_get = {
            'is_active': 1
        }
        popsafe_pricing = BoxDao.get_pricing_popsafe(param_get)
        if len(popsafe_pricing) < 1:
            res['isSuccess'] = "false"
        else:
            res['message'] = popsafe_pricing
            # pricing_result = {
            #     "card_no": global_payment_info[4:20],
            #     "last_balance": global_payment_info[32:40].lstrip('0'),
            #     "show_date": timestamp_payment,
            #     "terminal_id": Configurator.get_value('popbox', 'terminalID'),
            #     "locker": locker_info["name"], 
            #     "raw" : global_payment_info + "|" + str(timestamp_payment),
            # }
            res['isSuccess'] = "true"
            # res['message'] = json.dumps(pricing_result)

        _POP_.popsafe_price_signal.emit(str(json.dumps(res)))
    except Exception as e:
        _LOG_.info(('Error get Popsafe Pricing : ',str(e)))
        res['isSuccess'] = "false"
        _POP_.popsafe_price_signal.emit(str(json.dumps(res)))

def get_parcel_data_idnumber(idnumber):
    ClientTools.get_global_pool().apply_async(parcel_data_idnumber, (idnumber,))

def parcel_data_idnumber(idnumber):
    print("id number : ", str(idnumber))
    response = {}
    send = {
        'customer_identification': idnumber,
        'status': 'IN_STORE'
    }
    try:
        check_express = ExpressDao.get_express_by_idnumber(send)
        if len(check_express) != 0:
            expr = check_express[0]
            param = {'id': expr['id']}
            data_express = ExpressDao.get_express_by_id(param)
            express = data_express[0]
            if express['overdueTime'] < ClientTools.now():
                response['isSuccess'] = 'true'
                response['is_overdue'] = 'true'
                _POP_.get_parcel_data_idnumber_signal.emit(json.dumps(response))
            else:
                response['isSuccess'] = 'true'
                response['data'] = {
                    'is_overdue': 'false',
                    'express_number': express['expressNumber'],
                    'id_express': express['id']
                }
                _POP_.get_parcel_data_idnumber_signal.emit(json.dumps(response))
        else:
            response['isSuccess'] = 'false'
            response['message'] = 'not_found'
            _POP_.get_parcel_data_idnumber_signal.emit(json.dumps(response))

    except Exception as e:
        response = {
            'isSuccess': 'false',
            'data': str(e)
        }
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.info(('[ERROR] get_data_by_id_number : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _POP_.get_parcel_data_idnumber_signal.emit(json.dumps(response))


def start_save_history_opendoor(parameter):
    ClientTools.get_global_pool().apply_async(save_history_opendoor, (parameter,))

def save_history_opendoor(parameter):
    try:
        json_param = json.loads(parameter)
        data_param = {
            'express_id': json_param['express_id'],
            'customer_identifications_number': json_param['customer_identifications_number'],
            'open_door_time': ClientTools.now()
        }
        # print("Data contactless_param : "+ str(contactless_param))
        save_opendoor_db = ExpressDao.save_history_opendoor(data_param)
        if save_opendoor_db is True:
            _LOG_.info(('[SUCCESS] save_opendoor_db', data_param))
    except Exception as e:
        _LOG_.debug(('save_opendoor_db ERROR :', e))


def start_get_opendoor_mode():
    ClientTools.get_global_pool().apply_async(get_opendoor_mode)

def get_opendoor_mode():
    try:
        openmode = Configurator.get_value('ClientInfo', 'openmode')
        _POP_.start_get_opendoor_mode_signal.emit(openmode)
    except Exception as e:
        openmode = "default"
        _POP_.start_get_opendoor_mode_signal.emit(openmode)
        _LOG_.debug(('get_opendoor_mode error :', e))
        

def create_db_history_opendoor():
    try:
        param = {
            'table_name' : 'HistoryOpenDoor',
            'table' : 'table'
        }
        tb_check = BoxDao.check_table_info(param)
        if tb_check is None or not tb_check:
            table_history_opendoor = """
            CREATE TABLE IF NOT EXISTS HistoryOpenDoor
            (
                express_id                        VARCHAR(255)  NOT NULL,
                customer_identifications_number   VARCHAR(255)  NOT NULL,
                open_door_time                    BIGINT
            );"""
            ClientDatabase.create_table(table_history_opendoor)
    except Exception as e:
        _LOG_.info('Error make database table_history_opendoor : '+str(e))


def start_get_theme():
    ClientTools.get_global_pool().apply_async(get_theme)

def get_theme():
    try:
        locker_theme = Configurator.get_value('ClientInfo', 'theme')
    except Exception as e:
        locker_theme = "default"
    _POP_.start_get_theme_signal.emit(locker_theme)

def start_get_scanner_version():
    ClientTools.get_global_pool().apply_async(get_scanner_version)

def get_scanner_version():
    scanner_version = Configurator.get_value('Scanner', 'version')
    _LOG_.info("scanner_version : " + str(scanner_version))
    _POP_.get_scanner_version_signal.emit(scanner_version)
