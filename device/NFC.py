from smartcard.scard import *
from smartcard.util import toHexString
import smartcard.util
from smartcard.ATR import ATR
from smartcard.CardType import AnyCardType
from smartcard.CardRequest import CardRequest
from smartcard.CardConnectionObserver import CardConnectionObserver
import time
import struct
import array
import sys
from PyQt5.QtCore import QObject, pyqtSignal
import Configurator


'''
RFID/NFC Reader/Writer: ACR122U-A9
Supported Frequency: 13.56MHz
Supported ISO: 14443-4A/B, ISO 18092.
Additional Supported Standards: Mifare, FeliCa, four types of NFC.
Documentation: http://downloads.acs.com.hk/drivers/en/API-ACR122U-2.02.pdf
Definitions:
ISO/IEC 14443 Identification cards -- Contactless integrated circuit cards -- Proximity cards is an international standard that defines proximity cards used for identification, and the transmission protocols for communicating with it.
(ATR) Answer To Reset: is a message output by a contact Smart Card conforming to ISO/IEC 7816 standards, following electrical reset of the card's chip by a card reader.
PCD: proximity coupling device (the card reader)
PICC: proximity integrated circuit card
'''
VERBOSE = False

attributes = {
	SCARD_ATTR_ATR_STRING: 'SCARD_ATTR_ATR_STRING',
	SCARD_ATTR_CHANNEL_ID: 'SCARD_ATTR_CHANNEL_ID',
	SCARD_ATTR_CHARACTERISTICS: 'SCARD_ATTR_CHARACTERISTICS',
	SCARD_ATTR_CURRENT_BWT: 'SCARD_ATTR_CURRENT_BWT',
	SCARD_ATTR_CURRENT_CWT: 'SCARD_ATTR_CURRENT_CWT',
	SCARD_ATTR_CURRENT_EBC_ENCODING: 'SCARD_ATTR_CURRENT_EBC_ENCODING',
	SCARD_ATTR_CURRENT_F: 'SCARD_ATTR_CURRENT_F',
	SCARD_ATTR_CURRENT_IFSC: 'SCARD_ATTR_CURRENT_IFSC',
	SCARD_ATTR_CURRENT_IFSD: 'SCARD_ATTR_CURRENT_IFSD',
	SCARD_ATTR_CURRENT_IO_STATE: 'SCARD_ATTR_CURRENT_IO_STATE',
	SCARD_ATTR_DEFAULT_DATA_RATE: 'SCARD_ATTR_DEFAULT_DATA_RATE',
	SCARD_ATTR_DEVICE_FRIENDLY_NAME_A: 'SCARD_ATTR_DEVICE_FRIENDLY_NAME_A',
	SCARD_ATTR_DEVICE_FRIENDLY_NAME_W: 'SCARD_ATTR_DEVICE_FRIENDLY_NAME_W',
	SCARD_ATTR_DEVICE_SYSTEM_NAME_A: 'SCARD_ATTR_DEVICE_SYSTEM_NAME_A',
	SCARD_ATTR_DEVICE_SYSTEM_NAME_W: 'SCARD_ATTR_DEVICE_SYSTEM_NAME_W',
	SCARD_ATTR_DEVICE_UNIT: 'SCARD_ATTR_DEVICE_UNIT',
	SCARD_ATTR_ESC_AUTHREQUEST: 'SCARD_ATTR_ESC_AUTHREQUEST',
	SCARD_ATTR_EXTENDED_BWT: 'SCARD_ATTR_EXTENDED_BWT',
	SCARD_ATTR_ICC_INTERFACE_STATUS: 'SCARD_ATTR_ICC_INTERFACE_STATUS',
	SCARD_ATTR_ICC_PRESENCE: 'SCARD_ATTR_ICC_PRESENCE',
	SCARD_ATTR_ICC_TYPE_PER_ATR: 'SCARD_ATTR_ICC_TYPE_PER_ATR',
	SCARD_ATTR_MAXINPUT: 'SCARD_ATTR_MAXINPUT',
	SCARD_ATTR_MAX_CLK: 'SCARD_ATTR_MAX_CLK',
	SCARD_ATTR_MAX_DATA_RATE: 'SCARD_ATTR_MAX_DATA_RATE',
	SCARD_ATTR_POWER_MGMT_SUPPORT: 'SCARD_ATTR_POWER_MGMT_SUPPORT',
	SCARD_ATTR_SUPRESS_T1_IFS_REQUEST: 'SCARD_ATTR_SUPRESS_T1_IFS_REQUEST',
	SCARD_ATTR_USER_AUTH_INPUT_DEVICE: 'SCARD_ATTR_USER_AUTH_INPUT_DEVICE',
	SCARD_ATTR_USER_TO_CARD_AUTH_DEVICE:
		'SCARD_ATTR_USER_TO_CARD_AUTH_DEVICE',
	SCARD_ATTR_VENDOR_IFD_SERIAL_NO: 'SCARD_ATTR_VENDOR_IFD_SERIAL_NO',
	SCARD_ATTR_VENDOR_IFD_TYPE: 'SCARD_ATTR_VENDOR_IFD_TYPE',
	SCARD_ATTR_VENDOR_IFD_VERSION: 'SCARD_ATTR_VENDOR_IFD_VERSION',
	SCARD_ATTR_VENDOR_NAME: 'SCARD_ATTR_VENDOR_NAME',
}

BLOCK_NUMBER = 0x04
AUTHENTICATE = [0xFF, 0x88, 0x00, BLOCK_NUMBER, 0x60, 0x00]

COMMAND = [0xFF, 0xCA, 0x00, 0x00, 0x00]


SELECT = [0xA0, 0xA4, 0x00, 0x00, 0x02]

GET_UID = [0xFF,0xCA,0x00,0x00,0x00]

READ_BYTES = [0xFF,0xB0,0x00,0x04,0x04]
WRITE_BLOCKS = [0xFF,0xD6,0x00,0x04,0x04,0xFF,0xFF,0xFF,0xFF] # Data are the last three items in the list.


READ_16_BINARY_BLOCKS = [0xFF,0xB0,0x00,0x04,0x10] # Read 16 bytes from the binary block 0x04h.
READ_4_BINARY_BLOCKS = [0xFF,0xB0,0x00,0x04,0x04] # Read 4 bytes from the binary block 0x04h.


NUMBER_BYTES_TO_UPDATE = 0x10
UPDATE_BLOCKS = [0xFF, 0xD6, 0x00, BLOCK_NUMBER, NUMBER_BYTES_TO_UPDATE, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
#BLOCK_DATA = [0x00,0x01,0x02,0x03]  #MIFARE ULTRAWEIGHT
									#UPDATE SINCE PYSCARD 2.0 and PYTHON 3.7.4
UPDATE_FIXED_BLOCKS = [0xFF, 0xD6, 0x00, BLOCK_NUMBER, NUMBER_BYTES_TO_UPDATE]


UPDATE_BLOCKS_WITH_DATA = [0xFF, 0xD6, 0x00, BLOCK_NUMBER]
READ_BLOCKS_RECENTLY_UPDATED = [0xFF,0xB0,0x00,BLOCK_NUMBER]

class NfcSignalHandler(QObject):
	ReaderAvailable=pyqtSignal(str)

_READER_=NfcSignalHandler()

class NFC_Reader():
	def __init__(self, uid = ""):
		try:
			self.uid = uid
			self.hresult, self.hcontext = SCardEstablishContext(SCARD_SCOPE_USER)
			self.hresult, self.readers = SCardListReaders(self.hcontext, [])
			assert len(self.readers) > 0
			self.reader = self.readers[0]
			#print("Found reader: " +  str(self.reader))
			
			self.hresult, self.hcard, self.dwActiveProtocol = SCardConnect(
					self.hcontext,
					self.reader,
					SCARD_SHARE_SHARED,
					SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1)
			self.data_blocks = []
		except Exception as e:
			print("READER NOT FOUND :", str (e))
			_READER_.ReaderAvailable.emit(str(False))

	def get_card_status(self):
		print("Getting card status...")
		try:
			hresult, reader, state, protocol, atr = SCardStatus(self.hcard)
			if hresult != SCARD_S_SUCCESS:
				print ('ERROOR')
				#raise error, 'failed to get status: ' + SCardGetErrorMessage(hresult)
			
			#print ('Reader: ', reader)
			#print ('State: ', state)
			#print ('Protocol: ', protocol)
			#print ('ATR: ')
			#for i in range(len(atr)):
			#	print('0x%.2X' % i)
			converted = toHexString(atr, format=0)
			#print("------------------------\n")
			return converted
		except Exception as e:
			print("CARD NOT FOUND!!", str(e))
			# print("TEMPELKAN KARTU")
			# time.sleep(5)
			# hresult, reader, state, protocol, atr = SCardStatus(self.hcard)
			return False

	def read_uid(self):
		try:
			value, self.uid = self.send_command(GET_UID)
			#print("UID:", self.uid)
			#print ("string UID:", str(self.uid))
			return self.uid
		except Exception as e:
			print("failed read NFC uid : "+ str(e))

	def send_command(self, command):
		print("Sending command...")
		try:
			for iteration in range(1):
				try:
					self.hresult, self.response = SCardTransmit(self.hcard,self.dwActiveProtocol,command)
					value = toHexString(self.response, format=0)
					if(VERBOSE):
						print("Value: " + value +  " , Response:  " + str(self.response) + " HResult: " + str(self.hresult))
				except Exception as e:
					print ("No Card Found",str (e))
				#time.sleep(1)
			#print("------------------------\n")
			return self.response, value
		except Exception as e:
			print("FAILED SEND COMMMAND" + str(e))
			_READER_.ReaderAvailable.emit(str(False))

	def write_data(self, string):
		int_array = list(map(ord, string))
		#print('INI ARRAY',int_array)

		#int_array = str(int_array)
		print("Writing data: " + str(int_array))

		# If the string is greater than 16 characters, break. 
		if(len(int_array) > 16):
			return

		#print(UPDATE_FIXED_BLOCKS)
		# Add the converted string to hex blocks to the APDU command.
		for value in int_array:
			UPDATE_FIXED_BLOCKS.append(value)

		#print(UPDATE_FIXED_BLOCKS)
		# Authenticate with the specified block with the APDU authenticate command.
		response, value = self.send_command(AUTHENTICATE)

		#print("Writing " + string + " to card...")
		if(response == [144, 0]):
			print("Authentication successful.")

			if(len(string) > 0):
				print("Writing data blocks...")
				#update_block= ''.join(map(str,UPDATE_FIXED_BLOCKS))
				self.send_command(UPDATE_FIXED_BLOCKS)
			else:
				print("Please provide a valid string.")
		else:
			print("Unable to authenticate.")	
		#print("------------------------\n")


	def read_data(self):
		response, value = self.send_command(AUTHENTICATE)
		print("Reading data from card...")
		if(response == [144, 0]):
			print("Authentication successful.")
			#print("Reading data blocks...")
			result, value = self.send_command(READ_16_BINARY_BLOCKS)

			if(VERBOSE):
				print("Value: " + value +  " , Response:  " + str(result))
			#print("------------------------\n")
			return result
		else:
			print("Unable to authenticate.")
			return False
	
	def get_balance(self,balance_value):
		raw_balance = balance_value[:3]
		balance= list(map(chr,raw_balance))
		balance = int("".join(balance))
		print("Current balance= ", balance)
		return balance

def get_nfc_status():
	try:
		reader=NFC_Reader()
		check_card= reader.get_card_status()
		if (check_card==False):
			#_READER_.CardAvailable.emit('false')
			print("TEMPELKAN KARTU")
			#time.sleep(5)
			reader = NFC_Reader()
			reader.get_card_status()
			return False
		else:
			#_READER_.CardAvailable.emit('true')
			return True
	except Exception as e:
		print("Error NFC NOT Found", str(e))
		return False
		#_READER_.CardAvailable.emit('ERROR')

def get_nfc_data():
	try:
		reader=NFC_Reader()
		print(reader)
		check_valid=reader.read_data()
		print("get nfc data = ",check_valid)
		if (check_valid != False):
			check_balance = reader.get_balance(check_valid)
			print("get nfc data balance = ", check_balance)
			return str(check_valid),check_balance
		else:
			return str(False),0
	except Exception as e:
		print("Failed get NFC data " + str(e))
		#return False

def init_reader():
    try:
        reader=NFC_Reader()
        value,response=reader.send_command([0x0A,0X00])
        true_response=response[:2]
        #print(true_response)
        if true_response != "AE":
            print("failed to init")
            # value,response=reader.send_command([0x0A,0X00])
            # true_response=response[:2]
            # print(true_response)
            return False
        else:
            print("succes to init reader for beep card")
            return True
    except Exception as e:
        print("Failed to init reader with beep card " + str(e))
