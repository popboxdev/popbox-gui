from PIL import Image
from thermalprinter import *
import Configurator

portid = Configurator.get_value('Printer', 'port')

def start_print_barcode(pincode, doorno, overdue_):
    # port='/dev/ttyUSB0', baudrate=115200
    with ThermalPrinter(port=portid, baudrate=115200) as printer:
        kodebuka = pincode
        nomor_locker = doorno
        overdue = overdue_
        printer.out(str('PopBox').encode('utf-8'), bold=True, justify="C",size="L")
        data = str("\r------------------------------ \r\n Nomor Pintu Loker : {} \r\n Batas\n Pengambilan: {} \r\n ------------------------------").format(nomor_locker,overdue)
        
        dataEncode = data.encode('utf-8')
        # printer.out('Bold', bold=True)
        printer.out(dataEncode, size="S")

        str_ambil = str("\r\n Kode Ambil \r")

        printer.out(str_ambil.encode('utf-8'), justify="C", size="L")
        # Bar codes
        printer.barcode_height(80)
        printer.barcode_position(BarCodePosition.HIDDEN)
        printer.barcode_width(5)
        printer.barcode(str(kodebuka+"0"), BarCode.JAN8)

        printer.out(str('\r\n{}').format(kodebuka).encode('utf-8'), justify="C", size="L")

        footer = "\r\n------------------------------ \n*Lakukan perpanjangan jika kamu mengambil melewati batas \npengambilan, kode ambil hanya \ndapat digunakan untuk 1x \npengambilan barang \r\n*Scan Barcode di loker untuk \nmelakukan pengambilan barang"
        printer.out(footer.encode('utf-8'))

        # Line feeds
        print("Barcode - RUN")
        printer.feed(2)
        printer.flush()
        printer.write(b'\x1D\x56\x42\x64')
        print("Cutter - RUN")
