import logging
from PyQt5.QtCore import QObject, pyqtSignal
import ClientTools
import zlib
import serial
import base64
import binascii
import Configurator
# import time
import platform

# os_platform = platform.system()     #changed

scanner = None
_LOG_ = logging.getLogger()
__author__ = 'wahyudi@popbox.asia'

class ScannerSignalHandler(QObject):
    barcode_result = pyqtSignal(str)
port = Configurator.get_value('Scanner', 'port')
version = Configurator.get_value('Scanner','version')
baudrate = Configurator.get_value('Scanner','baudrate')     #changed
time_out = 5
if(version=="3.0"):
    time_out = 1
elif (version=="4.0"):
    time_out=1

_SCANNER_ = ScannerSignalHandler()

# port_ = Configurator.get_value('Scanner','port')
# version = Configurator.get_value('Scanner','version')
try:
    ser = serial.Serial(port = port, baudrate=baudrate,bytesize=serial.EIGHTBITS,timeout=time_out, stopbits=serial.STOPBITS_ONE)
except Exception as e:
    print(("ERROR SCANNER : " + str(e)))

def start_stop_scanner():
    try:
        global ser
        if version == '1.0':
            ser.write((255, 85, 13))
            # pass
        # ser.close()
    except Exception as e:
        _LOG_.debug(('start_stop_scanner ERROR :', e))

def start_get_text_info():
    ClientTools.get_global_pool().apply_async(get_text_info)

def get_text_info():
    global ser
    try:
        # try:
        #     ser.open()
        # except Exception as e:
        #     ser.close()
        #     ser.open()
        #     print(("ERROR SCANNER : " + str(e)))
        ser.flushInput()
        if version == '1.0':
            ser.write((255, 84, 13))
        scanner_result = ser.readline()
        if scanner_result == b'':
            return
        result = str(scanner_result, encoding='utf-8')
        result = result.replace(' ', '').strip().strip('\r\n').strip('\n')
        _SCANNER_.barcode_result.emit(result)
        # print(result)
        _LOG_.debug(('scanner_result is :', result))
    except Exception as e:
        _LOG_.debug(('scanner get_text_info ERROR :', e))
        _SCANNER_.barcode_result.emit('ERROR')
