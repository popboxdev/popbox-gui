import socket
import time
import sys
import pickle
import Configurator

server_port=int(Configurator.get_value('grpc','port'))
dummy_beep_trans = int(Configurator.get_value('popbox','dummy^transaction'))
dummy_beep_amount=650000
if dummy_beep_trans ==0:
    from device import NFC

def get_uid():
    try:
        global reader
        reader=NFC.NFC_Reader()
        uid=reader.read_uid()
        uid=uid[:-6]
        uid = bytes.fromhex(uid)
        print("card uid : ",uid)
        return uid
    except Exception as e:
        print("Failed get uid : " + str(e))
        return False

def init_reader():
    try:
        if dummy_beep_trans == 1:  # dummy init reader
            return True
        else:
            reader=NFC.NFC_Reader()
            value,response=reader.send_command([0x0A,0X00])
            true_response=response[:2]
            #print(true_response)
            if true_response != "AE":
                print("failed to init")
                # value,response=reader.send_command([0x0A,0X00])
                # true_response=response[:2]
                # print(true_response)
                return False
            else:
                print("succes to init reader for beep card")
                return True
    except Exception as e:
        print("Failed to init reader with beep card " + str(e))

def litering_by_two(a):
    return ' '.join([a[i:i + 2] for i in range(0, len(a), 2)])

def convert_response(response):   #for sending to the beep card only before send response function
    check=[]
    for k in response:
        a=(int(k,base=16))
        check.append(a)
    #print(check)
    return check

def send_response(response):
    try:
        global reader
        reader=NFC.NFC_Reader()
        card_feedback=reader.send_command(response)
        #print("card feedback type : ",type(card_feedback))
        return card_feedback[1]
    except Exception as e:
        print("Failed get card feedback (send_response) : "+ str(e))

def send_to_card(param=None):
    try:
        init_response=param.hex()
        init_response=litering_by_two(init_response).split()
        init_response=convert_response(init_response) 
        print("send command to card : ", str(init_response))
        card_response=send_response(init_response)
        #print("card response card: ",card_response)
        #print("card response type: ", type(card_response)) 
        if card_response == "00 90 00":
            card= card_response[:2]
            #print("card response card: ",card)
            return card
        else:
            #print("sending to card : ", card_response)
            return card_response
    except Exception as e:
        print("Failed send to card : " + str(e))
        return False

def init_to_server(service,uid):
    try:
        #service : 1 for Getbalance on server
        #service :2 fot Debit on Server
        #service = int(service)
        if service == 1:
            s.send(b'1')
            time.sleep(0.5)
            s.send(uid)
            print("init server for GetBalance service")
        elif service ==2:
            s.send(b'2')
            time.sleep(0.5)
            s.send(uid)
            print("init server for Debit service")

    except Exception as e:
        print("faield init to server "+ str(e))

def grpc_getbalance():  #param =1 for GetBalance , param =2 for Debit
    global s
    print("start grpc getbalance service !!")
    try:
        if dummy_beep_trans == 1: #beepcard dummy transaction
            print("dummy beepcard trasnsaction")
            card_CAN = 6378059900457998    #SVC DUMMY CARD
            card_status = "Card Valid"
            card_balance = dummy_beep_amount
            uid = "044b331a5a4998"
            return card_CAN,card_status,card_balance,uid
        else:
            init=init_reader()
            if init:
                uid=get_uid()
                s=socket.socket()
                host = socket.gethostname()
                port = server_port
                s.connect((host,port))
                init_to_server(1,uid)
                while 1:
                    s.settimeout(5)
                    data1=s.recv(4096)
                    print("received from server : " + str(data1.hex()))
                    if data1== b'DONE':
                        card=s.recv(4096)
                        card=pickle.loads(card) 
                        print(card)
                        s.send(b'DONE')
                        card_CAN = card[0]
                        card_status= card[1]
                        card_balance= card[2]
                        uid=uid.hex()
                        s.close()
                        return card_CAN,card_status,card_balance,uid
                    card_response_client=send_to_card(data1)
                    print("send to server : " + str(card_response_client))
                    s.send(card_response_client.encode())
    except Exception as e:
        print("Main error on Client side : " + str(e))
        return False,False,False,False

def grpc_debit():  #param =1 for GetBalance , param =2 for Debit
    global s
    try:
        if dummy_beep_trans == 1: #beepcard dummy transaction
            card_CAN = 6378059900457998    #SVC DUMMY CARD
            card_status = "Card Valid"
            card_balance = dummy_beep_amount
            uid = "044b331a5a4998"
            return card_CAN,card_status,card_balance,uid
        else:
            init=init_reader()
            if init:
                uid=get_uid()
                s=socket.socket()
                host = socket.gethostname()
                port = server_port
                s.connect((host,port))
                init_to_server(2,uid)
                while 1:
                    s.settimeout(5)
                    data1=s.recv(4096)
                    print("received from server : " + str(data1.hex()))
                    if data1== b'DONE':
                        card=s.recv(4096)
                        card=pickle.loads(card) 
                        print(card)
                        s.send(b'DONE')
                        card_CAN = card[0]
                        card_status= card[1]
                        card_balance= card[2]
                        uid=uid.hex()
                        s.close()
                        return card_CAN,card_status,card_balance,uid
                    card_response_client=send_to_card(data1)
                    print("send to server : " + str(card_response_client))
                    s.send(card_response_client.encode())
    except Exception as e:
        print("Main error on Client side : " + str(e))
        return False,False,False,False

# if __name__=="__main__":
#     try:
#         can,status,balance =grpc_getbalance()
      
#     except Exception as e:
#         print("Main error : " + str(e))
    