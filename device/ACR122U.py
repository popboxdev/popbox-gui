import subprocess
import time
import mouse
import ClientTools


def openACR():
    try:
        path= "C:\Program Files (x86)\Advanced Card Systems Ltd\ACR122U NFC Reader SDK\Tools\ACR122U Tool\ACR122UTool.exe"
        return subprocess.Popen(path)
    except Exception as e:
        print("Ërror open  :  " + str(e))

def closeACR():
    global p
    try:
        p.terminate()
    except Exception as e:
        print("Error close : " + str(e))

def initACR():
    global p
    p=openACR()
    time.sleep(0.3)
    mouse.move(20,56)
    mouse.click('left')
    time.sleep(0.3)
    mouse.move(518,417)
    mouse.click('left')
    time.sleep(0.3)
    mouse.move(20,56)
    mouse.click('left')
    closeACR()
    print("INIT ACR FINISHED")

def start_init_ACR():
    ClientTools.get_global_pool().apply_async(initACR)
