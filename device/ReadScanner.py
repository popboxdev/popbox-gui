import serial
import Configurator

port = Configurator.get_value('Scanner', 'port')
baudrate = Configurator.get_value('Scanner','baudrate')

ser=serial.Serial(port=port, baudrate=baudrate)

def read_scanner():
    while True:
        if (ser.in_waiting>0):
            line=ser.readline()
            return line
           # line=line.decode('utf-8')
            #line=line.rstrip()
        