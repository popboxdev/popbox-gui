Internet Wifi dengan D-Link karena autodetect oleh Ubuntu 18.04 

************************************************
sudo apt update
sudo apt install openssh-server
sudo systemctl status ssh
sudo ufw allow ssh ## jika diperlukan
systemctl restart ssh

apt-get install python3-pip
apt-get install git
pip3 install requests
pip3 install pyserial
pip3 install pyqrcode
pip3 install opencv-python
pip3 install pygame
sudo apt-get install python3-pyqt5
sudo apt-get install python3-pyqt5.qtquick
sudo apt-get install qml-module-qtquick-controls
sudo apt-get install qml-module-qt-labs-folderlistmodel
sudo apt-get install libqt5multimedia5-plugins qml-module-qtmultimedia

Hardware Info :
pip3 install psutil

Install Gstreamer Untuk kebutuhan play TVC:
sudo apt-get install gstreamer1.0-libav ubuntu-restricted-extras ## Wajib

Stop TVC ketika Mouse Klik :
buat file input.conf dengan isi:
MOUSE_BTN0 quit
MOUSE_BTN1 ignore
lalu ketik perintah : 
cp input.conf ~/.mplayer/

Setting Grub untuk mengatasi Hang Reboot : 
sudo nano /etc/default/grub 
GRUB_CMDLINE_LINUX_DEFAULT="acpi=force"

Install Remote PC Anydesk :
download file anydesk.deb for debian
sudo dpkg -i anydesk.deb
sudo apt-get install -f

Setting Crontab :
crontab -e
pastikan file run_gui.sh sesuai path
@reboot sh /home/popbox/run_gui.sh

Setting Device /dev/ttyS untuk akses device emoney / stringboard oleh user :
cd /dev/ && ls -la | grep ttyS
sudo adduser popbox dialout

Install sqlite Browser :
sudo apt-get update
sudo apt-get install sqlitebrowser

Jika error:
pip install configparser ##  jika butuh untuk masalah config parser

Disable popup "System program problem detected" :
sudo rm /var/crash/*
sudo nano /etc/default/apport
jadikan enable=0

Hide Panel Ubuntu 18.04 : 
install gnome tweaks di menu "ubuntu software" 
sudo apt-get install make #jika tidak ada make
git clone https://github.com/home-sweet-gnome/dash-to-panel.git
make install
restart