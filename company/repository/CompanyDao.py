# Python bytecode 3.4 (3310)
# Embedded file name: C:\Users\admin\Downloads\pakpoboxclient-b68a562d2449f67a60e96ff67ee3efa68449cf2c\pakpoboxclient\company\repository\CompanyDao.py
# Compiled at: 2016-08-18 02:05:38
# Size of source mod 2**32: 948 bytes
# Decompiled by https://python-decompiler.com
from database import ClientDatabase
__author__ = 'gaoyang'

def init_company(param):
    sql = 'INSERT INTO Company (id, companyType, name, parentCompany_id,deleteFlag)VALUES (:id,:companyType,:name,:parentCompany_id,:deleteFlag)'
    ClientDatabase.insert_or_update_database(sql, param)


def get_company_by_id(company_param):
    sql = 'SELECT * FROM Company WHERE id = :id'
    return ClientDatabase.get_result_set(sql, company_param)


def insert_company(param):
    sql = 'INSERT INTO Company (id, companyType, name, parentCompany_id,deleteFlag)VALUES (:id,:companyType,:name,:parentCompany_id,:deleteFlag)'
    ClientDatabase.insert_or_update_database(sql, param)


def update_company(param):
    sql = 'UPDATE Company SET companyType =:companyType, name =:name, parentCompany_id =:parentCompany_id,deleteFlag =:deleteFlag WHERE id =:id'
    ClientDatabase.insert_or_update_database(sql, param)