@echo off

SCHTASKS /Change /TN "BypassService" /Disable

powershell -Command git reset --hard
powershell -Command git pull origin master-ppc-insulator

REM powershell -Command python -m pip install %CD%\library/pyscreenshot-0.5.1.tar.gz
REM powershell -Command python -m pip install %CD%\library/Pillow-5.1.0-cp34-cp34m-win32.whl
REM powershell -Command python -m pip install %CD%\library/EasyProcess-0.2.7-py2.py3-none-any.whl

set _interval=5
set _processName=python.exe
set _processCmd=%CD%\PopboxClient.py
set _processTimeout=5

:LOOP
set /a isAlive=false

tasklist | find /C "%_processName%" > temp.txt
set /p num= < temp.txt
del /F temp.txt

echo "%num%"

if "%num%" == "0" (
python %_processCmd% | echo FOUND %_processName% in Running System %time%
choice /D y /t %_processTimeout% > nul
)
if "%num%" == "1" (
python %_processCmd% | echo FOUND %_processName% in Running System %time%
choice /D y /t %_processTimeout% > nul
)

if "%num%" GTR "1" echo STARTED

choice /D y /t %_interval% >nul

goto LOOP
