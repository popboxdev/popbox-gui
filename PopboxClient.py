import os
import sys
from PyQt5.QtCore import QUrl, QObject, pyqtSlot, QTranslator, Qt
from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQuick import QQuickView
import pygame
import Configurator
import device
from box.service import BoxService
from database import ClientDatabase
from device import Camera
from express.service import ExpressService
from express.popbox import PopboxService
import express.repository.ExpressDao as ExpressDao
from network import HttpClient
from sync import PushMessage, PullMessage
from user.service import UserService
from alert.service import AlertService
from service import PPOB
from service import APService
import logging
import logging.handlers
import subprocess
import atexit
import json
from time import *
import ClientTools
import math
import requests
import os.path
import datetime
import sys, os
from tkinter import *
from time import time, sleep, clock
from box.repository import BoxDao
import platform
bit_platform =  platform.architecture()[0]
os_platform = platform.system()
if(bit_platform=="64bit") and (os_platform != 'Linux'):
    from device import NFC
    if Configurator.get_value('ClientInfo','language')=='PH':
        from device import ACR122U

# path_project=Configurator.get_value('PathGUI','path')
if os_platform == 'Linux': 
    from device import Printer
    # sys.path.insert(1,path_project + 'device')
else:       #windows
    import win32api, win32con, win32gui
    import wmi
    import winreg

class SlotHandler(QObject):
    __qualname__ = 'SlotHandler'

    # NEW

    def start_get_gui_version(self):
        PopboxService.start_get_gui_version()

    start_get_gui_version = pyqtSlot()(start_get_gui_version)

    def start_get_scanner_version(self):
        PopboxService.start_get_scanner_version()

    start_get_scanner_version = pyqtSlot()(start_get_scanner_version)

    def start_get_opendoor_mode(self):
        PopboxService.start_get_opendoor_mode()

    start_get_opendoor_mode = pyqtSlot()(start_get_opendoor_mode)

    def start_get_mode_insulator(self):
        PopboxService.start_get_mode_insulator()

    start_get_mode_insulator = pyqtSlot()(start_get_mode_insulator)

    def start_get_locker_name(self):
        PopboxService.start_get_locker_name()

    start_get_locker_name = pyqtSlot()(start_get_locker_name)

    def start_get_language(self):
        PopboxService.start_get_language()

    start_get_language = pyqtSlot()(start_get_language)

    def start_get_theme(self):
        PopboxService.start_get_theme()

    start_get_theme = pyqtSlot()(start_get_theme)

    def start_get_contactless(self):
        PopboxService.start_get_contactless()

    start_get_contactless = pyqtSlot()(start_get_contactless)

    def start_idle_mode(self):
        PullMessage.start_idle_mode()

    start_idle_mode = pyqtSlot()(start_idle_mode)

    def set_logout_user(self):
        UserService.set_logout_user()

    set_logout_user = pyqtSlot()(set_logout_user)

    def stop_courier_scan_barcode(self):
        ExpressService.stop_get_express_number_by_barcode()

    stop_courier_scan_barcode = pyqtSlot()(stop_courier_scan_barcode)

    def stop_idle_mode(self):
        PullMessage.stop_idle_mode()

    stop_idle_mode = pyqtSlot()(stop_idle_mode)

    def start_courier_scan_barcode(self):
        ExpressService.start_get_express_number_by_barcode()

    start_courier_scan_barcode = pyqtSlot()(start_courier_scan_barcode)

    def start_get_free_mouth_mun(self, flagInsulator=None):
        BoxService.start_get_free_mouth_mun( flagInsulator )

    start_get_free_mouth_mun = pyqtSlot(str)(start_get_free_mouth_mun)

    def start_get_tvc_timer(self):
        PopboxService.start_get_tvc_timer()

    start_get_tvc_timer = pyqtSlot()(start_get_tvc_timer)

    def start_get_file_banner(self, dir):
        PopboxService.start_get_file_banner(dir)

    start_get_file_banner = pyqtSlot(str)(start_get_file_banner)

    def start_get_free_mouth_mun_insulator(self):
        BoxService.start_get_free_mouth_mun_insulator()

    start_get_free_mouth_mun_insulator = pyqtSlot()(start_get_free_mouth_mun_insulator)

    def start_get_internet_status(self):
        PopboxService.start_get_internet_status()

    start_get_internet_status = pyqtSlot()(start_get_internet_status)

    def background_login(self, username, password, identity):
        UserService.background_login_start(username, password, identity)

    background_login = pyqtSlot(str, str, str)(background_login)

    def get_user_info(self):
        UserService.get_user_info()

    get_user_info = pyqtSlot()(get_user_info)

    def start_video_capture(self, filename):
        Camera.start_video_capture(filename)

    start_video_capture = pyqtSlot(str)(start_video_capture)



def signal_handler():
    ExpressService._EXPRESS_.barcode_signal.connect(view.rootObject().barcode_result)
    PopboxService._POP_.get_scanner_version_signal.connect(view.rootObject().get_scanner_version_result)
    PopboxService._POP_.start_get_opendoor_mode_signal.connect(view.rootObject().start_get_openmode_mode_result)
    PopboxService._POP_.start_get_contactless_signal.connect(view.rootObject().start_get_contactless_result)
    PopboxService._POP_.start_get_theme_signal.connect(view.rootObject().start_get_theme_result)
    PopboxService._POP_.start_get_language_signal.connect(view.rootObject().start_get_language_result)
    PullMessage._PULL_.maintenance_status_signal.connect(view.rootObject().maintenance_status_result)
    PopboxService._POP_.start_get_tvc_timer_signal.connect(view.rootObject().start_get_tvc_timer_result)
    BoxService._BOX_.free_mouth_num_signal_all.connect(view.rootObject().free_mouth_result_all)
    BoxService._BOX_.free_mouth_num_signal_insulator.connect(view.rootObject().free_mouth_result_insulator)
    BoxService._BOX_.free_mouth_num_signal.connect(view.rootObject().free_mouth_result)
    PopboxService._POP_.start_get_locker_name_signal.connect(view.rootObject().start_get_locker_name_result)
    PopboxService._POP_.start_get_file_banner_signal.connect(view.rootObject().start_get_file_banner_result)
    PopboxService._POP_.start_get_gui_version_signal.connect(view.rootObject().start_get_gui_version_result)
    PopboxService._POP_.start_get_mode_insulator_signal.connect(view.rootObject().start_get_mode_insulator_result)
    PopboxService._POP_.start_internet_status_result_signal.connect(view.rootObject().start_internet_status_result)
    UserService._USER_.user_login_signal.connect(view.rootObject().user_login_result)
    UserService._USER_.user_info_signal.connect(view.rootObject().user_info_result)


_LOG_ = None


def configuration_log():
    global _LOG_
    try:
        if not os.path.exists(sys.path[0] + '/log/'):
            os.makedirs(sys.path[0] + '/log/')
        handler = logging.handlers.TimedRotatingFileHandler(filename=sys.path[0] + '/log/base.log',
                                                            when='MIDNIGHT',
                                                            interval=1,
                                                            backupCount=60)
        logging.basicConfig(handlers=[handler],
                            level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(funcName)s:%(lineno)d: %(message)s',
                            datefmt='%d/%m %H:%M:%S')
        _LOG_ = logging.getLogger()
    except Exception as e:
        print("Logging Configuration ERROR : ", e)


def get_disk_info():        #changed
    if os_platform == 'Linux':
        disk_info = '012345'	
        try:
            output = subprocess.Popen('lsblk --nodeps -o name,serial | grep "sda" | awk "{print $2}"',shell=True, stdout=subprocess.PIPE).communicate()[0]		 
            disk_info = output.split()[1].split(b'x')		        
            disk_info = disk_info[0].decode("utf-8")		          
        except Exception as e:		    
            _LOG_.warning(('Error Getting Disk Info : ', e))		          
            disk_info = '012345'
        HttpClient._DISK_SN_ = disk_info
    else:
        encrypt_str = ''
        disk_info = []
        try:
            c = wmi.WMI()
            for physical_disk in c.Win32_DiskDrive():
                encrypt_str = encrypt_str + physical_disk.SerialNumber.strip()
        except Exception as e:
            encrypt_str = 'None'
            _LOG_.warning(('Error Getting Disk Info : ', e))
        disk_info.append(encrypt_str)
        HttpClient._DISK_SN_ = disk_info[0]

def process_exists(processname):
    tlcall = 'TASKLIST', '/FI', 'imagename eq %s' % processname
    tlproc = subprocess.Popen(tlcall, shell=True, stdout=subprocess.PIPE)
    tlout = tlproc.communicate()[0].decode('utf-8').strip().split("\r\n")
    # print('Checking Process for : ' + processname)
    if len(tlout) > 1 and processname in tlout[-1]:
        print(processname + ' is Found!')
        return True
    else:
        # print(processname + ' is not Found!')
        return False

def set_tvc_player(command):        #changed
    if os_platform == 'Linux':
        global GLOBAL_SETTING
        if command == "":
            return
        elif command == "STOP":
            # os.system(sys.path[0] + './player/stop.sh')
            os.system(sys.path[0] + subprocess.call("./player/stop.sh"))
        elif command == "START":
            # os.system(sys.path[0] + './player/start.sh')
            os.system(sys.path[0] + subprocess.call("./player/start.sh"))
            # if process_exists("PopBoxTVCPlayer.scr"):
            #     os.system(sys.path[0] + '/player/stop.bat')
            # timer_val = PopboxService.get_tvc32_timer()
            # os.system(sys.path[0] + '/player/start.bat' + ' ' + str(timer_val))
        else:
            return

    else:     #windows
        global GLOBAL_SETTING
        if command == "":
            return
        elif command == "STOP":
            os.system(sys.path[0] + '/player/stop.bat')
        elif command == "START":
            if process_exists("PopBoxTVCPlayer.scr"):
                os.system(sys.path[0] + '/player/stop.bat')
            timer_val = PopboxService.get_tvc32_timer()
            os.system(sys.path[0] + '/player/start.bat' + ' ' + str(timer_val))
        else:
            return

def check_database(data_name):
    if not os.path.exists(sys.path[0] + '/database/' + data_name + '.db'):
        ClientDatabase.init_database()
        BoxService.get_box()
    _LOG_.info(("DB : ", data_name))


def init_reader():
    try:
        init__emoney = device.QP3000S.init_serial(x=1)
        _LOG_.info(('init_emoney flag is ', init__emoney))
        if init__emoney:
            init_result = device.QP3000S.initSAM()
            sync_time = device.QP3000S.syncTime()
            Configurator.set_value('panel', 'emoney', 'enabled')
            _LOG_.info(('init_SAM & sync_Time result is ', init_result, sync_time))
        else:
            Configurator.set_value('panel', 'emoney', 'disabled')
            _LOG_.warning('init_SAM & sync_Time result is ERROR')
    except Exception as e:
        Configurator.set_value('panel', 'emoney', 'disabled')
        _LOG_.warning(str(e))


def kill_explorer():
    if GLOBAL_SETTING['dev^mode'] is False:
        os.system('taskkill /f /im explorer.exe')
    else:
        _LOG_.info('Development Mode is ON')


def disable_screensaver():
    try:
        os.system('reg delete "HKEY_CURRENT_USER\Control Panel\Desktop" /v SCRNSAVE.EXE /f')
    except Exception as e:
        _LOG_.warning(('Screensaver Disabling ERROR : ', e))


def init_nfc_reader():
    if GLOBAL_SETTING['reader'] and not GLOBAL_SETTING['dev^mode']:
        init_reader()
    else:
        _LOG_.info('eMoney Reader is not ACTIVATED!')


def init_time(file):
    if file is None:
        return
    try:
        import platform
        os_ver = platform.platform()
        if 'Windows-7' in os_ver:
            process = subprocess.Popen(sys.path[0] + file, shell=True, stdout=subprocess.PIPE)
            output = process.communicate()[0].decode('utf-8').strip().split("\r\n")
            _LOG_.info(('time initiation is success : ', str(output)))
            sleep(5)
        else:
            _LOG_.debug(('time initiation is failed : ', str(os_ver)))
    except Exception as e:
        _LOG_.warning(('time initiation is failed : ', e))
    # print(('init time result : ', init_time_result))


def set_ext_keyboard(command):
    if command == "":
        return
    elif command == "STOP":
        os.system('taskkill /f /IM osk.exe')
    elif command == "START":
        if not process_exists('osk.exe'):
            os.system('osk')
        else:
            print('External Keyboard is already running..!')
    else:
        return


@atexit.register
def exit_message():
    print(('[Info] Please re-run the app'))


GLOBAL_SETTING = dict()

def init_setting():
    global GLOBAL_SETTING
    Configurator.set_value('panel', 'force^test', '0')
    if not Configurator.get_value('panel', 'emoney') == 'disabled':
        init_reader()

    if Configurator.get_value('panel', 'gui') == "development":
        GLOBAL_SETTING['dev^mode'] = True
    else:
        GLOBAL_SETTING['dev^mode'] = False
    if Configurator.get_value('panel', 'emoney') == "enabled":
        GLOBAL_SETTING['reader'] = True
    else:
        GLOBAL_SETTING['reader'] = False
    if 'pr0x' not in Configurator.get_value('ClientInfo', 'serveraddress'):
        GLOBAL_SETTING['db'] = 'pakpobox'
    else:
        GLOBAL_SETTING['db'] = 'popboxclient'
    if Configurator.get_value('Scanner','contactless')=='enable':
        GLOBAL_SETTING['contactless'] = True
    else:
        GLOBAL_SETTING['contactless'] = False
        GLOBAL_SETTING['contactless'] = Configurator.get_or_set_value('Scanner', 'contactless', 'disable')
    if Configurator.get_value('ClientInfo','language')=='MY':
        GLOBAL_SETTING['language'] = 'my.qm'
    if Configurator.get_value('ClientInfo','language')=='PH':
        GLOBAL_SETTING['language'] = 'ph.qm'
    else:
        GLOBAL_SETTING['language'] = 'id.qm'
        GLOBAL_SETTING['language'] = Configurator.get_or_set_value('ClientInfo', 'language', 'ID')
    GLOBAL_SETTING['use^mode'] = Configurator.get_or_set_value('ClientInfo', 'use^mode', 'regular')
    GLOBAL_SETTING['backend'] = Configurator.get_value('ClientInfo', 'serveraddress')
    # GLOBAL_SETTING['language'] = Configurator.get_or_set_value('ClientInfo', 'language', 'regular')
    # Set Temporary code contactless enable All
    Configurator.set_value('Scanner', 'contactless', 'enable')
    Configurator.set_value('Scanner', 'baudrate', '115200')
    Configurator.set_value('ClientInfo', 'openmode', 'default')
    version = Configurator.get_value('Scanner','version')
    if(version=="4.0"):
        Configurator.set_value('Scanner', 'baudrate', '9600')
    GLOBAL_SETTING['main^module'] = 'Main.qml'
    _LOG_.info(GLOBAL_SETTING)
    return GLOBAL_SETTING

def checking_column_db():
    # check column express
    check_column = ExpressDao.check_column_exist({}, 'Express')
    checking = 0
    for list_ in check_column:
        if list_['name'] == 'is_needed_pod':
            checking += 1

    if checking == 0:
        adding_column = ExpressDao.adding_column({},'is_needed_pod', 0, 'Express')

    for list_ in check_column:
        if list_['name'] == 'flagRedrop':
            checking += 1

    if checking == 1:
        ExpressDao.adding_column({},'flagRedrop', 0, 'Express')

    # check column transactionRecord
    check_column_tr = ExpressDao.check_column_exist({}, 'TransactionRecord')
    checking_tr = 0
    for list_tr in check_column_tr:
        if list_tr['name'] == 'paymentParam':
            checking_tr += 1
    if checking_tr == 0:
        ExpressDao.adding_column({},'paymentParam', 0, 'TransactionRecord')

    for list_tr in check_column_tr:
        if list_tr['name'] == 'paymentStatus':
            checking_tr += 1
    if checking_tr == 1:
        ExpressDao.adding_column({},'paymentStatus', 0, 'TransactionRecord')

    for list_tr in check_column_tr:
        if list_tr['name'] == 'deleteFlag':
            checking_tr += 1
    if checking_tr == 2:
        ExpressDao.adding_column({},'deleteFlag', 0, 'TransactionRecord')


def check_bypass():
    bypass_server = 'http://127.0.0.1:8000'
    try:
        r = requests.get(bypass_server, timeout=3)
        r.raise_for_status()
        _LOG_.info("BALIKANNYA:", r)
    except requests.exceptions.HTTPError as errh:
        _LOG_.info("BALIKANNYA:", errh)
    except requests.exceptions.ConnectionError as errc:
        _LOG_.info("BALIKANNYA Connecting:", errc)
    except requests.exceptions.Timeout as errt:
        _LOG_.info("Timeout BALIKANNYA:", errt)
    except requests.exceptions.RequestException as err:
        _LOG_.info("BALIKANNYA : Something Else", err)
    # try:
    #     bypass_server = 'http://127.0.0.1:8000'
    #     r = requests.get(bypass_server)
    #     # return value
    #     # _LOG_.warning("BALIKANNYA : " + r)
    #     _LOG_.info("BALIKAN " + str(r))
    # except requests.exceptions.HTTPError as e:
    #     _LOG_.warning(("bypass_server is failed : ", e))
    #     print(str(r))

def init_paid_overdue():
    print('start_init_paid_overdue')
    BoxService.start_init_paid_overdue()
    return

def init_popsafe_price():
    print('start_init_popsafe_price')
    BoxService.start_init_popsafe_price()
    return

def file_local_time():
    if os.path.isfile('local_time.txt'):
        if os.stat('local_time.txt').st_size == 0:
            print('Update Local Time')
            file = open("local_time.txt","w+")
            time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            print('time', str(time))
            file.write(str(time))
            file.close
        else:
            return
    else:
        file = open("local_time.txt","w+")
        time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print('time', str(time))
        file.write(str(time))
        file.close

def check_file_config():
    if os.path.isfile('config.conf'):
        if os.stat('config.conf').st_size == 0:
            print("lakukan update config.conf yang kosong dari file system_config.txt")
        else:
            if os.path.isfile('system_config.txt'):
                return
            else:
                print("lakukan pembentukan system_config.txt dengan isi file dari config.conf")
    else:
        print("lakukan pembentukan config.conf, lalu isi dari file system_config.txt")

def check_file_db():
    if os.path.isfile('database/popboxclient.db'):
        return
    else:
        file = open("database/popboxclient.db")
        file.close

def check_file_door_price():
    try:
        if os.path.isfile('qml/door_price.js'):
            return
        else:
            print('create_new_file')
            with open("qml/door_price_bak.js") as file:
                with open("qml/door_price.js", "w+") as door_price:
                    for line in file:
                        door_price.write(line)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] create_file_door_price : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def check_folder_qr_payment():
    try:
        qr_folder = os.path.exists('qr-payment')
        if qr_folder is False:
            os.mkdir('qr-payment')
        else:
            return
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] create_folder_qr_payment : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def set_desktop_background(path): #changed
    if os_platform == 'Linux':
        print("no background")
    else:   #windows
        key = win32api.RegOpenKeyEx(win32con.HKEY_CURRENT_USER,"Control Panel\\Desktop",0,win32con.KEY_SET_VALUE)
        win32api.RegSetValueEx(key, "WallpaperStyle", 0, win32con.REG_SZ, "0")
        win32api.RegSetValueEx(key, "TileWallpaper", 0, win32con.REG_SZ, "0")
        win32gui.SystemParametersInfo(win32con.SPI_SETDESKWALLPAPER, path, 1+2)

def check_insulator_config_from_server():
    try:
        BoxService.start_init_insulator()
        return
    except Exception as e:
        print("error_config_insulator : ", e)

def check_ovo_configuration():
    try:
        if os.path.isfile('config.conf'):
            with open("config.conf") as file:
                if "[QrOvo]" in file.read():
                    check_is_deployed = Configurator.get_value('QrOvo', 'is_deployed')
                    if check_is_deployed is None or check_is_deployed == "0":
                        PopboxService.get_ovo_config()
                    else:
                        print('ovo_already_configured')
                else:
                    Configurator.add_section('QrOvo')
                    Configurator.set_value('QrOvo', 'merchant_id', "ovo")
                    Configurator.set_value('QrOvo', 'mid', "ovo")
                    Configurator.set_value('QrOvo', 'tid', "ovo")
                    Configurator.set_value('QrOvo', 'store_id', "ovo")
                    Configurator.set_value('QrOvo', 'is_deployed', "0")
                    PopboxService.get_ovo_config()

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] checking_config_ovo : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))    

# def send_message_telegram(message):
#     bot_token = '1087914386:AAEE6Zljo_Y7-KyoroWNpPLfdRVSUic0PP4'
#     bot_chatID = '-1001273178241'
#     send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + message
#     response = requests.get(send_text)

def check_dblocal_dbcounter():
    try:
        box = BoxDao.get_box_info()
        namepath  = [
            'peoplecounter',
            'peopleCounter',
            'People Counter',
            'people counter',
            'people_counter'
        ]
        for item in namepath :
            flag = os.path.exists('D:/'+item)
            if flag:
                if os.path.isfile('D:/'+item+'/database/counter.db'):
                    cnt = BoxDao.get_box_counter(item)
                    if box[0]['orderNo'] != cnt[0]['orderNo']:
                        bot_message = box[0]['name']+" hasn't initiated its peopleCounter data yet. The data still using "+cnt[0]['name']+" location"
                        # send_message_telegram(bot_message)
                        break
    except Exception as e:
        box = BoxDao.get_box_info()
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        msg = box[0]['name']+" is ERROR with message: " + str(e) +", FILENAME: "+str(fname)+", LINE: "+str(exc_tb.tb_lineno)
        # send_message_telegram(msg)

def get_gui_version():
    version_path = os.path.join(os.getcwd(), 'version.txt')
    version = open(version_path, 'r').read().strip()
    return version

def update_specific_version():
    try:
        temp = get_gui_version()
        temp_int = int(temp[7])
        print("temp in : ",str(temp_int))
        branch = "master"
        if temp_int < 7:
            files= [
                "/advertisement/apservice/banner-05.jpg",
                "/advertisement/apservice/banner-06.jpg",
                "/advertisement/source/banner-05.jpg",
                "/advertisement/source/banner-06.jpg"
            ]
            for file_ in files:
                path_del = sys.path[0]+file_
                os.remove(path_del)
            try:
                # subprocess.call("git checkout .")
                subprocess.call("git checkout "+ branch)
                subprocess.call("git pull origin "+ branch)
            except Exception as e:
                print("Error pull git=> " + str(e))
    except Exception as e:
        print("Error Check Version => " + str(e))

class Splash:
    def __init__(self, root, file, wait):
        self.__root = root
        self.__file = file
        self.__wait = wait + clock()

    def __enter__(self):
        # Hide the root while it is built.
        self.__root.withdraw()
        # Create components of splash screen.
        window = Toplevel(self.__root)
        # splash = PhotoImage(master=window, file=self.__file, format="png")

        self.__root.overrideredirect(True)
        self.__root.wm_attributes("-topmost", True)
        if os_platform == 'Linux':
            pass
        else:   #windows
            self.__root.wm_attributes("-disabled", True)
            self.__root.wm_attributes("-transparentcolor", "white")

        splash = PhotoImage(master=window, file=self.__file, format="png")
        canvas = Label(window, text=d_status.get(), fg='black', bg='white', image=splash, compound=TOP)

        # Get the screen's width and height.
        scrW = window.winfo_screenwidth()
        scrH = window.winfo_screenheight()
        # Get the images's width and height.
        imgW = splash.width()
        imgH = splash.height()        
        # Compute positioning for splash screen.
        Xpos = (scrW - imgW) // 2
        Ypos = (scrH - imgH) // 2
        # Configure the window showing the logo.
        window.overrideredirect(True)
        window.geometry('+{}+{}'.format(Xpos, Ypos))
        canvas.grid()
        # Show the splash screen on the monitor.
        window.update()
        # Save the variables for later cleanup.
        self.__window = window
        self.__canvas = canvas
        self.__splash = splash

    def __exit__(self, exc_type, exc_val, exc_tb):
        # Ensure that required time has passed.
        now = clock()
        if now < self.__wait:
            sleep(self.__wait - now)
        # Free used resources in reverse order.
        del self.__splash
        self.__canvas.destroy()
        self.__window.destroy()
        # Give control back to the root program.
        self.__root.update_idletasks()
        self.__root.deiconify()

def delay_for_everything():
    for a in range(1,5):
        sleep(1)
        print(a)

def set_online_offline():
    try:
        BoxService.set_online_offline_gui()
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] set_online_offline : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))    

if __name__ == '__main__':

    if os_platform == 'Linux':
        configuration_log()
    else:
        pass
    image_splash = "15.png"

    root = Tk()
    d_status = StringVar()
    d_status.set("PopBox Initializing...")
    animation = 'qml/asset/global/splash_screen/' + image_splash
    # animation = 'qml/asset/global/animation/lgspinnerx.gif'
    with Splash(root, animation, 0):
        # update_specific_version()
        file_local_time()
        check_file_db()
        checking_column_db()
        check_file_door_price()
        check_folder_qr_payment()
        check_ovo_configuration()
        check_insulator_config_from_server()
        if(bit_platform=="64bit") and (os_platform != 'Linux'):
            if Configurator.get_value('ClientInfo','language')=='PH':
                ACR122U.start_init_ACR()
        # ClientTools.reset_system_time_date()
        if os_platform == 'Linux': #changed
            pass
        else:#windows
            configuration_log()
        d_status.set("configurations is setting up...")
        # init_time('/neutronSync.bat')
        PopboxService.set_localTime()
        # check_bypass()
        # update_module({})
        init_setting()
        init_paid_overdue()
        init_popsafe_price()
        kill_explorer()     # TODO URGENT
        check_database(GLOBAL_SETTING['db'])
        path = sys.path[0] + '/qml/'
        if os.name == 'nt':
            path = 'qml/'
        slot_handler = SlotHandler()
        app = QGuiApplication(sys.argv)
        view = QQuickView()
        context = view.rootContext()
        translator = QTranslator()
        context.setContextProperty('slot_handler', slot_handler)
        translator.load(path + GLOBAL_SETTING['language'])
        app.installTranslator(translator)
        view.engine().quit.connect(app.quit)
        view.setSource(QUrl(path + GLOBAL_SETTING['main^module']))
        signal_handler()
        if not GLOBAL_SETTING['dev^mode']:
            app.setOverrideCursor(Qt.BlankCursor)     # TODO URGENT
            # pass            
        view.setFlags(Qt.WindowFullscreenButtonHint)
        view.setFlags(Qt.FramelessWindowHint)
        view.resize(1023, 768)
        disable_screensaver()
        get_disk_info()
        PopboxService.start_delete_express_by_range(limit=1, duration='MONTH')
        PopboxService.create_database_survey()
        PopboxService.create_db_signature()
        PopboxService.create_db_contactless()
        PopboxService.start_update_survey_express()
        PopboxService.create_table_mini_insulator()
        PopboxService.create_db_history_opendoor()
        check_dblocal_dbcounter()
        # if Configurator.get_value('ClientInfo','language')=='ID': #Disable Sync Contactless
        #     PopboxService.sync_contactless()
        # PopboxService.start_sync_signature()
        PopboxService.start_post_gui_info()
        PopboxService.check_connection_time()
        PushMessage.start_sync_message()
        PullMessage.start_pull_message()
        # APService.start_get_mode()
        if Configurator.get_value('PullRemote','isactive') == 1 or Configurator.get_value('PullRemote','isactive') is None:
            print("pull message auto is running .... ")
    
        PopboxService.change_overdue_price()
        BoxService.start_set_online_offline_gui()
        pygame.init()
        Camera.init_camera()
        # sleep(3) # delay before launch view
        root.after(3000, root.destroy)
        root.mainloop()
        view.show() # after all qml loaded then apps launch
        app.exec_()
        del view
        d_status.set("Done!")
    root.destroy()