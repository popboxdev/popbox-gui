DROP TABLE IF EXISTS Box;
CREATE TABLE Box
(
  id           VARCHAR(255) PRIMARY KEY NOT NULL,
  currencyUnit VARCHAR(255),
  deleteFlag   INT                      NOT NULL,
  name         VARCHAR(255)             NOT NULL,
  orderNo      VARCHAR(255)             NOT NULL,
  syncFlag     INT,
  validateType VARCHAR(255),
  operator_id  VARCHAR(255)             NOT NULL,
  freeDays     INT,
  freeHours    INT,
  holidayType  VARCHAR(255),
  overdueType  VARCHAR(255),
  receiptNo    BIGINT
);
DROP TABLE IF EXISTS Holiday;
CREATE TABLE Holiday
(
  id          VARCHAR(255) PRIMARY KEY NOT NULL,
  startTime   BIGINT                   NOT NULL,
  endTime     BIGINT                   NOT NULL,
  delayDay    INT,
  holidayType VARCHAR(255)             NOT NULL
);
DROP TABLE IF EXISTS Cabinet;
CREATE TABLE Cabinet
(
  id         VARCHAR(255) PRIMARY KEY NOT NULL,
  deleteFlag INT                      NOT NULL,
  number     INT                      NOT NULL
);
DROP TABLE IF EXISTS Company;
CREATE TABLE Company
(
  id               VARCHAR(255) PRIMARY KEY NOT NULL,
  companyType      VARCHAR(255),
  name             VARCHAR(255)             NOT NULL,
  parentCompany_id VARCHAR(255),
  deleteFlag       INT                      NOT NULL
);
DROP TABLE IF EXISTS Express;
CREATE TABLE Express
(
  id                       VARCHAR(255) PRIMARY KEY NOT NULL,
  brandId                  VARCHAR(255),
  customerStoreNumber      VARCHAR(255),
  expressNumber            VARCHAR(128),
  expressType              VARCHAR(255),
  overdueTime              BIGINT,
  status                   VARCHAR(255),
  storeTime                BIGINT,
  syncFlag                 INT                      NOT NULL,
  takeTime                 BIGINT,
  takeUserPhoneNumber      VARCHAR(255),
  storeUserPhoneNumber     VARCHAR(255),
  validateCode             VARCHAR(255),
  version                  INT                      NOT NULL,
  barcode_id               VARCHAR(255),
  box_id                   VARCHAR(255),
  endAddress_id            VARCHAR(255),
  logisticsCompany_id      VARCHAR(255),
  mouth_id                 VARCHAR(255),
  operator_id              VARCHAR(255),
  rangePrice_id            VARCHAR(255),
  startAddress_id          VARCHAR(255),
  storeUser_id             VARCHAR(255),
  takeUser_id              VARCHAR(255),
  recipientName            VARCHAR(255),
  weight                   BIGINT,
  recipientUserPhoneNumber VARCHAR(255),
  groupName                VARCHAR(255),
  staffTakenUser_id        VARCHAR(255),
  importTime               BIGINT,
  lastModifiedTime         BIGINT,
  chargeType               VARCHAR(255),
  continuedHeavy           INT,
  continuedPrice           INT,
  endAddress               VARCHAR(255),
  startAddress             VARCHAR(255),
  firstHeavy               INT,
  firstPrice               INT,
  designationSize          VARCHAR(255),
  payOfAmount              INT,
  electronicCommerce_id    VARCHAR(255),
  paymentParam             VARCHAR(255),
  paymentOverdue           VARCHAR(255),
  dataExtend               VARCHAR(255),
  dataOperator             VARCHAR(255),
  dataInsulator            VARCHAR(255),
  courier_phone            VARCHAR(255),
  other_company_name       VARCHAR(255),
  transactionId            VARCHAR(255),
  drop_by_courier_login    INT(5),
  reason                   VARCHAR(255),
  is_needed_pod            VARCHAR(255) NOT NULL DEFAULT(0),
  is_insulator             VARCHAR(10) NOT NULL DEFAULT(0),
  flagRedrop               INT NOT NULL DEFAULT(0),
  customer_identification  VARCHAR(255) NOT NULL DEFAULT(0)
);

DROP TABLE IF EXISTS Item;
CREATE TABLE Item
(
  id         VARCHAR(255) PRIMARY KEY NOT NULL,
  express_id VARCHAR(255)             NOT NULL,
  item_id    VARCHAR(255)             NOT NULL,
  syncFlag   INT
);

DROP TABLE IF EXISTS Mouth;
CREATE TABLE Mouth
(
  id              VARCHAR(255) PRIMARY KEY NOT NULL,
  deleteFlag      INT                      NOT NULL,
  number          INT                      NOT NULL,
  numberInCabinet INT                      NOT NULL,
  overduePrice    BIGINT                   NOT NULL,
  status          VARCHAR(255),
  is_insulator    VARCHAR(255) DEFAULT(0),
  syncFlag        INT,
  usePrice        BIGINT                   NOT NULL,
  box_id          VARCHAR(255)             NOT NULL,
  cabinet_id      VARCHAR(255)             NOT NULL,
  express_id      VARCHAR(255),
  mouthType_id    VARCHAR(255)             NOT NULL,
  openOrder       INT
);
DROP TABLE IF EXISTS MouthType;
CREATE TABLE MouthType
(
  id                  VARCHAR(255) PRIMARY KEY NOT NULL,
  defaultOverduePrice BIGINT                   NOT NULL,
  defaultUsePrice     BIGINT                   NOT NULL,
  name                VARCHAR(255)             NOT NULL,
  deleteFlag          INT
);
DROP TABLE IF EXISTS Alert;
CREATE TABLE Alert
(
  id          VARCHAR(255) PRIMARY KEY NOT NULL,
  alertType   VARCHAR(255)             NOT NULL,
  box_id      VARCHAR(255)             NOT NULL,
  operator_id VARCHAR(255)             NOT NULL,
  createTime  BIGINT                   NOT NULL,
  alertStatus VARCHAR(255)             NOT NULL,
  syncFlag    INT                      NOT NULL,
  value_id    VARCHAR(255),
  alertvalue  VARCHAR(255)             NOT NULL
);
DROP TABLE IF EXISTS Activity;
CREATE TABLE Activity
(
  id             VARCHAR(255) PRIMARY KEY NOT NULL,
  activityType   VARCHAR(255),
  chargeType     VARCHAR(255),
  firstHeavy     BIGINT,
  firstPrice     BIGINT,
  continuedHeavy BIGINT,
  continuedPrice BIGINT,
  startTime      BIGINT,
  endTime        BIGINT,
  payOfAmount    BIGINT,
  position       BIGINT,
  box_id         VARCHAR(255)
);
DROP TABLE IF EXISTS TransactionRecord;
CREATE TABLE TransactionRecord
(
  id              VARCHAR(255) PRIMARY KEY NOT NULL,
  amount          BIGINT,
  createTime      BIGINT,
  paymentType     VARCHAR(255),
  paymentParam    VARCHAR(255),
  paymentStatus   VARCHAR(255),
  transactionType VARCHAR(255),
  express_id      VARCHAR(255),
  mouth_id        VARCHAR(255),
  deleteFlag      VARCHAR(255)    
);
DROP TABLE IF EXISTS Download;
CREATE TABLE Download
(
  id        VARCHAR(255) PRIMARY KEY NOT NULL,
  url       VARCHAR(255),
  filename  VARCHAR(255),
  position  BIGINT,
  status    BIGINT,
  type      VARCHAR(255),
  startTime BIGINT,
  endTime   BIGINT,
  MD5       VARCHAR(255),
  flagTime  BIGINT
);

CREATE TABLE IF NOT EXISTS User 
(
  id_user	      VARCHAR(255) PRIMARY KEY NOT NULL,
  userName	    VARCHAR(255),
  displayName	  VARCHAR(255),
  phone	        VARCHAR(255),
  userPassword	VARCHAR(255),
  company	      VARCHAR(255),
  userLevel	    INT,
  id_company	  VARCHAR(255),
  role	        VARCHAR(255),
  usr_type	    VARCHAR(255)
);

DROP TABLE IF EXISTS ReturnRules;
CREATE TABLE IF NOT EXISTS ReturnRules 
(
	id	            VARCHAR(255) PRIMARY KEY NOT NULL,
	regularContent	VARCHAR(255),
	groupName	      VARCHAR(255),
	operator_id	    VARCHAR(255),
	logistic_id	    VARCHAR(255),
	ecommerce_id	  VARCHAR(255),
	deleteFlag	    VARCHAR(255)
);

DROP TABLE IF EXISTS Survey;
CREATE TABLE IF NOT EXISTS Survey
(
  id_express        VARCHAR(255) PRIMARY KEY NOT NULL,
  expressNumber     VARCHAR(255),
  survey_store      VARCHAR(255),
  survey_take       VARCHAR(255),
  storetime_survey  VARCHAR(255),
  taketime_survey   VARCHAR(255),
  syncFlag          INT,
  deleteFlag        INT
);

DROP TABLE IF EXISTS PaidOverdue;
CREATE TABLE PaidOverdue
(
  logisticsCompany_id   VARCHAR(255)   NOT NULL,
  company_name          VARCHAR(255),
  costs_size_mini       INT NOT NULL DEFAULT(1),
  costs_size_small      INT NOT NULL DEFAULT(1),
  costs_size_medium     INT NOT NULL DEFAULT(1),
  costs_size_large      INT NOT NULL DEFAULT(1),
  costs_size_extra_large     INT NOT NULL DEFAULT(1),
  costs_overdue_day     INT NOT NULL DEFAULT(1),
  costs_overdue_hour    INT NOT NULL DEFAULT(1),
  cost_per_day         INT NOT NULL DEFAULT(1),
  flat_costs_instore    INT NOT NULL DEFAULT(1),
  flat_costs_overdue    INT NOT NULL DEFAULT(1),
  tier_percentage       VARCHAR(10),
  payment_type          VARCHAR(100),
  calculate_price_by    VARCHAR(10),
  free_days             VARCHAR(2),
  max_day_costs         VARCHAR(2),
  abort_charging        VARCHAR(2) DEFAULT(0),
  is_active             INT NOT NULL DEFAULT(0),
  deleteFlag            INT DEFAULT(0)
);

DROP TABLE IF EXISTS UserOtp;
CREATE TABLE IF NOT EXISTS UserOtp
(
    phone_number      VARCHAR(15) PRIMARY KEY NOT NULL,
    valid_date        VARCHAR(50),
    used_times        VARCHAR(10),
    deleteFlag        VARCHAR(2) DEFAULT(0)
);

DROP TABLE IF EXISTS Epod;
CREATE TABLE IF NOT EXISTS Epod
(
  express_id        VARCHAR(255) PRIMARY KEY NOT NULL,
  barcode           VARCHAR(255),
  groupname         VARCHAR(255),
  pincode           VARCHAR(255),
  cust_name         VARCHAR(255),
  create_at         VARCHAR(255),
  signature         BLOB,
  syncFlag          INT,
  deleteFlag        INT
);

DROP TABLE IF EXISTS Contactless;
CREATE TABLE IF NOT EXISTS Contactless
(
  express_id        VARCHAR(255),
  express_number    VARCHAR(255),
  locker_name       VARCHAR(255),
  box_id            VARCHAR(255),
  locker_size       VARCHAR(255),
  locker_number     INT,
  is_taken          INT,
  is_drop           INT,
  is_pushed         INT,
  storeTime         BIGINT,
  takeTime          BIGINT,
  push_date         BIGINT,
  is_success        VARCHAR(255),
  qr_content        VARCHAR(255),
  syncFlag          INT
);

DROP TABLE IF EXISTS InsulatorPricing;
CREATE TABLE IF NOT EXISTS InsulatorPricing
(
    locker_id   VARCHAR(255)   NOT NULL,
    locker_name           VARCHAR(255),
    order_no              VARCHAR(255),
    overdue_day           INT NOT NULL DEFAULT(1),
    overdue_hour          INT NOT NULL DEFAULT(1),
    logisticsCompany_id   VARCHAR(100),
    company_name          VARCHAR(100),
    S                     VARCHAR(100),
    M                     VARCHAR(100),
    L                     VARCHAR(100),
    XL                    VARCHAR(100),
    is_active             INT DEFAULT(1),
    deleteFlag            INT DEFAULT(0),
    abort_charging        VARCHAR(2) DEFAULT(0)
);

DROP TABLE IF EXISTS HistoryOpenDoor;
CREATE TABLE HistoryOpenDoor
(
  express_id                        VARCHAR(255)  NOT NULL,
  customer_identifications_number   VARCHAR(255)  NOT NULL,
  open_door_time                    BIGINT
);

CREATE UNIQUE INDEX UK_dr2kya9pdulgpd4bgw8nuhwcg ON Box(name);
CREATE UNIQUE INDEX UK_itq51i9x3mlbgor90wxgqhoy8 ON Box(orderNo);
CREATE UNIQUE INDEX UK_7jxprbacypavkjykkaesjm9cp ON Company(name);
CREATE INDEX FK_k0q1oxj3os8acfcfpfcsp3kse ON Company(parentCompany_id);
CREATE INDEX FK_qlkikayvbymtes0pjkj32829g ON Mouth(cabinet_id);
CREATE UNIQUE INDEX UK_66ojrpd72bvdbnoh3b0feisso ON MouthType(name);
CREATE INDEX FK_h3vypb16nkqlkh7r1e4o6p4vl ON TransactionRecord(express_id);

